<?php

namespace Database\Seeders;

use App\Models\Profile;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $data = [
            'nama_lembaga' => 'Pesantren Persis 27',
            'nspp' => "0920192092",
            'npsn' => "1928192892",
            'tgl_hijriah' => 1447,
            'tgl_masehi' => 5,
            'alamat' => 'Jl Pagarsih Barat, Gg. Madrasah',
            'rt' => "RT 01",
            'rw' => "RW 01",
            'provinsi' => "Jawa Barat",
            'kota' => "Bandung",
            'kecamatan' => "Babakan Ciparay",
            'kode_pos' => 40222,
        ];

        Profile::create($data);
    }
}
