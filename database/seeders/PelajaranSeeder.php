<?php

namespace Database\Seeders;

use App\Models\Pelajaran;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class PelajaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $no = $faker->numberBetween(1, 1000);

        $data = [
            [
                'kode' => "TFRAM"."01",
                'nama_pelajaran' => "Tafsir`am",
                'kelompok_id' => "1",
                'urutan' => 1,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "TFRAHK"."02",
                'nama_pelajaran' => "Tafsir`Ahkam",
                'kelompok_id' => "1",
                'urutan' => 2,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "ULMQU"."03",
                'nama_pelajaran' => "`Ulumul-Qur`an",
                'kelompok_id' => "1",
                'urutan' => 3,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "MATAN"."04",
                'nama_pelajaran' => "Matan Hadits",
                'kelompok_id' => "1",
                'urutan' => 4,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "MSTLHA"."05",
                'nama_pelajaran' => "Mushthalah Hadits",
                'kelompok_id' => "1",
                'urutan' => 5,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "TAUHID"."06",
                'nama_pelajaran' => "Tauhid",
                'kelompok_id' => "1",
                'urutan' => 6,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "AKHLAQ"."07",
                'nama_pelajaran' => "Akhlaq",
                'kelompok_id' => "1",
                'urutan' => 7,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "FIQIH"."08",
                'nama_pelajaran' => "FIQIH",
                'kelompok_id' => "1",
                'urutan' => 8,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "USHULFQ"."09",
                'nama_pelajaran' => "Ushul Fiqih",
                'kelompok_id' => "1",
                'urutan' => 9,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "FARAIDL"."10",
                'nama_pelajaran' => "Fara`idl",
                'kelompok_id' => "1",
                'urutan' => 10,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "NAHWU"."11",
                'nama_pelajaran' => "Nahwu",
                'kelompok_id' => "2",
                'urutan' => 1,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "SHARAF"."12",
                'nama_pelajaran' => "Sharaf",
                'kelompok_id' => "2",
                'urutan' => 2,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "HIWAR"."13",
                'nama_pelajaran' => "Hiwar",
                'kelompok_id' => "2",
                'urutan' => 3,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "BALAGH"."14",
                'nama_pelajaran' => "Balaghah",
                'kelompok_id' => "2",
                'urutan' => 4,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "SNABAWI"."15",
                'nama_pelajaran' => "Sirah Nabawiyyah",
                'kelompok_id' => "2",
                'urutan' => 5,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "BINDO"."16",
                'nama_pelajaran' => "B Indonesia",
                'kelompok_id' => "2",
                'urutan' => 6,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "BINGGRIS"."17",
                'nama_pelajaran' => "B Inggris",
                'kelompok_id' => "2",
                'urutan' => 7,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "MTK"."18",
                'nama_pelajaran' => "Matematika",
                'kelompok_id' => "3",
                'urutan' => 1,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "ILHISAB"."19",
                'nama_pelajaran' => "Ilmu Hisab",
                'kelompok_id' => "3",
                'urutan' => 2,
                'tingkat_sekolah_id' => "MLN"
            ],
            [
                'kode' => "TFRAM"."20",
                'nama_pelajaran' => "Tafsir`am",
                'kelompok_id' => "1",
                'urutan' => 1,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "ILTAJWID"."21",
                'nama_pelajaran' => "Ilmu Tajwid",
                'kelompok_id' => "1",
                'urutan' => 2,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "MATAN"."22",
                'nama_pelajaran' => "Matan Hadits",
                'kelompok_id' => "1",
                'urutan' => 3,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "TAUHID"."22",
                'nama_pelajaran' => "Tauhid",
                'kelompok_id' => "1",
                'urutan' => 4,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "AKHLAQ"."23",
                'nama_pelajaran' => "Akhlaq",
                'kelompok_id' => "1",
                'urutan' => 5,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "FIQIH"."24",
                'nama_pelajaran' => "Fiqih",
                'kelompok_id' => "1",
                'urutan' => 6,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "USHULFQ"."25",
                'nama_pelajaran' => "Ushul Fiqih",
                'kelompok_id' => "1",
                'urutan' => 7,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "NAHWU"."26",
                'nama_pelajaran' => "Nahwu",
                'kelompok_id' => "2",
                'urutan' => 1,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "SHARAF"."27",
                'nama_pelajaran' => "Sharaf",
                'kelompok_id' => "2",
                'urutan' => 2,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "HIWAR"."28",
                'nama_pelajaran' => "Hiwar",
                'kelompok_id' => "2",
                'urutan' => 3,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "SNABAWI"."29",
                'nama_pelajaran' => "Sirah Nabawiyyah",
                'kelompok_id' => "2",
                'urutan' => 4,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "BINDO"."30",
                'nama_pelajaran' => "B Indonesia",
                'kelompok_id' => "2",
                'urutan' => 5,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "BINGGRIS"."31",
                'nama_pelajaran' => "B Inggris",
                'kelompok_id' => "2",
                'urutan' => 6,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "MTK"."32",
                'nama_pelajaran' => "Matematika",
                'kelompok_id' => "3",
                'urutan' => 1,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "IPA"."33",
                'nama_pelajaran' => "IPA",
                'kelompok_id' => "3",
                'urutan' => 2,
                'tingkat_sekolah_id' => "TSN"
            ],
            [
                'kode' => "QA"."33",
                'nama_pelajaran' => "Qowaid",
                'kelompok_id' => "1",
                'urutan' => 11,
                'tingkat_sekolah_id' => "TSN"
            ],
        ];

        for($i=0; $i < count($data); $i++) {
            Pelajaran::create($data[$i]);
        }
    }
}
