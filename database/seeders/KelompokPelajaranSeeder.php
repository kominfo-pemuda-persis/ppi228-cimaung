<?php

namespace Database\Seeders;

use App\Models\KelompokPelajaran;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelompokPelajaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama_kelompok_pelajaran' => "AL-`ULUMUS-SYAR'IYYAH",
                'keterangan' => "ILMU KEAGAMAAN"
            ],
            [
                'nama_kelompok_pelajaran' => "AL-`ULUMUL-INSANIYYAH",
                'keterangan' => "HUMANIORA DAN ILMU SOSIAL"
            ],
            [
                'nama_kelompok_pelajaran' => "AL-`ULUMUL-KAUNIYYAH",
                'keterangan' => "MATEMATIKA DAN ILMU ALAM"
            ],
            
        ];

        for($i=0; $i < 3; $i++) {
            KelompokPelajaran::create($data[$i]);
        }
    }
}
