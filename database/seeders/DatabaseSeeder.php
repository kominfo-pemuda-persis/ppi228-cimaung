<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\AsatidzPelajaran;use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);
        $this->call(AsatidzSedeer::class);
        // $this->call(SantriSeeder::class);
        $this->call(AsatidzPelajaranSeeder::class);
        $this->call(KelompokPelajaranSeeder::class);
        // $this->call(MuaddibSeeder::class);
        // $this->call(OrangtuaSeeder::class);
        $this->call(KontakSeeder::class);
        $this->call(SemesterSeeder::class);
        $this->call(ProfileSeeder::class);
        $this->call(WaliKelasSeeder::class);
        $this->call(EkstrakurikulerSeeder::class);
        $this->call(PelajaranSeeder::class);
        $this->call(JuzSeeder::class);
        $this->call(CounterSeeder::class);
        $this->call(ProgramUnggulanSeeder::class);
        $this->call(KokurikulerSeeder::class);
    }
}
