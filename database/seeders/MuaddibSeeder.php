<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class MuaddibSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 10; $i++){
          DB::table('tb_muaddib')->insert([
            'asatidz_id' => $i,
            'kelas_id' => $faker->randomElement($array = array ('1', '2', '3')),
            'tingkat_sekolah_id' => $faker->randomElement($array = array ('TSN', 'MLN'))
          ]);
        }
    }
}
