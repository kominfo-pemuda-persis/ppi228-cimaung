<?php

namespace Database\Seeders;

use App\Models\JadwalKegiatanExtra;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class JadwalKegiatanExtraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        // Loop untuk membuat data seeder
        for ($i = 0; $i <= 84; $i++) {
            JadwalKegiatanExtra::create([
                'hari' => $faker->numberBetween(1, 6),
                'jam_id' => $faker->randomElement([3,7,9,10,11,12,13,14]),
                'asatidz_id' => $faker->numberBetween(8, 10),
                'pelajaran_id' => $faker->numberBetween(31, 32),
                'tingkat' => $faker->randomElement(['MLN', 'TSN']),
                'kelas' => $faker->randomElement([1, 2, 3]),
                'urutan_id' => $i + 1,
            ]);
        }
    }
}

