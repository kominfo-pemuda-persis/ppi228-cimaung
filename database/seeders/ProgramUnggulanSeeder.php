<?php

namespace Database\Seeders;

use App\Models\ProgramUnggulan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProgramUnggulanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'nama' => 'Tahfizh al-Qur`an' ],
            [ 'nama' => 'Tahfizh Bulughul-Maram' ],
            [ 'nama' => 'Tahfizh Riyadlus-Shalihin' ],
            [ 'nama' => 'Tahfizh Shahihul-Bukhari' ],
        ];

        for($i=0; $i < count($data); $i++) {
            ProgramUnggulan::create($data[$i]);
        }
    }
}
