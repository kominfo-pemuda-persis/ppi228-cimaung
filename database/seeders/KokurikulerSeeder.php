<?php

namespace Database\Seeders;

use App\Models\Ekstrakulikuler;
use App\Models\Kokurikuler;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KokurikulerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama' => "Bahasa Arab",
            ],
            [
                'nama' => "Bahasa Inggris",
            ],
            [
                'nama' => "Kitabul-‘Ilmi",
            ],
            [
                'nama' => "Syarah Shahih Muslim",
            ],
            [
                'nama' => "Fathul-Bari",
            ]            
        ];

        for($i=0; $i < count($data); $i++) {
            Kokurikuler::create($data[$i]);
        }
    }
}
