<?php

namespace Database\Seeders;

use App\Models\JadwalKegiatanIntra;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class JadwalKegiatanIntraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        // Loop untuk membuat data seeder
        for ($i = 0; $i < 252; $i++) {
            JadwalKegiatanIntra::create([
                'hari' => $faker->numberBetween(1, 6),
                'jam_id' => $faker->numberBetween(1, 6),
                'asatidz_id' => $faker->numberBetween(1, 10),
                'pelajaran_id' => $faker->numberBetween(1, 10),
                'tingkat' => $faker->randomElement(['MLN', 'TSN']),
                'kelas' => $faker->randomElement([1, 2, 3]),
                'urutan_id' => $i + 1,
            ]);
        }
    }
}

