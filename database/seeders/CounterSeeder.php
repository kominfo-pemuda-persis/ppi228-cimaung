<?php

namespace Database\Seeders;

use App\Models\Counter;
use App\Models\CounterType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CounterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'NIS',
                'level' => 'TSN',
                'office_code' => '27',
                'start_year' => 2023,
                'code' => '01',
                'counter' => 1,
                'digit_total' => 3,
            ],
            [
                'name' => 'NIS',
                'level' => 'MLN',
                'office_code' => '27',
                'start_year' => 2023,
                'code' => '02',
                'counter' => 1,
                'digit_total' => 3,
            ],
            [
                'name' => 'NISM',
                'office_code' => '27',
                'start_year' => 2023,
                'code' => '500032730094',
                'counter' => 1,
                'digit_total' => 3,
            ]
        ];

        for($i=0; $i < count($data); $i++) {
            Counter::create($data[$i]);
        }
    }
}
