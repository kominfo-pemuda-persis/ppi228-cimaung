<?php

namespace Database\Seeders;

use App\Models\Juz;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JuzSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 30; $i++) { 
            Juz::create([
                'nama' => 'Juz ' . $i
            ]);
        }
    }
}
