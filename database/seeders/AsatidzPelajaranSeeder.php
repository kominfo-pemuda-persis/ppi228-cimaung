<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;


class AsatidzPelajaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        $users = DB::table('tb_asatidz')->get();

        foreach($users as $user) {
          DB::table('tb_asatidz_pelajaran')->insert([
            'asatidz_id' => $user->id,
            'pelajaran_id' => $faker->numberBetween(1, 20),
            'kelas_id' => $faker->randomElement($array = array ('1', '2', '3')),
            'tingkat_sekolah_id' => $faker->randomElement($array = array ('TSN', 'MLN'))
          ]);
        }
    }
}
