<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class OrangtuaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 10; $i++){
          DB::table('tb_orangtua')->insert([
            'nama' => $faker->name,
            'hp' => $faker->randomNumber(9, true),
            'alamat' => $faker->address,
            'kerja' => 'wiraswasta',
            'keterangan' => '-'
          ]);
        }
    }
}
