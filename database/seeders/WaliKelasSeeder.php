<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class WaliKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $users_tsn = DB::table('tb_asatidz_pelajaran')
                ->where('tingkat_sekolah_id', 'TSN')->get();
        $users_mln = DB::table('tb_asatidz_pelajaran')
                ->where('tingkat_sekolah_id', 'MLN')->get();

        $no1 = 1;
        foreach($users_tsn as $user) {
          if($no1 > 3) {
            break;
          }
          DB::table('tb_wali_kelas')->insert([
            'asatidz_id' => $user->asatidz_id,
            'kelas_id' => $no1++,
            'tingkat_sekolah_id' => $faker->randomElement($array = array ('TSN'))
          ]);
        }

        $no2 = 1;
        foreach($users_mln as $user) {
          if($no2 > 3) {
            break;
          }
          DB::table('tb_wali_kelas')->insert([
            'asatidz_id' => $user->asatidz_id,
            'kelas_id' => $no2++,
            'tingkat_sekolah_id' => $faker->randomElement($array = array ('MLN'))
          ]);
        }
    }
}
