<?php

namespace Database\Seeders;

use App\Models\Asatidz;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class AsatidzSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 0; $i < 10; $i++){
            $id = \Ramsey\Uuid\Uuid::uuid4()->toString();
            $niatRandom = rand(10000000, 99999999);

            $user = \App\Models\User::create([
                'id' => $id,
                'name' => $faker->name,
                'username' => $niatRandom,
                'password' => bcrypt('12345')
            ]);

            $user->attachRole('Asatidz');
    	}

        $users = \App\Models\User::all();

        foreach ($users as $user) {
            Asatidz::create([
                'nama_lengkap' => $user->name,
                'niat' => $user->username,
                'jenis_kelamin' => $faker->randomElement($array = array ('L', 'P')),
                'tsn' => "MTS Konoha 100",
                'mln' => "PPI Gakure 200",
                's1' => "Universitas Kyubi",
                's2' => "Universitas Negeri Hyuga",
                's3' => "Universitas Negeri Hatori",
                'pendidikan_terakhir' => $faker->randomElement($array = array ('S1', 'S2', 'S3')),
                'no_telp' => $faker->randomNumber(9, true),
                'alamat' => $faker->address,
                'tempat_lahir' => $faker->city,
                'tgl_lahir' => date('Y-m-d'),
                'hafalan' => $faker->randomElement($array = array ('1 Juz', '2 Juz', '3 juz', '4 Juz', '5 Juz', '6 Juz', '7 Juz', '8 Juz', '9 Juz', '10 Juz', '11 Juz', '12 Juz', '13 Juz', '14 Juz', '15 Juz', '16 Juz', '17 Juz', '18 Juz', '19 Juz', '20 Juz', '21 Juz', '22 Juz', '24 Juz', '25 Juz', '26 Juz', '27 Juz', '28 Juz', '29 Juz', '30 juz')),
                'user_id' => $user->id,
            ]);
        }
    }
}
