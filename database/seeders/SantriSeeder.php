<?php

namespace Database\Seeders;

use App\Models\Orangtua;
use App\Models\Santri;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class SantriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $idUNIQortuTSN = array();
        $idUNIQortuMLN = array();
        for($i = 0; $i < 10; $i++){
            $id = \Ramsey\Uuid\Uuid::uuid4()->toString();
            $idUNIQortuTSN[] = $id;
        }
        for($i = 0; $i < 10; $i++){
            $id = \Ramsey\Uuid\Uuid::uuid4()->toString();
            $idUNIQortuMLN[] = $id;
        }
        
        for($i = 0; $i < 10; $i++){
            $id = \Ramsey\Uuid\Uuid::uuid4()->toString();
            $niatRandom = rand(10000000, 99999999);

            $user = \App\Models\User::create([
                'id' => $id,
                'name' => $faker->name,
                'username' => $niatRandom,
                'password' => bcrypt('12345')
            ]);

            $orangtua_tsn = \App\Models\User::create([
                'id' => $idUNIQortuTSN[$i],
                'name' => $faker->name,
                'username' => $niatRandom.'.ortu',
                'password' => bcrypt('12345')
            ]);

            $orangtua_tsn->attachRole("orangtua_tsn");

            $ortu = Orangtua::create([
                'user_id' => $orangtua_tsn->id,
                'nama' => $faker->name,
                'hp' => $faker->randomNumber(9, true),
                'alamat' => $faker->address,
                'kerja' => "Guru",
                'keterangan' => '-'
            ]);

            $santri = Santri::create([
                'nama_lengkap' => $user->name,
                'jk' => $faker->randomElement($array = array ('RG', 'UG')),
    			'tempat_lahir' => $faker->city,
    			'tanggal_lahir' => date('Y-m-d'),
                'nis' => $faker->randomNumber(9, true),
                'nik' => $faker->randomNumber(9, true),
                'nisn' => $faker->randomNumber(9, true),
                'alamat' => $faker->address,
    			'no_hp' => $faker->randomNumber(9, true),
    			'email' => $faker->email(),
    			'asal_sekolah' => "MTS Konoha 100",
    			'nama_lengkap_ayah' => $faker->firstNameMale,
    			'pekerjaan_ayah' => "Guru",
                'nama_lengkap_ibu' => $faker->firstNameFemale,
    			'pekerjaan_ibu' => "Guru",
    			'nama_lengkap_wali' => $faker->name,
    			'pekerjaan_wali' => "Guru",
    			'no_hp_ayah' => $faker->randomNumber(9, true),
    			'no_hp_ibu' => $faker->randomNumber(9, true),
    			'no_hp' => $faker->randomNumber(9, true),
    			'tingkat_sekolah_id' => 1,
    			'kelas' => $faker->numberBetween(1, 3),
                'start_year' => 2023,
                'status_keperluan_asrama' => $faker->randomElement($array = array ('ya', 'tidak')),
                'status_pembayaran' => $faker->randomElement($array = array ('pending', 'approved', 'rejected')),
                'status_active' => $faker->randomElement($array = array ('active', 'non active')),
                'orangtua_id'=> $ortu->id,
                'user_id' => $user->id
            ]);

            $user->attachRole("santri_tsn");
    	}

        for($i = 0; $i < 10; $i++){
            $id = \Ramsey\Uuid\Uuid::uuid4()->toString();
            $niatRandom = rand(10000000, 99999999);

            $user = \App\Models\User::create([
                'id' => $id,
                'name' => $faker->name,
                'username' => $niatRandom,
                'password' => bcrypt('12345')
            ]);

            $orangtua_mln = \App\Models\User::create([
                'id' => $idUNIQortuMLN[$i],
                'name' => $faker->name,
                'username' => $niatRandom.'.ortu',
                'password' => bcrypt('12345')
            ]);

            $orangtua_mln->attachRole("orangtua_mln");

            $ortu = Orangtua::create([
                'user_id' => $orangtua_mln->id,
                'nama' => $faker->name,
                'hp' => $faker->randomNumber(9, true),
                'alamat' => $faker->address,
                'kerja' => "Guru",
                'keterangan' => '-'
            ]);

            $santri = Santri::create([
                'nama_lengkap' => $user->name,
                'jk' => $faker->randomElement($array = array ('RG', 'UG')),
    			'tempat_lahir' => $faker->city,
    			'tanggal_lahir' => date('Y-m-d'),
                'nis' => $faker->randomNumber(9, true),
                'nik' => $faker->randomNumber(9, true),
                'nisn' => $faker->randomNumber(9, true),
                'alamat' => $faker->address,
    			'no_hp' => $faker->randomNumber(9, true),
    			'email' => $faker->email(),
    			'asal_sekolah' => "MTS Konoha 100",
    			'nama_lengkap_ayah' => $faker->firstNameMale,
    			'pekerjaan_ayah' => "Guru",
                'nama_lengkap_ibu' => $faker->firstNameFemale,
    			'pekerjaan_ibu' => "Guru",
    			'nama_lengkap_wali' => $faker->name,
    			'pekerjaan_wali' => "Guru",
    			'no_hp_ayah' => $faker->randomNumber(9, true),
    			'no_hp_ibu' => $faker->randomNumber(9, true),
    			'no_hp' => $faker->randomNumber(9, true),
    			'tingkat_sekolah_id' => 2,
    			'kelas' => $faker->numberBetween(1, 3),
                'start_year' => 2023,
                'status_keperluan_asrama' => $faker->randomElement($array = array ('ya', 'tidak')),
                'status_pembayaran' => $faker->randomElement($array = array ('pending', 'approved', 'rejected')),
                'status_active' => $faker->randomElement($array = array ('active', 'non active')),
                'orangtua_id'=> $ortu->id,
                'user_id' => $user->id
            ]);

            $user->attachRole("santri_mln");
    	}
    }
}
