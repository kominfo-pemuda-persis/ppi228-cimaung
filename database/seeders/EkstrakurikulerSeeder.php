<?php

namespace Database\Seeders;

use App\Models\Ekstrakulikuler;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EkstrakurikulerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama' => "Mubahatsah",
                'keterangan' => "-"
            ],
            [
                'nama' => "Ta’lim Madrasah Diniyyah",
                'keterangan' => "-"
            ],
            [
                'nama' => "Mabit/Tahajjud Berjama’ah",
                'keterangan' => "-"
            ],
            [
                'nama' => "Rofi IPP/IPPI",
                'keterangan' => "-"
            ],
            [
                'nama' => "Memanah",
                'keterangan' => "-"
            ],
            [
                'nama' => "Sepakbola",
                'keterangan' => "-"
            ],
            [
                'nama' => "Khotibah",
                'keterangan' => "-"
            ],
            [
                'nama' => "Badminton",
                'keterangan' => "-"
            ],
            [
                'nama' => "Renang",
                'keterangan' => "-"
            ],
            [
                'nama' => "Bela Diri",
                'keterangan' => "-"
            ],
            [
                'nama' => "Super Camp Al-Qur’an",
                'keterangan' => "-"
            ],
            
        ];

        for($i=0; $i < count($data); $i++) {
            Ekstrakulikuler::create($data[$i]);
        }
    }
}
