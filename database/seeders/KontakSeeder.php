<?php

namespace Database\Seeders;

use App\Models\Kontak;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KontakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'no_telp' => "0828828191222",
            'website' => "www.pesantrenpersis27.com",
            'email' => "pesantrenpersis27@gmail.com"
        ];

        Kontak::create($data);
    }
}
