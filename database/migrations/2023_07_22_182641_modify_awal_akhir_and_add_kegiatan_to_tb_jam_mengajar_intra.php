<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_jam_mengajar_intra', function (Blueprint $table) {
            $table->string('awal', 255)->change();
            $table->string('akhir', 255)->change();
        });
        Schema::table('tb_jam_mengajar_intra', function (Blueprint $table) {
            $table->enum('kegiatan', ['KBM', 'Istirahat', 'Hafalan'])->after('akhir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_jam_mengajar_intra', function (Blueprint $table) {
            $table->time('awal')->change();
            $table->time('akhir')->change();
        });

        Schema::table('tb_jam_mengajar_intra', function (Blueprint $table) {
            $table->dropColumn('kegiatan');
        });
    }
};
