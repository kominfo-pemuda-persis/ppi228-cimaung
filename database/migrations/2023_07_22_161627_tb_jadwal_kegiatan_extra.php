<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jadwal_kegiatan_extra', function (Blueprint $table) {
            $table->id();
            $table->integer('hari');
            $table->bigInteger('jam_id');
            $table->bigInteger('asatidz_id')->nullable();
            $table->bigInteger('pelajaran_id')->nullable();
            $table->enum('tingkat', ['MLN', 'TSN']);
            $table->integer('kelas');
            $table->String('urutan_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_tb_jadwal_kegiatan_extra');
    }
};
