<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('tb_semester', function (Blueprint $table) {
        //     $table->enum('status', ['draft', 'aktif', 'tidak-aktif'])->default('draft')->after('end_year');
        // });
        DB::statement('ALTER TABLE tb_semester MODIFY status ENUM("draft", "aktif", "tidak-aktif") NULL DEFAULT "draft"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('tb_semester', function (Blueprint $table) {
        //     $table->dropColumn('status');
        // });
    }
};
