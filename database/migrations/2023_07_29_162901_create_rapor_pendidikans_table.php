<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_rapor_pendidikan', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('santri_id')->nullable();
            $table->integer('semester_id')->nullable();
            $table->enum('kelas', ['1', '2', '3'])->nullable();
            $table->enum('tingkat_sekolah_id', ["TSN","MLN"])->nullable();
            $table->string('orangtua_id')->nullable();
            $table->integer('sakit')->default(0);
            $table->integer('izin')->default(0);
            $table->integer('alpa')->default(0);
            $table->text('catatan_walikelas')->nullable();
            $table->text('tanggapan_orangtua')->nullable();
            $table->string('walikelas_id')->nullable();
            $table->enum('status', ['PENILAIAN', 'BAGI RAPOR'])->default('PENILAIAN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rapor_pendidikans');
    }
};
