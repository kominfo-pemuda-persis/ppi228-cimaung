<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_kokurikuler_rapor_pendidikan', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('rapor_pendidikan_id');
            $table->string('kokurikuler_id');
            $table->integer('nilai')->default(0);
            $table->string('predikat')->nullable();
            $table->text('deskripsi')->nullable();
            $table->string('status')->default('aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kokurikuler_rapor_pendidikans');
    }
};
