<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jam_mengajar_extra', function (Blueprint $table) {
            $table->id();
            $table->integer('jam_ke');
            $table->String('awal');
            $table->String('akhir');
            $table->String('kegiatan')->default('KBM');
            $table->timestamps();
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jam_mengajar_extra');
    }
};
