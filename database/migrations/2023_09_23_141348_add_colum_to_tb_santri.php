<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_santri', function (Blueprint $table) {
            $table->string('path_kk')->nullable()->after('kelas');
            $table->string('path_akte')->nullable();
            $table->string('path_foto_santri')->nullable();
            $table->string('path_ijazah_sd')->nullable();
            $table->string('path_ijazah_mts')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_santri', function (Blueprint $table) {
            $table->dropColumn('path_kk');
            $table->dropColumn('path_akte');
            $table->dropColumn('path_foto_santri');
            $table->dropColumn('path_ijazah_sd');
            $table->dropColumn('path_ijazah_mts');
        });
    }
};
