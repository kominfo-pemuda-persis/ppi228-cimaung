<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_karya_tulis_ilmiah', function (Blueprint $table) {
            $table->char('pembimbing', 36)->nullable()->after('keterangan');
            $table->char('penguji', 36)->nullable()->after('pembimbing');
            $table->enum('status', ['pending', 'publish'])->default('pending')->after('penguji');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_karya_tulis_ilmiah', function (Blueprint $table) {
            $table->dropColumn('pembimbing');
            $table->dropColumn('penguji');
            $table->dropColumn('status');
        });
    }
};
