<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_psb', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('start_year')->unique()->nullable();
            $table->integer('end_year')->unique()->nullable();
            $table->enum('status', ['DRAFT', 'ACTIVE', 'NON ACTIVE'])->default('DRAFT');
            $table->date('closed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_s_b_s');
    }
};
