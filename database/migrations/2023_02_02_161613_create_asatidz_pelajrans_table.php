<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_asatidz_pelajaran', function (Blueprint $table) {
            $table->id();
            $table->string('asatidz_id');
            $table->integer('pelajaran_id');
            $table->enum('kelas_id', ['1','2','3']);
            $table->enum('tingkat_sekolah_id', ['TSN','MLN']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_asatidz_pelajaran');
    }
};
