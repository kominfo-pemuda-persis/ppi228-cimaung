<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_asatidz', function (Blueprint $table) {
            $table->string('photo_path')->after('hafalan')->default(null)->nullable();
        });

        Schema::table('tb_santri', function (Blueprint $table) {
            $table->string('photo_path')->after('email')->default(null)->nullable();
        });

        Schema::table('tb_orangtua', function (Blueprint $table) {
            $table->string('photo_path')->after('keterangan')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_asatidz', function (Blueprint $table) {
            $table->dropColumn('photo_path');
        });

        Schema::table('tb_santri', function (Blueprint $table) {
            $table->dropColumn('photo_path');
        });

        Schema::table('tb_orangtua', function (Blueprint $table) {
            $table->dropColumn('photo_path');
        });
    }
};
