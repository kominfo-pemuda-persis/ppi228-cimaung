<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_semester', function (Blueprint $table) {
            DB::statement('ALTER TABLE tb_semester MODIFY tahun INTEGER');
            $table->renameColumn('tahun', 'start_year');
            $table->integer('end_year')->nullable()->after('tahun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_semester', function (Blueprint $table) {
            $table->dropColumn('end_year');
            DB::statement('ALTER TABLE tb_semester MODIFY tahun VARCHAR(255)');
            $table->renameColumn('start_year', 'tahun');
        });
    }
};
