<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_santri', function (Blueprint $table) {
            \DB::statement("ALTER TABLE tb_santri MODIFY kelas enum('1', '2', '3', 'TIDAK_NAIK_KELAS_1', 'TIDAK_NAIK_KELAS_2', 'TIDAK_NAIK_KELAS_3', 'LULUS', 'TIDAK_LULUS')");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_santri', function (Blueprint $table) {
            //
        });
    }
};
