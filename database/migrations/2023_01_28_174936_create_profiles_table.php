<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_profil', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lembaga');
            $table->string('nspp')->nullable();
            $table->string('npsn')->nullable();
            $table->bigInteger('tgl_hijriah');
            $table->bigInteger('tgl_masehi');
            $table->string('alamat');
            $table->string('rt');
            $table->string('rw');
            $table->string('provinsi');
            $table->string('kota');
            $table->string('kecamatan');
            $table->bigInteger('kode_pos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_profil');
    }
};
