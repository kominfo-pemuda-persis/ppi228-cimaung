<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_asatidz', function (Blueprint $table) {
            \DB::statement('ALTER TABLE tb_asatidz MODIFY pendidikan_terakhir ENUM("S1", "S2", "S3", "SMA_SMK_MLN") NULL DEFAULT NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_asatidz', function (Blueprint $table) {
            //
        });
    }
};
