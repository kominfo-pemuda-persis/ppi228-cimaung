<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_santri', function (Blueprint $table) {
            if (Schema::hasColumn('tb_santri', 'jenis_prestasi')) {
                $table->dropColumn('jenis_prestasi');
            }
            if (Schema::hasColumn('tb_santri', 'tingkat')) {
                $table->dropColumn('tingkat');
            }
            if (Schema::hasColumn('tb_santri', 'tahun')) {
                $table->dropColumn('tahun');
            }
        });

        Schema::create('tb_prestasi', function (Blueprint $table) {
            $table->id();
            $table->char('santri_id', 36);
            $table->string('jenis_prestasi');
            $table->string('tingkat');
            $table->string('tahun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_prestasi');
    }
};
