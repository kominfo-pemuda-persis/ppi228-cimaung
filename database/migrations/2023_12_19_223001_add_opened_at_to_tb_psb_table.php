<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_psb', function (Blueprint $table) {
            $table->date('opened_at')->nullable();
            $table->date('opened_mi_at')->nullable();
            $table->date('closed_mi_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_psb', function (Blueprint $table) {
            //
        });
    }
};
