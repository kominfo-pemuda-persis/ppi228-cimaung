<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_santri', function (Blueprint $table) {
            $table->string('approved_payment_by_name')->after('approved_payment_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_santri', function (Blueprint $table) {
            $table->dropColumn('approved_payment_by_name');
        });
    }
};
