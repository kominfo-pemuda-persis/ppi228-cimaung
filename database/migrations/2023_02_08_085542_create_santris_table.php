<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_santri', function (Blueprint $table) {
            $table->id();
            $table->string('nism')->nullable();
            $table->string('nis')->nullable();
            $table->string('nama_lengkap');
            $table->enum('jk',['RG','UG'])->nullable();
            $table->string('nik')->nullable();
            $table->string('nisn')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('alamat_kota')->nullable();
            $table->text('alamat')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('email')->nullable();
            $table->string('asal_sekolah')->nullable();
            $table->string('alamat_asal_sekolah')->nullable();
            $table->enum('tingkat_sekolah_id', ["TSN","MLN"]);

            $table->string('nama_lengkap_ayah')->nullable();
            $table->string('no_ktp_ayah')->nullable();
            $table->string('no_hp_ayah')->nullable();
            $table->string('pendidikan_ayah')->nullable();
            $table->string('pekerjaan_ayah')->nullable();
            $table->string('penghasilan_ayah')->nullable();

            $table->string('nomor_kartu_keluarga')->nullable();
            $table->string('nama_lengkap_ibu');
            $table->string('no_ktp_ibu')->nullable();
            $table->string('no_hp_ibu')->nullable();
            $table->string('pendidikan_ibu')->nullable();
            $table->string('pekerjaan_ibu')->nullable();
            $table->string('penghasilan_ibu')->nullable();

            $table->string('nama_lengkap_wali')->nullable();
            $table->string('no_ktp_wali')->nullable();
            $table->string('no_hp_wali')->nullable();
            $table->string('pendidikan_wali')->nullable();
            $table->string('pekerjaan_wali')->nullable();

            $table->string('jenis_prestasi')->nullable();
            $table->string('tingkat')->nullable();
            $table->string('tahun')->nullable();

            $table->string('hafalan')->nullable();
            $table->enum('status_keperluan_asrama', ['ya','tidak'])->nullable();
            $table->enum('status_pembayaran', ['pending','approved','rejected'])->default('pending');
            $table->enum('status_active', ['active', 'non active', 'pindah', 'meninggal', 'dikeluarkan', 'rejected'])->default('non active');

            $table->string('orangtua_id')->nullable();
            $table->string('muaddib_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_santri');
    }
};
