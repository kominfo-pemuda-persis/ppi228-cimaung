<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_jadwal_kegiatan_intra', function (Blueprint $table) {
            $table->dropColumn('kegiatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // reverse the migration
        Schema::table('tb_jadwal_kegiatan_intra', function (Blueprint $table) {
            $table->enum('kegiatan', ['KBM', 'Istirahat', 'Hafalan']);
        });
    }
};
