<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_asatidz', function (Blueprint $table) {
            $table->string('upload_ijazah')->after('photo_path')->default(null)->nullable();
            $table->string('upload_kk')->after('upload_ijazah')->default(null)->nullable();
            $table->string('upload_akte')->after('upload_kk')->default(null)->nullable();
            $table->string('upload_ktp')->after('upload_akte')->default(null)->nullable();
            $table->string('upload_buku_rekening')->after('upload_ktp')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_asatidz', function (Blueprint $table) {
            $table->dropColumn('upload_ijazah');
            $table->dropColumn('upload_kk');
            $table->dropColumn('upload_akte');
            $table->dropColumn('upload_ktp');
            $table->dropColumn('upload_buku_rekening');
        });
    }
};
