<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_karya_tulis_ilmiah', function (Blueprint $table) {
            $table->char('id', 36);
            $table->string('judul_kti');
            $table->string('santri_id');
            $table->string('angkatan');
            $table->date('tanggal_dibuat');
            $table->date('tanggal_diujikan');
            $table->string('kti_path');
            $table->string('thumbnail_kti_path');
            $table->integer('lihat')->nullable();
            $table->integer('halaman')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_karya_tulis_ilmiah');
    }
};
