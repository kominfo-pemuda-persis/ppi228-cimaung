<?php

use App\Http\Controllers\AsatidzController;
use App\Http\Controllers\AsatidzPelajaranController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DataPSB\DashboardPSBController;
use App\Http\Controllers\DataPSB\DataCalonSantriController;
use App\Http\Controllers\DataPSB\DataGagalTestController;
use App\Http\Controllers\DataPSB\DataLolosTestController;
use App\Http\Controllers\DataPSB\RekapCalonSantriController;
use App\Http\Controllers\DataPSB\GroupWhatsappController;
use App\Http\Controllers\EkstrakulikulerController;
use App\Http\Controllers\FrontKtiController;
use App\Http\Controllers\InformasiController;
use App\Http\Controllers\JadwalPelajaranController;
use App\Http\Controllers\JamMengajarController;
use App\Http\Controllers\KelompokPelajaranController;
use App\Http\Controllers\KokurikulerController;
use App\Http\Controllers\KTIController;
use App\Http\Controllers\MuaddibController;
use App\Http\Controllers\NomorUrutAsatidzController;
use App\Http\Controllers\OrangtuaController;
use App\Http\Controllers\PelajaranController;
use App\Http\Controllers\Pengaturan\MenuController;
use App\Http\Controllers\Pengaturan\PermissionController;
use App\Http\Controllers\Pengaturan\RoleController;
use App\Http\Controllers\Pengaturan\UserController;
use App\Http\Controllers\PenilaianController;
use App\Http\Controllers\PrintController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProgramUnggulanController;
use App\Http\Controllers\PSBController;
use App\Http\Controllers\RaporTSNController;
use App\Http\Controllers\SantriController;
use App\Http\Controllers\SantriDaftarController;
use App\Http\Controllers\SemesterController;
use App\Http\Controllers\TasykilController;
use App\Http\Controllers\WaliKelasController;
use App\Http\Controllers\TypeAdabController;
use App\Models\Ekstrakulikuler;
use Illuminate\Support\Facades\Route;
/**
* ROUTE AUTH
*/
Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    /**
    * PREFIX ADMIN
    */
    Route::group(['prefix' => 'admin'], function () {
        /**
        * DASHBOARD
        */
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // PROFILE
        Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
        Route::put('profile/{user}', [ProfileController::class, 'update'])->name('profile.update');
        Route::post('profile/uploadfile/{user}', [ProfileController::class, 'uploadfile'])->name('profile.uploadfile');
        Route::post('profile/change-password', [ProfileController::class, 'ChangePassword'])->name('profile.changePassword');

        /**
        * Data Calon Santri
        */
        Route::group(['prefix' => 'data-psb'], function () {
            //DATA SET AKTIVASI PSB
            Route::resource('psb', PSBController::class)
                ->middleware(['permission:menu_psb-read']);
            Route::post('psb-active', [PSBController::class, 'active'])->name('psb.active');
            
            //DATA SET GROUP WHATSAPP
            Route::get('adding-group-whatsapp',
                [GroupWhatsappController::class, 'index'])
                ->middleware(['permission:menu_set_group_whatsapp-read'])
                ->name('groupwhatsapp.index');
            Route::post('adding-group-whatsapp', [GroupWhatsappController::class, 'store'])->name('groupwhatsapp.store');
            Route::get('adding-group-whatsapp/{id}', [GroupWhatsappController::class, 'detail'])->name('groupwhatsapp.detail');
            Route::put('adding-group-whatsapp-update/{id}', [GroupWhatsappController::class, 'update'])->name('groupwhatsapp.update');
            Route::delete('adding-group-whatsapp-delete/{id}', [GroupWhatsappController::class, 'delete'])->name('groupwhatsapp.delete');
            
            //DATA DASHBOARD PSB
            Route::get('dashboard',
                [DashboardPSBController::class, 'index'])
                ->middleware(['permission:menu_dashboard_calon_santri-read'])
                ->name('data-psb.dashboard.index');
            
                //DATA REKAP SANTRI
            Route::get('rekap-calon-santri',
                [RekapCalonSantriController::class, 'index'])
                ->middleware(['permission:menu_rekap_calon_santri-read'])
                ->name('rekap.calon.santri');
            Route::any('rekap-calon-santri-data/{jenis}', [RekapCalonSantriController::class, 'data'])->name('rekap.calon.data');
            Route::get('rekap-calon-santri/{id}', [RekapCalonSantriController::class, 'detail'])->name('rekap.calon.santri.detail');
            Route::put('rekap-calon-santri/{id}', [RekapCalonSantriController::class, 'bayar'])->name('rekap.calon.santri.bayar');
            Route::get('rekap-calon-santri-edit/{id}', [RekapCalonSantriController::class, 'edit'])->name('rekap.calon.santri.edit');
            Route::put('rekap-calon-santri-update/{id}', [RekapCalonSantriController::class, 'update'])->name('rekap.calon.santri.update');
            
            //DATA CALON SANTRI
            Route::get('data-calon-santri', 
                [DataCalonSantriController::class, 'index'])
                ->middleware(['permission:menu_data_calon_santri-read'])
                ->name('data.calon.santri');
            Route::put('data-calon-santri-lolos/{id}', [DataCalonSantriController::class, 'lolos'])->name('data.calon.santri.lolos');
            Route::put('data-calon-santri-gagal/{id}', [DataCalonSantriController::class, 'gagal'])->name('data.calon.santri.gagal');
            Route::any('data-calon-santri-data/{jenis}', [DataCalonSantriController::class, 'data'])->name('data.calon.santri.data');
            Route::get('data-calon-santri-edit/{id}', [DataCalonSantriController::class, 'edit'])->name('data.calon.santri.edit');
            Route::put('data-calon-santri-update/{id}', [DataCalonSantriController::class, 'update'])->name('data.calon.santri.update');
            
            //DATA GAGAL TEST
            Route::get('data-gagal-test', 
                [DataGagalTestController::class, 'index'])
                ->middleware(['permission:menu_data_gagal_test-read'])
                ->name('data.gagal.test');
            Route::any('data-gagal-test/{jenis}', [DataGagalTestController::class, 'data'])->name('data.gagal.test.data');
            
            //DATA LOLOS TEST
            Route::get('data-lolos-test', 
                [DataLolosTestController::class, 'index'])
                ->name('data.lolos.test');
            Route::any('data-lolos-test/{jenis}', [DataLolosTestController::class, 'data'])->name('data.lolos.test.data');
            
        });
        /**
        * Data TASYKIL
        */
        Route::get('tasykil',
            [TasykilController::class, 'index'])
            ->middleware(['permission:menu_tasykil-read'])
            ->name('tasykil.index');        
        Route::any('tasykil-data/{jenis}', [TasykilController::class, 'data'])->name('tasykil.data');
        Route::get('tasykil-detail/{id}', [TasykilController::class, 'detail'])->name('tasykil.detail');
        Route::post('tasykil/export', [TasykilController::class, 'export_aktif'])->name('tasykil-aktif.export');
        /**
        * Data ASATIDZ
        */
        Route::get('asatidz',
            [AsatidzController::class, 'index'])
            ->middleware(['permission:menu_asatidz-read'])
            ->name('asatidz.index');
        Route::get('asatidz-add', 
            [AsatidzController::class, 'add'])
            ->middleware(['permission:menu_asatidz-create'])
            ->name('asatidz.add');
        Route::post('asatidz-add', [AsatidzController::class, 'store'])->name('asatidz.store');
        Route::get('asatidz-detail/{id}', [AsatidzController::class, 'detail'])->name('asatidz.detail');
        Route::get('asatidz-edit/{id}', 
            [AsatidzController::class, 'edit'])
            ->middleware(['permission:menu_asatidz-update'])
            ->name('asatidz.edit');
        Route::put('asatidz-update/{id}', [AsatidzController::class, 'update'])->name('asatidz.update');
        Route::get('asatidz-delete/{id}', [AsatidzController::class, 'delete'])->name('asatidz.delete');
        Route::any('asatidz-data/{jenis}', [AsatidzController::class, 'data'])->name('asatidz.data');
        Route::get('asatidz/export/', [AsatidzController::class, 'export'])->name('asatidz.export');
        Route::post('asatidz/import/', [AsatidzController::class, 'import'])->name('asatidz.import');
        Route::any('asatidz/file/download', [AsatidzController::class, 'download']);
        Route::post('asatidz/export', [AsatidzController::class, 'export_aktif'])->name('asatidz-aktif.export');
        /**
        * Data WALI KELAS
        */
        Route::get('walikelas', 
            [WaliKelasController::class, 'index'])
            ->middleware(['permission:menu_wali_kelas-read'])
            ->name('walikelas.index');
        Route::post('walikelas', [WaliKelasController::class, 'store'])->name('walikelas.store');
        Route::get('walikelas-ubah/{id}', 
            [WaliKelasController::class, 'edit'])
            ->middleware(['permission:menu_wali_kelas-update'])
            ->name('walikelas.edit');
        Route::put('walikelas-ubah/{id}', [WaliKelasController::class, 'update'])->name('walikelas.update');
        Route::get('walikelas-delete/{id}', [WaliKelasController::class, 'delete'])->name('walikelas.delete');
        Route::any('walikelas-data/{jenis}', [WaliKelasController::class, 'data'])->name('walikelas.data');
        /**
        * Data MUADDIB
        */
        Route::get('muaddib', 
            [MuaddibController::class, 'index'])
            ->middleware(['permission:menu_muaddib-read'])
            ->name('muaddib.index');
        Route::post('muaddib', [MuaddibController::class, 'store'])->name('muaddib.store');
        Route::get('muaddib-ubah/{id}', [MuaddibController::class, 'edit'])->name('muaddib.edit');
        Route::put('muaddib-ubah/{id}', [MuaddibController::class, 'update'])->name('muaddib.update');
        Route::get('muaddib-delete/{id}', [MuaddibController::class, 'delete'])->name('muaddib.delete');
        Route::any('muaddib-data/{jenis}', [MuaddibController::class, 'data'])->name('muaddib.data');
        /**
        * Data ASATIDZ PELAJARAN
        */
        Route::get('asatidz-pelajaran', 
            [AsatidzPelajaranController::class, 'index'])
            ->middleware(['permission:menu_astidz_pelajaran-read'])
            ->name('asatidz-pelajaran.index');
        Route::get('asatidz-pelajaran-ubah/{id}', [AsatidzPelajaranController::class, 'edit'])->name('asatidz-pelajaran.edit');
        Route::put('asatidz-pelajaran-ubah/{id}', [AsatidzPelajaranController::class, 'update'])->name('asatidz-pelajaran.update');
        Route::get('asatidz-pelajaran-delete/{id}', [AsatidzPelajaranController::class, 'delete'])->name('asatidz-pelajaran.delete');
        Route::post('asatidz-pelajaran-store', [AsatidzPelajaranController::class, 'store'])->name('asatidz-pelajaran.store');
        Route::any('asatidz-pelajaran-data/{jenis}', [AsatidzPelajaranController::class, 'data'])->name('asatidz-pelajaran.data');
        /**
        * Data JADWAL PELAJARAN
        */
        Route::group(['prefix' => 'jadwal-pelajaran'], function () {
            /**
            * Data NOMOR URUT ASAATIDZ
            */
            Route::get('nomor-urut', 
                [NomorUrutAsatidzController::class, 'index'])
                ->middleware(['permission:menu_nomor_urut_asatidz-read'])
                ->name('nomor.urut');
            Route::post('nomor-urut/store', [NomorUrutAsatidzController::class, 'store'])->name('nomor.urut.store');
            Route::post('nomor-urut/update/{id}', [NomorUrutAsatidzController::class, 'update'])->name('nomor.urut.update');
            Route::get('nomor-urut/delete/{id}', [NomorUrutAsatidzController::class, 'destroy'])->name('nomor.urut.delete');
            /**
            * Data JAM MENGAJAR
            */
            Route::get('jam-mengajar', [JamMengajarController::class, 'intra'])->name('jam.mengajar.index');
            Route::get('jam-extra', [JamMengajarController::class, 'extra'])->name('jam.mengajar.extra');
            Route::post('jam-mengajar/store', [JamMengajarController::class, 'store'])->name('jam.mengajar.store');
            Route::post('jam-mengajar/update/{id}', [JamMengajarController::class, 'update'])->name('jam.mengajar.update');
            Route::delete('jam-mengajar/delete/{id}', [JamMengajarController::class, 'destroy'])->name('jam.mengajar.delete');
            Route::delete('jam-extra/delete/{id}', [JamMengajarController::class, 'destroyExtra'])->name('jam.mengajar.deleteExtra');
            /**
            * Data JADWAL INTRAKURIKULER
            */
            Route::get('intra', 
                [JadwalPelajaranController::class, 'intra'])
                ->middleware(['permission:menu_jadwal_mengajar_intra-read'])
                ->name('jadwal.intra');
            Route::get('show-data-add', [JadwalPelajaranController::class, 'showDataAdd'])->name('jadwal.showAdd');
            Route::get('show-data-edit/{id}', [JadwalPelajaranController::class, 'showDataEdit'])->name('jadwal.showEdit');
            Route::post('intra-update/{id}', [JadwalPelajaranController::class, 'intraUpdate'])->name('jadwal.intra.update');
            Route::delete('intra-delete/{id}', [JadwalPelajaranController::class, 'intraDelete'])->name('jadwal.intra.delete');
            Route::post('intra-add', [JadwalPelajaranController::class, 'intraStore'])->name('jadwal.intra.add');
            /**
            * Data JADWAL EKSTRAKURIKULER
            */
            Route::get('ekstra', 
                [JadwalPelajaranController::class, 'extra'])
                ->middleware(['permission:menu_jadwal_mengajar_ekstra-read'])
                ->name('jadwal.ekstra');
            Route::get('show-extra-add', [JadwalPelajaranController::class, 'showDataAddExtra'])->name('jadwal.extra.showAdd');
            Route::get('show-extra-edit/{id}', [JadwalPelajaranController::class, 'showDataEditExtra'])->name('jadwal.extra.showEdit');
            Route::post('extra-update/{id}', [JadwalPelajaranController::class, 'extraUpdate'])->name('jadwal.extra.update');
            Route::delete('extra-delete/{id}', [JadwalPelajaranController::class, 'extraDelete'])->name('jadwal.extra.delete');
            Route::post('extra-add', [JadwalPelajaranController::class, 'extraStore'])->name('jadwal.extra.add');
            /**
            * Data EXPORT LAPORT
            */
        });
        /**
        * Data Karya Tulis Ilmiah
        */
        Route::get('karya-tulis-ilmiah', 
            [KTIController::class, 'index'])
            ->middleware(['permission:menu_karya_tulis_ilmiah-read'])
            ->name('karya-tulis-ilmiah.index');
        Route::post('karya-tulis-ilmiah-add', [KTIController::class, 'store'])->name('karya-tulis-ilmiah.store');
        Route::get('karya-tulis-ilmiah/{id}', [KTIController::class, 'show'])->name('karya-tulis-ilmiah.edit');
        Route::post('karya-tulis-ilmiah-update/{id}', [KTIController::class, 'update'])->name('karya-tulis-ilmiah.update');
        Route::get('karya-tulis-ilmiah-delete/{id}', [KTIController::class, 'delete'])->name('karya-tulis-ilmiah.delete');
        Route::any('karya-tulis-ilmiah-data/{jenis}', [KTIController::class, 'data'])->name('karya-tulis-ilmiah.data');
        Route::patch('karya-tulis-ilmiah/{id}/{status}', [KTIController::class, 'toggleStatus']);
        /**
        * Data SANTRI AKTIF
        */
        Route::get('santri', 
            [SantriController::class, 'index'])
            ->middleware(['permission:menu_data_santri-read'])
            ->name('santri.index');
        Route::get('santri-add', 
            [SantriController::class, 'create'])
            ->middleware(['permission:menu_data_santri-create'])
            ->name('santri.create');
        Route::post('santri-add', [SantriController::class, 'store'])->name('santri.store');
        Route::get('santri-detail/{id}', [SantriController::class, 'detail'])->name('santri.detail');
        Route::get('santri-edit/{id}', 
            [SantriController::class, 'edit'])
            ->middleware(['permission:menu_data_santri-update'])
            ->name('santri.edit');
        Route::put('santri-edit/{id}', [SantriController::class, 'update'])->name('santri.update');
        Route::get('santri-delete/{id}', [SantriController::class, 'delete'])->name('santri.delete');
        Route::any('santri-data/{jenis}', [SantriController::class, 'data'])->name('santri.data');
        Route::get('santri/export/', [SantriController::class, 'export'])->name('santri.export');
        Route::post('santri/export', [SantriController::class, 'export_aktif'])->name('santri-aktif.export');
        Route::post('santri/export-tidak-aktif/', [SantriController::class, 'export_tidak_aktif'])->name('santri-tidak-aktif.export');
        Route::post('santri/alumni/', [SantriController::class, 'export_alumni'])->name('santri-alumni.export');
        Route::post('santri/import/', [SantriController::class, 'import'])->name('santri.import');
        Route::put('santri/set-lulus', [SantriController::class, 'setBatchLulus'])
            ->middleware(['permission:menu_data_santri-update'])
            ->name('santri.setBatchLulus');
        Route::put('santri/set-kenaikan', [SantriController::class, 'setBatchKenaikan'])
            ->middleware(['permission:menu_data_santri-update'])
            ->name('santri.setBatchKenaikan');
        Route::any('santri/file/download', [SantriController::class, 'download']);
        /**
        * Data SANTRI TIDAK AKTIF
        */
        Route::get('santri-tidak-aktif', 
            [SantriController::class, 'santri_inactive'])
            ->middleware(['permission:menu_data_santri-read'])
            ->name('santri-tidak-aktif.index');
        Route::any('santri-tidak-aktif-data/{jenis}', [SantriController::class, 'data_tidak_aktif'])->name('santri-tidak-aktif-data.data');
        /**
        * Data SANTRI ALUMNI
        */
        Route::get('santri-alumni', 
            [SantriController::class, 'santri_alumni'])
            ->middleware(['permission:menu_data_santri-read'])
            ->name('santri-alumni.index');
        Route::any('santri-alumni/{jenis}', [SantriController::class, 'data_alumni'])->name('santri-alumni.data');
        /**
        * Data ORANGTUA
        */
        Route::resource('orangtua', OrangtuaController::class);
        Route::get('orangtua-delete/{id}', [OrangtuaController::class, 'delete'])->name('orangtua.delete');
        Route::any('orangtua-data/{jenis}', [OrangtuaController::class, 'data'])->name('orangtua.data');
        //MASTER
        Route::group(['prefix' => 'master'], function () {
            /**
            * Data KELOMPOK PELAJARAN
            */
            Route::get('kelompok-pelajaran', 
                [KelompokPelajaranController::class, 'index'])
                ->middleware(['permission:menu_kelompok_pelajaran-read'])
                ->name('kelompok_pelajaran.index');
            Route::get('kelompok-pelajaran-ubah/{id}', [KelompokPelajaranController::class, 'edit'])->name('kelompok_pelajaran.ubah');
            Route::put('kelompok-pelajaran-ubah/{id}', [KelompokPelajaranController::class, 'update'])->name('kelompok_pelajaran.update');
            Route::get('kelompok-pelajaran-delete/{id}', [KelompokPelajaranController::class, 'delete'])->name('kelompok_pelajaran.delete');
            Route::post('kelompok-pelajaran-store', [KelompokPelajaranController::class, 'store'])->name('kelompok_pelajaran.store');
            /**
            * Data SEMESTER
            */
            Route::get('semester', 
                [SemesterController::class, 'index'])
                ->middleware(['permission:menu_semester-read'])
                ->name('semester.index');
            Route::get('semester-ubah/{id}', [SemesterController::class, 'edit'])->name('semester.ubah');
            Route::put('semester-ubah/{id}', [SemesterController::class, 'update'])->name('semester.update');
            Route::get('semester-delete/{id}', [SemesterController::class, 'delete'])->name('semester.delete');
            Route::post('semester-store', [SemesterController::class, 'store'])->name('semester.store');
            Route::post('semester-aktif', [SemesterController::class, 'aktif'])->name('semester.aktif');
            /**
            * Data EKSTRAKURIKULER
            */
            Route::get('ekstrakurikuler', 
                [EkstrakulikulerController::class, 'index'])
                ->middleware(['permission:menu_ekstrakurikuler-read'])
                ->name('ekstrakurikuler.index');
            Route::get('ekstrakurikuler-ubah/{id}', [EkstrakulikulerController::class, 'edit'])->name('ekstrakurikuler.ubah');
            Route::put('ekstrakurikuler-ubah/{id}', [EkstrakulikulerController::class, 'update'])->name('ekstrakurikuler.update');
            Route::get('ekstrakurikuler-delete/{id}', [EkstrakulikulerController::class, 'delete'])->name('ekstrakurikuler.delete');
            Route::post('ekstrakurikuler-store', [EkstrakulikulerController::class, 'store'])->name('ekstrakurikuler.store');
            /**
            * Data PELAJARAN
            */
            Route::get('pelajaran', 
                [PelajaranController::class, 'index'])
                ->middleware(['permission:menu_pelajaran-read'])
                ->name('pelajaran.index');
            Route::get('pelajaran-ubah/{id}', 
                [PelajaranController::class, 'edit'])
                ->middleware(['permission:menu_pelajaran-update'])
                ->name('pelajaran.ubah');
            Route::put('pelajaran-ubah/{id}', [PelajaranController::class, 'update'])->name('pelajaran.update');
            Route::post('pelajaran-store', [PelajaranController::class, 'store'])->name('pelajaran.store');
            Route::get('pelajaran-delete/{id}', [PelajaranController::class, 'delete'])->name('pelajaran.delete');
            Route::get('pelajaran/export/', [PelajaranController::class, 'export'])->name('pelajaran.export');
            Route::post('pelajaran/import/', [PelajaranController::class, 'import'])->name('pelajaran.import');
            Route::any('pelajaran-data/{jenis}', [PelajaranController::class, 'data'])->name('pelajaran.data');
            /**
            * Data TYPE ADAB
            */
            Route::get('type-adab', 
                [TypeAdabController::class, 'index'])
                ->middleware(['permission:menu_type_adab-read'])
                ->name('type-adab.index');
            Route::get('type-adab-add', 
                [TypeAdabController::class, 'create'])
                ->middleware(['permission:menu_type_adab-create'])
                ->name('type-adab.create');
            Route::post('type-adab-add', [TypeAdabController::class, 'store'])->name('type-adab.store');
            Route::get('type-adab-edit/{id}', 
                [TypeAdabController::class, 'edit'])
                ->middleware(['permission:menu_type_adab-update'])
                ->name('type-adab.edit');
            Route::put('type-adab-edit/{id}', [TypeAdabController::class, 'update'])->name('type-adab.update');
            Route::delete('type-adab-delete/{id}', [TypeAdabController::class, 'delete'])->name('type-adab.delete');

            //Program Unggulan
            Route::resource('program-unggulan', ProgramUnggulanController::class);

            //Kokurikuler
            Route::resource('kokurikuler', KokurikulerController::class);
        });
        /**
        * Data MENU INFORMASI
        */
        Route::get('menu-informasi', [InformasiController::class, 'index'])
            ->middleware(['permission:menu_informasi-read'])
            ->name('informasi.index');
        Route::post('menu-informasi-add', [InformasiController::class, 'store'])->name('informasi.store');
        Route::get('menu-informasi-edit/{id}', [InformasiController::class, 'show'])->name('informasi.edit');
        Route::post('menu-informasi-update', [InformasiController::class, 'update'])->name('informasi.update');
        Route::get('menu-informasi-delete/{id}', [InformasiController::class, 'destroy'])->name('informasi.delete');

        /**
        * Data MENU PENGOLAHAN
        */
        Route::get('penilaian', [PenilaianController::class, 'index'])
            ->middleware(['permission:menu_penilaian_pendidikan-read'])
            ->name('penilaian.index');
        Route::post('penilaian', [PenilaianController::class, 'createRaporNilai'])->name('penilaian.createRaporNilai');
        Route::post('updatePenilaian', [PenilaianController::class, 'updatePenilaian'])->name('penilaian.updatePenilaian');

        /**
        * Data MENU PENGOLAHAN
        */
        Route::group(['prefix' => 'data-rapor'], function () {
            Route::resource('raportsn', RaporTSNController::class);
            Route::post('lihat-rapor-tsn', [RaporTSNController::class, 'lihatRaporTSN'])->name('lihatRaporTSN');
            Route::post('simpanNilaiProgramUnggulan', [RaporTSNController::class, 'simpanNilaiProgramUnggulan'])->name('simpanNilaiProgramUnggulan');
            Route::post('simpanNilaiKokurikuler', [RaporTSNController::class, 'simpanNilaiKokurikuler'])->name('simpanNilaiKokurikuler');
            Route::post('simpanDeskripsiEkstrakurukuler', [RaporTSNController::class, 'simpanDeskripsiEkstrakurukuler'])->name('simpanDeskripsiEkstrakurukuler');
            Route::put('simpanKetidakhadiran/{id}', [RaporTSNController::class, 'simpanKetidakhadiran'])->name('simpanKetidakhadiran');
            Route::put('simpanCatatanWalikelas/{id}', [RaporTSNController::class, 'simpanCatatanWalikelas'])->name('simpanCatatanWalikelas');
            Route::put('simpanTanggapanOrangtua/{id}', [RaporTSNController::class, 'simpanTanggapanOrangtua'])->name('simpanTanggapanOrangtua');
            Route::get('printRaporPendidikanTSN/{id}', [PrintController::class, 'printRaporPendidikanTSN'])->name('printRaporPendidikanTSN');
        });

        /**
        * Data PENGATURAN
        */
        Route::group(['prefix' => 'pengaturan'], function () {
            Route::resource('users', UserController::class);
            Route::get('refresh-data-duplicate', [UserController::class, 'refreshDataDuplicate'])->name('refresh.data.duplicate');
            Route::any('users-data/{jenis}', [UserController::class, 'data'])->name('users.data');
            Route::resource('menu', MenuController::class);
            Route::resource('permission', PermissionController::class);
            Route::resource('role', RoleController::class);
        });
    });

    // route test export page
    Route::get('export-page', [JadwalPelajaranController::class, 'exportPage'])->name('jadwal.export');

    // route test template rapor adab
    Route::get('template-rapor-adab', function () {
        return view('admin.data_rapor.template.adab');
    })->name('template.rapor');
    
    // // route test template rapor pendidikan
    Route::get('template-rapor-pendidikan', function () {
        return view('admin.data_rapor.template.pendidikan');
    })->name('template.rapor.pendidikan');
});

/**
* PUBLIC
*/

/**
* PAGE LOGIN
*/
Route::get('/', function () {
    return view('auth.login');
});
/**
* PSB
*/
Route::get('psb', [SantriDaftarController::class, 'index'])->name('psb');
Route::post('daftarSantri', [SantriDaftarController::class, 'daftarSantri']);
Route::get('terdaftar/{id}', [SantriDaftarController::class, 'terdaftar'])->name('terdaftar');
Route::get('formulir-pendaftaran/{kode}', [SantriDaftarController::class, 'preview'])->name('preview');
/**
* Karya Tulis Ilmiah
*/
Route::get('kti',[FrontKtiController::class, 'index'])->name('template.kti');
Route::get('kti/data',[FrontKtiController::class, 'dataTableIndex']);
Route::get('kti/list',[FrontKtiController::class, 'listKti'])->name('template.kti.list_karya');
Route::get('kti/detail/{id}',[FrontKtiController::class, 'detailKti'])->name('template.kti.detail');
Route::get('kti/increase-view/{id}',[FrontKtiController::class, 'increaseView']);