@extends('user.kti.layout.app')
@section('content')
<div class="container">
    <div class="row justify-content-center my-3">
        <div class="col-12">
            <h4 class="mb-4 mt-4 judul-title"><a href="/kti" class="text-decoration-none">Home</a> / List Karya</h4>
            {{-- search --}}
            <div class="d-flex justify-content-center">
                <form method="get" class="col-12">
                <div class="col-12 d-flex">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Cari karya" aria-label="Recipient's username" aria-describedby="button-addon2" name="search" value="{{ request()->get('search') }}">
                            <button class="btn btn-success" type="submit" id="button-addon2">Cari</button>
                        </div>
                        <div class="input-group mb-3 ms-2" style="width: 14em">
                            <select class="form-select" name="angkatan">
                                <option selected value="">Angkatan</option>
                                {{-- 3 tahun terakhir --}}
                                @php
                                 $now = date('Y');   
                                @endphp
                                @for($i = 0; $i < 3; $i++)
                                    @if(request()->get('angkatan') == $now - $i)
                                    <option selected value="{{ $now - $i }}">{{ $now - $i }}</option>
                                    @else
                                    <option value="{{ $now - $i }}">{{ $now - $i }}</option>
                                    @endif
                                @endfor
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <div class="row mx-1 mx-md-0">
                    @forelse($data as $item)
                    <div class="p-md-1 green-dark p-half col-lg-2 col-md-3 col-4">
                        <a href="/kti/detail/{{ $item->id }}" class="d-block text-decoration-none text-dark">
                            <div class="p-1 bg-white shadow rounded border">
                                <div class="img-wrapper mb-md-2 mb-1 rounded img-buku" style="overflow:hidden; position: relative;">
                                    <img src="{{ $item->thumbnail_kti_path }}" class="img-thumb">
                                </div>
                                <div class="detail p-md-3 p-1">
                                @php
                                    $judul = substr($item->judul_kti, 0, 25);
                                    if (strlen($item->judul_kti) > 20) {
                                        $judul .= '...';
                                    }
                                @endphp
                                <h5 class="text-judul mb-1">{{ $judul }}</h5>
                                    <h5 class="mb-0 text-nama">{{ $item->nama_lengkap }} <span>{{ $item->kelas }}-{{ $item->tingkat_sekolah_id }}</span></h5>
                                    <span class="text-tahun d-block">Angkatan : {{ $item->angkatan }}</span>
                                    <span class="text-tahun d-block">tanggal diujikan : {{ $item->tanggal_diujikan }}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @empty
                    <div class="col-12">
                        <h5 class="text-center">Karya tulis tidak ditemukan</h5>
                    </div>
                    @endforelse
                </div>
                <div class="d-flex justify-content-center mt-3">
                    {{ $data->links() }}
                </div> 
            </div>
        </div>
        {{-- footer --}}
        <div class="col-12">
            <div class="d-flex justify-content-center">
                <div class="col-5">
                    <hr style="border: 2px solid #b3b3b3">
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <div class="col-12">
                    <div class="col-12 green-dark ">
                        <p class="text-center fs-6"><a href="https://pesantrenpersis27.com" style="text-decoration: none; color:#198754;">pesantrenpersis27.com</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
