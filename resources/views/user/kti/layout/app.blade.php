<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="https://pesantrenpersis27.com/wp-content/uploads/2022/02/cropped-LOGO-PPI-27-192x192.png">
    <link rel="canonical" href="{{ env('APP_URL') }}">
    <meta name=author content="pesantrenpersis27.com">
    <meta name=language content="Indonesia">
    <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
    <meta name=description content="Karya Tulis Ilmiah PPI27">
    <meta name=keywords content="pesantrenpersis27.com, ppi27, pesantren persis, persis, pemuda persis, pemudipersis, pendidikan">
    <meta name=twitter:card content="summary">
    <meta name=title content="Karya Tulis Ilmiah PPI27">
    <meta property="og:title" content="Karya Tulis Ilmiah PPI27" />
    <meta property="og:url" content="{{ env('APP_URL') }}">
    <meta property="og:description" content="Karya Tulis Ilmiah PPI27">
    <meta property="og:image" content="https://pesantrenpersis27.com/wp-content/uploads/2022/02/cropped-LOGO-PPI-27-192x192.png">
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Karya Tulis Ilmiah PPI27" />
    <meta property="og:image:type" content="image/jpg">
    <meta property='og:image:width' content='300' />
    <meta property='og:image:height' content='300' />
    <title>Karya Tulis Ilmiah Pesantren Persis 27</title>

    @stack('style_css')
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Arabic:wght@400;600&family=Tajawal:wght@400;500;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=El+Messiri:wght@400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/assets/css/costum-raport-adab-1.css') }}">
    
    <!-- Styles -->
    <link href="{{ url('assets/css/boostrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('assets/css/card.costum.css') }}" rel="stylesheet">
</head>
<body>
    <nav class="navbar shadow bg-green-light">
        <div class="container" style="font-family: 'El Messiri', sans-serif; ">
            <a class="navbar-brand d-flex align-items-center" href="/kti">
                <img src="/img/logo-ppi.png" alt="Logo" class="d-inline-block align-text-top img-logo">
                <p class="ms-2 green-dark font-style" style="margin-bottom: 0em;">
                    Karya Tulis Ilmiah
                </p>
              </a>
            <div class="d-flex">
                <a href="/kti/list" class="nav-link text-decoration-none font-style green-dark me-2 me-md-1">List Karya tulis ilmiah</a>
            </div>
        </div>
      </nav>
    <div id="app">
        @yield('content')
    </div>
    <script src="{{ url('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ url('assets/js/img-thumb.resizer.js') }}"></script>
    @stack('scripts')
</body>
</html>
