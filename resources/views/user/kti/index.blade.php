@extends('user.kti.layout.app')

@push('style_css')
 <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css">
 <link rel="stylesheet" href="{{ url('/assets/css/costum-raport-adab-1.css') }}">
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center my-3">
        <div class="col-12 row align-items-center">
            <div class="col-md-5 col-sm-12  order-md-1 order-2" style="font-family: 'Tajawal' , sans-serif;">
                 <h2 class="fw-bold display-5 green-dark" style="font-size: 16px;">Karya Tulis Ilmiah</h2>
                 <h4 class="fw-bold display-6 green-dark" style="font-size: 24px;">الكتابة العلمية للطالب</h4>
                 <h5 class="fw-bold display-6 green-dark">Pesantren Persis 27</h5>
            </div>
            <div class="col-md-5 col-sm-12 offset-md-2 order-md-2 order-1">
                <div class="w-100">
                    <img src="{{ url('/img/kti/hero.png') }}" alt="KTI" class="w-100 my-5">
                </div>
            </div>
        </div>
        <br>
        <div class="col-12 row my-2 justify-content-center">
            <div class="col-5">
                <hr style="border: 2px solid #b3b3b3">
            </div>
        </div>
        <div class="col-12 my-5 text-center">
            <h1 class="mb-4 green-dark judul-title">List Karya tulis</h1>
            {{-- tabel --}}
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="tabelListKti">
                    <thead class="green-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Judul</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Angkatan</th>
                            <th scope="col">Tanggal diujikan</th>
                            <th scope="col">Pembimbing</th>
                            <th scope="col">Penguji</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <br>
            <h1 class="mb-4 green-dark judul-title">Karya tulis terbaru</h1>
            <div class="row mx-1 mx-md-0">
                @forelse($data as $item)
                <div class="p-md-1 green-dark p-half col-lg-2 col-md-3 col-4">
                    <a href="/kti/detail/{{ $item->id }}" class="d-block text-decoration-none text-dark">
                        <div class="p-1 bg-white shadow rounded border">
                            <div class="img-wrapper mb-md-2 mb-1 rounded img-buku" style="overflow:hidden; position: relative;">
                                <img src="{{ $item->thumbnail_kti_path }}" class="img-thumb">
                            </div>
                            <div class="detail p-md-3 p-1">
                                @php
                                    $judul = explode(' ', $item->judul_kti);
                                    $judul = array_slice($judul, 0, 3);
                                    $judul = implode(' ', $judul);
                                @endphp
                                <h5 class="text-judul mb-1">{{ $judul }}</h5>
                                <h5 class="mb-0 text-nama">{{ $item->nama_lengkap }} <span>{{ $item->kelas }}-{{ $item->tingkat_sekolah_id }}</span></h5>
                                <span class="text-tahun d-block">Angkatan : {{ $item->angkatan }}</span>
                                <span class="text-tahun d-block">tanggal diujikan : {{ $item->tanggal_diujikan }}</span>
                            </div>
                        </div>
                    </a>
                </div> 
                @empty
                <div class="col-12">
                    <h5 class="text-center judul-title">Karya tulis terbaru tidak ditemukan</h5>
                </div>
                @endforelse
            </div>
            {{-- tombol selengpapnya --}}
            <div class="d-flex justify-content-center mt-5">
                <a href="/kti/list" class="btn btn-success">Selengkapnya</a>
            </div>
        </div>
        {{-- footer --}}
        <div class="col-12">
            <div class="d-flex justify-content-center">
                <div class="col-5">
                    <hr style="border: 2px solid #b3b3b3">
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <div class="col-12 green-dark ">
                    <p class="text-center fs-6"><a href="https://pesantrenpersis27.com" style="text-decoration: none; color:#198754;">pesantrenpersis27.com</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
<script>
    // document ready jquery
    $(document).ready(function() {
        // datatable
        $('#tabelListKti').DataTable({
            serverSide: true,
            ajax: "{{ url('kti/data') }}",
            dataSrc: 'data',
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'judul_kti', name: 'judul_kti'},
                {data: 'nama_lengkap', name: 'nama_lengkap'},
                {data: 'angkatan', name: 'angkatan'},
                {data: 'tanggal_diujikan', name: 'tanggal_diujikan'},
                {data: 'nama_pembimbing', name: 'nama_pembimbing'},
                {data: 'nama_penguji', name: 'nama_penguji'},
                {data: 'status', name: 'status'},
            ]
        });
    });
</script>
@endpush
