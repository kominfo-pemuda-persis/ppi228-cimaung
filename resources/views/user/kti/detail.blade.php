@extends('user.kti.layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center my-3">
        <div class="col-12 my-4">
            <h4 class="mb-4 judul-title"><a href="/kti" class="text-decoration-none">Home</a> / <a href="/kti/list" class="text-decoration-none">List Karya</a> / Detail / {{ $data->judul_kti }}</h4>
            <div class="row">
                <div class="col-md-3 d-none d-md-block">
                    <div class="bg-white shadow rounded border w-90 p-1">
                        <div class="img-wrapper mb-md-2 mb-1 rounded" style="height: 20em; overflow:hidden; position: relative;">
                            <img src="{{ $data->thumbnail_kti_path }}" class="img-thumb">
                        </div>
                        <div class="detail p-2">
                            <h5 class="text-judul mb-1">{{ $data->judul_kti }}</h5>
                            <h5 class="mb-0 text-nama">{{ $data->nama_lengkap }} <span>{{ $data->kelas }}-{{ $data->tingkat_sekolah_id }}</span></h5>
                            <span class="text-tahun d-block">Angkatan : {{ $data->angkatan }}</span>
                            <span class="text-tahun d-block">Penguji : {{ $data->nama_penguji }}</span>
                            <span class="text-tahun d-block">Pembimbing : {{ $data->nama_pembimbing }}</span>
                            <span class="text-tahun d-block">tanggal diujikan : {{ $data->tanggal_diujikan }}</span>
                            <div class="d-flex justify-content-end">
                                {{-- content di kanan dengan icon mata dan jumlah dilihat --}}
                                <div class="d-flex align-items-center me-4">
                                    <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 576 512">
                                        <path d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z"/>
                                    </svg>
                                    <span class="ms-2">{{ $data->lihat }} x</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="bg-white shadow rounded border p-2 flex">
                        <div class="m-auto" style="aspect-ratio: 3 / 4;">   
                            <iframe src="{{ $data->kti_path }}" id="show-data" height="100%" width="100%" frameborder="0" scrolling="auto"></iframe>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end d-block d-md-none">
                        {{-- content di kanan dengan icon mata dan jumlah dilihat --}}
                        <div class="d-flex align-items-center mt-3 me-4  text-secondary" style="font-size: smaller"> 
                            <svg xmlns="http://www.w3.org/2000/svg" fill="#6c757d" height="1em" viewBox="0 0 576 512">
                                <path d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z"/>
                            </svg>
                            <span class="ms-2">{{ $data->lihat }} x</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- footer --}}
        <div class="col-12">
            <div class="d-flex justify-content-center">
                <div class="col-5">
                    <hr style="border: 2px solid #b3b3b3">
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <div class="col-12 green-dark ">
                    <p class="text-center fs-6"><a href="https://pesantrenpersis27.com" style="text-decoration: none; color:#198754;">pesantrenpersis27.com</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        // check if refresh
        if (performance.navigation.type == 1) {
        } else {
            increaseView();
        }
    });

    function increaseView() {
        const id = window.location.pathname.split('/').pop();
        $.ajax({
            url: '/kti/increase-view/' + id,
            type: 'GET',
            success: function(data) {
                console.log(data);
            }
        });
    }
</script>
@endpush
