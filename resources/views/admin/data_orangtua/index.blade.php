@extends('layouts.dashboard')

@section('title', 'Data Orangtua')

@section('orangtua', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Data Orangtua</li>
@endsection

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="ml-auto">
                    @permission('menu_data_orangtua-create')
                    <!-- <a href="{{ route('orangtua.create') }}" class="btn btn-primary btn-sm">Tambah Orangtua</a> -->
                    <a href="{{ route('orangtua.create') }}" class="btn btn-danger btn-sm">Restore Data</a>
                    @endpermission
                </div>
            </div>

        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table id="table-orangtua" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Nama Ortu</th>
                            <th>HP</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script>
    const table = $('#table-orangtua').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "order": [
                [1, "desc"]
            ],
            "autoWidth": false,
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/orangtua-data/{jenis}",
                type: "POST",
                // data:function(d){
                //     d.kelas = kelas;
                //     d.jenjang = jenjang;
                //     return d
                // }
            },
            // "initComplete": function(settings, json) {
            //     const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
            //     $.each(all_checkbox_view, function(key, checkbox) {
            //         let kolom = $(checkbox).data('kolom')
            //         let is_checked = checkbox.checked
            //         table.column(kolom).visible(is_checked)
            //     })
            //     setTimeout(function() {
            //         table.columns.adjust().draw();
            //     }, 3000)
            // },
            columnDefs: [
                {
                    targets: '_all',
                    visible: true
                },
                {
                    "targets": 0,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nama;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.hp;
                    }
                },
                {
                    "targets": 2,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.alamat;
                    }
                },
                {
                    "targets": 3,
                    "render": function(data, type, row, meta) {
                        let tampilan = `
                        <div class="d-flex">
                            @permission('menu_data_orangtua-update')
                            <a href="/admin/orangtua/${row.id}/edit" class="btn btn-sm btn-warning mr-2">Edit</a>
                            @endpermission
                        </div>`
                        return tampilan;
                    }
                },
                // {
                //     "targets": 3,
                //     "render": function(data, type, row, meta) {
                //         let tampilan = `
                //         <div class="d-flex">
                //             @permission('menu_data_orangtua-update')
                //             <a href="/admin/orangtua/${row.id}/edit" class="btn btn-sm btn-warning mr-2">Edit</a>
                //             @endpermission
                //             </div>
                //             `
                //             // @permission('menu_data_orangtua-delete')
                //             // <a href="/admin/orangtua-delete/${row.id}" onclick="return confirm('Apakah ingin menghapus asatidz dan pelajaran ini')"  class="btn btn-sm btn-danger mr-2">Delete</a>
                //             // @endpermission
                //         return tampilan;
                //     }
                // },

            ]
        });
</script>
@endpush