@extends('layouts.dashboard')

@section('title', 'Tambah Orangtua')

@section('orangtua', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Tambah Orangtua</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <a href="{{ route('orangtua.index') }}" class="btn btn-dark"><i class="fas fa-arrow-left"></i> Kembali</a>
        </div>
        <form action="{{ route('orangtua.store') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" required placeholder="Nama..." value="{{ old('nama') }}">
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hp">No. HP</label>
                            <input type="text" class="form-control @error('hp') is-invalid @enderror" name="hp" id="hp" placeholder="081..." value="{{ old('hp') }}">
                            @error('hp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kerja">Pekerjaan</label>
                            <input type="text" class="form-control @error('kerja') is-invalid @enderror" name="kerja" id="kerja" placeholder="Pekerjaan" value="{{ old('kerja') }}">
                            @error('kerja')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="alamat" placeholder="Alamat..." value="{{ old('alamat') }}">
                            @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary"><i class="fas fa-paper-plane"></i> Simpan</button>
            </div>
        </form>
    </div>

@endsection