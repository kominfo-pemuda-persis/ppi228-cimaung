@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Edit Pelajaran</li>
@endsection

@section('title', 'Edit Data Pelajaran')

@section('content')
    <div class="card p-2">
        <form action="{{ route('pelajaran.update', $dataid->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="kode_pelajaran">Kode</label>
                        <input type="text" id="kode_pelajaran" value="{{ $dataid->kode }}" class="form-control" name="kode">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pelajaran">Nama Pelajaran</label>
                        <input type="text" id="pelajaran" value="{{ $dataid->nama_pelajaran }}" class="form-control" name="nama_pelajaran">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="kelompok_id">Kelompok Pelajaran</label>
                        <select name="kelompok_id" id="edit_kelompok_id" class="form-control">
                            @foreach ($datakelompokpel as $data)
                                <option value="{{ $data->id }}" {{ ($data->id == $dataid->kelompok_id) ? 'selected' : '' }}>{{ $data->nama_kelompok_pelajaran }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                        <select name="tingkat_sekolah_id" id="tingkat_sekolah_id" class="form-control">
                            <option value="TSN" {{ $dataid->tingkat_sekolah_id == "TSN" ? 'selected' : '' }}>TSN</option>
                            <option value="MLN" {{ $dataid->tingkat_sekolah_id == "MLN" ? 'selected' : '' }}>MLN</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="urutan">Urutan</label>
                        <input type="number" value="{{ $dataid->urutan }}" id="edit_urutan" class="form-control" name="urutan">
                    </div>
                </div>
            </div>
            <a href="{{ route('pelajaran.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
@endsection

@push('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })
</script>
@endpush