@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Pelajaran</li>
@endsection

@section('title', 'Data Pelajaran')

@section('content')
    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{$error}} <br>
            @endforeach
        </ul>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_pelajaran-create')
                <button data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-sm mr-1">Tambah
                    Pelajaran</button>
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-default">Import</button>
                @endpermission
                <a href="{{ route('pelajaran.export') }}" class="btn btn-primary btn-sm">Export</a>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                {{-- <div class="col-md-4">
                    <label>Kelas</label>
                    <select id="filter-kelas" class="form-control filter">
                        <option value="">Pilih Kelas</option>
                        <option value="X">Kelas 1</option>
                        <option value="XI">Kelas 2</option>
                        <option value="XII">Kelas 3</option>
                    </select>
                </div> --}}
                <div class="col-md-4">
                    <label>Jenjang</label>
                    <select id="filter-jenjang" class="form-control filter">
                        <option value="">Pilih Jenjang</option>
                        <option value="TSN">Tsanawiyyah</option>
                        <option value="MLN">Mu'allimin</option>
                    </select>
                </div>
            </div>
            <hr class="divider">
            <div class="table-responsive">
                <table id="table-pelajaran" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama Pelajaran</th>
                            <th>Kelompok Pelajaran</th>
                            <th>Tingkat</th>
                            <th>Urutan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Pelajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('pelajaran.store') }}" id="ModalTambahPel" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="pelajaran">Kode</label>
                            <input type="text" class="form-control" name="kode" placeholder="Isi Pelajaran">
                        </div>
                        <div class="form-group">
                            <label for="pelajaran">Nama Pelajaran</label>
                            <input type="text" class="form-control" name="pelajaran" placeholder="Isi Nama Pelajaran">
                        </div>
                        <div class="form-group">
                            <label for="kelompok_id">Kelompok Pelajaran</label>
                            <select name="kelompok_id" id="kelompok_id" class="form-control" required>
                                <option value="" selected disabled>Pilih Kelompok Pelajaran</option>
                                @foreach ($kelompok_pelajaran as $key => $data)
                                    <option value="{{ $key }}">{{ $data }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                            <select name="tingkat_sekolah_id" id="tingkat_sekolah_id" class="form-control" required>
                                <option value="" selected disabled>Pilih Tingkat Sekolah</option>
                                <option value="TSN">TSN</option>
                                <option value="MLN">MLN</option>
                            </select>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                <h6 class="modal-title">Import File pelajaran</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form action="{{ route('pelajaran.import') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-2">
                            <a href="{{ Storage::disk('s3')->url('static/template-pelajaran.xlsx') }}" download="template-pelajaran" class="btn btn-success btn-sm">Download Template</a>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="file" required class="custom-file-input @error('file') is-invalid @enderror" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile" id="fileRemark">Pilih file</label>
                                    </div>                
                                </div>
                                @error('file')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                    </div>     
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success btn-sm">Import</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@push('scripts')
<!-- bs-custom-file-input -->
<script src="{{ asset('admin_lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script>
  $(function () {
    bsCustomFileInput.init();
  });
</script>
    <script>
        // Initialize the DataTable
        // let kelas = $("#filter-kelas").val()
        let jenjang = $("#filter-jenjang").val();

        const table = $('#table-pelajaran').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "order": [
                [1, "desc"]
            ],
            "autoWidth": false,
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/master/pelajaran-data/{jenis}",
                type: "POST",
                data:function(d){
                    // d.kelas = kelas;
                    d.jenjang = jenjang;
                    return d
                }
            },
            "initComplete": function(settings, json) {
                const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
                $.each(all_checkbox_view, function(key, checkbox) {
                    let kolom = $(checkbox).data('kolom')
                    let is_checked = checkbox.checked
                    table.column(kolom).visible(is_checked)
                })
                setTimeout(function() {
                    table.columns.adjust().draw();
                }, 3000)
            },
            columnDefs: [
                // {
                //     "targets": 0,
                //     "class": "text-nowrap",
                //     "sortable": false,
                //     "render": function(data, type, row, meta) {
                //         return `<input type="checkbox" class="cb-child" value="${row.id}">`;
                //     }
                // },
                {
                    targets: '_all',
                    visible: true
                },
                {
                    "targets": 0,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        // list_karyawan[row.id] = row;
                        console.log(row)
                        return row.kode;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nama_pelajaran;
                    }
                },
                {
                    "targets": 2,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.kelompok_pelajaran.nama_kelompok_pelajaran;
                    }
                },
                {
                    "targets": 3,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.tingkat_sekolah_id;
                    }
                },
                {
                    "targets": 4,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.urutan;
                    }
                },
                {
                    "targets": 5,
                    "render": function(data, type, row, meta) {
                        let tampilan = `
                        <div class="d-flex">
                            @permission('menu_pelajaran-update')
                            <a href="/admin/master/pelajaran-ubah/${row.id}" class="btn btn-sm btn-primary mr-2">Edit</a>
                            @endpermission
                            @permission('menu_pelajaran-delete')
                            <a href="/admin/master/pelajaran-delete/${row.id}" class="btn btn-sm btn-warning mr-2">Delete</a>
                            @endpermission
                            </div>
                            `
                        return tampilan;
                    }
                },

            ]
        });

        $(".filter").on('change',function(){
            kelas = $("#filter-kelas").val()
            jenjang = $("#filter-jenjang").val()
            table.ajax.reload(null,false)
        })

        $("#row-tampilan input[type='checkbox']").on('change',function(){
            let checkbox = $(this)
            let kolom = $(this).data('kolom')
            let is_checked = checkbox[0].checked
            table.column(kolom).visible(is_checked)
        })

        $('#edit_kode').val("");
        $('#edit_pelajaran').val("");
        $('#edit_kelompok_id').val("");
        $('#edit_tingkat_sekolah_id').val("");
        $('#edit_urutan').val("");

        function showFileName(){
            let input = document.getElementById('exampleInputFile');
            let fileName = input.files[0].name;
            document.getElementById('fileRemark').innerText = 'File : ' + fileName;
        }
    </script>
@endpush
