<table>
  <thead>
    <tr>
      <th>NO</th>
      <th>KODE</th>
      <th>NAMA PELAJARAN</th>
      <th>KELOMPOK</th>
      <th>TINGKAT</th>
    </tr>
  </thead>
  <tbody>
    @foreach($pelajaran as $data)
    <tr>
      <td>{{ $loop->iteration }}</td>
      <td>{{ $data->kode }}</td>
      <td>{{ $data->nama_pelajaran }}</td>
      <td>{{ $data->kelompok_pelajaran->nama_kelompok_pelajaran }}</td>
      <td>{{ $data->tingkat_sekolah_id }}</td>
    </tr>
    @endforeach
  </tbody>
</table>