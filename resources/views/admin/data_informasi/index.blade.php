@extends('layouts.dashboard')
<?php
use Carbon\Carbon;
?>

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Menu Informasi</li>
@endsection

@section('title', 'Menu Informasi')

@section('content')
    <div class="card">
        @permission('menu_informasi-create')
        <div class="card-header mb-2">
            <div class="card-tools">
                <button data-toggle="modal" data-target="#show-informasi" class="btn btn-primary btn-sm">Muat Informasi</button>
            </div>          
        </div>
        @endpermission

        <div class="card-body">
            <div class="table-responsive">
                <table id="table-informasi" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Publisher</th>
                            <th>Keterangan</th>
                            <th>Tanggal dibuat</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($datas as $data)
                        @php
                            $date = Carbon::parse($data->tanggal_dibuat)->locale('id');
                            $date->settings(['formatFunction' => 'translatedFormat']);
                        @endphp 
                        <tr>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->keterangan }}</td>
                            <td>{{ $date->format('l, j F Y') }}</td>
                            <td>
                                <a class="btn btn-primary btn-sm mr-2 mb-1 file-show" data-toggle="modal" data-file="{{ $data->file }}" data-target="#show-pdf" href="#">Show PDF</a>
                                @permission('menu_informasi-update')
                                <a href="#" id="modal-edit" data-id="{{ $data->id }}" class="btn btn-sm btn-primary mr-2 mb-1">Edit</a>
                                @endpermission
                                @permission('menu_informasi-delete')
                                <a href="{{url('/admin/menu-informasi-delete/'.$data->id)}}" onclick="return confirm('yakin?')" class="btn btn-sm btn-danger mr-2 mb-1">Delete</a>
                                @endpermission
                                <a href="{{ Storage::disk('s3')->url('data/informasi/'). $data->file }}" download="{{ $data->file }}" class="btn btn-sm btn-success mb-1">Download PDF</a>
                            </td>
                        </tr>
                        @empty
                        <div class="alert alert-danger">
                            Data Informasi tidak ada
                        </div>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        {{-- ADD --}}
        <div class="modal fade" id="show-informasi" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Muat Informasi</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('informasi.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <input type="text" class="form-control" name="keterangan" placeholder="Masukkan Keterangan">
                            </div>
                            <div class="form-group">
                                <label for="informasi">Upload Informasi</label>
                                <input class="form-control" name="file" type="file" id="informasi">
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- EDIT --}}
        <div class="modal fade" id="edit-informasi" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Muat Informasi</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('informasi.update') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" id="idEdit" name="id">
                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <input type="text" id="keteranganEdit"  class="form-control" name="keterangan" placeholder="Masukkan Keterangan">
                            </div>
                            <div class="form-group">
                                <label for="informasi">Upload Informasi</label>
                                <input class="form-control" required ="informasiEdit" name="file" type="file">
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        {{-- MODAL --}}

        <div class="modal fade" id="show-pdf" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body" style="height: 35rem">
                        <iframe src="" id="show-data" height="100%" width="100%" frameborder="0" scrolling="auto"></iframe>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@push('scripts')
<script>
    const table = $('#table-informasi').DataTable();

    const data = "{{ Storage::disk('s3')->url('data/informasi') }}"
    $('body').on('click', '.file-show' , function() {
        $('#show-data').attr('src', data+'/'+$(this).attr('data-file'))
    });

    // onclick edit
    $('body').on('click', '#modal-edit', function() {
        const id = $(this).attr('data-id');
        $('#idEdit').val(id);

        $.ajax({
            url: "{{ url('admin/menu-informasi-edit') }}"+'/'+id,
            method: "GET",
            data: {
                id: id
            },
            success: function(data) {
                $('#edit-informasi').modal('show');
                $('#keteranganEdit').val(data.keterangan);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
</script>
@endpush