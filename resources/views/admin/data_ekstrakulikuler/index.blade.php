@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Ekstrakurikuler</li>
@endsection

@section('title', 'Data Ekstrakurikuler')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_ekstrakurikuler-create')
                <button data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-sm">Tambah
                    Ekstrakurikuler</button>
                @endpermission
            </div>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kegiatan</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($datas as $data)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->keterangan }}</td>
                            <td>
                                @permission('menu_ekstrakurikuler-update')
                                <a href="javascript:void(0)" data-id="{{ $data->id }}" id="ubahEktrakurikuler"
                                class="btn btn-warning">Edit</a>
                                @endpermission
                                @permission('menu_ekstrakurikuler-delete')
                                <a href="{{ route('ekstrakurikuler.delete', $data->id) }}"
                                onclick="return confirm('apakah yakin ingin menghapus data ini?')"
                                class="btn btn-danger">Hapus</a>
                                @endpermission
                            </td>
                        </tr>
                    @empty
                        <div class="alert alert-danger">
                            Data Ekstrakurikuler belum Tersedia.
                        </div>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Ekstrakurikuler</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('ekstrakurikuler.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama Ekstrakurikuler</label>
                            <input type="text" class="form-control" name="nama"
                                placeholder="Masukkan Nama Ekstrakurikuler">
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <input type="text" class="form-control" name="keterangan" placeholder="masukkan keterangan">
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- EDIT --}}
    <div class="modal fade" id="modal-edit" ria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Ektrakurikuler</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_ekstrakurikuler">
                    <div class="form-group">
                        <label for="nama">Nama Kegiatan</label>
                        <input type="text" id="edit_nama" class="form-control"
                            placeholder="Masukkan nama kegiatan">
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" class="form-control" id="edit_keterangan" placeholder="masukkan keterangan">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="saveUpdate" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $('#edit_nama').val("");
        $('#edit_keterangan').val("");

        $('body').on('click', '#ubahEktrakurikuler', function() {
            let id = $(this).data('id');
            //fetch detail post with ajax
            $.ajax({
                url: `/admin/ekstrakurikuler-ubah/${id}`,
                type: "GET",
                cache: false,
                success: function(response) {
                    //fill data to form
                    $('#id_ekstrakurikuler').val(response.data.id);
                    $('#edit_nama').val(response.data.nama);
                    $('#edit_keterangan').val(response.data.keterangan);
                    //open modal
                    $('#modal-edit').modal('show');
                }
            });
        });


        //action update post
        $('#saveUpdate').click(function(e) {
            e.preventDefault();
            let id = $('#id_ekstrakurikuler').val();
            //define variable
            let edit_nama = $('#edit_nama').val();
            let edit_keterangan = $('#edit_keterangan').val();

            //ajax
            $.ajax({

                url: `/admin/ekstrakurikuler-ubah/${id}`,
                type: "PUT",
                cache: false,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "nama": edit_nama,
                    "keterangan": edit_keterangan,
                },
                success: function(response) {
                    console.log(response);
                    //show success message
                    toastr.success(response.success, 'BERHASIL!');
                    setTimeout(() => {
                        window.location.href = response.Url
                    }, 1000);

                    //close modal
                    $('#modal-edit').modal('hide');

                }
            });
        });
    </script>
@endpush
