@extends('layouts.dashboard')
<?php
use Carbon\Carbon;
?>

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Menu Karya Tulis Ilmiah</li>
@endsection
@push('css')
    <link rel="stylesheet" href="{{asset('admin_lte/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.css')}}">
    <link rel="stylesheet" href="{{ asset('datepicker/css/yearpicker.css') }}" />
    <style>
        .yearpicker-container {
            z-index: 9999 !important;
        }
    </style>
@endpush

@section('title', 'Menu Karya Tulis Ilmiah')

@section('content')
    <div class="card">
        @permission('menu_karya_tulis_ilmiah-create')
        <div class="card-header mb-2">
            <div class="card-tools">
                <button data-toggle="modal" data-target="#show-kti" class="btn btn-primary btn-sm">Muat KTI</button>
            </div>          
        </div>
        @endpermission
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Jenjang</label>
                    <select id="filter-jenjang" class="form-control filter">
                        <option value="">Pilih Jenjang</option>
                        <option value="TSN">Tsanawiyyah</option>
                        <option value="MLN">Mu'allimin</option>
                    </select>
                </div>
            </div>
            <hr class="divider">
            <div class="table-responsive">
                <table id="table-kti" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Thumbnail KTI</th>
                            <th>Judul KTI</th>
                            <th>Nama Santri</th>
                            <th>Kelas</th>
                            <th>Angkatan</th>
                            <th>Tanggal dibuat</th>
                            <th>Tanggal diujikan</th>
                            <th>Action</th>
                            <th>Published</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        {{-- ADD --}}
        <div class="modal fade" id="show-kti" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Muat Karya Tulis Ilmiah</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('karya-tulis-ilmiah.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="judul_kti">Judul KTI</label>
                                <input type="text" class="form-control" name="judul_kti" placeholder="Masukkan Judul Karya Tulis Ilmiah" required>
                            </div>
                            <div class="form-group">
                                <label for="santri_id">Nama Santri</label>
                                <select name="santri_id" id="santri_id" class="form-control select2bs4" required>
                                    <option value="" selected disabled>Pilih Santri</option>
                                    @foreach ($santri as $data)
                                    <option value="{{ $data->id }}">{{ $data->nama_lengkap }} - {{$data->tingkat_sekolah_id}}-{{$data->kelas}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-flex">
                                <div class="w-50" style="padding-right: .2em">
                                    <div class="form-group">
                                        <label for="add_pembimbing">Nama Pembimbing</label>
                                        <select name="pembimbing" id="add_pembimbing" class="form-control select2bs4" required>
                                            <option value="" selected disabled>Pilih Pembimbing</option>
                                            @foreach ($asatidz as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_lengkap }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="w-50" style="padding-left: .2em">
                                    <div class="form-group">
                                        <label for="add_penguji">Nama Penguji</label>
                                        <select name="penguji" id="add_penguji" class="form-control select2bs4" required>
                                            <option value="" selected disabled>Pilih Penguji</option>
                                            @foreach ($asatidz as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_lengkap }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="angkatan">Angkatan</label>
                                <input type="number" class="yearpicker form-control @error('angkatan') is-invalid @enderror"
                                placeholder="Angkatan" id="angkatan" name="angkatan" value="{{ date('Y') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_diujikan">Tanggal Diujikan</label>
                                <input type="date" class="form-control @error('tanggal_diujikan') is-invalid @enderror"
                                placeholder="Tanggal Diujikan" id="tanggal_diujikan" name="tanggal_diujikan" required>
                            </div>
                            <div class="form-group">
                                <label for="thumbnail_kti_path">Upload Thumbnail</label>
                                <input class="form-control" name="thumbnail_kti_path" type="file" id="thumbnail_kti_path">
                            </div>
                            <div class="form-group">
                                <label for="kti_path">Upload KTI</label>
                                <input class="form-control" name="kti_path" type="file" id="kti_path">
                            </div>
                            <div class="form-group">
                                <label for="halaman">Halaman</label>
                                <input type="number" class="yearpicker form-control @error('halaman') is-invalid @enderror"
                                placeholder="Masukkan halaman" id="halaman" name="halaman">
                            </div>
                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <textarea class="form-control" name="keterangan" id="keterangan" cols="10" rows="2"></textarea>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{-- EDIT --}}
        <div class="modal fade" id="edit-kti" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit KTI</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" enctype="multipart/form-data" id="edit-kti-form">
                            @csrf
                            <input type="hidden" id="idEdit" name="id">
                            <div class="form-group">
                                <label for="judul_kti">Judul KTI</label>
                                <input type="text" class="form-control" name="edit_judul_kti" id="edit_judul_kti" placeholder="Masukkan Judul Karya Tulis Ilmiah" required>
                            </div>
                            <div class="form-group">
                                <label for="santri_id">Nama Santri</label>
                                <select name="edit_santri_id" id="edit_santri_id" class="form-control select2bs4" required>
                                    <option value="" selected disabled>Pilih Santri</option>
                                    @foreach ($santri as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama_lengkap }} - {{$data->tingkat_sekolah_id}}-{{$data->kelas}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-flex">
                                <div class="w-50" style="padding-right: .2em">
                                    <div class="form-group">
                                        <label for="edit_pembimbing">Nama Pembimbing</label>
                                        <select name="pembimbing" id="edit_pembimbing" class="form-control select2bs4" required>
                                            <option value="" selected disabled>Pilih Pembimbing</option>
                                            @foreach ($asatidz as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_lengkap }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="w-50" style="padding-left: .2em">
                                    <div class="form-group">
                                        <label for="edit_penguji">Nama Penguji</label>
                                        <select name="penguji" id="edit_penguji" class="form-control select2bs4" required>
                                            <option value="" selected disabled>Pilih Penguji</option>
                                            @foreach ($asatidz as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_lengkap }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="angkatan">Angkatan</label>
                                <input type="number" class="edit-yearpicker form-control @error('angkatan') is-invalid @enderror"
                                placeholder="Angkatan" id="edit_angkatan" name="edit_angkatan" required>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_diujikan">Tanggal Diujikan</label>
                                <input type="date" class="form-control @error('tanggal_diujikan') is-invalid @enderror"
                                placeholder="Tanggal Diujikan" id="edit_tanggal_diujikan" name="edit_tanggal_diujikan" required>
                            </div>
                            <div class="form-group">
                                <label for="thumbnail_kti_path">Upload Thumbnail</label>
                                <input class="form-control" name="edit_thumbnail_kti_path" type="file" id="edit_thumbnail_kti_path">
                            </div>
                            <div class="form-group">
                                <label for="kti_path">Upload KTI</label>
                                <input class="form-control" name="edit_kti_path" type="file" id="edit_kti_path">
                            </div>
                            <div class="form-group">
                                <label for="halaman">Halaman</label>
                                <input type="number" class="form-control @error('halaman') is-invalid @enderror" id="edit_halaman" name="edit_halaman">
                            </div>
                            <div class="form-group">
                                <label for="edit_keterangan">Keterangan</label>
                                <textarea name="edit_keterangan" id="edit_keterangan" class="form-control" id="" cols="10" rows="2"></textarea>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" id="edit-save" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        {{-- MODAL --}}
        <div class="modal fade" id="show-pdf" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body" style="height: 35rem">
                        <iframe src="" id="show-data" height="100%" width="100%" frameborder="0" scrolling="auto"></iframe>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@push('scripts')
<script src="{{ asset('datepicker/js/yearpicker.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    let jenjang = $("#filter-jenjang").val();

    $(document).ready(function() {
        const thisYear = (new Date).getFullYear();
        const startYear = thisYear - 2;
        const endYear = thisYear + 4;
        $("#angkatan").yearpicker({
            year: thisYear,
            startYear: startYear,
            endYear: endYear,
        });

        $(".edit-yearpicker").yearpicker({
            year: thisYear,
            startYear: startYear,
            endYear: endYear,
        });
    });
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
    
    const tumbnail = "{{ Storage::disk('s3')->url('data/santri/thumbnail_kti/') }}"
    const table = $('#table-kti').DataTable({
        "pageLength": 10,
        "lengthMenu": [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'semua']
        ],
        "bLengthChange": true,
        "bFilter": true,
        "bInfo": true,
        "processing": true,
        "bServerSide": true,
        "order": [
            [1, "desc"]
        ],
        "autoWidth": false,
        "ajax": {
            url: "{{ env('APP_URL') }}/admin/karya-tulis-ilmiah-data/{jenis}",
            type: "POST",
            data:function(d){
                d.jenjang = jenjang;
                return d
            }
        },
        "initComplete": function(settings, json) {
            const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
            $.each(all_checkbox_view, function(key, checkbox) {
                let kolom = $(checkbox).data('kolom')
                let is_checked = checkbox.checked
                table.column(kolom).visible(is_checked)
            })
            setTimeout(function() {
                table.columns.adjust().draw();
            }, 3000)
        },
        columnDefs: [
            {
                targets: '_all',
                visible: true
            },
            {
                "targets": 0,
                "class": "text-nowrap",
                "render": function(data, type, row, meta) {
                    let image = tumbnail+row.thumbnail_kti_path
                    return `<img src="${image}" height="80"/>`;
                }
            },
            {
                "targets": 1,
                "class": "text-nowrap",
                "render": function(data, type, row, meta) {
                    return row.judul_kti;
                }
            },
            {
                "targets": 2,
                "class": "text-nowrap",
                "render": function(data, type, row, meta) {
                    return row.santri.nama_lengkap;
                }
            },
            {
                "targets": 3,
                "class": "text-nowrap",
                "render": function(data, type, row, meta) {
                    return row.santri.tingkat_sekolah_id + " - " + row.santri.kelas;
                }
            },
            {
                "targets": 4,
                "class": "text-nowrap",
                "render": function(data, type, row, meta) {
                    return row.angkatan;
                }
            },
            {
                "targets": 5,
                "class": "text-nowrap",
                "render": function(data, type, row, meta) {
                    return row.tanggal_dibuat;
                }
            },
            {
                "targets": 6,
                "class": "text-nowrap",
                "render": function(data, type, row, meta) {
                    return row.tanggal_diujikan;
                }
            },
            {
                "targets": 7,
                "render": function(data, type, row, meta) {
                    let tampilan = `
                    <div class="d-flex">
                        <a class="btn btn-primary btn-sm mr-2 mb-1 file-show ${row.kti_path == 'unset' ? 'd-none' : ''}" data-toggle="modal" data-file="${row.kti_path}" data-target="#show-pdf" href="#">Show KTI</a>
                        @permission('menu_karya_tulis_ilmiah-update')
                        <a href="#" id="modal-edit" data-id="${row.id}" class="btn btn-sm btn-primary mr-2 mb-1">Edit</a>
                        @endpermission
                        @permission('menu_karya_tulis_ilmiah-delete')
                        <a href="{{ url('/admin/karya-tulis-ilmiah-delete') }}/${row.id}" onclick="return confirm('yakin?')" class="btn btn-sm btn-danger mr-2 mb-1">Delete</a>
                        @endpermission
                    </div>
                    `
                    return tampilan;
                }
            },
            {
                "targets": 8,
                "class": "text-nowrap",
                "render": function(data, type, row, meta) {
                    // toggle publish
                    let checked = row.status == "publish" ? 'checked' : '';
                    let tampilan = `
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch${row.id}" ${checked} data-id="${row.id}">
                        <label class="custom-control-label" for="customSwitch${row.id}"></label>
                    </div>
                    `
                    return tampilan;
                }
            },

        ]
    });

    $(".filter").on('change',function(){
        jenjang = $("#filter-jenjang").val()
        table.ajax.reload(null,false)
    })

    const data = "{{ Storage::disk('s3')->url('data/santri/kti') }}"
    $('body').on('click', '.file-show' , function() {
        $('#show-data').attr('src', data+'/'+$(this).attr('data-file'))
    });

    // onclick edit
    $(document).on('submit', '#edit-kti-form', function(e) {
        e.preventDefault();
        
        let id = $('#idEdit').val();
        let editFormData = new FormData($('#edit-kti-form')[0]);

        $.ajax({
            type:"POST",
            url: "{{ url('admin/karya-tulis-ilmiah-update') }}"+'/'+id,
            data: editFormData,
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            contentType:false,
            processData:false,
            success: function(response) {
                if(response.success == true){ // Uploaded successfully
                    // Response message
                    toastr.success('{{ session('response.message') }}', response.message); 
                    setTimeout(() => {
                        window.location.href = response.url;
                    }, 3000);
                }else{
                    toastr.error('{{ session('response.message') }}', response.message); 
                    setTimeout(() => {
                        window.location.href = response.url;
                    }, 3000);
                }
            },
            error: function(response) {
                if(response.success == false){ // Uploaded gagal
                    // Response message
                    toastr.error('{{ session('response.message') }}', response.message); 
                    setTimeout(() => {
                    window.location.href = response.url;
                    }, 3000);
                }
            }
        });
    });

    $('body').on('click', '#modal-edit', function() {
        const id = $(this).attr('data-id');
        $('#idEdit').val(id);

        $.ajax({
            url: "{{ url('admin/karya-tulis-ilmiah') }}"+'/'+id,
            method: "GET",
            data: {
                id: id
            },
            success: function(data) {
                $('#edit-kti').modal('show');
                $('#edit_judul_kti').val(data.judul_kti)
                $('#edit_santri_id').select2().val(data.id_santri).trigger("change");
                $('#edit_angkatan').val(data.angkatan)
                $('#edit_tanggal_diujikan').val(data.tanggal_diujikan)
                $('#edit_halaman').val(data.halaman)
                $('#edit_keterangan').val(data.keterangan)
                $('#edit_pembimbing').select2().val(data.pembimbing).trigger("change");
                $('#edit_penguji').select2().val(data.penguji).trigger("change");
            },
            error: function(response) {
                if(response.success == false){ // Uploaded gagal
                    // Response message
                    toastr.error('{{ session('response.message') }}', response.message); 
                    setTimeout(() => {
                    window.location.href = response.url;
                    }, 3000);
                }
            }
        });
    });

    // toggle publish
    $('body').on('change', '.custom-control-input', function() {
        let id = $(this).data('id');
        let status = $(this).is(':checked') ? 'publish' : 'pending';
        $.ajax({
            url: "{{ url('admin/karya-tulis-ilmiah') }}"+'/'+id+'/'+status,
            method: "PATCH",
            data: {
                _token: '{{ csrf_token() }}',
            },
            success: function(data) {
                toastr.success('{{ session('response.message') }}', data.message); 
                setTimeout(() => {
                    window.location.href = data.url;
                }, 3000);
            },
            error: function(response) {
                if(response.success == false){ // Uploaded gagal
                    // Response message
                    toastr.error('{{ session('response.message') }}', response.message); 
                    setTimeout(() => {
                    window.location.href = response.url;
                    }, 3000);
                }
            }
        });
    });
</script>
@endpush