@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Data Kelompok Pelajaran</li>
@endsection

@section('title', 'Data Kelompok Pelajaran')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_kelompok_pelajaran-create')
                <button data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-sm mr-1">Tambah 
                    Kelompok Pelajaran</button>
                @endpermission
            </div>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID</th>
                        <th>Nama Pelajaran</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($datas as $data)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->id }}</td>
                            <td>{{ $data->nama_kelompok_pelajaran }}</td>
                            <td>{{ $data->keterangan }}</td>
                            <td>
                                @permission('menu_kelompok_pelajaran-update')
                                <a href="javascript:void(0)" data-id="{{ $data->id }}" id="ubahKP"
                                class="btn btn-warning">Edit</a>
                                @endpermission
                                @permission('menu_kelompok_pelajaran-delete')
                                <a href="{{ route('kelompok_pelajaran.delete', $data->id) }}" onclick="return confirm('apakah yakin ingin menghapus data ini?')"
                                class="btn btn-danger">Hapus</a>
                                @endpermission
                            </td>
                        </tr>
                    @empty
                        <div class="alert alert-danger">
                            Data Kelompok pelajaran belum Tersedia.
                        </div>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Kelompok Pelajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('kelompok_pelajaran.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_kelompok_pelajaran">Nama Pelajaran</label>
                            <input type="text" class="form-control" name="nama_kelompok_pelajaran">
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <input type="text" class="form-control" name="keterangan">
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- EDIT --}}
    <div class="modal fade" id="modal-edit" ria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Pelajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_kelompok_pelajaran">
                    <div class="form-group">
                        <label for="pelajaran">Nama Pelajaran</label>
                        <input type="text" id="edit_kelompok_pelajaran" class="form-control" name="pelajaran">
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="type" id="edit_kelompok_keterangan" class="form-control" name="keterangan">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="saveUpdate" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $('#edit_kelompok_pelajaran').val("");
        $('#edit_keterangan').val("");
        
        $('body').on('click', '#ubahKP', function() {
            let id = $(this).data('id');
            //fetch detail post with ajax
            $.ajax({
                url: `/admin/kelompok-pelajaran-ubah/${id}`,
                type: "GET",
                cache: false,
                success: function(response) {
                    console.log(response)
                    //fill data to form
                    $('#id_kelompok_pelajaran').val(response.data.id);
                    $('#edit_kelompok_pelajaran').val(response.data.nama_kelompok_pelajaran);
                    $('#edit_kelompok_keterangan').val(response.data.keterangan);
                    //open modal
                    $('#modal-edit').modal('show');
                }
            });
        });
        

        //action update post
        $('#saveUpdate').click(function(e) {
                e.preventDefault();
                let id = $('#id_kelompok_pelajaran').val();
                //define variable
                let edit_kelompok_pelajaran = $('#edit_kelompok_pelajaran').val();
                let edit_kelompok_keterangan = $('#edit_kelompok_keterangan').val();

                //ajax
                $.ajax({

                    url: `/admin/kelompok-pelajaran-ubah/${id}`,
                    type: "PUT",
                    cache: false,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "kelompok_pelajaran": edit_kelompok_pelajaran,
                        "kelompok_keterangan": edit_kelompok_keterangan,
                    },
                    success: function(response) {
                        //show success message
                        toastr.success(response.success, 'BERHASIL!');
                        setTimeout(() => {
                            window.location.href = response.Url
                        }, 1000);

                        //close modal
                        $('#modal-edit').modal('hide');

                    }
                });
            });
    </script>
@endpush
