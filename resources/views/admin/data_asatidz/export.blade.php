<table>
  <thead>
    <tr>
      <th>No</th>
      <th>NIAT</th>
      <th>Nama Lengkap</th>
      <th>Jenis Kelamin</th>
      <th>SLTP/Sederajat</th>
      <th>SLTA/Sederajat</th>
      <th>S1</th>
      <th>S2</th>
      <th>S3</th>
      <th>Pendidikan Terakhir</th>
      <th>No HP</th>
      <th>Alamat</th>
      <th>Tempat Lahir</th>
      <th>Tanggal Lahir</th>
    </tr>
  </thead>
  <tbody>
    @foreach($asatidz as $data)
    <tr style="text-align: left;">
      <td>{{ $loop->iteration }}</td>
      <td>{{ $data->niat }}</td>
      <td>{{ $data->nama_lengkap }}</td>
      <td>{{ $data->jenis_kelamin }}</td>
      <td>{{ $data->tsn }}</td>
      <td>{{ $data->mln }}</td>
      <td>{{ $data->s1 }}</td>
      <td>{{ $data->s2 }}</td>
      <td>{{ $data->s3 }}</td>
      <td>{{ $data->pendidikan_terakhir }}</td>
      <td>{{ $data->no_telp }}</td>
      <td>{{ $data->alamat }}</td>
      <td>{{ $data->tempat_lahir }}</td>
      <td>{{ $data->tgl_lahir }}</td>
    </tr>
    @endforeach
  </tbody>
</table>