@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Detail Asatidz</li>
@endsection

@section('title', 'Data Asatidz')

@section('content')

<style>
table th, table td{
    padding:0.75rem;
}
</style>

    <a href="{{ route('asatidz.index') }}" class="btn btn-secondary my-2">Kembali</a>

<div class="row">
<div class="col-12 col-md-6 col-lg-6">
    <div class="card">
        <div class="card-body">
                <table>
                    <th>BIODATA ASATIDZ</th>
                    <tr>
                        <td>NIAT/NPA</td>
                        <td>:</td>
                        <td>{{ $asatidz->niat }}</td>
                    </tr>
                    <tr>
                        <td>Nama Lengkap</td> 
                        <td>:</td>
                        <td>{{ $asatidz->nama_lengkap }}</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ $asatidz->jenis_kelamin == 'L' ? 'laki-laki' : 'perempuan' }}</td>
                    </tr>
                    <tr>
                        <td>Status Pernikahan</td>
                        <td>:</td>
                        <td>{{ $asatidz->status_menikah}}</td>
                    </tr>
                    <tr>
                        <td>SMP/MTS/TSANAWIYYAH</td>
                        <td>:</td>
                        <td>{{ $asatidz->tsn}}</td>
                    </tr>
                    <tr>
                        <td>SMA/SMK/MUALLIMIEN</td>
                        <td>:</td>
                        <td>{{ $asatidz->mln}}</td>
                    </tr>
                    <tr>
                        <td>Asal S1 dan Jurusan</td>
                        <td>:</td>
                        <td>{{ $asatidz->s1}}</td>
                    </tr>
                    <tr>
                        <td>Asal S2 dan Jurusan</td>
                        <td>:</td>
                        <td>{{ $asatidz->s2}}</td>
                    </tr>
                    <tr>
                        <td>Asal S3 dan Jurusan</td>
                        <td>:</td>
                        <td>{{ $asatidz->s3}}</td>
                    </tr>
                    <tr>
                        <td>Pendidikan Terakhir</td> 
                        <td>:</td> 
                        <td>{{ $asatidz->pendidikan_terakhir == 'SMA_SMK_MLN' ? 'SMA/SMK/MLN' : $asatidz->pendidikan_terakhir }}</td>
                    </tr>
                    <tr>
                        <td>No Telp</td> 
                        <td>:</td> 
                        <td>{{ $asatidz->no_telp}}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td> 
                        <td>:</td> 
                        <td>{{ $asatidz->alamat}}</td>
                    </tr>
                    <tr>
                        <td>Tempat Lahir</td> 
                        <td>:</td> 
                        <td>{{ $asatidz->tempat_lahir}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir</td> 
                        <td>:</td> 
                        <td>{{ $asatidz->tgl_lahir}}<td>
                    </tr>
                    <tr>
                        <td>Hafalan</td> 
                        <td>:</td> 
                        <td>{{ $asatidz->hafalan}}</td>
                    </tr>
                </table>
        </div>
    </div>
</div>
<div class="col-12 col-md-6 col-lg-6">
    <div class="card">
        <div class="card-body">
            <table>
                <th>KELENGKAPAN EMIS</th>
                <tr>
                    <td>Ijazah</td> 
                    <td>:</td> 
                    <td>@if(!empty($asatidz->upload_ijazah))
                                <a href="/admin/asatidz/file/download?url=data/asatidz/ijazah/{{ $asatidz->upload_ijazah     }}&filename={{ $asatidz->upload_ijazah }}" download="{{ $asatidz->upload_ijazah }}">Download</a>
                                @else
                                <span class="text-danger">belum upload ijazah</span>
                            @endif
                    </td>
                </tr>
                <tr>
                    <td>KK</td> 
                    <td>:</td> 
                    <td>@if(!empty($asatidz->upload_kk))
                                <a href="/admin/asatidz/file/download?url=data/asatidz/kartu_keluarga/{{ $asatidz->upload_kk }}&filename{{ $asatidz->upload_kk }}" download="{{ $asatidz->upload_kk }}">Download</a>
                                @else
                                <span class="text-danger">belum upload kartu keluarga</span>
                            @endif
                    </td>
                </tr>
                <tr>
                    <td>Akte</td> 
                    <td>:</td> 
                    <td>@if(!empty($asatidz->upload_akte))
                                <a href="/admin/asatidz/file/download?url=data/asatidz/akte/{{ $asatidz->upload_akte }}&filename{{ $asatidz->upload_akte }}" download="{{ $asatidz->upload_akte }}">Download</a>
                                @else
                                <span class="text-danger">belum upload akte</span>
                            @endif
                    </td>
                </tr>
                <tr>
                    <td>KTP</td> 
                    <td>:</td> 
                    <td>@if(!empty($asatidz->upload_ktp))
                                <a href="/admin/asatidz/file/download?url=data/asatidz/ktp/{{ $asatidz->upload_ktp }}&filename={{ $asatidz->upload_ktp }}" download="{{ $asatidz->upload_ktp }}">Download</a>
                                @else
                                <span class="text-danger">belum upload ktp</span>
                            @endif
                    </td>
                </tr>
                <tr>
                    <td>Buku Rekening</td> 
                    <td>:</td> 
                    <td>@if(!empty($asatidz->upload_buku_rekening))
                                <a href="/admin/asatidz/file/download?url=data/asatidz/buku_rekening/{{ $asatidz->upload_buku_rekening }}&filename={{ $asatidz->upload_buku_rekening }}" download="{{ $asatidz->upload_buku_rekening }}">Download</a>
                                @else
                                <span class="text-danger">belum upload buku rekening</span>
                            @endif
                    </td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush
