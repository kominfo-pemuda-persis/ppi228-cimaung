@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Asatidz</li>
@endsection

@section('title', 'Data Asatidz')

@section('content')
    <div class="card">
        <div class="card-header mb-2">
            <div class="card-tools">
                @permission('menu_asatidz-create')
                <a href="{{ route('asatidz.add') }}" class="btn btn-primary btn-sm">Tambah Asatidz</a>
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-default">Import</button>
                <a href="{{ route('asatidz.export') }}" class="btn btn-primary btn-sm">Export</a>
                @endrole
            </div>          
        </div>
        <div class="row" id="row-tampilan">
            <div class="col-md-4" style="display: none;">
                <label>
                    <input type="checkbox" class="tampilan" data-kolom="4" checked> Username
                </label>
            </div>
        </div>
        <div class="card-body p-3">
            @permission('menu_asatidz-create')
            <div class="row" id="row-tampilan">
                <div class="col-md-4">
                    <label>
                        <input type="checkbox" class="tampilan" data-kolom="4" checked> Username
                    </label>
                </div>
                {{-- show export --}}
                <div class="col-sm-4">
                    <label>
                        <input type="checkbox" class="tampilan" data-kolom="0" data-type="export" checked> Show Export
                    </label>
                    <div class="mb-12 col-md-12 d-none" id="btn-export-asatidz">
                        <button class="btn btn-sm btn-success btn-block">Export</button>
                    </div>
                    <form action="{{ route('asatidz-aktif.export') }}" method="post" id="form-export-asatidz">
                        @csrf
                        <input type="hidden" name="ids" id="ids">
                    </form>
                </div>
            </div>
            @endrole
            <hr class="divider">
            <div class="table-responsive">
                <table id="table-asatidz" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="cb-parent">
                            </th>
                            <th>NIAT</th>
                            <th>Nama Lengkap</th>
                            <th width="20%">Pendidikan Terakhir</th>
                            <th>Username</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                <h6 class="modal-title">Import File Asatidz</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form action="{{ route('asatidz.import') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-2">
                            <a href="{{ Storage::disk('s3')->url('static/template-asatidz.xlsx') }}" download="template-asatidz" class="btn btn-success btn-sm">Download Tempalte</a>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="custom-file">
                                    <input type="file" name="file" required class="custom-file-input @error('file') is-invalid @enderror" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile" id="fileRemark">Pilih file</label>
                                    </div>                
                                </div>
                                @error('file')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success btn-sm">Import</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@push('scripts')
<!-- bs-custom-file-input -->
<script src="{{ asset('admin_lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script>
  $(function () {
    bsCustomFileInput.init();
  });
</script>

    <script>
        $('.tampilan').prop('checked', false);

        const table = $('#table-asatidz').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "order": [
                [1, "desc"]
            ],
            "autoWidth": false,
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/asatidz-data/{jenis}",
            },
            "initComplete": function(settings, json) {
                const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
                $.each(all_checkbox_view, function(key, checkbox) {
                    let kolom = $(checkbox).data('kolom')
                    let is_checked = checkbox.checked
                    table.column(kolom).visible(is_checked)
                })
                setTimeout(function() {
                    table.columns.adjust().draw();
                }, 3000)
            },
            columnDefs: [
                {
                    targets: '_all',
                    visible: true
                },
                {
                    "targets": 0,
                    "class": "text-nowrap",
                    "sortable": false,
                    "render": function(data, type, row, meta) {
                        return `<input type="checkbox" class="cb-child" value="${row.id}">`;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.niat;
                    }
                },
                {
                    "targets": 2,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nama_lengkap;
                    }
                },
                {
                    "targets": 3,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        let pendidikan_row = row.pendidikan_terakhir === 'SMA_SMK_MLN' 
                            ? 'SMA/SMK/MLN' 
                            : (row.pendidikan_row !== null ? row.pendidikan_terakhir : 'BELUM DI ISI' );
                        return pendidikan_row;
                    }
                },
                {
                    "targets": 4,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        let textHandle = '';
                        if (row.user == undefined) {
                            textHandle = 'belum ada username';
                        }else{
                            textHandle = row.user.username;
                        }
                        return textHandle;
                    }
                },
                {
                    "targets": 5,
                    "render": function(data, type, row, meta) {
                        let tampilan = `
                        <div class="d-flex">
                            @permission('menu_asatidz-update')
                            <a href="/admin/asatidz-detail/${row.id}" class="btn btn-sm mr-2 mb-1 btn-success">Detail</a>
                            <a href="/admin/asatidz-edit/${row.id}" class="btn btn-sm mr-2 mb-1 btn-warning">Edit</a>
                            @endpermission
                            </div>
                            `
                            // @permission('menu_asatidz-delete')
                            // <a href="/admin/asatidz-delete/${row.id}" onclick="return confirm('apakah yakin ingin menghapus data ini?')" class="btn btn-sm mr-2 btn-danger">Delete</a>
                            // @endpermission
                            //{{-- <a href="{{ route('formAssignUser', $data->id) }}" class="btn btn-sm mr-2 btn-info">Assign User</a> --}}
                        return tampilan;
                    }
                },

            ]
        });

        $("#row-tampilan input[type='checkbox']").on('change',function(){
            let checkbox = $(this)
            let kolom = $(this).data('kolom')
            let type = $(this).data('type')
            let is_checked = checkbox[0].checked
            table.column(kolom).visible(is_checked)
            if (kolom == 0) {
                if (type == 'export') {
                    if (is_checked) {
                        $("#btn-export-asatidz").removeClass('d-none')
                    }else{
                        $("#btn-export-asatidz").addClass('d-none')
                    }
                } else {
                    if (is_checked) {
                        $("#btn-setlulus").removeClass('d-none')
                    }else{
                        $("#btn-setlulus").addClass('d-none')
                    }
                }
            }
        })

        // .cb-parent on change
        $(".cb-parent").on('change', function() {
            let is_checked = this.checked
            $(".cb-child").prop('checked', is_checked)
        })

        // onclick btn-export-asatidz
        $("#btn-export-asatidz button").on('click', function() {
            let ids = []
            $.each($(".cb-child"), function(key, checkbox) {
                if (checkbox.checked) {
                    ids.push(checkbox.value)
                }
            });
            if (ids.length == 0) {
                alert('Pilih Asatidz terlebih dahulu')
                return false
            }
            if (ids.length > 0) {
                $("#ids").val(ids)
                $("#form-export-asatidz").submit()
            }
        })

        function showFileName(){
            let input = document.getElementById('exampleInputFile');
            let fileName = input.files[0].name;
            document.getElementById('fileRemark').innerText = 'File : ' + fileName;
        }
    </script>
@endpush