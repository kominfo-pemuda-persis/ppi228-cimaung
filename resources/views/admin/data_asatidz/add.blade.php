@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Tambah Asatidz</li>
@endsection

@section('title', 'Data Asatidz')

@section('content')
    <div class="card p-2">
        <form action="{{ route('asatidz.store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="niat">NIAT</label>
                        <input type="text" class="form-control @error('niat') is-invalid @enderror" value="{{ old('niat') }}" required name="niat" placeholder="Masukkan No.NIAT">
                        @error('niat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_lengkap">Nama Lengkap</label>
                        <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" value="{{ old('nama_lengkap') }}" required name="nama_lengkap" placeholder="Masukkan Nama Lengkap">
                        @error('nama_lengkap')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control @error('jenis_kelamin') is-invalid @enderror" value="{{ old('jenis_kelamin') }}" required>
                            <option value="L">laki-laki</option>
                            <option value="P">perempuan</option>
                        </select>
                        @error('jenis_kelamin')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="status_menikah">Status Pernikahan</label>
                        <select name="status_menikah" id="status_menikah" class="form-control  @error('status_menikah') is-invalid @enderror" value="{{ old('status_menikah') }}"  name="status_menikah">
                            <option value="SINGLE">SINGLE</option>
                            <option value="MENIKAH">MENIKAH</option>
                            <option value="DUDA">DUDA</option>
                            <option value="JANDA">JANDA</option>
                        </select>
                        @error('status_menikah')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tsn">SMP/MTS/TSANAWIYYAH</label>
                        <input type="text" class="form-control @error('tsn') is-invalid @enderror" value="{{ old('tsn') }}" name="tsn" placeholder="Masukkan Nama Sekolah SMP/MTS/TSANAWIYYAH">
                        @error('tsn')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="mln">SMA/SMK/MUALLIEN</label>
                        <input type="text" class="form-control @error('mln') is-invalid @enderror" value="{{ old('mln') }}" name="mln" placeholder="Masukkan Nama Sekolah SMA/SMK/MUALLIMIEN">
                        @error('mln')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="s1">Asal S1 dan Jurusan</label>
                        <input type="text" class="form-control @error('s1') is-invalid @enderror" value="{{ old('s1') }}" name="s1" placeholder="Masukkan Nama Kuliah S1">
                        @error('s1')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="s2">Asal S2 dan Jurusan</label>
                        <input type="text" class="form-control @error('s2') is-invalid @enderror" value="{{ old('s2') }}" name="s2" placeholder="Masukkan Nama Kuliah S2">
                        @error('s2')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="s3">Asal S3 dan Jurusan</label>
                        <input type="text" class="form-control @error('s3') is-invalid @enderror" value="{{ old('s3') }}" name="s3" placeholder="Masukkan Nama Kuliah S3">
                        @error('s3')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                        <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control  @error('pendidikan_terakhir') is-invalid @enderror" value="{{ old('pendidikan_terakhir') }}"  name="pendidikan_terakhir">
                            <option value="SMA_SMK_MLN">SMA/SMK/MLN</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                        </select>
                        @error('pendidikan_terakhir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="no_telp">No Telp</label>
                        <input type="number" class="form-control @error('no_telp') is-invalid @enderror" value="{{ old('no_telp') }}"  name="no_telp" placeholder="Masukkan No Telp">
                        @error('no_telp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" required name="alamat" placeholder="Masukkan Alamat" cols="30" rows="10">{{ old('alamat') }}</textarea>
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" value="{{ old('tempat_lahir') }}" required name="tempat_lahir" placeholder="Masukkan Tempat Lahir">
                        @error('tempat_lahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tgl_lahir">Tanggal Lahir</label>
                        <input type="date" class="form-control @error('tgl_lahir') is-invalid @enderror" value="{{ old('tgl_lahir') }}" required name="tgl_lahir" placeholder="Masukkan Tanggal Lahir">
                        @error('tgl_lahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="hafalan">Hafalan</label>
                        <input type="text" class="form-control @error('hafalan') is-invalid @enderror" value="{{ old('hafalan') }}" required name="hafalan" placeholder="Masukkan Hafalan contoh (30 Juz)">
                        @error('hafalan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required name="email" placeholder="Email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" required name="username" placeholder="Username">
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" required name="password" placeholder="Password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password-confirm">Konfirmasi Password</label>
                        <input type="password" id="password-confirm" class="form-control" required name="password_confirmation" placeholder="Konfirmasi Password">
                    </div>
                </div>
            </div>
            <a href="{{ route('asatidz.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
@endsection
