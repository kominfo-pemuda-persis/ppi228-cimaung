@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Tambah Asatidz</li>
@endsection

@section('title', 'Data Asatidz')

@section('content')
    <div class="card p-2">
        <form action="{{ route('asatidz.update', $asatidz->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="niat">NIAT</label>
                        <input type="text" class="form-control @error('niat') is-invalid @enderror" name="niat" value="{{ old('niat') ?? $asatidz->niat }}" placeholder="NIAT">
                        @error('niat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_lengkap">Nama Lengkap</label>
                        <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{ old('nama_lengkap') ?? $asatidz->nama_lengkap }}" placeholder="Nama Lengkap">
                        @error('nama_lengkap')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control @error('jenis_kelamin') is-invalid @enderror">
                            <option value="{{ $asatidz->jenis_kelamin }}" selected>{{ $asatidz->jenis_kelamin == 'L' ? 'laki-laki' : 'perempuan' }}</option>
                            <option disabled>___________</option>
                            <option value="L" {{ old('jenis_kelamin') == 'L' ? 'selected' : '' }}>laki-laki</option>
                            <option value="P" {{ old('jenis_kelamin') == 'P' ? 'selected' : '' }}>perempuan</option>
                        </select>
                        @error('jenis_kelamin')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="status_menikah">Status Pernikahan</label>
                        <select name="status_menikah" id="status_menikah" class="form-control  @error('status_menikah') is-invalid @enderror" value="{{ old('status_menikah') ?? $asatidz->status_menikah }}">
                            <option value="SINGLE" {{ $asatidz->status_menikah == 'SINGLE' ? 'selected' : '' }} {{ old('status_menikah') == 'SINGLE' ? 'selected' : '' }}>SINGLE</option>
                            <option value="MENIKAH" {{ $asatidz->status_menikah == 'MENIKAH' ? 'selected' : '' }} {{ old('status_menikah') == 'MENIKAH' ? 'selected' : '' }}>MENIKAH</option>
                            <option value="DUDA" {{ $asatidz->status_menikah == 'DUDA' ? 'selected' : '' }} {{ old('status_menikah') == 'DUDA' ? 'selected' : '' }}>DUDA</option>
                            <option value="JANDA" {{ $asatidz->status_menikah == 'JANDA' ? 'selected' : '' }} {{ old('status_menikah') == 'JANDA' ? 'selected' : '' }}>JANDA</option>
                        </select>
                        @error('status_menikah')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tsn">SMP/MTS/TSANAWIYYAH</label>
                        <input type="text" class="form-control @error('tsn') is-invalid @enderror" name="tsn" value="{{ old('tsn') ?? $asatidz->tsn }}" placeholder="SMP/MTS/TSANAWIYYAH">
                        @error('tsn')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="mln">SMA/SMK/MUALLIMIEN</label>
                        <input type="text" class="form-control @error('mln') is-invalid @enderror" name="mln" value="{{ old('mln') ?? $asatidz->mln }}" placeholder="SMA/SMK/MUALLIMIEN">
                        @error('mln')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="s1">Asal S1 dan Jurusan</label>
                        <input type="text" class="form-control @error('s1') is-invalid @enderror" name="s1" value="{{ old('s1') ?? $asatidz->s1 }}" placeholder="Asal S1 dan Jurusan">
                        @error('s1')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="s2">Asal S2 dan Jurusan</label>
                        <input type="text" class="form-control @error('s2') is-invalid @enderror" name="s2" value="{{ old('s2') ?? $asatidz->s2 }}" placeholder="Asal S2 dan Jurusan">
                        @error('s2')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="s3">Asal S3 dan Jurusan</label>
                        <input type="text" class="form-control @error('s3') is-invalid @enderror" name="s3" value="{{ old('s3') ?? $asatidz->s3 }}" placeholder="Asal S3 dan Jurusan">
                        @error('s3')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                        <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control  @error('pendidikan_terakhir') is-invalid @enderror" value="{{ old('pendidikan_terakhir') ?? $asatidz->pendidikan_terakhir }}">
                            <option value="SMA_SMK_MLN" {{ $asatidz->pendidikan_terakhir == 'SMA_SMK_MLN' ? 'selected' : ''  }} {{ old('pendidikan_terakhir') == 'SMA_SMK_MLN' ? 'selected' : '' }}>SMA/SMK/MLN</option>
                            <option value="S1" {{ $asatidz->pendidikan_terakhir == 'S1' ? 'selected' : ''  }} {{ old('pendidikan_terakhir') == 'S1' ? 'selected' : '' }}>S1</option>
                            <option value="S2" {{ $asatidz->pendidikan_terakhir == 'S2' ? 'selected' : ''  }} {{ old('pendidikan_terakhir') == 'S2' ? 'selected' : '' }}>S2</option>
                            <option value="S3" {{ $asatidz->pendidikan_terakhir == 'S3' ? 'selected' : ''  }} {{ old('pendidikan_terakhir') == 'S3' ? 'selected' : '' }}>S3</option>
                        </select>
                        @error('pendidikan_terakhir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="no_telp">No Telp</label>
                        <input type="text" class="form-control @error('no_telp') is-invalid @enderror" name="no_telp" value="{{ old('no_telp') ?? $asatidz->no_telp }}" placeholder="No Telp">
                        @error('no_telp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ old('alamat') ?? $asatidz->alamat }}" placeholder="Alamat">
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{ old('tempat_lahir') ?? $asatidz->tempat_lahir }}" placeholder="Tempat Lahir">
                        @error('tempat_lahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tgl_lahir">Tanggal Lahir</label>
                        <input type="date" class="form-control @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" value="{{ old('tgl_lahir') ?? $asatidz->tgl_lahir }}" placeholder="Tempat Lahir">
                        @error('tgl_lahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="hafalan">Hafalan</label>
                        <input type="text" class="form-control @error('hafalan') is-invalid @enderror" name="hafalan" value="{{ old('hafalan') ?? $asatidz->hafalan }}" placeholder="Hafalan">
                        @error('hafalan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ $asatidz->user->email ?? '' }}" name="email" placeholder="Email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" value="{{ $asatidz->user->username ?? '' }}" required name="username" placeholder="Username">
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="gantiPassword">
                        <label class="form-check-label" for="gantiPassword">Ganti Password</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input disabled type="password" id="password" class="form-control @error('password') is-invalid @enderror" required name="password" placeholder="Password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password-confirm">Konfirmasi Password</label>
                        <input disabled type="password" id="password-confirm" class="form-control" required name="password_confirmation" placeholder="Konfirmasi Password">
                    </div>
                </div>
            </div>
            <a href="{{ route('asatidz.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
@endsection
@push('scripts')
<script>
    $('#gantiPassword').click(function (event) {
        // $('#password').removeAttr('disabled', false)
        if (this.checked) {
            $('#password').removeAttr('disabled')
            $('#password-confirm').removeAttr('disabled')
        } else {
            $('#password').attr('disabled', true)
            $('#password-confirm').attr('disabled', true)
        }
    })
</script>
@endpush
