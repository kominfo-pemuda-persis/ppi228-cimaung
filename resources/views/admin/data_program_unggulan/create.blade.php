@extends('layouts.dashboard')

@section('title', 'Tambah Program Unggulan')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Tambah Program Unggulan</li>
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('program-unggulan.index') }}" class="btn btn-sm btn-dark">Kembali</a>
            </div>
            <form action="{{ route('program-unggulan.store') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" required placeholder="Masukkan Nama" value="{{ old('nama') }}">
                        @error('nama')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>  
    
                    <hr>
                    <button type="submit" class="btn btn-primary w-100"><i class="fas fa-paper-plane"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection