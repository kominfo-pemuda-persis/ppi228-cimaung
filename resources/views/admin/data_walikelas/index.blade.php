@extends('layouts.dashboard')

@push('css')
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.css')}}">
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Data Wali Kelas</li>
@endsection

@section('title', 'Data Wali Kelas')

@section('content')
    @if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{$error}} <br>
        @endforeach
    </ul>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_wali_kelas-create')
                <button type="button" data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-sm">Tambah
                    Wali Kelas</button>
                @endpermission
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Kelas</label>
                    <select id="filter-kelas" class="form-control filter">
                        <option value="">Pilih Kelas</option>
                        <option value="1">Kelas 1</option>
                        <option value="2">Kelas 2</option>
                        <option value="3">Kelas 3</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Jenjang</label>
                    <select id="filter-jenjang" class="form-control filter">
                        <option value="">Pilih Jenjang</option>
                        <option value="TSN">Tsanawiyyah</option>
                        <option value="MLN">Mu'allimin</option>
                    </select>
                </div>
            </div>
            <hr class="divider">
            <div class="table-responsive">
                <table id="table-walikelas" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Wali Kelas</th>
                            <th>Kelas</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Wali Kelas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('walikelas.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="asatidz_id">Nama Asatidz</label>
                            <select name="asatidz_id" id="asatidz_id" class="form-control select2bs4">
                                @foreach ($asatidz as $data)
                                    <option value="{{ $data->id }}">{{ $data->nama_lengkap }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="kelas_id">Kelas</label>
                            <select name="kelas_id" id="kelas_id" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                            <select name="tingkat_sekolah_id" id="tingkat_sekolah_id" class="form-control">
                                <option value="TSN">TSN</option>
                                <option value="MLN">MLN</option>
                            </select>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
<script src="{{ asset('admin_lte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
        // Initialize the DataTable
        let kelas = $("#filter-kelas").val()
        let jenjang = $("#filter-jenjang").val();

        const table = $('#table-walikelas').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "order": [
                [1, "desc"]
            ],
            "autoWidth": false,
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/walikelas-data/{jenis}",
                type: "POST",
                data:function(d){
                    d.kelas = kelas;
                    d.jenjang = jenjang;
                    return d
                }
            },
            "initComplete": function(settings, json) {
                const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
                $.each(all_checkbox_view, function(key, checkbox) {
                    let kolom = $(checkbox).data('kolom')
                    let is_checked = checkbox.checked
                    table.column(kolom).visible(is_checked)
                })
                setTimeout(function() {
                    table.columns.adjust().draw();
                }, 3000)
            },
            columnDefs: [
                {
                    targets: '_all',
                    visible: true
                },
                {
                    "targets": 0,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.asatidz.nama_lengkap;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        // return row.tingkat_sekolah_id + " - " + (row.kelas_id == 'XII' ? '3' : '' || row
                        //     .kelas_id == 'XI' ? '2' : '' || row.kelas_id == 'X' ? '1' : '');
                        return row.tingkat_sekolah_id + " - " + row.kelas_id;
                    }
                },
                {
                    "targets": 2,
                    "render": function(data, type, row, meta) {
                        let tampilan = `
                        <div class="d-flex">
                            @permission('menu_wali_kelas-update')
                            <a href="/admin/walikelas-ubah/${row.id}" class="btn btn-sm btn-warning mr-2">Edit</a>
                            @endpermission
                            @permission('menu_wali_kelas-delete')
                            <a href="/admin/walikelas-delete/${row.id}" onclick="return confirm('Apakah ingin menghapus asatidz dan pelajaran ini')"  class="btn btn-sm btn-danger mr-2">Delete</a>
                            @endpermission
                            </div>
                            `
                        return tampilan;
                    }
                },

            ]
        });


        $(".filter").on('change',function(){
            kelas = $("#filter-kelas").val()
            jenjang = $("#filter-jenjang").val()
            table.ajax.reload(null,false)
        })

        $("#row-tampilan input[type='checkbox']").on('change',function(){
            let checkbox = $(this)
            let kolom = $(this).data('kolom')
            let is_checked = checkbox[0].checked
            table.column(kolom).visible(is_checked)
        })
    </script>
@endpush
