<!DOCTYPE html>
<!-- Created by pdf2htmlEX (https://github.com/pdf2htmlEX/pdf2htmlEX) -->
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta charset="utf-8"/>
      <meta name="generator" content="pdf2htmlEX"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
      <link rel="stylesheet" href="{{url('/assets/css/costum-raport-adab.css')}}">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@400;500;700&display=swap" rel="stylesheet">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
      <link rel="stylesheet" href="{{url('/assets/css/costum-raport-adab-1.css')}}">
      <title></title>
   </head>
   <body onbeforeprint="removePadding()">
      <div id="sidebar">
         <div id="outline"></div>
      </div>
      <div id="page-container">
         <div id="pf1" class="pf w0 h0">
            <div class="pc pc1 w0 h0">
               <img class="sticky-top w-100" style="z-index: 0;" alt="" src="{{url('/img/template_raport/bg.jpg')}}"/>
               <div class="c x6 y3 w4 h4">
                  <div class="t m0 x4 fw-bold h5 y4 ff1 fs2 fc0 sc0 ls0 ws0">RAHMA KHOIRUNISA</div>
               </div>
               <div class="c x6 y5 w4 h4">
                  <div class="t m0 x4 fw-bold h5 y6 ff1 fs2 fc0 sc0 ls0 ws0">1 MU’ALLIMIN</div>
               </div>
               <div class="c x6 y7 w4 h4">
                  <div class="t m0 x4 fw-bold h5 y8 ff1 fs2 fc0 sc0 ls0 ws0">1/2022-2023</div>
               </div>
               <div class="c x6 y9 w4 h6">
                  <div class="t m0 x4 fw-bold h5 ya ff1 fs2 fc0 sc0 ls0 ws0">NILAN AGUSTINA</div>
               </div>
            </div>
         </div>
         <div class="pf w0 remove-padding" style="padding: 80px 60px;">
            <div class="w-100 mx-1">
                <div class="page-first">
                    <p>Bulan : Juli - Desember 2022</p>
                    <p class="fw-bold">A. ADAB DALAM ILMU</p>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th style="width: 5%;">NO</th>
                                <th style="width: 40%;">ADAB YANG DI NILAI</th>
                                <th style="width: 5%;">NILAI</th>
                                <th style="width: 50%;">DESKRIPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td>
                                    Thalabul-‘ilmi. <br> Aspek-aspek yang dinilai: semangat belajar ilmu fardlu ‘ain dan fardlu kifayah, belajar ilmu di luar Pesantren, tidak mencontek, muraja’ah dan muthala’ah pelajaran, dan kesabaran dalam belajar.
                                </td>
                                <td class="text-center">A</td>
                                <td>
                                    Ananda Rahma dalam semangat kegiatan belajar sudah baik, hal itu terlihat dari Ananda Rahma mengikuti kegiatan belajar di Pesantren. Ananda sangat bersemangat dalam mempelajari semua mata pelajaran yg ada di pesantren. Dalam hal mencontek in syaa allah Ananda tidak pernah melakukannya. Ananda perlu meningkatkan kembali muraja'ah dan muthala'ah dalam pelajaran, agar pemahaman Ananda terhadap ilmu yg dipelajari semakin meningkat. Untuk kegiatan belajar ekstrakurikuler Alhamdulillah Ananda selalu hadir saat ekstrakurikuler petang maupun malam. Ananda Rahma cukup sabar dalam belajar dan berusaha meningkatkan kemampuan belajarnya. Mudah mudahan Ananda selalu semangat,sabar dan istiqamah dalam mencari ilmu.
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td>
                                    Adab dalam majelis ilmu <br>Aspek-aspek yang dinilai: kedisiplinan waktu, isti`dzan, menghormati Ustadz dan teman semajelis, khidmat dan tidak gaduh, dan duduk dengan adab.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam kedisiplinan waktu, dan adab kepada asatidz sudah baik. Ananda selalu berusaha menghormati semua asatidzah dan mentaati semua nasihatnya. Namun, perlu diperhatikan kembali dalam adab majelis. Ananda diketahui masih mengantuk,mengobrol, dan bermain gadget ketika asatidz sedang mengajar. Ananda juga diketahui pernah terlambat datang ke majmu'ah.  Mudah mudahan kedepannya Ananda lebih khidmat dalam kegiatan belajar dan lebih disiplin lagi.</td>
                            </tr>
                            <tr>
                                <td class="text-center">3</td>
                                <td>
                                    Adab dalam membaca Al-Qur’an <br>Aspek-aspek yang dinilai: tartil, tajwid, dan makhorijul huruf.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam membaca Al-Qur'an sudah baik. Ananda sudah bisa membaca Al-Qur'an dengan tartil, tajwid, dan makhorijul huruf. Namun, perlu diperhatikan kembali dalam tartil dan tajwid. Ananda diketahui masih kurang dalam tartil dan tajwid. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam membaca Al-Qur'an dan lebih memperhatikan tartil dan tajwid.</td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td>
                                    Adab dalam berdo’a <br>Aspek-aspek yang dinilai: khusyu’, tuma’ninah, dan menghadap kiblat.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam berdo'a sudah baik. Ananda sudah bisa berdo'a dengan khusyu', tuma'ninah, dan menghadap kiblat. Namun, perlu diperhatikan kembali dalam khusyu' dan tuma'ninah. Ananda diketahui masih kurang dalam khusyu' dan tuma'ninah. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam berdo'a dan lebih memperhatikan khusyu' dan tuma'ninah.</td>
                            </tr>
                            <tr>
                                <td class="text-center">5</td>
                                <td>
                                    Adab dalam beribadah <br>Aspek-aspek yang dinilai: khusyu’, tuma’ninah, dan menghadap kiblat.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam beribadah sudah baik. Ananda sudah bisa beribadah dengan khusyu', tuma'ninah, dan menghadap kiblat. Namun, perlu diperhatikan kembali dalam khusyu' dan tuma'ninah. Ananda diketahui masih kurang dalam khusyu' dan tuma'ninah. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam beribadah dan lebih memperhatikan khusyu' dan tuma'ninah.</td>
                            </tr>
                    </table>
                </div>
                <br>
                <div class="new-page">
                    <p class="fw-bold">B. ADAB DALAM IBADAH</p>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th style="width: 5%;">NO</th>
                                <th style="width: 40%;">ADAB YANG DI NILAI</th>
                                <th style="width: 5%;">NILAI</th>
                                <th style="width: 50%;">DESKRIPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td>
                                    Shalat <br>Aspek-aspek yang dinilai: khusyu’, tuma’ninah, dan menghadap kiblat.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam shalat sudah baik. Ananda sudah bisa shalat dengan khusyu', tuma'ninah, dan menghadap kiblat. Namun, perlu diperhatikan kembali dalam khusyu' dan tuma'ninah. Ananda diketahui masih kurang dalam khusyu' dan tuma'ninah. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam shalat dan lebih memperhatikan khusyu' dan tuma'ninah.</td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td>
                                    Puasa <br>Aspek-aspek yang dinilai: khusyu’, tuma’ninah, dan menghadap kiblat.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam puasa sudah baik. Ananda sudah bisa puasa dengan khusyu', tuma'ninah, dan menghadap kiblat. Namun, perlu diperhatikan kembali dalam khusyu' dan tuma'ninah. Ananda diketahui masih kurang dalam khusyu' dan tuma'ninah. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam puasa dan lebih memperhatikan khusyu' dan tuma'ninah.</td>
                            </tr>
                            <tr>
                                <td class="text-center">3</td>
                                <td>
                                    Shalat Dhuha <br>Aspek-aspek yang dinilai: khusyu’, tuma’ninah, dan menghadap kiblat.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam shalat dhuha sudah baik. Ananda sudah bisa shalat dhuha dengan khusyu', tuma'ninah, dan menghadap kiblat. Namun, perlu diperhatikan kembali dalam khusyu' dan tuma'ninah. Ananda diketahui masih kurang dalam khusyu' dan tuma'ninah. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam shalat dhuha dan lebih memperhatikan khusyu' dan tuma'ninah.</td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td>
                                    Shalat Tahajjud <br>Aspek-aspek yang dinilai: khusyu’, tuma’ninah, dan menghadap kiblat.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam shalat tahajjud sudah baik. Ananda sudah bisa shalat tahajjud dengan khusyu', tuma'ninah, dan menghadap kiblat. Namun, perlu diperhatikan kembali dalam khusyu' dan tuma'ninah. Ananda diketahui masih kurang dalam khusyu' dan tuma'ninah. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam shalat tahajjud dan lebih memperhatikan khusyu' dan tuma'ninah.</td>
                            </tr>
                            <tr>
                                <td class="text-center">5</td>
                                <td>
                                    Shalat Sunnah <br>Aspek-aspek yang dinilai: khusyu’, tuma’ninah, dan menghadap kiblat.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam shalat sunnah sudah baik. Ananda sudah bisa shalat sunnah dengan khusyu', tuma'ninah, dan menghadap kiblat. Namun, perlu diperhatikan kembali dalam khusyu' dan tuma'ninah. Ananda diketahui masih kurang dalam khusyu' dan tuma'ninah. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam shalat sunnah dan lebih memperhatikan khusyu' dan tuma'ninah.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <div class="new-page">
                    <p class="fw-bold">B. ADAB DALAM KESEHARIAN</p>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th style="width: 5%;">NO</th>
                                <th style="width: 40%;">ADAB YANG DI NILAI</th>
                                <th style="width: 5%;">NILAI</th>
                                <th style="width: 50%;">DESKRIPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td>
                                    Adab terhadap orang tua <br>Aspek-aspek yang dinilai: hormat, taat, dan bakti.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam adab terhadap orang tua sudah baik. Ananda sudah bisa hormat, taat, dan bakti kepada orang tua. Namun, perlu diperhatikan kembali dalam taat dan bakti. Ananda diketahui masih kurang dalam taat dan bakti. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam adab terhadap orang tua dan lebih memperhatikan taat dan bakti.</td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td>
                                    Adab terhadap guru <br>Aspek-aspek yang dinilai: hormat, taat, dan bakti.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam adab terhadap guru sudah baik. Ananda sudah bisa hormat, taat, dan bakti kepada guru. Namun, perlu diperhatikan kembali dalam taat dan bakti. Ananda diketahui masih kurang dalam taat dan bakti. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam adab terhadap guru dan lebih memperhatikan taat dan bakti.</td>
                            </tr>
                            <tr>
                                <td class="text-center">3</td>
                                <td>
                                    Adab terhadap teman <br>Aspek-aspek yang dinilai: hormat, taat, dan bakti.
                                </td>
                                <td class="text-center">A</td>
                                <td>Alhamdulillah Ananda Rahma dalam adab terhadap teman sudah baik. Ananda sudah bisa hormat, taat, dan bakti kepada teman. Namun, perlu diperhatikan kembali dalam taat dan bakti. Ananda diketahui masih kurang dalam taat dan bakti. Mudah mudahan kedepannya Ananda lebih semangat lagi dalam adab terhadap teman dan lebih memperhatikan taat dan bakti.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <div class="new-page">
                    <p class="fw-bold">D. CATATAN PELANGGARAN</p>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th style="width: 5%;">NO</th>
                                <th style="width: 45%;">ADAB YANG DI LANGGAR</th>
                                <th style="width: 50%;">DESKRIPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td>Berkhalwat</td>
                                <td>Berkenaan dengan pelanggaran ini, Pihak Pesantren sudah memanggil kedua orangtua santri dan memberikan peringatan. Mudah mudahan hal ini tidak diulangi lagi selamanya.</td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td>Menggunakan gadget</td>
                                <td>Berkenaan dengan pelanggaran ini, Pihak Pesantren sudah memanggil kedua orangtua santri dan memberikan peringatan. Mudah mudahan hal ini tidak diulangi lagi selamanya.</td>
                            </tr>
                            <!-- baris kosong -->
                            <tr style="height: 40px;">
                                <td class="text-center">3</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="row">
                        <div class="col-5 offset-7">
                            <p>
                                Badung, 20 Juli 2022 M
                                <br>
                                Muaddibah
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <b>Nilan Ayu Agustina</b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
         </div>
      </div>
      <script>
            function removePadding() {
                // remove padding top and bottom on style .remove-padding
                var element = document.getElementsByClassName("remove-padding");
                for (var i = 0; i < element.length; i++) {
                    element[i].style.paddingTop = "0";
                    element[i].style.paddingBottom = "0";
                }
            }
            // body after print
            window.onafterprint = function () {
                // add padding top and bottom on style .remove-padding
                var element = document.getElementsByClassName("remove-padding");
                for (var i = 0; i < element.length; i++) {
                    element[i].style.paddingTop = "80px";
                    element[i].style.paddingBottom = "80px";
                }
            }
      </script>
   </body>
</html>