<!DOCTYPE html>
<!-- Created by pdf2htmlEX (https://github.com/pdf2htmlEX/pdf2htmlEX) -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta charset="utf-8"/>
   <meta name="generator" content="pdf2htmlEX"/>
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
   <link rel="stylesheet" href="{{ url('/assets/css/costum-css-rapor-pendidikan.css') }}">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@400;500;700&display=swap" rel="stylesheet">
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
   <link rel="stylesheet" href="{{ url('/assets/css/costum-css-rapor-pendidikan1.css') }}">
   <title></title>
</head>
<body onbeforeprint="removePadding()" onafterprint="revertPadding()">
   <div id="page-container">
      <div id="pf1" class="pf w0 h0" data-page-no="1">
         <div class="pc pc1 w0 h0">
            <img class="bi x0 y0 w1 h1" alt="" src="{{url('/img/template_raport/bg2.jpg')}}"/>
            <div class="c x5 y5 w2 h5">
               <div style="position: absolute; top: 70%; left: 50%; transform: translate(-50%, -50%);" class="tajawal fw-bold">{{ $rapor->santri->nama_lengkap }}</div>
            </div>
            <div class="c x5 y8 w2 h5">
               <div style="position: absolute; top: 70%; left: 50%; transform: translate(-50%, -50%);" class="tajawal fw-bold">{{ $rapor->santri->nis }}/{{ $rapor->santri->nisn }}</div>
            </div>
         </div>
      </div>
      <div class="pf w0 remove-padding" style="padding: 60px 40px;">
         <div class="w-100 mx-1">
            <div class="page-first">
               <p class="text-center fw-bold">KETENTUAN UMUM</p>
               <ol>
                  <li class="mb-2">
                     Rapor Pendidikan (Bithaqah Taqrir Ta`dibiyyah) adalah rapor penilaian kegiatan pendidikan santri selama satu semester.
                  </li>
                  <li class="mb-2">
                     Kolom nilai pada Kegiatan Intrakurikuler dan Kokurikuler ditulis dalam bentuk bilangan bulat pada skala 60-100 beserta predikatnya dengan keterangan sebagai berikut:
                     <table class="table table-bordered w-50">
                        <thead>
                           <tr>
                              <th>NILAI</th>
                              <th>PREDIKAT</th>
                              <th>KETERANGAN</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>60-69</td>
                              <td>C</td>
                              <td>Kurang<i>/maqbul</i></td>
                           </tr>
                           <tr>
                              <td>70-79</td>
                              <td>B</td>
                              <td>Cukup<i>/jayyid</i></td>
                           </tr>
                           <tr>
                              <td>80-89</td>
                              <td>A</td>
                              <td>Baik<i>/jayyid jiddan</i></td>
                           </tr>
                           <tr>
                              <td>90-100</td>
                              <td>A+</td>
                              <td>Sangat Baik<i>/mumtaz</i></td>
                           </tr>
                        </tbody>
                     </table>
                  </li>
                  <li class="mb-2">
                     Kolom deskripsi pada Kegiatan Intrakurikuler dan Kokurikuler diisi dengan singkat menggunakan kalimat positif untuk capaian tertinggi dan kalimat yang memotivasi untuk capaian terendah. Kolom deskripsi juga bisa berupa penjelasan atas nilai yang diberikan.
                  </li>
                  <li class="mb-2">
                     Kolom jenis kegiatan pada Kegiatan Ekstrakurikuler diisi dengan jenis kegiatan yang diikuti oleh santri baik dalam bidang akademik ataupun non akademik, seperti: Khithabah, Mubahatsah, Lomba Tahfizh al-Qur`an tingkat Kota Bandung, Kaderisasi Anggota IPP/IPPI, Kaderisasi Anggota Pemuda/Pemudi Persatuan Islam.
                  </li>
                  <li class="mb-2">
                     Kolom keterangan pada Kegiatan Ekstrakurikuler diisi dengan peran santri dalam kegiatan tersebut atau prestasi yang diraih santri dalam kegiatan yang dimaksud.
                  </li>
                  <li class="mb-2">
                     Ketidakhadiran diisi dengan data akumulasi ketidakhadiran santri karena sakit, izin, atau tanpa keterangan selama satu semester.
                  </li>
                  <li class="mb-2">
                     Catatan Wali Kelas diisi dengan saran-saran bagi santri dan orangtua untuk diperhatikan.
                  </li>
                  <li class="mb-2">
                     Tanggapan orangtua/wali adalah komentar atas pencapaian hasil belajar santri.
                  </li>
                  <li class="mb-2">
                     Penilaian yang ada dalam Rapor Pendidikan tentunya adalah penilaian manusia yang tidak mutlak benar dan salahnya, karena hanya Allah swt Yang Mahabenar dalam penilaiannya. Meski demikian, penilaian ini tetap diperlukan untuk menjadi standar acuan pendidikan bagi santri, orangtua santri, dan para asatidzah di Pesantren. Maka dari itu, jika sekiranya ditemukan nilai yang dirasa kurang berkenan, diharapkan melakukan tabayyun (konfirmasi) dan memperbaikinya dengan cara musyawarah sebagaimana diajarkan syari’at Islam.
                  </li>
               </ol>
            </div>
            <hr class="hide-print">
            <div class="new-page">
               <p class="text-center fw-bold mb-3">IDENTAS SANTRI</p>
               <div class="box-tabel px-1">
                  <div class="row mb-3">
                     <div class="col-4">
                        Nama Lengkap
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->nama_lengkap }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Tempat, Tanggal Lahir
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->tempat_lahir }}, {{ $rapor->santri->tanggal_lahir }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Jenis Kelamin
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->jk == 'RG' ? 'Laki-laki':'Perempuan' }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Nomor Induk Santri
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->nis }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Alamat Rumah
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->alamat }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Nomor HP/WhatsApp
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->no_hp }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Email
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->email }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Asal Sekolah
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->asal_sekolah }}
                     </div>
                  </div>
                  <br>
                  <div class="row mb-3">
                     <div class="col-4">
                        Nama Lengkap Ayah
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->nama_lengkap_ayah }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Pekerjaan
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->pekerjaan_ayah }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Name Lengkap Ibu
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->nama_lengkap_ibu }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Pekerjaan
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->pekerjaan_ibu }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Nama Lengkap Wali
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->nama_lengkap_wali }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Pekerjaan
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->pekerjaan_wali }}
                     </div>
                  </div>
                  <div class="row mb-3">
                     <div class="col-4">
                        Nomor HP/WhatsApp
                     </div>
                     <div class="col-8">
                        <span class="me-3">:</span>{{ $rapor->santri->no_hp_ayah }}
                     </div>
                  </div>
               </div>
               <br>
               <br>
               <div class="d-flex justify-content-between">
                  <div class="box border border-1 d-flex align-items-center justify-content-center border-dark pas-foto">
                     <div class="text-center">
                        <p class="fw-bold">Pas Foto</p>
                        <p class="fw-bold">3x4</p>
                     </div>
                  </div>
                  <div class="box">
                     Bandung, 12 Januari 2000
                     <br>
                     Mudir Tsanawiyah
                     <br>
                     <br>
                     <br>
                     <br>
                     <br>
                     <br>
                     {{ $mudirTSN->mudirTSN->nama_lengkap }} <br>
                     NPA: {{ $mudirTSN->mudirTSN->niat }}
                  </div>
               </div>
            </div>
            <hr class="hide-print">
            <div class="new-page">
               <table class="table table-borderless">
                  <tr>
                     <td style="width: 22%">
                        Nama Santri 
                     </td>
                     <td style="width: 30%">
                        : {{ $rapor->santri->nama_lengkap }}
                     </td >
                     <td style="width: 22%">
                        Semester 
                     </td>
                     <td style="width: 26%">
                        : {{ $rapor->semester->nama == 'Ganjil' ? '1':'2' }}
                     </td>
                  </tr>
                  <tr>
                     <td style="width: 22%">
                        NIS/NISN 
                     </td>
                     <td style="width: 30%">
                        : {{ $rapor->santri->nis }}
                     </td>
                     <td style="width: 22%">
                        Tahun  
                     </td>
                     <td style="width: 26%">
                        : {{ $rapor->semester->start_year }}/{{ $rapor->semester->end_year }}
                     </td>
                  </tr>
                  <tr>
                     <td style="width: 22%">
                        Kelas 
                     </td>
                     <td style="width: 30%">
                        : Tsanawiyyah
                     </td>
                     <td style="width: 22%">
                     </td>
                     <td style="width: 26%">
                     </td>
                  </tr>
               </table>
               <b>A.KEGIATAN INTRAKURIKULER</b>
               <table class="table table-bordered">
                  <thead class="text-center">
                     <tr>
                        <th style="width: 5%">
                           No
                        </th>
                        <th style="width: 29%">
                           Mata Pelajaran
                        </th>
                        <th style="width: 8%">
                           Nilai
                        </th>
                        <th style="width: 8%">
                           Predikat
                        </th>
                        <th style="width: 50%">
                           Deskripsi
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($dataRapor as $data)
                     <tr>
                        <th colspan="5" class="text-start">
                           {{ $data['kelompok'] }}
                        </th>
                     </tr>
                        @foreach($data['nilai'] as $nl)
                           <tr class="text-start">
                              <td style="width: 5%">
                                 {{ $loop->iteration }}
                              </td>
                              <td style="width: 29%">
                                 {{ $nl['pelajaran'] }}
                              </td>
                              <td style="width: 8%">
                                 {{ $nl['nilai'] }}
                              </td>
                              <td style="width: 8%">
                                 {{ $nl['predikat'] }}
                              </td>
                              <td style="width: 50%">
                                 {{ $nl['deskripsi'] }}
                              </td>
                           </tr>
                        @endforeach
                     @endforeach
                  </tbody>
               </table>
            </div>
            <hr class="hide-print">
            <div class="new-page">
               <b>B.PROGRAM UNGGULAN</b>
               <table class="table table-bordered">
                  <thead class="text-center">
                     <tr>
                        <th style="width: 5%">
                           No
                        </th>
                        <th style="width: 25%">
                           Mata Pelajaran
                        </th>
                        <th style="width: 8%">
                           Nilai
                        </th>
                        <th style="width: 8%">
                           Predikat
                        </th>
                        <th style="width: 9%">
                           Jumlah Hafalan
                        </th>
                        <th style="width: 45%">
                           Deskripsi
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($rapor->programUnggulan as $program)
                     <tr class="text-start">
                        <td style="width: 5%">
                           {{ $loop->iteration }}
                        </td>
                        <td style="width: 25%">
                           {{ $program->programUnggulan->nama }}
                        </td>
                        <td style="width: 8%">
                           {{ $program->nilai }}
                        </td>
                        <td style="width: 8%">
                           {{ $program->predikat }}
                        </td>
                        <td style="width: 9%">
                           {{ $program->jumlah_hafalan }}
                        </td>
                        <td style="width: 45%">
                           {{ $program->deskripsi }}
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
               <p>
                  <small>Keterangan: Jumlah seharusnya hafalan al-Qur’an 20  juz; Bulughul-Maram 64 hadits; Riyadlus-Shalihin 73 hadits; Shahih al-Bukhari 56 hadits.</small>
               </p>
            </div>
            <div class="new-page">
               <b>C.KEGIATAN KOKURIKULER</b>
               <table class="table table-bordered">
                  <thead class="text-center">
                     <tr>
                        <th style="width: 5%">
                           No
                        </th>
                        <th style="width: 29%">
                           Mata Pelajaran
                        </th>
                        <th style="width: 8%">
                           Nilai
                        </th>
                        <th style="width: 8%">
                           Predikat
                        </th>
                        <th style="width: 50%">
                           Deskripsi
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                  @foreach($rapor->kokurikulers as $ko)
                  <tr class="text-start">
                     <td style="width: 5%">
                        {{ $loop->iteration }}
                     </td>
                     <td style="width: 29%">
                        {{ $ko->kokurikuler->nama }}
                     </td>
                     <td style="width: 8%">
                        {{ $ko->nilai }}
                     </td>
                     <td style="width: 8%">
                        {{ $ko->predikat }}
                     </td>
                     <td style="width: 50%">
                        {{ $ko->deskripsi }}
                     </td>
                  </tr>
                  @endforeach
                  </tbody>
               </table>
               <br>
               <table class="table table-bordered">
                  <tr>
                     <th>
                        Jumlah Rata-rata
                     </th>
                     <th>
                        -
                     </th>
                  </tr>
                  <tr>
                     <th>
                        Nilai Rata-rata Kelas
                     </th>
                     <th>
                        -
                     </th>
                  </tr>
               </table>
               <br>
            </div>
            <div class="new-page">
               <b>D.KEGIATAN EKSTRAKURIKULER</b>
               <table class="table table-bordered">
                  <thead class="text-center">
                     <tr>
                        <th style="width: 5%">
                           No
                        </th>
                        <th style="width: 35%">
                           kegiatan
                        </th>
                        <th style="width: 60%">
                           Deskripsi
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                  @foreach($rapor->ekstrakurikulerRaporPendidikan as $eks)
                  <tr class="text-start">
                     <td style="width: 5%">
                        {{ $loop->iteration }}
                     </td>
                     <td style="width: 35%">
                        {{ $eks->ekstra->nama }}
                     </td>
                     <td style="width: 60%">
                        {{ $eks->deskripsi }}
                     </td>
                  </tr>
                  @endforeach
                  </tbody>
               </table>
               <br>
            </div>
            <div class="new-page">
               <b>E.KETIDAKHADIRAN</b>
               <table class="table w-50 table-bordered">
                  <tr>
                     <td>Sakit</td>
                     <td>{{ $rapor->sakit }} Hari</td>
                  </tr>
                  <tr>
                     <td>Izin</td>
                     <td>{{ $rapor->izin }} Hari</td>
                  </tr>
                  <tr>
                     <td>Tanpa Keterangan</td>
                     <td>{{ $rapor->alpa }} Hari</td>
                  </tr>
               </table>
            </div>
            <div class="new-page">
               <b>F.CATATAN WALI KELAS</b>
               <table class="table table-bordered">
                  <tr style="height: 100px">
                     <td>
                        {{ $rapor->catatan_walikelas }}
                     </td>
                  </tr>
               </table>
               <br>
               <b>G.TANGGAPAN ORANGTUA/WALI</b>
               <table class="table table-bordered">
                  <tr style="height: 200px">
                     <td>
                        {{ $rapor->tanggapan_orangtua }}
                     </td>
                  </tr>
               </table>
               <div class="row">
                  <div class="col-4 offset-8">
                     <p>
                        Bandung, 18 Juni 2022 M
                        <br>
                        Wali Kelas
                     </p>
                     <br><br>
                     <p class="mb-1">
                        <b><u>{{ $rapor->walikelas->nama_lengkap }}</u></b>
                     </p>
                     <p class="mt-0">NPA.: {{ $rapor->walikelas->niat }}</b>
                        <br><br><br>
                        <p>Bandung, {{ $rapor->tanggal_tanggapan_orangtua }}</p>
                        <p>
                           Orang Tua/Wali
                        </p>
                        <br>
                        <br>
                        <br>
                        <p>
                           <p>{{ $rapor->santri->nama_lengkap_ayah }}</p>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script>
         function removePadding() {
            // remove padding top and bottom on style .remove-padding
            var element = document.getElementsByClassName("remove-padding");
            for (var i = 0; i < element.length; i++) {
               element[i].style.paddingTop = "0";
               element[i].style.paddingBottom = "0";
               element[i].style.paddingLeft = "80px";
               element[i].style.paddingRight = "80px";
            }
         }
         // body after print
         function revertPadding() {
            // add padding top and bottom on style .remove-padding
            var element = document.getElementsByClassName("remove-padding");
            for (var i = 0; i < element.length; i++) {
               element[i].style.paddingTop = "60px";
               element[i].style.paddingBottom = "60px";
               element[i].style.paddingLeft = "40px";
               element[i].style.paddingRight = "40px";
            }
         }
      </script>
   </body>
   </html>