@extends('layouts.dashboard')

@section('title', 'Rapor Tsanawiyyah')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Rapor Tsanawiyyah</li>
@endsection

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif

    <!-- PILIH RAPOR -->
    <div class="card card-outline card-success shadow">
      <div class="card-header" data-card-widget="collapse">
        <i class="fas fa-angle-down"></i> &nbsp;
        <h5 class="d-inline"><strong>PILIH SANTRI DAN SEMESTER</strong></h5>
      </div>

      <div class="card-body">
            <form action="{{  route('lihatRaporTSN') }}" method="post">
              @csrf
            <div class="row">

              <div class="col-md-6">
                <div class="form-group">
                    <label for="santri">Pilih Santri</label>
                    <select name="santri" id="santri" required class="form-control select2 select2bs4">
                      @foreach($dataSantri as $santri)
                      <option value="{{ $santri->id }}" {{ request('santri') == $santri->id ? 'selected':'' }}>{{ $santri->nama_lengkap }}</option>
                      @endforeach
                    </select>
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                    <label for="kelas">PIlih Kelas</label>
                    <select name="kelas" id="kelas" required class="form-control">
                        <option value="1" {{ request('kelas') == '1' ? 'selected':'' }}>1</option>
                        <option value="2" {{ request('kelas') == '2' ? 'selected':'' }}>2</option>
                        <option value="2" {{ request('kelas') == '3' ? 'selected':'' }}>3</option>
                    </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                    <label for="semester">Pilih Semester</label>
                    <select name="semester" id="semester" required class="form-control">
                      @foreach($dataSemester as $semester)
                      <option value="{{ $semester->id }}" {{ request('semester') == $semester->id ? 'selected':'' }}>{{ $semester->nama }} | {{ $semester->start_year }} - {{ $semester->end_year }}</option>
                      @endforeach
                    </select>
                </div>
              </div>

            </div>

      </div>
      <div class="card-footer text-right">
        <button type="submit" class="btn btn-success">Lihat Rapor</button>
      </form>
      </div>
    </div>

    @if($rapor)

      <!-- DATA SANTRI DAN RAPOR -->
      <div class="card card-outline card-success shadow">
        <div class="card-header" data-card-widget="collapse">
          <i class="fas fa-angle-down"></i> &nbsp;
          <h5 class="d-inline"><strong>DATA SANTRI DAN RAPOR SEMESTER</strong></h5>
        </div>

        <div class="card-body">
          <table class="table table-sm borderless">
            <tr>
              <td>Nama Santri</td>
              <th>: {{ $raporPendidikan->santri->nama_lengkap }}</th>
              <td>&nbsp;</td>
              <td>Semester</td>
              @php
                $semester = 2 * $raporPendidikan->kelas;
                if($raporPendidikan->semester->nama == 'Ganjil') {
                  $semester--;
                }
              @endphp
              <td>: {{ $semester }}</td>
            </tr>
            <tr>
              <td>NIS/NISN</td>
              <td>: {{ $raporPendidikan->santri->nis }}/{{ $raporPendidikan->santri->nisn }}</td>
              <td>&nbsp;</td>
              <td>Tahun</td>
              <td>: {{ $raporPendidikan->semester->start_year }} - {{ $raporPendidikan->semester->end_year}}</td>
            </tr>
            <tr>
              <td>Kelas</td>
              <td>: {{ $raporPendidikan->santri->kelas }} Tsanawiyyah</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </div>
      </div>

      <!-- A. KEGIATAN INTRAKURIKULER -->
      <div class="card card-outline card-success shadow">
        <div class="card-header" data-card-widget="collapse">
          <i class="fas fa-angle-down"></i> &nbsp;
          <h5 class="d-inline"><strong>A. KEGIATAN INTRAKURIKULER</strong></h5>
        </div>

        <div class="card-body">
          <table class="table table-striped text-center table-bordered">
            <thead>
              <tr>
                <th>NO.</th>
                <th>MATA PELAJARAN</th>
                <th>NILAI</th>
                <th>PREDIKAT</th>
                <th>DESKRIPSI</th>
              </tr>
            </thead>
            <tbody>
              @foreach($dataRapor as $rapor)
                <tr>
                  <th colspan="5" class="text-left">{{ $rapor['kelompok'] }}</th>
                </tr>
                  @foreach($rapor['nilai'] as $nl)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>
                        {{ $nl['pelajaran'] }}
                        <br>
                        <div class="badge badge-success">{{ $nl['asatidz'] }}</div>
                      </td>
                      <td>{{ $nl['nilai'] }}</td>
                      <td>{{ $nl['predikat'] }}</td>
                      <td>{{ $nl['deskripsi'] }}</td>
                  @endforeach
              @endforeach
            </tbody>
          </table>
        </div>
      </div>  

      <!-- B. PROGRAM UNGGULAN -->
      <div class="card card-outline card-success shadow">
        <div class="card-header" data-card-widget="collapse">
          <i class="fas fa-angle-down"></i> &nbsp;
          <h5 class="d-inline"><strong>B. PROGRAM UNGGULAN</strong></h5>
        </div>

        <div class="card-body">
          <form action="{{ route('simpanNilaiProgramUnggulan') }}" method="post">
            @csrf
            <table class="table table-striped text-center table-bordered">
              <thead>
                <tr>
                  <th>NO.</th>
                  <th>MATA PELAJARAN</th>
                  <th>NILAI</th>
                  <th>PREDIKAT</th>
                  <th>JUMLAH HAFALAN</th>
                  <th>DESKRIPSI</th>
                </tr>
              </thead>
              <tbody>
                @foreach($raporPendidikan->programUnggulan as $program)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $program->programUnggulan->nama }}</td>
                    <td>
                        <input type="hidden" name="id[]" required class="form-control" value="{{ $program->id }}">
                        <input type="number" name="nilai[]" required class="form-control" value="{{ $program->nilai }}" min="0" max="100">
                    </td>
                    <td>{{ $program->predikat }}</td>
                    <td>
                        <input type="text" name="jumlah_hafalan[]" required class="form-control" value="{{ $program->jumlah_hafalan }}" maxlength="200">
                    </td>
                    <td>
                      <textarea name="deskripsi[]" required class="form-control" placeholder="Masukkan Deskripsi" maxlength="2000">{{ $program->deskripsi }}</textarea>
                    </td>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-success">Simpan Nilai Program Unggulan</button>
          </form>
          </div>
      </div>

      <!-- C. KEGIATAN KOKURIKULER -->
      <div class="card card-outline card-success shadow">
        <div class="card-header" data-card-widget="collapse">
          <i class="fas fa-angle-down"></i> &nbsp;
          <h5 class="d-inline"><strong>C. KEGIATAN KOKURIKULER</strong></h5>
        </div>

        <div class="card-body">
          <form action="{{ route('simpanNilaiKokurikuler') }}" method="post">
            @csrf
            <table class="table table-striped text-center table-bordered">
              <thead>
                <tr>
                  <th>NO.</th>
                  <th>MATA PELAJARAN</th>
                  <th>NILAI</th>
                  <th>PREDIKAT</th>
                  <th>DESKRIPSI</th>
                </tr>
              </thead>
              <tbody>
                @foreach($raporPendidikan->kokurikulers as $ko)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $ko->kokurikuler->nama }}</td>
                    <td>
                        <input type="hidden" name="id[]" required class="form-control" value="{{ $ko->id }}">
                        <input type="number" name="nilai[]" required class="form-control" value="{{ $ko->nilai }}" min="0" max="100">
                    </td>
                    <td>{{ $ko->predikat }}</td>
                    <td>
                      <textarea name="deskripsi[]" required class="form-control" placeholder="Masukkan Deskripsi" maxlength="2000">{{ $ko->deskripsi }}</textarea>
                    </td>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-success">Simpan Nilai Kokurikuler</button>
          </form>
          </div>
      </div>

      <!-- D.	KEGIATAN EKSTRAKURIKULER -->
      <div class="card card-outline card-success shadow">
        <div class="card-header" data-card-widget="collapse">
          <i class="fas fa-angle-down"></i> &nbsp;
          <h5 class="d-inline"><strong>D.	KEGIATAN EKSTRAKURIKULER</strong></h5>
        </div>

        <div class="card-body">
          <form action="{{ route('simpanDeskripsiEkstrakurukuler') }}" method="post">
            @csrf
            <table class="table table-striped text-center table-bordered">
              <thead>
                <tr>
                  <th>NO.</th>
                  <th>KEGIATAN</th>
                  <th>DESKRIPSI</th>
                </tr>
              </thead>
              <tbody>
                @foreach($raporPendidikan->ekstrakurikulerRaporPendidikan as $eks)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $eks->ekstra->nama }}</td>
                    <td>
                      <input type="hidden" name="id[]" required class="form-control" value="{{ $eks->id }}">
                      <textarea name="deskripsi[]" required class="form-control" placeholder="Masukkan Deskripsi" maxlength="2000">{{ $eks->deskripsi }}</textarea>
                    </td>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-success">Simpan Deskripsi Ekstrakulikuler</button>
          </form>
          </div>
      </div>

      <!-- E.	KETIDAKHADIRAN -->
      <div class="row">
        <div class="col-md-6">
          
          <div class="card card-outline card-success shadow">
            <div class="card-header" data-card-widget="collapse">
              <i class="fas fa-angle-down"></i> &nbsp;
              <h5 class="d-inline"><strong>E.	KETIDAKHADIRAN</strong></h5>
            </div>
    
            <div class="card-body">
              <form action="{{ route('simpanKetidakhadiran', $raporPendidikan->id) }}" method="post">
                @method('put')
                @csrf
                <table class="table table-striped text-center table-bordered">
                  <tbody>
                    <tr>
                      <td>Sakit</td>
                      <td>
                        <input type="number" name="sakit" required class="form-control" value="{{ $raporPendidikan->sakit }}" min="0" max="100">
                      </td>
                      <td>hari</td>
                    </tr>
                    <tr>
                      <td>Izin</td>
                      <td>
                        <input type="number" name="izin" required class="form-control" value="{{ $raporPendidikan->izin }}" min="0" max="100">
                      </td>
                      <td>hari</td>
                    </tr>
                    <tr>
                      <td>Tanpa Keterangan</td>
                      <td>
                        <input type="number" name="alpa" required class="form-control" value="{{ $raporPendidikan->alpa }}" min="0" max="100">
                      </td>
                      <td>hari</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="card-footer text-right">
                <button type="submit" class="btn btn-success">Simpan Ketidakharidan</button>
              </form>
              </div>
          </div>

        </div>
      </div>

      <!-- F.	CATATAN WALI KELAS -->
      <div class="card card-outline card-success shadow">
        <div class="card-header" data-card-widget="collapse">
          <i class="fas fa-angle-down"></i> &nbsp;
          <h5 class="d-inline"><strong>F.	CATATAN WALI KELAS</strong></h5>
        </div>

        <div class="card-body">
          <form action="{{ route('simpanCatatanWalikelas', $raporPendidikan->id) }}" method="post">
            @method('put')
            @csrf
            <textarea name="catatanWalikelas" rows="10" required class="form-control @error('catatanWalikelas') is-invalid @enderror" placeholder="Masukkan Catatan Walikelas" maxlength="2000">{{ $raporPendidikan->catatan_walikelas }}</textarea>
            @error('catatanWalikelas')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-success">Simpan Catatan Walikelas</button>
          </form>
          </div>
      </div>

      <!-- G.	TANGGAPAN ORANGTUA/WALI -->
      <div class="card card-outline card-success shadow">
        <div class="card-header" data-card-widget="collapse">
          <i class="fas fa-angle-down"></i> &nbsp;
          <h5 class="d-inline"><strong>G.	TANGGAPAN ORANGTUA/WALI</strong></h5>
        </div>

        <div class="card-body">
          <form action="{{ route('simpanTanggapanOrangtua', $raporPendidikan->id) }}" method="post">
            @method('put')
            @csrf
            <textarea name="tanggapanOrangtua" rows="10" required class="form-control @error('tanggapanOrangtua') is-invalid @enderror" placeholder="Masukkan Tanggapan Orangtua/Wali" maxlength="2000">{{ $raporPendidikan->tanggapan_orangtua }}</textarea>
            @error('tanggapanOrangtua')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-success">Simpan Tanggapan Orangtua/Wali</button>
          </form>
          </div>
      </div>

      <div class="row">
        <div class="col-md-4 offset-8">
          <div class="card card-outline card-success shadow">
            <div class="card-body">
                <!-- WALI KELAS -->
                <p>Bandung, 22 Desember 2023 M</p>
                <p>Wali Kelas</p>
                <br><br><br>
                <p><strong>{{ $raporPendidikan->walikelas->nama_lengkap }}</strong></p>

                <br><br><br><br>
                <!-- ORANGTUA/WALI -->
                <p>Bandung, 25 Desember 2023 M</p>
                <p>Orangtua/Wali</p>
                <br><br><br>
                <p><strong>{{ $raporPendidikan->santri->nama_lengkap_ayah }}</strong></p>

            </div>
          </div>

          <a href="{{ route('printRaporPendidikanTSN', $raporPendidikan->id) }}" class="btn btn-primary w-100" title="Print Rapor Pendidikan" target="_blank"><i class="fas fa-print"></i> Print</a>
        </div>
      </div>

      <br><br>

    @endif

@endsection

@push('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

<style>
  .borderless td, .borderless th {
      border: none;
  }

</style>
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })

    $(".card-header").on("click", function () {
      $(this).children("i").toggleClass('fa-angle-right')
   });
</script>
@endpush