@extends('layouts.dashboard')

@section('title', 'Edit Rapor Tsanawiyyah')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Edit Rapor Tsanawiyyah</li>
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('raportsn.index') }}" class="btn btn-sm btn-dark">Kembali</a>
            </div>
            <form action="{{ route('raportsn.update', $rapor->id) }}" method="post">
                @method('put')
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="santri">PIlih Santri</label>
                        <select name="santri" id="santri" required class="form-control @error('santri') is-invalid @enderror">
                            <option value="">Pilih Santri</option>
                            @foreach($dataSantri as $santri)
                            <option value="{{ $santri->id }}" {{ $santri->id == $rapor->santri_id ? 'selected':'' }}>{{ $santri->nama_lengkap }}</option>
                            @endforeach
                        </select>
                        @error('santri')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="semester">PIlih Semester</label>
                        <select name="semester" id="semester" required class="form-control @error('semester') is-invalid @enderror">
                            <option value="">Pilih semester</option>
                            @foreach($dataSemester as $semester)
                            <option value="{{ $semester->id }}" {{ $semester->id == $rapor->semester_id ? 'selected':'' }}>{{ $semester->nama }} | {{ $semester->tahun }} - {{ $semester->tahun + 1 }}</option>
                            @endforeach
                        </select>
                        @error('semester')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="kelas">PIlih Kelas</label>
                        <select name="kelas" id="kelas" required class="form-control @error('kelas') is-invalid @enderror">
                            <option value="">Pilih kelas</option>
                            <option value="1" {{ $rapor->kelas == '1' ? 'selected':'' }}>1</option>
                            <option value="2" {{ $rapor->kelas == '2' ? 'selected':'' }}>2</option>
                            <option value="3" {{ $rapor->kelas == '3' ? 'selected':'' }}>3</option>
                        </select>
                        @error('kelas')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <hr>
                    <button type="submit" class="btn btn-warning w-100"><i class="fas fa-paper-plane"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection