@extends('layouts.dashboard')

@section('breadcrumb')

@section('title', 'Dashboard')

@section('content')
<!-- Main content -->
<section class="content">

    <div class="container-fluid">

        @if(Auth::user()->hasRole('admin') == 1 || Auth::user()->hasRole('mudir_am') == 2 || Auth::user()->hasRole('mudir_tsn') == 4)
        <!-- ===== -->
        <!-- dashboard TSN -->
        <!-- ===== -->
        
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-secondary">
                    <div class="inner">
                        <h3>{{ $asatidz_tsn }}</h3>

                        <p>Asatidz TSN</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-child"></i>
                    </div>
                    <a href="{{ route('asatidz-pelajaran.index') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-secondary">
                    <div class="inner">
                        <h3>{{ $data['santri_tsn'] }}</h3>

                        <p>Santri TSN</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-chalkboard-teacher"></i>
                    </div>
                    <a href="{{ route('santri.index') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-secondary">
                    <div class="inner">
                        <h3>{{ $pelajaran_tsn }}</h3>
                        <p>Pelajaran TSN</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-quran"></i>
                    </div>
                    <a href="{{ route('asatidz-pelajaran.index') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-secondary">
                    <div class="inner">
                        <h3>4</h3>

                        <p>Kelas TSN</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-table"></i>
                    </div>
                    <a href="#pembelajaran" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-secondary">
                    <div class="inner">
                        <h3>{{ $data['total_tsn_sakan_rg'] ?? 0 }}</h3>

                        <p>Total Santri Sakan TSN (RG)</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-table"></i>
                    </div>
                    <a href="#pembelajaran" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-secondary">
                    <div class="inner">
                        <h3>{{ $data['total_tsn_sakan_ug'] ?? 0 }}</h3>

                        <p>Total Santri Sakan TSN (UG)</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-table"></i>
                    </div>
                    <a href="#pembelajaran" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-warning">
                    <div class="inner text-center">
                        <h3>{{ $asatidzh }}</h3>
                        <p>Total Asatidz/ah</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-child"></i>
                    </div>
                    <a href="{{ route('asatidz.index') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-warning">
                    <div class="inner text-center">
                        <h3>{{ $santri }}</h3>
                        <p>Total Santri</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-child"></i>
                    </div>
                    <a href="{{ route('asatidz.index') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        @endif
        @if(Auth::user()->hasRole('admin') == 1 || Auth::user()->hasRole('mudir_am') == 2 || Auth::user()->hasRole('mudir_mln') == 3)
        <!-- ===== -->
        <!-- dashboard MLN -->
        <!-- ===== -->
        <div class="row">

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-success">
                    <div class="inner">
                        <h3>{{ $asatidz_mln }}</h3>

                        <p>Asatidz MLN</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-child"></i>
                    </div>
                    <a href="{{ route('asatidz-pelajaran.index') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-success">
                    <div class="inner">
                        <h3>{{ $data['santri_mln'] }}</h3>

                        <p>Santri MLN</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-chalkboard-teacher"></i>
                    </div>
                    <a href="{{ route('santri.index') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-success">
                    <div class="inner">
                        <h3>{{ $pelajaran_mln }}</h3>

                        <p>Pelajaran MLN</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-quran"></i>
                    </div>
                    <a href="{{ route('asatidz-pelajaran.index') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-success">
                    <div class="inner">
                        <h3>4</h3>
                        <p>Kelas MLN</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-table"></i>
                    </div>
                    <a href="#pembelajaran" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-success">
                    <div class="inner">
                        <h3>{{ $data['total_mln_sakan_rg'] ?? 0 }}</h3>

                        <p>Total Santri Sakan MLN (RG)</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-table"></i>
                    </div>
                    <a href="#pembelajaran" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-success">
                    <div class="inner">
                        <h3>{{ $data['total_mln_sakan_ug'] ?? 0 }}</h3>

                        <p>Total Santri Sakan MLN (UG)</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-table"></i>
                    </div>
                    <a href="#pembelajaran" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        @endif

        @if(Auth::user()->hasRole('admin') == 1 || Auth::user()->hasRole('mudir_am') == 2 || Auth::user()->hasRole('mudir_tsn') == 4)
        <!-- ===== -->
        <!-- Jenis kelamin santri TSN-->
        <!-- ===== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Jumlah Santri TSN</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Tabel Santri</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>Laki-laki</td>
                                        <td class="text-center">{{ $data['santri_tsn_lk'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Perempuan</td>
                                        <td class="text-center">{{ $data['santri_tsn_pr'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Santri</td>
                                        <td class="text-center">{{ $data['santri_tsn'] }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Grafik Santri</p>
                                <canvas id="donutChartTSN"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Jumlah Asatidz TSN</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Tabel Asatidz</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>Laki-laki</td>
                                        <td class="text-center">{{ $asatidz_jk_tsn_L }}</td>
                                    </tr>
                                    <tr>
                                        <td>Perempuan</td>
                                        <td class="text-center">{{ $asatidz_jk_tsn_P }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Asatidz</td>
                                        <td class="text-center">{{ ($asatidz_jk_tsn_L+$asatidz_jk_tsn_P) }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Grafik Asatidz</p>
                                <canvas id="pieChartTSN"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        @endif
        
        @if(Auth::user()->hasRole('admin') == 1 || Auth::user()->hasRole('mudir_am') == 2 || Auth::user()->hasRole('mudir_mln') == 3)
        <!-- ===== -->
        <!-- Jenis kelamin santri MLN -->
        <!-- ===== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold"> Jumlah Santri MLN</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Tabel Santri</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>Laki-laki</td>
                                        <td class="text-center">{{ $data['santri_mln_lk'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Perempuan</td>
                                        <td class="text-center">{{ $data['santri_mln_pr'] }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Santri</td>
                                        <td class="text-center">{{ $data['santri_mln'] }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Grafik Santri</p>
                                <canvas id="donutChartMLN"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>


                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Jumlah Asatidz MLN</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Tabel Asatidz</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>Laki-laki</td>
                                        <td class="text-center">{{ $asatidz_jk_mln_L }}</td>
                                    </tr>
                                    <tr>
                                        <td>Perempuan</td>
                                        <td class="text-center">{{ $asatidz_jk_mln_P }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Asatidz</td>
                                        <td class="text-center">{{ ($asatidz_jk_mln_L+$asatidz_jk_mln_P) }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Grafik Asatidz</p>
                                <canvas id="pieChartMLN"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        @endif

        @if(Auth::user()->hasRole('admin') == 1 || Auth::user()->hasRole('mudir_am') == 2 || Auth::user()->hasRole('mudir_mln') == 3)
        <!-- ===== -->
        <!-- Jenis kelamin santri MLN -->
        <!-- ===== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Grafik Status Merital Asatidzah</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <br>
                                <p class="text-center font-weight-bold">Tabel Status Merital Asatidzah</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>TIDAK DIKETAHUI</td>
                                        <td class="text-center">{{ $jumlahTidakDiketahui_MA }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>SINGLE</td>
                                        <td class="text-center"> {{ $jumlahSINGLE }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MENIKAH</td>
                                        <td class="text-center">{{ $jumlahMENIKAH }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>DUDA</td>
                                        <td class="text-center">{{ $jumlahDUDA }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>JANDA</td>
                                        <td class="text-center">{{ $jumlahJANDA }}
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <figure class="highcharts-figure">
                                    <div id="container-status-merital"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Grafik Pendidikan Asatidzah</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <br>
                                <p class="text-center font-weight-bold">Tabel Pendidikan Asatidzah</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>TIDAK DIKETAHUI</td>
                                        <td class="text-center">{{ $jumlahTidakDiketahui }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>SMA/SMK/MLN</td>
                                        <td class="text-center"> {{ $jumlahSMA_SMK }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>S1</td>
                                        <td class="text-center">{{ $jumlahS1 }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>S2</td>
                                        <td class="text-center">{{ $jumlahS2 }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>S3</td>
                                        <td class="text-center">{{ $jumlahS3 }}
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <figure class="highcharts-figure">
                                    <div id="container-pendidikan"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        @endif
    </div>
</section>
<!-- /.content -->
@endsection

@push('css')
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
{{-- Style Highcharts --}}
<style>
    .highcharts-figure,
.highcharts-data-table table {
    min-width: 320px;
    max-width: 660px;
    margin: 1em auto;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}

.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}

.highcharts-title {
    font-size: 14px!important;
}

.highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
    padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}

.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>
@endpush

@push('scripts')
<!-- SweetAlert2 -->
<script src="{{ asset('admin_lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- DataTables  & admin_lte -->
<script src="{{ asset('admin_lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('admin_lte/plugins/chart.js/Chart.min.js') }}"></script>
<!-- FLOT CHARTS -->
<script src="{{ asset('admin_lte/plugins/flot/jquery.flot.js') }}"></script>
{{-- HIGHCHARTS --}}
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
    $(function () {
        //-------------
        //- DONUT CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var donutChartCanvas = $('#donutChartTSN')
        var donutData        = {
            labels: [
                'Laki-laki',
                'Perempuan',
            ],
            datasets: [
                {
                data: [{{ $data['santri_tsn_lk'] }}, {{ $data['santri_tsn_pr'] }}],
                backgroundColor : ['#00a65a','#fdff80'],
                }
            ]
        }
        var donutOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })

        //-------------
        //- DONUT CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var donutChartCanvas = $('#donutChartMLN')
        var donutData = {
        labels: [
            'Laki-laki',
            'Perempuan',
        ],
        datasets: [
            {
                data: [{{ $data['santri_mln_lk'] }}, {{ $data['santri_mln_pr'] }}],
                    backgroundColor : ['#00a65a','#fdff80'],
                }
            ]
        }
        var donutOptions  = {
            maintainAspectRatio : false,
            responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })

        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var DataMLN        = {
        labels: [
            'Laki-laki',
            'Perempuan',
        ],
        datasets: [
            {
            data: [{{ $asatidz_jk_mln_L }}, {{ $asatidz_jk_mln_P }}],
            backgroundColor : ['#00a65a','#fdff80'],
            }
        ]
        }
        var pieChartCanvas = $('#pieChartMLN');
        var pieDataMLN       = DataMLN;
        var pieOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        new Chart(pieChartCanvas, {
            type: 'pie',
            data: pieDataMLN,
            options: pieOptions
        })

        var DataTSN        = {
        labels: [
            'Laki-laki',
            'Perempuan',
        ],
        datasets: [
            {
            data: [{{ $asatidz_jk_tsn_L }}, {{ $asatidz_jk_tsn_P }}],
            backgroundColor : ['#00a65a','#fdff80'],
            }
        ]
        }
        var pieChartCanvas = $('#pieChartTSN')
        var pieDataTSN        = DataTSN;
        var pieOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        new Chart(pieChartCanvas, {
            type: 'pie',
            data: pieDataTSN,
            options: pieOptions
        })

        //-------------
        //- GRAFIK ASATIDZ STATUS MERITAL -
        //-------------
        // Build the chart
        Highcharts.chart('container-status-merital', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Grafik Status Merital Asatidzah',
                align: 'left'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
            },
            series: [{
                name: 'Jumlah',
                colorByPoint: true,
                data: [{
                    name: 'TIDAK DIKETAHUI',
                    y: {{ $jumlahTidakDiketahui_MA }},
                    sliced: true,
                    selected: true
                },  {
                    name: 'SINGLE',
                    y: {{ $jumlahSINGLE }}
                },  {
                    name: 'MENIKAH',
                    y: {{  $jumlahMENIKAH }}
                }, {
                    name: 'DUDA',
                    y: {{ $jumlahDUDA }}
                }, {
                    name: 'JANDA',
                    y: {{ $jumlahJANDA }}
                }]
            }]
        });

        //-------------
        //- GRAFIK ASATIDZ PENDIDIKAN -
        //-------------
        // Build the chart
        Highcharts.chart('container-pendidikan', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Grafik Pendidikan Asatidzah',
                align: 'left'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
            },
            series: [{
                name: 'Jumlah',
                colorByPoint: true,
                data: [{
                    name: 'TIDAK DIKETAHUI',
                    y: {{ $jumlahTidakDiketahui }},
                    sliced: true,
                    selected: true
                },  {
                    name: 'SMA/SMK/MLN',
                    y: {{ $jumlahSMA_SMK }}
                },  {
                    name: 'S1',
                    y: {{  $jumlahS1 }}
                }, {
                    name: 'S2',
                    y: {{ $jumlahS2 }}
                }, {
                    name: 'S3',
                    y: {{ $jumlahS3 }}
                }]
            }]
        });
    });
</script>
@endpush