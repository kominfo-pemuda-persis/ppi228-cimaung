<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    {{-- DATATABLE --}}
    <link rel="stylesheet" href="{{asset('admin_lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{asset('admin_lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin_lte/dist/css/adminlte.min.css') }}">
    <style>
        @page {
            size: A3 landscape;
        }
        /* @media-print {
            body {
                transform: scale(1.7);
            }
        } */
    </style>
    <style type="text/css"> * {margin:0; padding:0; text-indent:0; }
         p { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: bold; text-decoration: none; font-size: 14pt; margin: 0 1pt; }
        h1 { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: bold; text-decoration: none; font-size: 13pt; }
        .s1 { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: bold; text-decoration: none; font-size: 11pt; }
        .s2 { color: black; font-family:"Times New Roman", serif; font-style: normal; font-weight: normal; text-decoration: none; font-size: 11pt; }
        .s3 { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: normal; text-decoration: none; font-size: 16pt; vertical-align: 1pt; }
        .s4 { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: normal; text-decoration: none; font-size: 16pt; }
        h2 { color: black; font-family:Arial, sans-serif; font-style: normal; font-weight: bold; text-decoration: none; font-size: 11pt; }
        .s5 { color: black; font-family:"Times New Roman", serif; font-style: normal; font-weight: normal; text-decoration: none; font-size: 10pt; }
        .s6 { color: black; font-family:"Times New Roman", serif; font-style: normal; font-weight: normal; text-decoration: none; font-size: 12pt; }
        li {display: block; }
        .text-left {
            text-align: left;
        }
        .center-text {
            text-align: center; /* Penyelarasan teks secara horizontal */
            vertical-align: middle; /* Penyelarasan teks secara vertikal */
        }
        
        .vertical-text {
            writing-mode: vertical-lr; /* Menuliskan teks dari bawah ke atas (top ke bottom) */
            transform: rotate(180deg); /* Memiringkan teks sehingga berjalan dari bawah ke atas */
            white-space: nowrap; /* Agar teks tidak di-wrap secara otomatis */
            text-align: center; /* Penyelarasan teks secara horizontal */
            vertical-align: middle; /* Penyelarasan teks secara vertikal */
        }
        .b2{
            border-top: 2pt solid;border-left: 2pt solid;border-bottom:2pt solid;border-right:2pt solid;
        }
        .b212{
            border-top: 2pt solid;border-left: 2pt solid;border-bottom:1pt solid;border-right:2pt solid;
        }
        .b122{
            border-top:1pt solid;border-left: 2pt solid;border-bottom:2pt solid;border-right:1pt solid;
        }
        .b112{
            border-top:1pt solid;border-left:1pt solid;border-bottom:2pt solid;border-right:2pt solid;
        }
        .b221{
            border-top: 2pt solid;border-left: 2pt solid;border-bottom:1pt solid;border-right:1pt solid;
        }
        .b121{
            border-top:1pt solid;border-left: 2pt solid;border-bottom:1pt solid;border-right:2pt solid;
        }
        .b1211{
            border-top:1pt solid;border-left: 2pt solid;border-bottom:1pt solid;border-right:1pt solid;
        }
        .b2112{
            border-top: 2pt solid;border-left:1pt solid;border-bottom:1pt solid;border-right:2pt solid;
        }
        .b111{
            border-top:1pt solid;border-left:1pt solid;border-bottom:1pt solid;border-right:2pt solid;
        }
        .b1222{
            border-top:1pt solid;border-left: 2pt solid;border-bottom:2pt solid;border-right:2pt solid;
        }
        .page-break {
            page-break-after: avoid;
        }
        .page-break-before {
            page-break-before: always;
        }
    </style>
</head>
<body onload="window.print()">
    <div>
        <img src="{{ url('/img/jadwal/header.jpg') }}" class="img-fluid" alt="">
    </div>
    <div class="page-break">
        <div class="text-center">
            <span class="h5"><b>JADWAL KEGIATAN BELAJAR MENGAJAR TAHUN AJARAN 2023-2024</b></span><span class="h5" style="display: block"><b>INTRAKURIKULER</b></span>
        </div>
        <div class="content container-fluid px-5">
            <table style="border-collapse:collapse;" class="w-100" cellspacing="0">
                <tr>
                    <td class="b2" style="width: 3%;" rowspan="2">
                        <p class="s1 center-text">Hari</p>
                    </td>
                    <td class="b2" style="width: 7%;" rowspan="2">
                        <p class="s1 center-text">Jam</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">1 Tsanawiyyah</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">2 Tsanawiyyah</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">3 Tsanawiyyah</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">1 Mu’allimin</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">2 Mu’allimin</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">3 Mu’allimin</p>
                    </td>
                </tr>
                <tr>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                </tr>
                @php
                    $hari =[1=>'SENIN',2=>'SELASA',3=>'RABU',4=>'KAMIS',5=>'JUMAT',6=>'SABTU'];
                    $tingkat = [[1,'TSN'],[2,'TSN'],[3,'TSN'],[1,'MLN'],[2,'MLN'],[3,'MLN']];
                @endphp
                @foreach($hari as $key => $value)
                @php
                    $itemIntra = $intra->where('hari',$key);
                @endphp
                <tr>
                    <td class="b2 vertical-text" rowspan="6">
                        <p class="s1 center-text" style="text-align: center;">{{ $value }}</p>
                    </td>
                    <td class="b212">
                        <p class="s2 center-text">{{ $jamDB[0]->where('jam_ke', 1)->first()->awal."-".$jamDB[0]->where('jam_ke', 1)->first()->akhir }}</p>
                    </td>
                    @foreach($tingkat as $keyTingkat => $valueTingkat)
                    @if ($itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 1)->where('tingkat', $valueTingkat[1])->first())
                    <td class="b221">
                        <p class="s2 center-text">{{ $itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 1)->where('tingkat', $valueTingkat[1])->first()->pelajaran_nama }}</p>
                    </td>
                    <td class="b2112">
                        <p class="s2 center-text">{{ $itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 1)->where('tingkat', $valueTingkat[1])->first()->nomor_urut }}</p>
                    </td>
                    @else
                    <td class="b221">
                        <p class="s2 center-text">-</p>
                    </td>
                    <td class="b2112">
                        <p class="s2 center-text">-</p>
                    </td>
                    @endif
                    @endforeach
                </tr>
                <tr>
                    <td class="b121">
                        <p class="s2 center-text">{{ $jamDB[0]->where('jam_ke', 2)->first()->awal."-".$jamDB[0]->where('jam_ke', 2)->first()->akhir }}</p>
                    </td>
                    @foreach($tingkat as $keyTingkat => $valueTingkat)
                    @if ($itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 2)->where('tingkat', $valueTingkat[1])->first())
                    <td class="b1211">
                        <p class="s2 center-text">{{ $itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 2)->where('tingkat', $valueTingkat[1])->first()->pelajaran_nama }}</p>
                    </td>
                    <td class="b111">
                        <p class="s2 center-text">{{ $itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 2)->where('tingkat', $valueTingkat[1])->first()->nomor_urut }}</p>
                    </td>
                    @else
                    <td class="b1211">
                        <p class="s2 center-text">-</p>
                    </td>
                    <td class="b111">
                        <p class="s2 center-text">-</p>
                    </td>
                    @endif
                    @endforeach
                </tr>
                <tr>
                    <td class="b121">
                        <p class="s2 center-text">{{ $jamDB[0]->where('jam_ke', 3)->first()->awal."-".$jamDB[0]->where('jam_ke', 3)->first()->akhir }}</p>
                    </td>
                    <td class="b121" colspan="12">
                        <p class="s2 center-text">ISTIRAHAT I</p>
                    </td>
                </tr>
                <tr>
                    <td class="b121">
                        <p class="s2 center-text">{{ $jamDB[0]->where('jam_ke', 4)->first()->awal."-".$jamDB[0]->where('jam_ke', 4)->first()->akhir }}</p>
                    </td>
                    @foreach($tingkat as $keyTingkat => $valueTingkat)
                    @if ($itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 4)->where('tingkat', $valueTingkat[1])->first())
                    <td class="b1211">
                        <p class="s2 center-text">{{ $itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 4)->where('tingkat', $valueTingkat[1])->first()->pelajaran_nama }}</p>
                    </td>
                    <td class="b111">
                        <p class="s2 center-text">{{ $itemIntra->where('kelas', $valueTingkat[0])->where('jam_ke', 4)->where('tingkat', $valueTingkat[1])->first()->nomor_urut }}</p>
                    </td>
                    @else
                    <td class="b1211">
                        <p class="s2 center-text">-</p>
                    </td>
                    <td class="b111">
                        <p class="s2 center-text">-</p>
                    </td>
                    @endif
                    @endforeach
                </tr>
                <tr>
                    <td class="b121">
                        <p class="s2 center-text">{{ $jamDB[0]->where('jam_ke', 5)->first()->awal."-".$jamDB[0]->where('jam_ke', 5)->first()->akhir }}</p>
                    </td>
                    <td class="b121" colspan="12">
                        <p class="s2 center-text">ISTIRAHAT II</p>
                    </td>
                </tr>
                <tr>
                    <td class="b1222">
                        <p class="s2 center-text">{{ $jamDB[0]->where('jam_ke', 6)->first()->awal."-".$jamDB[0]->where('jam_ke', 6)->first()->akhir }}</p>
                    </td>
                    @foreach($tingkat as $keyTingkat => $valueTingkat)
                    <td class="b122">
                        <p class="s2 center-text">Tahfizh</p>
                    </td>
                    <td class="b112">
                        <p class="s2 center-text">M</p>
                    </td>
                    @endforeach
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <br>
    <div class="page-break mt-2">
        <div class="text-center">
            <span class="h5"><b>EXTRAKURIKULER</b></span>
        </div>
        <div class="content container-fluid px-5">
            <table style="border-collapse:collapse;" class="w-100" cellspacing="0">
                <tr>
                    <td class="b2" style="width: 3%;" rowspan="2">
                        <p class="s1 center-text">Hari</p>
                    </td>
                    <td class="b2" style="width: 7%;" rowspan="2">
                        <p class="s1 center-text">Jam</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">1 Tsanawiyyah</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">2 Tsanawiyyah</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">3 Tsanawiyyah</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">1 Mu’allimin</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">2 Mu’allimin</p>
                    </td>
                    <td class="b212" style="width: 15%" colspan="2">
                        <p class="s1 center-text">3 Mu’allimin</p>
                    </td>
                </tr>
                <tr>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                    <td class="b122">
                        <p class="s1 center-text">Mata Pelajaran</p>
                    </td>
                    <td class="b112" style="width: 2%">
                        <p class="s1 center-text">A</p>
                    </td>
                </tr>
                @php
                    $think = [[1,'TSN'],[2,'TSN'],[3,'TSN'],[1,'MLN'],[2,'MLN'],[3,'MLN']];
                    $days =[1=>'SENIN',2=>'SELASA',3=>'RABU',4=>'KAMIS'];
                    $kegiatan = ['يراخبلا حيحص حرش يرابلا حتف', 'ملعلما لوسرلا و ملعلا باتك', ' حرش ىلع يوونلا حيحص ملسم', 'Kegiatan PC. Pemuda Persis'];
                    $p = 0;
                @endphp
                @foreach($days as $key => $value)
                    @php
                        $itemExtra = $extra->where('hari',$key);
                    @endphp
                    <tr>
                        <td class="b2 vertical-text" rowspan="2">
                            <p class="s1 center-text" style="text-align: center;">{{ $value }}</p>
                        </td>
                        <td class="b212">
                            <p class="s2 center-text">{{ $jamDB[1]->where('jam_ke', 1)->first()->awal."-".$jamDB[1]->where('jam_ke', 1)->first()->akhir }}</p>
                        </td>
                        @foreach($think as $ky => $vt)
                            @if ($itemExtra->where('kelas', $vt[0])->where('jam_ke', 1)->where('tingkat', $vt[1])->first())
                            <td class="b221">
                                <p class="s2 center-text">{{ $itemExtra->where('kelas', $vt[0])->where('jam_ke', 1)->where('tingkat', $vt[1])->first()->pelajaran_nama }}</p>
                            </td>
                            <td class="b2112">
                                <p class="s2 center-text">{{ $itemExtra->where('kelas', $vt[0])->where('jam_ke', 1)->where('tingkat', $vt[1])->first()->urutan_id }}</p>
                            </td>
                            @else
                            <td class="b221">
                                <p class="s2 center-text">-</p>
                            </td>
                            <td class="b2112">
                                <p class="s2 center-text">-</p>
                            </td>
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td class="b122">
                            <p class="s2 center-text">{{ $jamDB[1]->where('jam_ke', 2)->first()->awal."-".$jamDB[1]->where('jam_ke', 2)->first()->akhir }}</p>
                        </td>
                        @for($i=0;$i<2;$i++)
                            @if ($key != 4)
                            <td class="b122">
                                <p class="s2 center-text">Tahfizh</p>
                            </td>
                            <td class="b112">
                                <p class="s2 center-text">M</p>
                            </td>
                            @else 
                            <td class="b122">
                                <p class="s2 center-text">-</p>
                            </td>
                            <td class="b112">
                                <p class="s2 center-text">-</p>
                            </td>
                            @endif
                        @endfor
                        <td class="b122" style="height: 80px" colspan="7">
                            <p class=" center-text">{{ isset($kegiatan[$p]) ? $kegiatan[$p] : "" }}</p>
                        </td>
                        <td class="b112">
                            <p class="s2 center-text">1</p>
                        </td>
                    </tr>
                    
                    @php
                        $p++;
                    @endphp
                @endforeach
                <tr>
                    <td class="b2 vertical-text" rowspan="2" style="height: 110px">
                        <p class="s1 center-text" style="text-align: center;">Jumat Sabtu</p>
                    </td>
                    <td class="b1222">
                        <p class="s2 center-text">{{ $jamDB[1]->where('jam_ke', 1)->first()->awal."-".$jamDB[1]->where('jam_ke', 1)->first()->akhir }}</p>
                    </td>
                    <td class="b112" colspan="12">
                        <p class="s2 center-text">Kegiatan RG/UG atau Klub Minat dan Bakat</p>
                    </td>
                </tr>
                <tr>
                    <td class="b1222">
                        <p class="s2 center-text">{{ $jamDB[1]->where('jam_ke', 2)->first()->awal."-".$jamDB[1]->where('jam_ke', 2)->first()->akhir }}</p>
                    </td>
                    <td class="b112" colspan="12">
                        <p class="s2 center-text">Kegiatan RG/UG atau Klub Minat dan Bakat</p>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
    <div class="page-break-before mt-2">
        <div class="content container-fluid px-5">
        <br>
            <b>keterangan</b>
            <div class="row">
                <div class="col-md-3">
                    <ul>
                        <li>A : Asatidz/Asatidzah</li>
                        <li>M : Muaddib/Muaddibah</li>
                        @for ($i = 0; $i < 6; $i++)
                            <li>{{$urutan[$i]->nomor_urut.". ".$urutan[$i]->nama_lengkap}}</li>
                        @endfor
                    </ul>
                </div>
                @php
                    $countUrutan = count($urutan);
                    $hitung = $countUrutan - 6;
                    $loop = 8;
                @endphp
                @while ($loop > 0)
                    <div class="col-md-3">
                        <ul>
                            @for ($i = 6; $i < $countUrutan; $i++)
                                <li>{{$urutan[$i]->nomor_urut.". ".$urutan[$i]->nama_lengkap}}</li>
                            @endfor
                        </ul>
                    </div>
                    @php
                        $hitung -= 8;
                        $loop--;
                    @endphp
                    @if ($hitung < 0)
                        @break
                    @endif
                @endwhile
            </div>
            <br><br><br>
            <div class="row justify-content-center">
                <div class="col-3">
                    <img src="{{url('/img/jadwal/ttd.png')}}" class="img-fluid w-100" alt="12">
                </div>
            </div>
        </div>
    </div>
</body>
</html>