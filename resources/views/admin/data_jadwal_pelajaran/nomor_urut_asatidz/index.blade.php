@extends('layouts.dashboard')

@push('css')
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.css')}}">
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Nomor Urut Asatidz</li>
@endsection

@section('title', 'Nomor Urut Asatidz')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_nomor_urut_asatidz-create')
                <button type="button" id="tambah" class="btn btn-primary btn-sm">tambah</button>
                @endpermission
            </div>
        </div>

        <div class="card-body">
            <div id="asatidz_wrapper">
                <table class="table table-bordered w-100" id="asatidz">
                    <thead>
                        <tr>
                            <th>Nomor Urut</th>
                            <th>Nama Asatidz</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($nomor_urut as $item)
                            <tr>
                                <td>{{ $item->nomor_urut }}</td>
                                <td>{{ $item->asatidz->nama_lengkap }}</td>
                                <td>
                                    @permission('menu_nomor_urut_asatidz-update')
                                    <button type="button" class="btn edit-btn btn-warning btn-sm" 
                                    data-id="{{ $item->id }}"
                                    data-nomor_urut="{{ $item->nomor_urut }}"
                                    data-asatidz_id="{{ $item->asatidz_id }}"
                                    >Edit</button>
                                    @endpermission
                                    @permission('menu_nomor_urut_asatidz-delete')
                                    <button type="button" class="btn btn-danger delete-btn btn-sm" data-id="{{ $item->id }}">Hapus</button>
                                    @endpermission
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">Data Kosong</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Nomor Asatidz</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('nomor.urut.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nomor_urut">Nomor Urut</label>
                                <input type="number" name="nomor_urut" id="nomor_urut" class="form-control">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="asatidz_id">Asatidz</label>
                                <select name="asatidz_id" id="asatidz_id" class="form-control">
                                    <option value="">-- Pilih Asatidz --</option>
                                    @foreach ($asatidz as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama_lengkap }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- EDIT --}}
    <div class="modal fade" id="modal-edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Nomor Asatidz</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="nomor_urut_edit">Nomor Urut</label>
                                <input type="number" name="nomor_urut_edit" id="nomor_urut_edit" class="form-control">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="asatidz_id_edit">Asatidz</label>
                                <select name="asatidz_id_edit" id="asatidz_id_edit" class="form-control">
                                    <option value="">-- Pilih Asatidz --</option>
                                    @foreach ($asatidz as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama_lengkap }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script src="{{ asset('admin_lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        // on click tambah
        $('#tambah').click(function() {
            $('#modal-tambah').modal('show');
        });

        // on click edit
        $('.edit-btn').click(function() {
            $('form').attr('action', '{{ url("/admin/jadwal-pelajaran/nomor-urut/update") }}' + '/' + $(this).data('id'));
            $('#modal-edit').modal('show');
            $('#nomor_urut_edit').val($(this).data('nomor_urut'));
            $('#asatidz_id_edit').val($(this).data('asatidz_id'));
        });

        // on click delete
        $('.delete-btn').click(function() {
            // confirm delete
            if (confirm('Yakin hapus data ini?')) {
                // ajax delete
                $.ajax({
                    url: '{{ url("/admin/jadwal-pelajaran/nomor-urut/delete") }}' + '/' + $(this).data('id'),
                    type: 'get',
                    data: {
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function(response) {
                        // remove row
                        $(this).parents('tr').remove();
                        // reload page
                        location.reload();
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                    }
                });
            }
        }
        );
    </script>
@endpush