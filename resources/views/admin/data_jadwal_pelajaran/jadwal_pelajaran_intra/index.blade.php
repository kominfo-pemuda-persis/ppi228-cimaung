@extends('layouts.dashboard')

@push('css')
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.css')}}">
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Jadwal Pelajaran Intra</li>
@endsection

@section('title', 'Jadwal Pelajaran Intra')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                <a href="{{ route('jadwal.export') }}" target="_blank" class="btn btn-sm btn-success">Export</a>
                @permission('menu_jadwal_mengajar_intra-create')
                <button type="button" id="tambah" class="btn btn-primary btn-sm">Tambah jadwal</button>
                @endpermission
                <a href="{{ url('/admin/jadwal-pelajaran/jam-mengajar') }}" class="btn btn-sm btn-dark">Atur Jam Pelajaran</a>
            </div>
        </div>

        <div class="card-body">
            {{-- make dropdown filter --}}
            <form action="#">
                <div class="row align-items-center">
                    <div class="form-group col-md-3">
                        <label for="tingkat">Tingkat</label>
                        <select name="tingkat" id="tingkat" class="form-control select2bs4">
                            <option value="">pilih tingkat</option>
                            <option value="TSN" {{ request('tingkat') == "TSN" ? "selected" : "" }}>TSN</option>
                            <option value="MLN" {{ request('tingkat') == "MLN" ? "selected" : "" }}>MLN</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="kelas">Kelas</label>
                        <select name="kelas" id="kelas" class="form-control select2bs4">
                            <option value="">pilih kelas</option>
                            <option value="1" {{ request('kelas') == 1 ? "selected" : "" }}>1</option>
                            <option value="2" {{ request('kelas') == 2 ? "selected" : "" }}>2</option>
                            <option value="3" {{ request('kelas') == 3 ? "selected" : "" }}>3</option>
                        </select>
                    </div>
                    <div class="mt-3 col-md-3">
                        <button type="submit" class="btn btn-dark btn-sm">Filter</button>
                        <a href="{{ url('/admin/jadwal-pelajaran/intra') }}" class="btn btn-dark btn-sm">clear</a>
                    </div>
                </div>
            </form>

            <div id="asatidz_wrapper">
                <table class="table table-bordered w-100" id="asatidz">
                    <thead>
                        <tr>
                            <th>Kelas</th>
                            <th>Hari</th>
                            <th>Jam ke</th>
                            <th>waktu</th>
                            <th>Pelajaran</th>
                            <th>Nama Asatidz</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
                            $tempHari = "";
                            $tempKelas = "";
                        @endphp
                        {{-- @dd($jadwal) --}}
                        @forelse ($jadwal as $item)
                            <tr>
                                @php
                                    // count item->hari where hari = item->hari
                                    $thisHariCount = $jadwal->where('hari', $item->hari)->where('kelas', $item->kelas)->where('tingkat', $item->tingkat)->count();
                                    $thisKelasCount = $jadwal->where('kelas', $item->kelas)->where('tingkat', $item->tingkat)->count();
                                @endphp
                                @if ($tempKelas != $item->kelas.'-'.$item->tingkat)
                                    @php
                                        $tempKelas = $item->kelas.'-'.$item->tingkat;
                                    @endphp
                                    <td rowspan="{{ $thisKelasCount }}">{{ $item->kelas }}-{{$item->tingkat}}</td>
                                @endif
                                @if ($tempHari != $item->hari.'-'.$item->kelas.'-'.$item->tingkat)
                                    @php
                                        $tempHari = $item->hari.'-'.$item->kelas.'-'.$item->tingkat;
                                    @endphp
                                    <td rowspan="{{ $thisHariCount }}">{{ $hari[$item->hari - 1] }}</td>
                                @endif
                                <td>{{ $item->jam->jam_ke }}</td>
                                <td>{{ $item->jam->awal }} - {{ $item->jam->akhir }}</td>
                                @if ($item->asatidz_id == null || $item->pelajaran == null)
                                    <td colspan="2"> {{ $item->jam->kegiatan }} </td>
                                @else
                                    <td>{{ $item->pelajaran->nama_pelajaran }}</td>
                                    <td>{{ $item->asatidz->nama_lengkap }}</td>
                                @endif
                                <td>
                                    @permission('menu_jadwal_mengajar_intra-update')
                                    <button type="button" id="ubah" data-id="{{ $item->id }}" data-hari="{{ $item->hari }}" data-kelas="{{ $item->kelas.'-'.$item->tingkat }}" class="btn btn-warning btn-sm">Ubah</button>
                                    @endpermission
                                    <form action="{{ url('/admin/jadwal-pelajaran/intra-delete/'. $item->id) }}" method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        @permission('menu_jadwal_mengajar_intra-delete')
                                        <button type="submit" onclick="return confirm('Yakin ingin menghapus data?')" class="btn btn-danger btn-sm">Hapus</button>
                                        @endpermission
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="8" class="text-center">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {!! $jadwal->links() !!}
            </div>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Jadwal Pelajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('jadwal.intra.add') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" id="edit_id">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="jam_id">Jam</label>
                                <select name="jam_id" id="jam_id" class="form-control select2bs4">
                                   
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="hari_id">Hari</label>
                                <select name="hari" id="hari_id" class="form-control">
                                    <option value="">Pilih hari</option>
                                   @foreach ($hari as $h)
                                        <option value="{{ $loop->iteration }}">{{ $h }}</option>
                                   @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="pelajaran_id">Kegiatan</label>
                                <select name="pelajaran_id" id="pelajaran_id" class="form-control select2bs4">
                                   
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="asatidz_id">Nama Pengajar</label>
                                <select name="asatidz_id" id="asatidz_id" class="form-control select2bs4">
                                   
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="kelas_id">Kelas</label>
                                <select name="kelas_id" id="kelas_id" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                                <select name="tingkat_sekolah_id" id="tingkat_sekolah_id" class="form-control">
                                    <option value="TSN">TSN</option>
                                    <option value="MLN">MLN</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Jadwal Pelajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form-edit">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="jam_id_edit">Jam</label>
                                <select name="jam_id" id="jam_id_edit" class="form-control select2bs4">
                                   
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="hari_id">Hari</label>
                                <select name="hari" id="hari_id_edit" class="form-control">
                                   @foreach ($hari as $key => $h)
                                        <option value="{{ $key+1 }}">{{ $h }}</option>
                                   @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="pelajaran_id_edit">Kegiatan</label>
                                <select name="pelajaran_id" id="pelajaran_id_edit" class="form-control select2bs4">
                                   
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="asatidz_id_edit">Nama Pengajar</label>
                                <select name="asatidz_id" id="asatidz_id_edit" class="form-control select2bs4">
                                   
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="kelas_id">Kelas</label>
                                <select name="kelas_id" id="kelas_id_edit" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                                <select name="tingkat_sekolah_id" id="tingkat_sekolah_id_edit" class="form-control">
                                    <option value="TSN">TSN</option>
                                    <option value="MLN">MLN</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>



@endsection

@push('scripts')
<script src="{{ asset('admin_lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        $('body').on('click', '#tambah', function() {
            // clear data
            $('#asatidz_id').html('');
            $('#pelajaran_id').html('');
            $('#jam_id').html('');
            $.ajax({
                url: `{{ route("jadwal.showAdd") }}`,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    console.log(response.data);
                    //fill data to form
                    $('#asatidz_id').val('');
                    $('#pelajaran_id').val('');
                    $('#jam_id').val('');

                    $('#asatidz_id').append(`<option value="" selected>--Pilih Asatidz--</option>`);
                    $.each(response.data.asatidz, function(key, value) {
                        $('#asatidz_id').append(`<option value="${value.id}">${value.nama_lengkap}</option>`);
                    });
                    $('#pelajaran_id').append(`<option selected value="">--pilih pelajaran--</option>`);
                    $.each(response.data.pelajaran, function(key, value) {
                        $('#pelajaran_id').append(`<option value="${value.id}">${value.nama_pelajaran} - ${value.tingkat_sekolah_id}</option>`);
                    });
                    $.each(response.data.jam, function(key, value) {
                        $('#jam_id').append(`<option value="${value.id}">(${value.jam_ke}) ${value.awal} - ${value.akhir} (${value.kegiatan})</option>`);
                    });
                    //open modal
                    $('#modal-tambah').modal('show');
                }
            });
        });

        // body on click ubah
        $('body').on('click', '#ubah', function() {
            let id = $(this).data('id');
            // set action form
            $('#form-edit').attr('action', `{{ url("admin/jadwal-pelajaran/intra-update") }}` + '/' + id);
            $.ajax({
                url: `{{ url("admin/jadwal-pelajaran/show-data-edit") }}`+'/'+id,
                type: "GET",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    //fill data to form
                    $('#asatidz_id_edit').empty();
                    $('#pelajaran_id_edit').empty();
                    $('#jam_id_edit').empty();
                    // clear selected
                    $('#asatidz_id_edit').val('');
                    $('#pelajaran_id_edit').val('');
                    $('#jam_id_edit').val('');

                    $('#asatidz_id_edit').append(`<option value="" selected>--Pilih Asatidz--</option>`);
                    $.each(response.data.asatidz, function(key, value) {
                        $('#asatidz_id_edit').append(`<option value="${value.id}">${value.nama_lengkap}</option>`);
                    });
                    $('#pelajaran_id_edit').append(`<option value="">--pilih kegiatan--</option>`);
                    $.each(response.data.pelajaran, function(key, value) {
                        $('#pelajaran_id_edit').append(`<option value="${value.id}">${value.nama_pelajaran} - ${value.tingkat_sekolah_id}</option>`);
                    });
                    $.each(response.data.jam, function(key, value) {
                        $('#jam_id_edit').append(`<option value="${value.id}">(${value.jam_ke}) ${value.awal} - ${value.akhir} (${value.kegiatan})</option>`);
                    });
                    //open modal
                    $('#modal-edit').modal('show');
                    
                    $('#pelajaran_id_edit').val(response.data.jadwal.pelajaran_id);
                    $('#asatidz_id_edit').val(response.data.jadwal.asatidz_id);
                    $('#jam_id_edit').val(response.data.jadwal.jam_id);
                    // SELECT KELAS
                    $('#kelas_id_edit').val(response.data.jadwal.kelas);
                    //select tingkat
                    $('#tingkat_sekolah_id_edit').val(response.data.jadwal.tingkat);
                    // select hari
                    $('#hari_id_edit').val(response.data.jadwal.hari);
                    // id 
                    $('#id_edit').val(response.data.jadwal.id);
                    
                    if (response.data.jadwal.kegiatan == 'Istirahat' || response.data.jadwal.kegiatan == 'Hafalan') {
                        disableAsatidzAndPelajaranEdit(true)
                    } else {
                        disableAsatidzAndPelajaranEdit(false)
                    }
                    
                }

            });
        });

        // on change jam_id
        $('#jam_id').on('change', function() {
            // if jam_id text is contain 'Istirahat' or 'Hafalan' disable asatidz_id and pelajaran_id
            if ($(this).find(':selected').text().includes('Istirahat') || $(this).find(':selected').text().includes('Hafalan')) {
                $('#asatidz_id').prop('disabled', true);
                $('#pelajaran_id').prop('disabled', true);
            } else {
                $('#asatidz_id').prop('disabled', false);
                $('#pelajaran_id').prop('disabled', false);
            }
        });

        // on change jam_id_edit
        $('#jam_id_edit').on('change', function() {
            // if jam_id text is contain 'Istirahat' or 'Hafalan' disable asatidz_id and pelajaran_id
            if ($(this).find(':selected').text().includes('Istirahat') || $(this).find(':selected').text().includes('Hafalan')) {
                disableAsatidzAndPelajaranEdit(true)
            } else {
                disableAsatidzAndPelajaranEdit(false)
            }
        });

        // function disable asatidz_id and pelajaran_id
        function disableAsatidzAndPelajaranEdit($condition) {
            if ($condition) {
                $('#asatidz_id_edit').prop('disabled', true);
                $('#pelajaran_id_edit').prop('disabled', true);
            } else {
                $('#asatidz_id_edit').prop('disabled', false);
                $('#pelajaran_id_edit').prop('disabled', false);
            }
        }

        
    </script>
@endpush