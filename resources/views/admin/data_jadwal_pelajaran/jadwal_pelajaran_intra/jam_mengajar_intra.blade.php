@extends('layouts.dashboard')

@push('css')
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.css')}}">
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('jadwal.intra') }}">Jadwal Pelajaran Intra</a></li>
    <li class="breadcrumb-item active">Jam Pelajaran</li>
@endsection

@section('title', 'Jam Pelajaran Intra')

@section('content')
    {{-- tombol back --}}
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('jadwal.intra') }}" class="btn btn-sm btn-secondary mb-3"><i class="fas fa-arrow-left"></i>
                Kembali</a>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_jadwal_mengajar_intra-create')
                <button type="button" id="tambah" class="btn btn-primary btn-sm">Tambah Jam Pelajaran</button>
                @endpermission
            </div>
        </div>

        <div class="card-body">
            <div id="asatidz_wrapper">
                <table class="table table-bordered w-100" id="asatidz">
                    <thead>
                        <tr>
                            <th>Jam Ke</th>
                            <th>Jam Pelajaran</th>
                            <th>Kegiatan</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($jam as $item)
                            <tr>
                                <td>{{ $item->jam_ke }}</td>
                                <td>{{ date("G:i", strtotime($item->awal)) }} - {{ date("G:i", strtotime($item->akhir)) }}</td>
                                <td>{{ $item->kegiatan }}</td>
                                <td>
                                    @permission('menu_jadwal_mengajar_intra-update')
                                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                        data-target="#modal-edit" data-id="{{ $item->id }}"
                                        data-jam_ke="{{ $item->jam_ke }}" data-awal="{{ $item->awal }}"
                                        data-akhir="{{ $item->akhir }}"
                                        data-kegiatan="{{ $item->kegiatan }}">Edit</button>
                                    @endpermission
                                    @permission('menu_jadwal_mengajar_intra-delete')
                                    <button type="button" class="btn btn-danger btn-sm" data-id="{{ $item->id }}">Hapus</button>
                                    @endpermission
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">Data Kosong</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Jam Pelajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('jam.mengajar.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="jam_ke">Nomor Urut Jam Pelajaran</label>
                                <select name="jam_ke" id="jam_ke" class="form-control">
                                    <option value="">Pilih Urutan Jam Pelajaran</option>
                                    @for ($i = 1; $i <= 6; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="awal">Jam Mulai</label>
                                <input type="text" name="awal" id="awal" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="akhir">Jam Selesai</label>
                                <input type="text" name="akhir" id="akhir" class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="akhir">Kegiatan</label>
                                {{-- select --}}
                                <select name="kegiatan" id="kegiatan" class="form-control select2bs4">
                                    <option value="">Pilih Kegiatan</option>
                                    <option value="KBM">KBM</option>
                                    <option value="Istirahat">Istirahat</option>
                                    <option value="Hafalan">Hafalan</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- EDIT --}}
    <div class="modal fade" id="modal-edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Jam Pelajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="jam_ke">Nomor Urut Jam Pelajaran</label>
                                <select name="jam_ke" id="jam_ke_edit" class="form-control">
                                    <option value="">Pilih Urutan Jam Pelajaran</option>
                                    @for ($i = 1; $i <= 6; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="awal">Jam Mulai</label>
                                <input type="text" name="awal" id="awal_edit" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="akhir">Jam Selesai</label>
                                <input type="text" name="akhir" id="akhir_edit" class="form-control">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="akhir">Kegiatan</label>
                                {{-- select --}}
                                <select name="kegiatan" id="kegiatan_edit" class="form-control select2bs4">
                                    <option value="">Pilih Kegiatan</option>
                                    <option value="KBM">KBM</option>
                                    <option value="Istirahat">Istirahat</option>
                                    <option value="Hafalan">Hafalan</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
<script src="{{ asset('admin_lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        // on click tambah
        $('#tambah').click(function() {
            $('#modal-tambah').modal('show');
        });

        // on click edit
        $('#modal-edit').on('show.bs.modal', function(event) {

            let button = $(event.relatedTarget)
            let id = button.data('id')
            let jam_ke = button.data('jam_ke')
            let awal = button.data('awal')
            let akhir = button.data('akhir')
            let kegiatan = button.data('kegiatan')
            let modal = $(this)
            let url = `{{ url("/admin/jadwal-pelajaran/jam-mengajar/update") }}/${id}`

            // set url action form
            modal.find('form').attr('action', url );

            modal.find('.modal-body #jam_ke_edit').val(jam_ke)
            modal.find('.modal-body #awal_edit').val(awal)
            modal.find('.modal-body #akhir_edit').val(akhir)
            modal.find('.modal-body #kegiatan_edit').val(kegiatan).trigger('change')
        });

        // on click hapus confrim and call ajax
        $('.btn-danger').click(function() {
            let id = $(this).data('id');
            let url = `{{ url("/admin/jadwal-pelajaran/jam-mengajar/delete") }}/${id}`

            confirm('Yakin Hapus Data ?') ? $.ajax({
                url: url,
                type: 'DELETE',
                data: {
                    _token: '{{ csrf_token() }}'
                },
                success: function(result) {
                    location.reload();
                }
            }) : '';
        });
    </script>
@endpush