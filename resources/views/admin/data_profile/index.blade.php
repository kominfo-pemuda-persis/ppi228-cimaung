@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Profile</li>
@endsection

@push('css')
<style>
  label {
    font-size: 13px!important;
    font-style: italic!important;
  }

  .hilang {
    display: none;
  }

  .image {
    border:none!important;
    text-align: center;
  }

  .profile-username {
    font-size: 14px!important;
  }
</style>
@endpush

@section('title', 'Profile')

@section('content')
<div class="row">
  <div class="col-md-3">
    <div class="card card-success card-outline">
        <div class="card-body box-profile">
          <div class="d-flex flex-column justify-content-center align-items-center">
            @if(empty($data->photo_path))
            <div class="rounded-circle border" style="overflow:hidden; width: 5em !important; height: 5em !important">
              <img class="profile-rounded-img"
                    src="{{ asset('admin_lte/dist/img/avatar.png') }}"
                    alt="User profile picture">
            </div>
            @else
            @role('admin|asatidz|asatidz_tsn|asatidz_mln|muaddib|mudir_tsn|mudir_mln|mudir_am')
            <div class="rounded-circle border" style="overflow:hidden; width: 5em !important; height: 5em !important">
              <img class="profile-rounded-img"
                      src="{{ Storage::disk('s3')->url('data/photo/asatidz/'.$data->photo_path) }}"
                      alt="User profile picture">
            </div>
            @endrole
            @role('santri_tsn|santri_mln')
            <div class="rounded-circle border" style="overflow:hidden; width: 5em !important; height: 5em !important">
              <img class="profile-rounded-img"
              src="{{ Storage::disk('s3')->url('data/photo/santri/'.$data->photo_path) }}"
              alt="User profile picture">
            </div>
            @endrole
            @role('orangtua_tsn|orangtua_mln')
            <div class="rounded-circle border" style="overflow:hidden; width: 5em !important; height: 5em !important">
              <img class="profile-rounded-img"
              src="{{ Storage::disk('s3')->url('data/photo/orangtua/'.$data->photo_path) }}"
              alt="User profile picture">
            </div>
            @endrole
            @endif
              <label for="inputFile" style="cursor: pointer;" class="px-2 rounded badge-success mt-2">Change Image</label>
              <input type="file" name="image" id="inputFile" style="display:none;">
          </div>
          <h3 class="profile-username text-center">{{ Auth()->user()->name }}</h3>

          <div class="mx-auto text-center">
            @foreach(Auth::user()->roles as $role)
              <label class="badge-success px-1 rounded py-1">{{ $role->display_name }}</label>
            @endforeach
          </div>
        </div>
    </div>
  </div>

  <div class="col-md-9">
    <div class="card">
      <div class="card-header">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
          @role('admin|asatidz|asatidz_tsn|asatidz_mln|muaddib|mudir_tsn|mudir_mln|mudir_am')
          <li class="nav-item"><a class="nav-link @if (request('pills') == 'general') active @endif" id="klikGeneral" href="" data-toggle="tab">Data Asatidz</a></li>
          @endrole
          @role('santri_tsn|santri_mln')
          <li class="nav-item"><a class="nav-link @if (request('pills') == 'general') active @endif" id="klikGeneral" href="" data-toggle="tab">Data Santri</a></li>
          <li class="nav-item"><a class="nav-link @if (request('pills') == 'dataOrtu') active @endif" id="klikDataOrtu" href="" data-toggle="tab">Data Ortu</a></li>
          @endrole
          @role('orangtua_tsn|orangtua_mln')
          <li class="nav-item"><a class="nav-link @if (request('pills') == 'general') active @endif" id="klikGeneral" href="" data-toggle="tab">Data Ortu</a></li>
          @endrole
          <li class="nav-item"><a class="nav-link @if (request('pills') == 'account') active @endif" id="klikProfile" href="" data-toggle="tab">Account</a></li>
          <li class="nav-item"><a class="nav-link @if (request('pills') == 'password') active @endif" id="klikPassword" href="" data-toggle="tab">Change Password</a></li>
        </ul>
      </div>

      <div class="card-body">
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade @if (request('pills') == 'general') show active @endif" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">
            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{ route('profile.update', Auth()->user()->id) }}?pills=general">
              @method('put')
              @csrf 
              @role('admin|asatidz|asatidz_tsn|asatidz_mln|muaddib|mudir_tsn|mudir_mln|mudir_am')
                <div class="form-group row">
                  <label for="niat" class="col-sm-2 col-form-label text-sm">NIAT</label>
                  <div class="col-sm-10">
                    <input name="niat" type="text" class="form-control" id="niat" value="{{ old('niat') ? old('niat') : ($data->niat ?? '') }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nama" class="col-sm-2 col-form-label">Nama Lengkap*</label>
                  <div class="col-sm-10">
                    <input required name="nama" type="text" class="form-control  @error('nama') is-invalid @enderror" id="nama" value="{{ old('nama') ? old('nama') : Auth()->user()->name }}">
                    @error('nama')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                  <div class="col-sm-10">
                    <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" value="{{ old('jenis_kelamin') }}">
                      <option value="L" {{ empty($data->jenis_kelamin) ? null : (($data->jenis_kelamin == 'L') ? 'selected' : '') }}>Laki-laki</option>
                      <option value="P" {{ empty($data->jenis_kelamin) ? null : (($data->jenis_kelamin == 'P') ? 'selected' : '') }}>Perempuan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="status_menikah" class="col-sm-2 col-form-label">Status Pernikahan*</label>
                  <div class="col-sm-10">
                    <select name="status_menikah" id="status_menikah" class="form-control  @error('status_menikah') is-invalid @enderror" value="{{ old('status_menikah') ?? $data->status_menikah }}">
                        <option value="SINGLE" {{ $data->status_menikah == 'SINGLE' ? 'selected' : ''  }} {{ old('status_menikah') == 'SINGLE' ? 'selected' : '' }}>SINGLE</option>
                        <option value="MENIKAH" {{ $data->status_menikah == 'MENIKAH' ? 'selected' : ''  }} {{ old('status_menikah') == 'MENIKAH' ? 'selected' : '' }}>MENIKAH</option>
                        <option value="DUDA" {{ $data->status_menikah == 'DUDA' ? 'selected' : ''  }} {{ old('status_menikah') == 'DUDA' ? 'selected' : '' }}>DUDA</option>
                        <option value="JANDA" {{ $data->status_menikah == 'JANDA' ? 'selected' : ''  }} {{ old('status_menikah') == 'JANDA' ? 'selected' : '' }}>JANDA</option>
                    </select>
                    @error('status_menikah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="tsn" class="col-sm-2 col-form-label">SMP/MTS/TSANAWIYYAH*</label>
                  <div class="col-sm-10">
                    <input required name="tsn" type="text" class="form-control  @error('tsn') is-invalid @enderror" id="tsn" value="{{ old('tsn') ? old('tsn') : ($data->tsn ?? '') }}" placeholder="Masukkan Nama SMP/MTS/TSANAWIYYAH">
                    @error('tsn')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="mln" class="col-sm-2 col-form-label">SMA/SMK/MA/MUALLIMIEN*</label>
                  <div class="col-sm-10">
                    <input required name="mln" type="text" class="form-control  @error('mln') is-invalid @enderror" id="mln" value="{{ old('mln') ? old('mln') : ($data->mln ?? '') }}" placeholder="Masukkan Nama SMA/SMK/MA/MUALLIMIEN">
                    @error('mln')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="s1" class="col-sm-2 col-form-label">Asal S1 dan Jurusan</label>
                  <div class="col-sm-10">
                    <input name="s1" type="text" class="form-control  @error('s1') is-invalid @enderror" id="s1" value="{{ old('s1') ? old('s1') : ($data->s1 ?? '') }}" placeholder="Masukkan Asal S1 dan Jurusan">
                    @error('s1')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="s2" class="col-sm-2 col-form-label">Asal S2 dan Jurusan</label>
                  <div class="col-sm-10">
                    <input name="s2" type="text" class="form-control  @error('s2') is-invalid @enderror" id="s2" value="{{ old('s2') ? old('s2') : ($data->s2 ?? '') }}" placeholder="Masukkan Asal S2 dan Jurusan">
                    @error('s2')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="s3" class="col-sm-2 col-form-label">Asal S3 dan Jurusan</label>
                  <div class="col-sm-10">
                    <input name="s3" type="text" class="form-control  @error('s3') is-invalid @enderror" id="s3" value="{{ old('s3') ? old('s3') : ($data->s3  ?? '' )}}" placeholder="Masukkan Asal S3 dan Jurusan">
                    @error('s3')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pendidikan_terakhir" class="col-sm-2 col-form-label">Pendidikan Terakhir*</label>
                  <div class="col-sm-10">
                    <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control  @error('pendidikan_terakhir') is-invalid @enderror" value="{{ old('pendidikan_terakhir') ?? $data->pendidikan_terakhir }}">
                        <option value="SMA_SMK_MLN" {{ $data->pendidikan_terakhir == 'SMA_SMK_MLN' ? 'selected' : ''  }} {{ old('pendidikan_terakhir') == 'SMA_SMK_MLN' ? 'selected' : '' }}>SMA/SMK/MLN</option>
                        <option value="S1" {{ $data->pendidikan_terakhir == 'S1' ? 'selected' : ''  }} {{ old('pendidikan_terakhir') == 'S1' ? 'selected' : '' }}>S1</option>
                        <option value="S2" {{ $data->pendidikan_terakhir == 'S2' ? 'selected' : ''  }} {{ old('pendidikan_terakhir') == 'S2' ? 'selected' : '' }}>S2</option>
                        <option value="S3" {{ $data->pendidikan_terakhir == 'S3' ? 'selected' : ''  }} {{ old('pendidikan_terakhir') == 'S3' ? 'selected' : '' }}>S3</option>
                    </select>
                    @error('pendidikan_terakhir')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="uploadIjazah" class="col-sm-2 col-form-label">Upload Ijazah*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->upload_ijazah))
                    <div class="mb-2">
                      <label for="uploadIjazah" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit Ijazah</label>
                      <input type="file" name="uploadIjazah" id="uploadIjazah" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file-upload-ijazah"></div>
                      <iframe src="{{ Storage::disk('s3')->url('data/asatidz/ijazah/'.$data->upload_ijazah) }}" frameborder="0" height="100%" width="100%">
                      </iframe>
                    </div>
                    @else
                      <label for="uploadIjazah" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload Ijazah</label>
                      <input type="file" name="uploadIjazah" id="uploadIjazah" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file-upload-ijazah"></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="upload_kk" class="col-sm-2 col-form-label">Upload KK*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->upload_kk))
                      <div class="mb-2">
                        <label for="upload_kk" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit KK</label>
                        <input type="file" name="upload_kk" id="upload_kk" style="display:none;">
                        <div style="font-size: 12px;" class="text-success" id="file-upload-kk"></div>
                        <iframe src="{{ Storage::disk('s3')->url('data/asatidz/kartu_keluarga/'.$data->upload_kk) }}" frameborder="0" height="100%" width="100%">
                        </iframe>
                      </div>
                    @else
                      <label for="upload_kk" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload KK</label>
                      <input type="file" name="upload_kk" id="upload_kk" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file-upload-kk"></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="upload_akte" class="col-sm-2 col-form-label">Upload AKTE*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->upload_akte))
                      <div class="mb-2">
                        <label for="upload_akte" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit AKTE</label>
                        <input type="file" name="upload_akte" id="upload_akte" style="display:none;">
                        <div style="font-size: 12px;" class="text-success" id="file-upload-akte"></div>
                        <iframe src="{{ Storage::disk('s3')->url('data/asatidz/akte/'.$data->upload_akte) }}" frameborder="0" height="100%" width="100%">
                        </iframe>
                      </div>
                    @else
                      <label for="upload_akte" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload AKTE</label>
                      <input type="file" name="upload_akte" id="upload_akte" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file-upload-akte"></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="upload_ktp" class="col-sm-2 col-form-label">Upload KTP*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->upload_ktp))
                      <div class="mb-2">
                        <label for="upload_ktp" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit KTP</label>
                        <input type="file" name="upload_ktp" id="upload_ktp" style="display:none;">
                        <div style="font-size: 12px;" class="text-success" id="file-upload-ktp"></div>
                        <iframe src="{{ Storage::disk('s3')->url('data/asatidz/ktp/'.$data->upload_ktp) }}" frameborder="0" height="100%" width="100%">
                        </iframe>
                      </div>
                    @else
                      <label for="upload_ktp" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload KTP</label>
                      <input type="file" name="upload_ktp" id="upload_ktp" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file-upload-ktp"></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="upload_buku_rekening" class="col-sm-2 col-form-label">Upload Buku Rekening*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->upload_buku_rekening))
                      <div class="mb-2">
                        <label for="upload_buku_rekening" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit Buku Rekening</label>
                        <input type="file" name="upload_buku_rekening" id="upload_buku_rekening" style="display:none;">
                        <div style="font-size: 12px;" class="text-success" id="file-upload-buku-rekening"></div>
                        <iframe src="{{ Storage::disk('s3')->url('data/asatidz/buku_rekening/'.$data->upload_buku_rekening) }}" frameborder="0" height="100%" width="100%">
                        </iframe>
                      </div>
                    @else
                      <label for="upload_buku_rekening" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload Buku Rekening</label>
                      <input type="file" name="upload_buku_rekening" id="upload_buku_rekening" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file-upload-buku-rekening"></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_telp" class="col-sm-2 col-form-label">Nomor Telepon / HP*</label>
                  <div class="col-sm-10">
                    <input required name="no_telp" type="number" class="form-control  @error('no_telp') is-invalid @enderror" id="no_telp" value="{{ old('no_telp') ? old('no_telp') : ($data->no_telp ?? '') }}" placeholder="Masukkan Nomor Telepon / HP">
                    @error('no_telp')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir*</label>
                  <div class="col-sm-10">
                    <input required name="tempat_lahir" type="text" class="form-control  @error('tempat_lahir') is-invalid @enderror" id="tempat_lahir" value="{{ old('tempat_lahir') ? old('tempat_lahir') : ($data->tempat_lahir ?? '') }}" placeholder="Masukkan Tempat Lahir">
                    @error('tempat_lahir')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir*</label>
                  <div class="col-sm-10">
                    <input required name="tgl_lahir" type="date" class="form-control  @error('tgl_lahir') is-invalid @enderror" id="tgl_lahir" value="{{ old('tgl_lahir') ? old('tgl_lahir') : ($data->tgl_lahir ?? '') }}" placeholder="Masukkan Tanggal Lahir">
                    @error('tgl_lahir')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="hafalan" class="col-sm-2 col-form-label">Hafalan*</label>
                  <div class="col-sm-10">
                    <input required name="hafalan" type="text" class="form-control  @error('hafalan') is-invalid @enderror" id="hafalan" value="{{ old('hafalan') ? old('hafalan') : ($data->hafalan ?? '') }}" placeholder="Masukkan Hafalan. Contoh (30 Juz)">
                    @error('hafalan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="alamat" class="col-sm-2 col-form-label">Alamat*</label>
                  <div class="col-sm-10">
                    <textarea  name="alamat"  id="alamat" class="form-control  @error('alamat') is-invalid @enderror" placeholder="Masukkan Alamat rumah">{{ old('alamat')? old('alamat') :($data->alamat ?? '') }}</textarea>
                    @error('alamat')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
              @endrole

              @role('orangtua_tsn|orangtua_mln')
                <div class="form-group row">
                  <label for="nik" class="col-sm-2 col-form-label">NIK*</label>
                  <div class="col-sm-10">
                    <input  name="nik" type="text" class="form-control  @error('nik') is-invalid @enderror" id="nik" value="{{ old('nik') ? old('nik') :($data->nik ?? '') }}" placeholder="Masukkan NIK">
                    @error('nik')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_kartu_keluarga" class="col-sm-2 col-form-label">No KK*</label>
                  <div class="col-sm-10">
                    <input  name="no_kartu_keluarga" type="text" class="form-control  @error('no_kartu_keluarga') is-invalid @enderror" id="no_kartu_keluarga" value="{{ old('no_kartu_keluarga') ? old('no_kartu_keluarga') : ($data->nomor_kartu_keluarga ?? '') }}" placeholder="Masukkan no kartu keluarga">
                    @error('no_kartu_keluarga')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <label for="nama_lengkap_ayah" class="col-sm-2 col-form-label">Nama Ayah*</label>
                  <div class="col-sm-10">
                    <input  name="nama_lengkap_ayah" type="text" class="form-control  @error('nama_lengkap_ayah') is-invalid @enderror" id="nama_lengkap_ayah" value="{{ old('nama_lengkap_ayah') ? old('nama_lengkap_ayah') : ($data->nama_lengkap_ayah ?? '') }}" placeholder="Masukkan Nama Ayah">
                    @error('nama_lengkap_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="alamat" class="col-sm-2 col-form-label">Alamat*</label>
                  <div class="col-sm-10">
                    <textarea  name="alamat" type="text" class="form-control"  @error('alamat') is-invalid @enderror id="alamat" placeholder="Masukkan Alamat rumah">{{ old('alamat') ? old('alamat') : ($data->alamat ?? '') }}</textarea>
                    @error('alamat')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_ktp_ayah" class="col-sm-2 col-form-label">No KTP Ayah*</label>
                  <div class="col-sm-10">
                    <input  name="no_ktp_ayah" type="text" class="form-control  @error('no_ktp_ayah') is-invalid @enderror" id="no_ktp_ayah" value="{{ old('no_ktp_ayah') ? old('no_ktp_ayah') : ($data->no_ktp_ayah ?? '') }}" placeholder="Masukkan No KTP Ayah">
                    @error('no_ktp_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_hp_ayah" class="col-sm-2 col-form-label">No HP Ayah</label>
                  <div class="col-sm-10">
                    <input  name="no_hp_ayah" type="text" class="form-control  @error('no_hp_ayah') is-invalid @enderror" id="no_hp_ayah" value="{{ old('no_hp_ayah') ? old('no_hp_ayah') : ($data->no_hp_ayah ?? '') }}" placeholder="Masukkan No KTP Ayah">
                    @error('no_hp_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pendidikan_ayah" class="col-sm-2 col-form-label">Pendidikan Ayah*</label>
                  <div class="col-sm-10">
                    <input  name="pendidikan_ayah" type="text" class="form-control  @error('pendidikan_ayah') is-invalid @enderror" id="pendidikan_ayah" value="{{ old('pendidikan_ayah') ? old('pendidikan_ayah') : ($data->pendidikan_ayah ?? '') }}" placeholder="Masukkan Pekerjaan Ayah">
                    @error('pendidikan_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pekerjaan_ayah" class="col-sm-2 col-form-label">Pekerjaan Ayah*</label>
                  <div class="col-sm-10">
                    <input  name="pekerjaan_ayah" type="text" class="form-control  @error('pekerjaan_ayah') is-invalid @enderror" id="pekerjaan_ayah" value="{{ old('pekerjaan_ayah') ? old('pekerjaan_ayah') : ($data->pekerjaan_ayah ?? '') }}" placeholder="Masukkan Pekerjaan Ayah">
                    @error('pekerjaan_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="penghasilan_ayah" class="col-sm-2 col-form-label">Penghasilan Ayah*</label>
                  <div class="col-sm-10">
                    <select  name="penghasilan_ayah" class="form-control  @error('penghasilan_ayah') is-invalid @enderror" id="penghasilan_ayah" value="{{ old('penghasilan_ayah') ? old('penghasilan_ayah') : ($data->penghasilan_ayah ?? '') }}" placeholder="Masukkan Penghasilan Ayah, Contoh 5.000.000-7.000.000">
                    <option value = "1" {{ $data->penghasilan_ayah == 1 ? 'selected' : ''}}>< Rp.2.000.000 </option>
                    <option value = "2" {{ $data->penghasilan_ayah == 2 ? 'selected' : ''}}>Rp.2.000.000 - Rp. 3.000.000</option>
                    <option value = "3" {{ $data->penghasilan_ayah == 3 ? 'selected' : ''}}>Rp.3.100.000 - Rp. 4.000.000</option>
                    <option value = "4" {{ $data->penghasilan_ayah == 4 ? 'selected' : ''}}>Rp.4.100.000 - Rp. 5.000.000</option>
                    <option value = "5" {{ $data->penghasilan_ayah == 5 ? 'selected' : ''}} >> Rp.5.000.000</option>
                    @error('penghasilan_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nama_lengkap_ibu" class="col-sm-2 col-form-label">Nama Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="nama_lengkap_ibu" type="text" class="form-control  @error('nama_lengkap_ibu') is-invalid @enderror" id="nama_lengkap_ibu" value="{{ old('nama_lengkap_ibu') ? old('nama_lengkap_ibu') : ($data->nama_lengkap_ibu ?? '') }}" placeholder="Masukan nama Ibu">
                    @error('nama_lengkap_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_ktp_ibu" class="col-sm-2 col-form-label">NO KTP Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="no_ktp_ibu" type="text" class="form-control  @error('no_ktp_ibu') is-invalid @enderror" id="no_ktp_ibu" value="{{ old('no_ktp_ibu') ? old('no_ktp_ibu') : ($data->no_ktp_ibu ?? '') }}" placeholder="Masukkan KTP Ibu">
                    @error('no_ktp_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_hp_ibu" class="col-sm-2 col-form-label">NO HP Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="no_hp_ibu" type="text" class="form-control  @error('no_hp_ibu') is-invalid @enderror" id="no_hp_ibu" value="{{ old('no_hp_ibu')? old('no_hp_ibu') : ($data->no_hp_ibu ?? '') }}" placeholder="Masukkan No HP Ibu">
                    @error('no_hp_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                  @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pendidikan_ibu" class="col-sm-2 col-form-label">Pendidikan Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="pendidikan_ibu" type="text" class="form-control  @error('pendidikan_ibu') is-invalid @enderror" id="pendidikan_ibu" value="{{ old('pendidikan_ibu') ? old('pendidikan_ibu') : ($data->pendidikan_ibu ?? '') }}" placeholder="Masukkan Pekerjaan Ayah">
                    @error('pendidikan_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pekerjaan_ibu" class="col-sm-2 col-form-label">Pekerjaan Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="pekerjaan_ibu" type="text" class="form-control  @error('pekerjaan_ibu') is-invalid @enderror" id="pekerjaan_ibu" value="{{ old('pekerjaan_ibu') ? old('pekerjaan_ibu') : ($data->pekerjaan_ibu ?? '') }}" placeholder="Masukkan Pekerjaan Ibu">
                    @error('pekerjaan_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="penghasilan_ibu" class="col-sm-2 col-form-label">Penghasilan Ibu*</label>
                  <div class="col-sm-10">
                    <select  name="penghasilan_ibu" class="form-control  @error('penghasilan_ibu') is-invalid @enderror" id="penghasilan_ibu" value="{{ old('penghasilan_ibu') ? old('penghasilan_ibu') : ($data->penghasilan_ibu ?? '') }}" placeholder="Masukkan Penghasilan Ibu, Contoh 5.000.000-7.000.000">
                    <option value = "1" {{ $data->penghasilan_ibu == 1 ? 'selected' : ''}}>< Rp.2.000.000 </option>
                    <option value = "2" {{ $data->penghasilan_ibu == 2 ? 'selected' : ''}}>Rp.2.000.000 - Rp. 3.000.000</option>
                    <option value = "3" {{ $data->penghasilan_ibu == 3 ? 'selected' : ''}}>Rp.3.100.000 - Rp. 4.000.000</option>
                    <option value = "4" {{ $data->penghasilan_ibu == 4 ? 'selected' : ''}}>Rp.4.100.000 - Rp. 5.000.000</option>
                    <option value = "5" {{ $data->penghasilan_ibu == 5 ? 'selected' : ''}} >> Rp.5.000.000</option>
                    @error('penghasilan_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                    </select>
                  </div>
                </div>
                <div class="col-md-12 mb-2">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="dataWali">
                        <label class="form-check-label" for="dataWali">Tampilkan Data Wali</label>
                    </div>
                </div>
                <hr>
                <div class="hilang">
                  <div class="form-group row">
                    <label for="nama_lengkap_wali" class="col-sm-2 col-form-label">Nama wali</label>
                    <div class="col-sm-10">
                      <input name="nama_lengkap_wali" type="text" class="form-control  @error('nama_lengkap_wali') is-invalid @enderror" id="nama_lengkap_wali" value="{{ $data->nama_lengkap_wali ?? '' }}" placeholder="Masukkan Nama wali">
                      @error('nama_lengkap_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="no_ktp_wali" class="col-sm-2 col-form-label">No KTP Wali</label>
                    <div class="col-sm-10">
                      <input name="no_ktp_wali" type="text" class="form-control  @error('no_ktp_wali') is-invalid @enderror" id="no_ktp_wali" value="{{ $data->no_ktp_wali ?? '' }}" placeholder="Masukkan No KTP Wali">
                      @error('no_ktp_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="no_hp_wali" class="col-sm-2 col-form-label">No HP Wali</label>
                    <div class="col-sm-10">
                      <input name="no_hp_wali" type="text" class="form-control  @error('no_hp_wali') is-invalid @enderror" id="no_hp_wali" value="{{ $data->no_hp_wali ?? '' }}" placeholder="Masukkan No KTP Wali">
                      @error('no_hp_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="pendidikan_wali" class="col-sm-2 col-form-label">Pendidikan Wali</label>
                    <div class="col-sm-10">
                      <input name="pendidikan_wali" type="text" class="form-control  @error('pendidikan_wali') is-invalid @enderror" id="pendidikan_wali" value="{{ $data->pendidikan_wali ?? '' }}" placeholder="Masukkan Pekerjaan Wali">
                      @error('pendidikan_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="pekerjaan_wali" class="col-sm-2 col-form-label">Pekerjaan Wali</label>
                    <div class="col-sm-10">
                      <input name="pekerjaan_wali" type="text" class="form-control  @error('pekerjaan_wali') is-invalid @enderror" id="pekerjaan_wali" value="{{ $data->pekerjaan_wali ?? '' }}" placeholder="Masukkan Pekerjaan Wali">
                      @error('pekerjaan_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                </div>
              @endrole

              @role('santri_tsn|santri_mln')
                <div class="form-group row">
                  <label for="nama" class="col-sm-2 col-form-label">Nama Lengkap*</label>
                  <div class="col-sm-10">
                    <input required name="nama_lengkap" type="text" class="form-control  @error('nama') is-invalid @enderror" id="nama" value="{{ $data->nama_lengkap }}">
                    @error('nama')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nis" class="col-sm-2 col-form-label">NIS</label>
                  <div class="col-sm-10">
                    <input readonly name="nis" type="text" class="form-control" id="nis" value="{{ $data->nis }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nisn" class="col-sm-2 col-form-label">NISN</label>
                  <div class="col-sm-10">
                    <input name="nisn" type="text" class="form-control" id="nisn" value="{{ $data->nisn }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nism" class="col-sm-2 col-form-label">NISM</label>
                  <div class="col-sm-10">
                    <input readonly name="nism" type="text" class="form-control" id="nism" value="{{ $data->nism }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="kelas" class="col-sm-2 col-form-label">Kelas</label>
                  <div class="col-sm-10">
                    <input readonly name="kelas" type="text" class="form-control" id="kelas" value="{{ $data->kelas }} - {{ $data->tingkat_sekolah_id }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="jk" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                  <div class="col-sm-10">
                    <select name="jk" id="jk" class="form-control">
                      <option value="RG" {{ $data->jk == 'RG' ? 'selected':'' }}>Laki-laki</option>
                      <option value="UG" {{ $data->jk == 'UG' ? 'selected':'' }}>Perempuan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir*</label>
                  <div class="col-sm-10">
                    <input name="tempat_lahir" type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" id="tempat_lahir" value="{{ $data->tempat_lahir }}" placeholder="Masukkan Tempat Lahir">
                    @error('tempat_lahir')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="tanggal_lahir" class="col-sm-2 col-form-label">Tanggal Lahir*</label>
                  <div class="col-sm-10">
                    <input  name="tanggal_lahir" type="date" class="form-control  @error('tanggal_lahir') is-invalid @enderror" id="tanggal_lahir" value="{{ $data->tanggal_lahir, date('Y-m-d') }}" placeholder="Masukkan Tanggal Lahir">
                    @error('tanggal_lahir')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="kota" class="col-sm-2 col-form-label">Kota*</label>
                  <div class="col-sm-10">
                    <input  name="kota" type="text" class="form-control  @error('kota') is-invalid @enderror" id="kota" value="{{ $data->alamat_kota }}" placeholder="Masukkan Alamat Kota">
                    @error('kota')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="alamat" class="col-sm-2 col-form-label">Alamat*</label>
                  <div class="col-sm-10">
                    <textarea  name="alamat" type="text" class="form-control"  @error('alamat') is-invalid @enderror id="alamat" placeholder="Masukkan Alamat rumah">{{ $data->alamat }}</textarea>
                    @error('alamat')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_hp" class="col-sm-2 col-form-label">Nomor Telepon / HP*</label>
                  <div class="col-sm-10">
                    <input  name="no_hp" type="number" class="form-control  @error('no_hp') is-invalid @enderror" id="no_hp" value="{{ $data->no_hp }}" placeholder="Masukkan Nomor Telepon / HP">
                    @error('no_hp')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <label for="asal_sekolah" class="col-sm-2 col-form-label">Asal Sekolah</label>
                  <div class="col-sm-10">
                    <input name="asal_sekolah" type="text" class="form-control  @error('asal_sekolah') is-invalid @enderror" id="asal_sekolah" value="{{ $data->asal_sekolah }}" placeholder="Masukkan Asal Sekolah">
                    @error('asal_sekolah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="alamat_asal_sekolah" class="col-sm-2 col-form-label">Alamat Asal Sekolah</label>
                  <div class="col-sm-10">
                    <textarea name="alamat_asal_sekolah" class="form-control  @error('alamat_asal_sekolah') is-invalid @enderror" id="alamat_asal_sekolah" placeholder="Masukkan Alamat Asal Sekolah">{{ $data->alamat_asal_sekolah }}</textarea>
                    @error('alamat_asal_sekolah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="upload_photo_santri" class="col-sm-2 col-form-label">Upload Photo Formal*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->path_foto_santri))
                    <div class="mb-2">
                      <label for="upload_photo_santri" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit Photo Formal</label>
                      <input type="file" name="upload_photo_santri" id="upload_photo_santri" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file_upload_photo_santri"></div>
                      <iframe src="{{ Storage::disk('s3')->url('data/santri/photo_formal/'.$data->path_foto_santri) }}" frameborder="0" height="100%" width="100%">
                      </iframe>
                    </div>
                    @else
                      <label for="upload_photo_santri" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload Photo Formal</label>
                      <input type="file" name="upload_photo_santri" id="upload_photo_santri" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file_upload_photo_santri"></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="upload_akte" class="col-sm-2 col-form-label">Upload Akte*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->path_akte))
                    <div class="mb-2">
                      <label for="upload_akte" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit Akte</label>
                      <input type="file" name="upload_akte" id="upload_akte" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file_upload_akte"></div>
                      <iframe src="{{ Storage::disk('s3')->url('data/santri/akte/'.$data->path_akte) }}" frameborder="0" height="100%" width="100%">
                      </iframe>
                    </div>
                    @else
                      <label for="upload_akte" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload Akte</label>
                      <input type="file" name="upload_akte" id="upload_akte" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file_upload_akte"></div>
                    @endif
                  </div>
                </div>
                @role('santri_tsn')
                <div class="form-group row">
                  <label for="uploadIjazah" class="col-sm-2 col-form-label">Upload Ijazah SD/MI*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->path_ijazah_sd))
                    <div class="mb-2">
                      <label for="upload_ijazah_sd" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit Ijazah SD/MI</label>
                      <input type="file" name="uploadIjazahSD" id="upload_ijazah_sd" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file_upload_ijazah_sd"></div>
                      <iframe src="{{ Storage::disk('s3')->url('data/santri/ijazah-sd/'.$data->path_ijazah_sd) }}" frameborder="0" height="100%" width="100%">
                      </iframe>
                    </div>
                    @else
                      <label for="upload_ijazah_sd" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload Ijazah SD/MI</label>
                      <input type="file" name="uploadIjazahSD" id="upload_ijazah_sd" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file_upload_ijazah_sd"></div>
                    @endif
                  </div>
                </div>
                @endrole
                @role('santri_mln')
                <div class="form-group row">
                  <label for="uploadIjazah" class="col-sm-2 col-form-label">Upload Ijazah SMP/MTS*</label>
                  <div class="col-sm-10">
                    @if(!empty($data->path_ijazah_mts))
                    <div class="mb-2">
                      <label for="upload_ijazah_mts" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Edit Ijazah SMP/MTS</label>
                      <input type="file" name="uploadIjazahMTS" id="upload_ijazah_mts" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file_upload_ijazah_mts"></div>
                      <iframe src="{{ Storage::disk('s3')->url('data/santri/ijazah-mts/'.$data->path_ijazah_mts) }}" frameborder="0" height="100%" width="100%">
                      </iframe>
                    </div>
                    @else
                      <label for="upload_ijazah_mts" style="cursor: pointer;" class="w-100 py-2 text-center rounded badge-success mt-2">Upload Ijazah SMP/MTS</label>
                      <input type="file" name="uploadIjazahMTS" id="upload_ijazah_mts" style="display:none;">
                      <div style="font-size: 12px;" class="text-success" id="file_upload_ijazah_mts"></div>
                    @endif
                  </div>
                </div>
                @endrole
                <div class="form-group row">
                  <label for="hafalan" class="col-sm-2 col-form-label">Hafalan Qur'an</label>
                  <div class="col-sm-10">
                    <input type="text" value="{{ $data->hafalan }}" name="hafalan" class="form-control  @error('hafalan') is-invalid @enderror" id="hafalan" placeholder="Masukkan Hafalan antum (contoh: 30 juz)">
                    @error('hafalan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                    @if(count($data->prestasi) > 2)
                      <div class="mr-2 ml-auto">
                          <button type="button" class="btn btn-success btn-sm" id="tambahPrestasi" disabled>Tambah Prestasi</button>
                      </div>
                    @else
                      <div class="mr-2 ml-auto">
                        <button type="button" class="btn btn-success btn-sm" id="tambahPrestasi" >Tambah Prestasi</button>
                      </div>
                    @endif
                </div>
                <div id="kolomPrestasiContainer">
                    @if(empty($data->prestasi))
                      <div class="form-group row">
                          <label for="jenis_prestasi" class="col-sm-2 col-form-label">Jenis Prestasi</label>
                          <div class="col-sm-10">
                              <textarea name="jenis_prestasi[]" required class="form-control @error('jenis_prestasi') is-invalid @enderror" id="jenis_prestasi" placeholder="Masukkan Jenis Prestasi">{{ $prestasi['jenis_prestasi'] ?? '' }}</textarea>
                              @error('jenis_prestasi')
                                  <div class="invalid-feedback">
                                      {{ $message }}
                                  </div>
                              @enderror
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="tingkat" class="col-sm-2 col-form-label">Tingkat Prestasi</label>
                          <div class="col-sm-10">
                              <input type="text" name="tingkat_prestasi[]" required value="{{ $prestasi['tingkat'] ?? '' }}" class="form-control @error('tingkat') is-invalid @enderror" id="tingkat" placeholder="Masukkan Tingkat Prestasi">
                              @error('tingkat')
                                  <div class="invalid-feedback">
                                      {{ $message }}
                                  </div>
                              @enderror
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="tahun" class="col-sm-2 col-form-label">Tahun Prestasi</label>
                          <div class="col-sm-10">
                              <select name="tahun_prestasi[]" required id="" class="form-control">
                                @php
                                  $tahun = 2010;
                                @endphp
                                @for ($tahun; $tahun <= date('Y'); $tahun++)
                                  <option value="{{ $tahun }}" {{ isset($prestasi['tahun']) == $tahun ? 'selected' : '' }}>{{ $tahun }}</option>
                                @endfor
                              </select>
                              @error('tahun')
                                  <div class="invalid-feedback">
                                      {{ $message }}
                                  </div>
                              @enderror
                          </div>
                      </div>
                      <hr>
                    @else
                      @foreach($data->prestasi as $key => $prestasi)
                        <div class="form-group row">
                            <label for="jenis_prestasi" class="col-sm-2 col-form-label">Jenis Prestasi</label>
                            <div class="col-sm-10">
                                <textarea name="jenis_prestasi[]" class="form-control @error('jenis_prestasi') is-invalid @enderror" id="jenis_prestasi" placeholder="Masukkan Jenis Prestasi">{{ $prestasi['jenis_prestasi'] ?? '' }}</textarea>
                                @error('jenis_prestasi')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tingkat" class="col-sm-2 col-form-label">Tingkat Prestasi</label>
                            <div class="col-sm-10">
                                <input type="text" name="tingkat_prestasi[]" value="{{ $prestasi['tingkat'] ?? '' }}" class="form-control @error('tingkat') is-invalid @enderror" id="tingkat" placeholder="Masukkan Tingkat Prestasi">
                                @error('tingkat')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tahun" class="col-sm-2 col-form-label">Tahun Prestasi</label>
                            <div class="col-sm-10">
                                <select name="tahun_prestasi[]" id="" class="form-control">
                                  @php
                                    $tahun = 2010;
                                  @endphp
                                  <option value="{{ $tahun }}" {{ isset($prestasi['tahun']) == $tahun ? 'selected' : '' }}>{{ $prestasi['tahun'] }}</option>
                                  <option value="" disabled>------</option>
                                  @for ($tahun; $tahun <= date('Y'); $tahun++)
                                    <option value="{{ $tahun }}">{{ $tahun }}</option>
                                  @endfor
                                </select>
                                @error('tahun')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <hr>
                      @endforeach
                    @endif
                </div>
              @endrole

              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Update Data</button>
                </div>
              </div>
            </form>
          </div>

          @role('santri_tsn|santri_mln')
          <div class="tab-pane fade @if (request('pills') == 'dataOrtu') show active @endif" id="pills-dataortu" role="tabpanel" aria-labelledby="pills-dataortu-tab">
            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{ route('profile.update', Auth()->user()->id) }}?pills=dataortu">
              @method('put') @csrf 
                <div class="form-group row">
                  <label for="nik" class="col-sm-2 col-form-label">NIK*</label>
                  <div class="col-sm-10">
                    <input  name="nik" type="text" class="form-control  @error('nik') is-invalid @enderror" id="nik" value="{{ old('nik') ? old('nik') :($data->nik ?? '') }}" placeholder="Masukkan NIK">
                    @error('nik')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_kartu_keluarga" class="col-sm-2 col-form-label">No KK*</label>
                  <div class="col-sm-10">
                    <input  name="no_kartu_keluarga" type="text" class="form-control  @error('no_kartu_keluarga') is-invalid @enderror" id="no_kartu_keluarga" value="{{ old('no_kartu_keluarga') ? old('no_kartu_keluarga') : ($data->nomor_kartu_keluarga ?? '') }}" placeholder="Masukkan no kartu keluarga">
                    @error('no_kartu_keluarga')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <label for="nama_lengkap_ayah" class="col-sm-2 col-form-label">Nama Ayah*</label>
                  <div class="col-sm-10">
                    <input  name="nama_lengkap_ayah" type="text" class="form-control  @error('nama_lengkap_ayah') is-invalid @enderror" id="nama_lengkap_ayah" value="{{ old('nama_lengkap_ayah') ? old('nama_lengkap_ayah') : ($data->nama_lengkap_ayah ?? '') }}" placeholder="Masukkan Nama Ayah">
                    @error('nama_lengkap_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="alamat" class="col-sm-2 col-form-label">Alamat*</label>
                  <div class="col-sm-10">
                    <textarea  name="alamat" type="text" class="form-control"  @error('alamat') is-invalid @enderror id="alamat" placeholder="Masukkan Alamat rumah">{{ old('alamat') ? old('alamat') : ($data->alamat ?? '') }}</textarea>
                    @error('alamat')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_ktp_ayah" class="col-sm-2 col-form-label">No KTP Ayah*</label>
                  <div class="col-sm-10">
                    <input  name="no_ktp_ayah" type="text" class="form-control  @error('no_ktp_ayah') is-invalid @enderror" id="no_ktp_ayah" value="{{ old('no_ktp_ayah') ? old('no_ktp_ayah') : ($data->no_ktp_ayah ?? '') }}" placeholder="Masukkan No KTP Ayah">
                    @error('no_ktp_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_hp_ayah" class="col-sm-2 col-form-label">No HP Ayah</label>
                  <div class="col-sm-10">
                    <input  name="no_hp_ayah" type="text" class="form-control  @error('no_hp_ayah') is-invalid @enderror" id="no_hp_ayah" value="{{ old('no_hp_ayah') ? old('no_hp_ayah') : ($data->no_hp_ayah ?? '') }}" placeholder="Masukkan No KTP Ayah">
                    @error('no_hp_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pendidikan_ayah" class="col-sm-2 col-form-label">Pendidikan Ayah*</label>
                  <div class="col-sm-10">
                    <input  name="pendidikan_ayah" type="text" class="form-control  @error('pendidikan_ayah') is-invalid @enderror" id="pendidikan_ayah" value="{{ old('pendidikan_ayah') ? old('pendidikan_ayah') : ($data->pendidikan_ayah ?? '') }}" placeholder="Masukkan Pekerjaan Ayah">
                    @error('pendidikan_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pekerjaan_ayah" class="col-sm-2 col-form-label">Pekerjaan Ayah*</label>
                  <div class="col-sm-10">
                    <input  name="pekerjaan_ayah" type="text" class="form-control  @error('pekerjaan_ayah') is-invalid @enderror" id="pekerjaan_ayah" value="{{ old('pekerjaan_ayah') ? old('pekerjaan_ayah') : ($data->pekerjaan_ayah ?? '') }}" placeholder="Masukkan Pekerjaan Ayah">
                    @error('pekerjaan_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="penghasilan_ayah" class="col-sm-2 col-form-label">Penghasilan Ayah*</label>
                  <div class="col-sm-10">
                    <select  name="penghasilan_ayah" class="form-control  @error('penghasilan_ayah') is-invalid @enderror" id="penghasilan_ayah" value="{{ old('penghasilan_ayah') ? old('penghasilan_ayah') : ($data->penghasilan_ayah ?? '') }}" placeholder="Masukkan Penghasilan Ayah, Contoh 5.000.000-7.000.000">
                    <option value = "1" {{ $data->penghasilan_ayah == 1 ? 'selected' : ''}}>< Rp.2.000.000 </option>
                    <option value = "2" {{ $data->penghasilan_ayah == 2 ? 'selected' : ''}}>Rp.2.000.000 - Rp. 3.000.000</option>
                    <option value = "3" {{ $data->penghasilan_ayah == 3 ? 'selected' : ''}}>Rp.3.100.000 - Rp. 4.000.000</option>
                    <option value = "4" {{ $data->penghasilan_ayah == 4 ? 'selected' : ''}}>Rp.4.100.000 - Rp. 5.000.000</option>
                    <option value = "5" {{ $data->penghasilan_ayah == 5 ? 'selected' : ''}} >> Rp.5.000.000</option>
                    @error('penghasilan_ayah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nama_lengkap_ibu" class="col-sm-2 col-form-label">Nama Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="nama_lengkap_ibu" type="text" class="form-control  @error('nama_lengkap_ibu') is-invalid @enderror" id="nama_lengkap_ibu" value="{{ old('nama_lengkap_ibu') ? old('nama_lengkap_ibu') : ($data->nama_lengkap_ibu ?? '') }}" placeholder="Masukan nama Ibu">
                    @error('nama_lengkap_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_ktp_ibu" class="col-sm-2 col-form-label">NO KTP Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="no_ktp_ibu" type="text" class="form-control  @error('no_ktp_ibu') is-invalid @enderror" id="no_ktp_ibu" value="{{ old('no_ktp_ibu') ? old('no_ktp_ibu') : ($data->no_ktp_ibu ?? '') }}" placeholder="Masukkan KTP Ibu">
                    @error('no_ktp_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="no_hp_ibu" class="col-sm-2 col-form-label">NO HP Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="no_hp_ibu" type="text" class="form-control  @error('no_hp_ibu') is-invalid @enderror" id="no_hp_ibu" value="{{ old('no_hp_ibu')? old('no_hp_ibu') : ($data->no_hp_ibu ?? '') }}" placeholder="Masukkan No HP Ibu">
                    @error('no_hp_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                  @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pendidikan_ibu" class="col-sm-2 col-form-label">Pendidikan Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="pendidikan_ibu" type="text" class="form-control  @error('pendidikan_ibu') is-invalid @enderror" id="pendidikan_ibu" value="{{ old('pendidikan_ibu') ? old('pendidikan_ibu') : ($data->pendidikan_ibu ?? '') }}" placeholder="Masukkan Pekerjaan Ayah">
                    @error('pendidikan_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pekerjaan_ibu" class="col-sm-2 col-form-label">Pekerjaan Ibu*</label>
                  <div class="col-sm-10">
                    <input  name="pekerjaan_ibu" type="text" class="form-control  @error('pekerjaan_ibu') is-invalid @enderror" id="pekerjaan_ibu" value="{{ old('pekerjaan_ibu') ? old('pekerjaan_ibu') : ($data->pekerjaan_ibu ?? '') }}" placeholder="Masukkan Pekerjaan Ibu">
                    @error('pekerjaan_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                  <label for="penghasilan_ibu" class="col-sm-2 col-form-label">Penghasilan Ibu*</label>
                  <div class="col-sm-10">
                    <select  name="penghasilan_ibu" class="form-control  @error('penghasilan_ibu') is-invalid @enderror" id="penghasilan_ibu" value="{{ old('penghasilan_ibu') ? old('penghasilan_ibu') : ($data->penghasilan_ibu ?? '') }}" placeholder="Masukkan Penghasilan Ibu, Contoh 5.000.000-7.000.000">
                    <option value = "1" {{ $data->penghasilan_ibu == 1 ? 'selected' : ''}}>< Rp.2.000.000 </option>
                    <option value = "2" {{ $data->penghasilan_ibu == 2 ? 'selected' : ''}}>Rp.2.000.000 - Rp. 3.000.000</option>
                    <option value = "3" {{ $data->penghasilan_ibu == 3 ? 'selected' : ''}}>Rp.3.100.000 - Rp. 4.000.000</option>
                    <option value = "4" {{ $data->penghasilan_ibu == 4 ? 'selected' : ''}}>Rp.4.100.000 - Rp. 5.000.000</option>
                    <option value = "5" {{ $data->penghasilan_ibu == 5 ? 'selected' : ''}} >> Rp.5.000.000</option>
                    @error('penghasilan_ibu')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                    </select>
                  </div>
                </div>
                <div class="col-md-12 mb-2">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="dataWali">
                        <label class="form-check-label" for="dataWali">Tampilkan Data Wali</label>
                    </div>
                </div>
                <hr>
                <div class="hilang">
                  <div class="form-group row">
                    <label for="nama_lengkap_wali" class="col-sm-2 col-form-label">Nama wali</label>
                    <div class="col-sm-10">
                      <input name="nama_lengkap_wali" type="text" class="form-control  @error('nama_lengkap_wali') is-invalid @enderror" id="nama_lengkap_wali" value="{{ $data->nama_lengkap_wali ?? '' }}" placeholder="Masukkan Nama wali">
                      @error('nama_lengkap_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="no_ktp_wali" class="col-sm-2 col-form-label">No KTP Wali</label>
                    <div class="col-sm-10">
                      <input name="no_ktp_wali" type="text" class="form-control  @error('no_ktp_wali') is-invalid @enderror" id="no_ktp_wali" value="{{ $data->no_ktp_wali ?? '' }}" placeholder="Masukkan No KTP Wali">
                      @error('no_ktp_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="no_hp_wali" class="col-sm-2 col-form-label">No HP Wali</label>
                    <div class="col-sm-10">
                      <input name="no_hp_wali" type="text" class="form-control  @error('no_hp_wali') is-invalid @enderror" id="no_hp_wali" value="{{ $data->no_hp_wali ?? '' }}" placeholder="Masukkan No KTP Wali">
                      @error('no_hp_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="pendidikan_wali" class="col-sm-2 col-form-label">Pendidikan Wali</label>
                    <div class="col-sm-10">
                      <input name="pendidikan_wali" type="text" class="form-control  @error('pendidikan_wali') is-invalid @enderror" id="pendidikan_wali" value="{{ $data->pendidikan_wali ?? '' }}" placeholder="Masukkan Pekerjaan Wali">
                      @error('pendidikan_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="pekerjaan_wali" class="col-sm-2 col-form-label">Pekerjaan Wali</label>
                    <div class="col-sm-10">
                      <input name="pekerjaan_wali" type="text" class="form-control  @error('pekerjaan_wali') is-invalid @enderror" id="pekerjaan_wali" value="{{ $data->pekerjaan_wali ?? '' }}" placeholder="Masukkan Pekerjaan Wali">
                      @error('pekerjaan_wali')
                      <div class="invalid-feedback">
                          {{ $message }}
                      </div>
                      @enderror
                    </div>
                  </div>
                </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Update Data</button>
                </div>
              </div>
            </form>
          </div>
          @endrole

          <div class="tab-pane fade @if (request('pills') == 'account') show active @endif" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <form method="post" class="form-horizontal" action="{{ route('profile.update', Auth()->user()->id) }}?pills=account">
              @method('put')
              @csrf
              <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Email*</label>
                <div class="col-sm-10">
                  <input required name="email" type="email" class="form-control  @error('email') is-invalid @enderror" id="email" value="{{ Auth()->user()->email }}" placeholder="Masukkan Email">
                  @error('email')
                  <div class="invalid-feedback">
                      {{ $message }}
                  </div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="username" class="col-sm-2 col-form-label">Username*</label>
                <div class="col-sm-10">
                  <input  required name="username" type="text" class="form-control  @error('username') is-invalid @enderror" id="username" value="{{ Auth()->user()->username }}">
                  @error('username')
                  <div class="invalid-feedback">
                      {{ $message }}
                  </div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Update Account</button>
                </div>
              </div>
            </form>
          </div>

          <div class="tab-pane fade @if (request('pills') == 'password') show active @endif" id="pills-password" role="tabpanel" aria-labelledby="pills-password-tab">
            <form method="post" action="{{ route('profile.changePassword') }}" class="form-horizontal">
              @csrf
              <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Password Baru</label>
                <div class="col-sm-10">
                  <input name="password" type="password" class="form-control  @error('password') is-invalid @enderror" id="password" placeholder="Masukkan Password Baru">
                  @error('password')
                  <div class="invalid-feedback">
                      {{ $message }}
                  </div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label for="password-confirm" class="col-sm-2 col-form-label">Konfrimasi Password Baru</label>
                <div class="col-sm-10">
                  <input name="password_confirmation" type="password" class="form-control  @error('password') is-invalid @enderror" id="password-confirm" placeholder="Masukkan Konfirmasi Password Baru">
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Update Password</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@push('scripts')
<script>
  var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
  // check if .profile-rounded-img potrait or landscape
  let img = document.querySelector('.profile-rounded-img');

  if (img.naturalWidth > img.naturalHeight) {
    img.style.height = '100%';
    // center the image
    img.style.left = '25%';
    img.style.transform = 'translateX(-21%)';
  } else {
    img.style.width = '100%';
  }
  @role('admin|asatidz|asatidz_tsn|asatidz_mln|muaddib|mudir_tsn|mudir_mln|mudir_am')
  var input1 = document.getElementById( 'uploadIjazah' );
  var infoArea1 = document.getElementById( 'file-upload-ijazah' );
  input1.addEventListener( 'change', showFileName1 );

  function showFileName1( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea1.textContent = 'Anda telah memasukan file : ' + fileName;
  }

  var input2 = document.getElementById( 'upload_kk' );
  var infoArea2 = document.getElementById( 'file-upload-kk' );
  input2.addEventListener( 'change', showFileName2 ); 
  function showFileName2( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea2.textContent = 'Anda telah memasukan file : ' + fileName;
  }

  var input3 = document.getElementById( 'upload_akte' );
  var infoArea3 = document.getElementById( 'file-upload-akte' );
  input3.addEventListener( 'change', showFileName3 ); 
  function showFileName3( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea3.textContent = 'Anda telah memasukan file : ' + fileName;
  }

  var input4 = document.getElementById( 'upload_ktp' );
  var infoArea4 = document.getElementById( 'file-upload-ktp' );
  input4.addEventListener( 'change', showFileName4 ); 
  function showFileName4( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea4.textContent = 'Anda telah memasukan file : ' + fileName;
  }

  var input5 = document.getElementById( 'upload_buku_rekening' );
  var infoArea5 = document.getElementById( 'file-upload-buku-rekening' );
  input5.addEventListener( 'change', showFileName5 ); 
  function showFileName5( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea5.textContent = 'Anda telah memasukan file : ' + fileName;
  }
  @endrole

  @role('santri_tsn')
  var input6 = document.getElementById( 'upload_ijazah_sd' );
  var infoArea6 = document.getElementById( 'file_upload_ijazah_sd' );
  input6.addEventListener( 'change', showFileName6 ); 
  function showFileName6( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea6.textContent = 'Anda telah memasukan file : ' + fileName;
  }
  @endrole

  @role('santri_mln')
  var input7 = document.getElementById( 'upload_ijazah_mts' );
  var infoArea7 = document.getElementById( 'file_upload_ijazah_mts' );
  input7.addEventListener( 'change', showFileName7 ); 
  function showFileName7( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea7.textContent = 'Anda telah memasukan file : ' + fileName;
  }
  @endrole

  @role('santri_tsn|santri_mln')
  var input8 = document.getElementById( 'upload_akte' );
  var infoArea8 = document.getElementById( 'file_upload_akte' );
  input8.addEventListener( 'change', showFileName8 ); 
  function showFileName8( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea8.textContent = 'Anda telah memasukan file : ' + fileName;
  }

  var input9 = document.getElementById( 'upload_photo_santri' );
  var infoArea9 = document.getElementById( 'file_upload_photo_santri' );
  input9.addEventListener( 'change', showFileName9 ); 
  function showFileName9( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea9.textContent = 'Anda telah memasukan file : ' + fileName;
  }

  $('body').on('click', '#klikGeneral', function() {
    location.href = "{{ route('profile.index') }}?pills=general";
  })
  $('body').on('click', '#klikDataOrtu', function() {
    location.href = "{{ route('profile.index') }}?pills=dataOrtu";
  })
  @endrole

  @role('orangtua_tsn|orangtua_mln')
  $('body').on('click', '#klikGeneral', function() {
    location.href = "{{ route('profile.index') }}?pills=general";
  })
  @endrole

  $('body').on('click', '#klikProfile', function() {
    location.href = "{{ route('profile.index') }}?pills=account";
  })

  $('body').on('click', '#klikPassword', function() {
    location.href = "{{ route('profile.index') }}?pills=password";
  })

  $('#dataWali').click(function (event) {
      // $('#password').removeAttr('disabled', false)
      if (this.checked) {
          $('.hilang').toggle()
      }else{
          $('.hilang').toggle()
      }
  })

  window.addEventListener('load', function () {
    document.querySelector('input[type="file"]').addEventListener('change', function () {

      var file = $('#inputFile')[0].files;
      // // Append data 
      var fd = new FormData();

      fd.append('image', file[0]);
      fd.append('_token', CSRF_TOKEN);

      // AJAX request 
      $.ajax({
          url: "{{ route('profile.uploadfile', Auth()->user()->id) }}",
          method: 'POST',
          data: fd,
          processData: false,
          contentType: false,
          dataType: 'json',
          success: function(response){                
            if(response.success == true){ // Uploaded successfully
              // Response message
              toastr.success('{{ session('response.message') }}', response.message); 
              setTimeout(() => {
                window.location.href = response.url;
              }, 3000);
            }
          },
          error: function(response){
            if(response.success == false){ // Uploaded successfully
              // Response message
              toastr.error('{{ session('response.message') }}', response.message); 
              setTimeout(() => {
                window.location.href = response.url;
              }, 3000);
            }
          }
      });
    });
  });

  document.addEventListener('DOMContentLoaded', function () {
      const tambahPrestasiButton = document.getElementById('tambahPrestasi');
      const kolomPrestasiContainer = document.getElementById('kolomPrestasiContainer');

      tambahPrestasiButton.addEventListener('click', function () {
          tambahKolomPrestasi();
      });

      // Use event delegation for dynamically added delete buttons
      kolomPrestasiContainer.addEventListener('click', function (event) {
          if (event.target.classList.contains('hapusKolomPrestasi')) {
              hapusKolomPrestasi(event.target);
          }
      });

      function tambahKolomPrestasi() {
        const kolomPrestasi = document.createElement('div');
        kolomPrestasi.className = 'tambah-prestasi'
        kolomPrestasi.innerHTML = `
              <div class="form-group row">
                <label for="jenis_prestasi" class="col-sm-2 mb-2 col-form-label">Jenis Prestasi</label>
                <div class="col-sm-10">
                    <textarea name="jenis_prestasi[]" class="form-control" placeholder="Masukkan Jenis Prestasi"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="tingkat_prestasi" class="col-sm-2 col-form-label">Tingkat Prestasi</label>
                <div class="col-sm-10">
                    <textarea name="tingkat_prestasi[]" class="form-control " placeholder="Masukkan Tingkat Prestasi"></textarea>
                </div>
              </div>
              
              <div class="form-group row">
                <label for="tahun_prestasi" class="col-sm-2 col-form-label">Tahun Prestasi</label>
                <div class="col-sm-10">
                  <select name="tahun_prestasi[]" id="" class="form-control">
                    @php
                      $tahun = 2010;
                    @endphp
                    @for ($tahun; $tahun <= date('Y'); $tahun++)
                      <option value="{{ $tahun }}">{{ $tahun }}</option>
                    @endfor
                  </select>
                </div>
              </div>


              <div class="form-group row">
                  <div class="mr-2 ml-auto">
                    <button type="button" class="btn btn-danger btn-sm  hapusKolomPrestasi">Hapus</button>
                  </div>
              </div>
              <hr>

        `;

        kolomPrestasiContainer.appendChild(kolomPrestasi);
      }

      function hapusKolomPrestasi(button) {
          button.closest('.tambah-prestasi').remove();
      }
  });

</script>
{{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script> --}}
@endpush
