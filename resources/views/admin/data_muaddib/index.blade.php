@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Muaddib</li>
@endsection

@push('css')
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.css')}}">

@endpush

@section('title', 'Data Muaddib')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_muaddib-create')
                <button type="button" data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-sm mr-2">Tambah
                    Muaddib</button>
                @endpermission
            </div>
        </div>
        @php
            $kelas = [
                "1" => "1",
                "2" => "2",
                "3" => "3",
            ];
        @endphp
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Kelas</label>
                    <select id="filter-kelas" class="form-control filter">
                        <option value="">Pilih Kelas</option>
                        <option value="1">Kelas 1</option>
                        <option value="2">Kelas 2</option>
                        <option value="3">Kelas 3</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Jenjang</label>
                    <select id="filter-jenjang" class="form-control filter">
                        <option value="">Pilih Jenjang</option>
                        <option value="TSN">Tsanawiyyah</option>
                        <option value="MLN">Mu'allimin</option>
                    </select>
                </div>
            </div>
            <hr class="divider">
            <table id="table-muaddib" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nama Muaddib</th>
                        <th width="30%">Santri</th>
                        <th width="5%">Majmu'ah</th>
                        <th>Kelas</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Muaddib</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-pane fade show active" id="asatidz-muadib" role="tabpanel" aria-labelledby="asatidz-muadib">
                        <form action="{{ route('muaddib.store') }}" method="POST">
                            @csrf
                            <div class="form-group mt-4">
                                <label for="asatidz_id">Nama Muaddib</label>
                                <select name="asatidz_id" id="asatidz_id" class="form-control select2bs4">
                                    <option value="" selected disabled>Pilih Asatidz</option>
                                    @foreach ($asatidz as $data)
                                    <option value="{{ $data->id }}">{{ $data->nama_lengkap }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kelas_id">Kelas</label>
                                <select name="kelas_id" id="kelas_id" class="form-control">
                                    <option value="" selected disabled>Pilih Kelas</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                                <select name="tingkat_sekolah_id" id="tingkat_sekolah_id" class="form-control">
                                    <option value="" selected disabled>Pilih Tingkat Sekolah</option>
                                    <option value="TSN">TSN</option>
                                    <option value="MLN">MLN</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="no_majmuah">Majmu'ah</label>
                                <input type="text" class="form-control" name="no_majmuah" id="no_majmuah" placeholder="Isi Majmuah">
                            </div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- EDIT --}}
    <div class="modal fade" id="modal-edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_muaddib">

                    <div class="form-group">
                        <label for="asatidz_id">Nama Muaddib</label>
                        <input type="text" id="edit_asatidz_id" class="form-control" disabled>
                    </div>
                    <div class="form-group">
                        <label for="kelas_id">Kelas</label>
                        <select name="kelas_id" id="edit_kelas_id" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                        <select name="tingkat_sekolah_id" id="edit_tingkat_sekolah_id" class="form-control">
                            <option value="TSN">TSN</option>
                            <option value="MLN">MLN</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="no_majmuah">Majmu'ah</label>
                        <input type="text" class="form-control" id="edit_no_majmuah" name="no_majmuah">
                    </div>
                    <div class="form-group">
                        <label for="santri_id">Santri</label>
                        <select class="edit_select form-control"  name="edit_santri[]" multiple="multiple" data-placeholder="Pilih Santri" style="width: 100%;">
                        </select>
                        <input type="hidden" name="before_edit[]" id="before_edit">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="saveUpdate" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
<script src="{{ asset('admin_lte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        // Initialize the DataTable
        let kelas = $("#filter-kelas").val()
        let jenjang = $("#filter-jenjang").val();

        const table = $('#table-muaddib').DataTable({
                "pageLength": 10,
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, 'semua']
                ],
                "bLengthChange": true,
                "bFilter": true,
                "bInfo": true,
                "processing": true,
                "bServerSide": true,
                "order": [
                    [1, "desc"]
                ],
                "autoWidth": false,
                "ajax": {
                    url: "{{ env('APP_URL') }}/admin/muaddib-data/{jenis}",
                    type: "POST",
                    data:function(d){
                        d.kelas = kelas;
                        d.jenjang = jenjang;
                        return d
                    }
                },
                "initComplete": function(settings, json) {
                    const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
                    $.each(all_checkbox_view, function(key, checkbox) {
                        let kolom = $(checkbox).data('kolom')
                        let is_checked = checkbox.checked
                        table.column(kolom).visible(is_checked)
                    })
                    setTimeout(function() {
                        table.columns.adjust().draw();
                    }, 3000)
                },
                columnDefs: [
                    {
                        targets: '_all',
                        visible: true
                    },
                    {
                        "targets": 0,
                        "class": "text-nowrap",
                        "render": function(data, type, row, meta) {
                            let asatidzId = row.asatidz_id;

                            if (isValidUUID(asatidzId)) {
                                return row.asatidz.nama_lengkap;
                            } else {
                                let santri = `{{ json_encode($santris->groupby('id')->toArray()) }}`;
                                santri = santri.replace(/&quot;/g, '"');
                                santri = JSON.parse(santri);
                                asatidzId = asatidzId.slice(0, -2);
                                return santri[asatidzId][0]['nama_lengkap'];
                            }
                        }
                    },
                    {
                        "targets": 1,
                        "class": "text-nowrap",
                        "render": function(data, type, row, meta) {
                            let santriData = row.santri;
                            let ul = '<ul>';
                            $.each(santriData, function(index, santri) {
                                ul += '<li>' + santri.nama_lengkap + '</li>';
                            });
                            ul += '</ul>';
                            return ul;
                        }
                    },
                    {
                        "targets": 2,
                        "class": "text-nowrap",
                        "render": function(data, type, row, meta) {
                            return row.no_majmuah;
                        }
                    },
                    {
                        "targets": 3,
                        "class": "text-nowrap",
                        "render": function(data, type, row, meta) {
                            console.log(row);
                            return row.tingkat_sekolah_id + " - " + row.kelas_id;
                        }
                    },
                    {
                        "targets": 4,
                        "render": function(data, type, row, meta) {
                            let idClass = row.id;
                            let idDelete = row.id;
                            if (!isValidUUID(row.asatidz_id)) {
                                idClass = row.id+"-s";
                            }
                            return ` 
                                @permission('menu_muaddib-update')
                                <a data-id="${idClass}" id="ubah" class="btn btn-sm btn-success">Edit</a>
                                @endpermission
                                @permission('menu_muaddib-delete')
                                <a href="{{ url('/admin/muaddib-delete') }}/${idDelete}"
                                onclick="return confirm('apakah yakin ingin menghapus data ini?')"
                                class="btn btn-sm btn-danger">Delete</a>
                                @endpermission`
                        }
                    },

                ]
            }); 

        $(".filter").on('change',function(){
            kelas = $("#filter-kelas").val()
            jenjang = $("#filter-jenjang").val()
            table.ajax.reload(null,false)
        })

        function isValidUUID(uuid) {
            const uuidPattern = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
            return uuidPattern.test(uuid);
        }

        $("#row-tampilan input[type='checkbox']").on('change',function(){
            let checkbox = $(this)
            let kolom = $(this).data('kolom')
            let is_checked = checkbox[0].checked
            table.column(kolom).visible(is_checked)
        })

        // Fungsi untuk mengecek apakah suatu string adalah angka
        function isNumericString(string) {
            return /^\d+$/.test(string);
        }

        let awalKelas = '';
        let awalTingkatSekolah = '';

        //Initialize Select2 Elements
        $('.edit_select').select2();

        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        $('body').on('click', '#ubah', function() {
            let id = $(this).data('id');

            $('#edit_kelas_id').attr("disabled", false);
            $('#edit_tingkat_sekolah_id').attr("disabled", false);

            //fetch detail post with ajax
            const kelasArray = {
                '3': '3',
                '2': '2',
                '1': '1'
            };
            $.ajax({
                url: `/admin/muaddib-ubah/${id}`,
                type: "GET",
                cache: false,
                success: function(response) {
                    //fill data to form
                    $('#id_muaddib').val(response.data.id);
                    $('#edit_asatidz_id').val(response.data.asatidz.nama_lengkap);
                    $('#edit_kelas_id').val(response.data.kelas_id);
                    $('#edit_tingkat_sekolah_id').val(response.data.tingkat_sekolah_id);
                    $('#edit_no_majmuah').val(response.data.no_majmuah);

                    awalTingkatSekolah = response.data.tingkat_sekolah_id;
                    awalKelas = response.data.kelas_id;

                    // append response.santris to select2
                    $(".edit_select").html('');
                    let santris = response.santris;
                    santris.forEach((el) => {
                        // if tingkat sekolah is TSN, then only show santri with tingkat sekolah TSN with kelas <= kelas muaddib
                        if (response.data.tingkat_sekolah_id == 'TSN') {
                            if (el.tingkat_sekolah_id == 'TSN' && el.kelas <= response.data.kelas_id) {
                                $(".edit_select").append(`<option value="${el.id}">${el.nama_lengkap} ${el.tingkat_sekolah_id}-${kelasArray[el.kelas]}</option>`);
                            }
                        }else if (response.data.tingkat_sekolah_id == 'MLN') {
                            if (el.tingkat_sekolah_id == 'MLN' && el.kelas <= response.data.kelas_id) {
                                $(".edit_select").append(`<option value="${el.id}">${el.nama_lengkap} ${el.tingkat_sekolah_id}-${kelasArray[el.kelas]}</option>`);
                            } else if (el.tingkat_sekolah_id == 'TSN') {
                                $(".edit_select").append(`<option value="${el.id}">${el.nama_lengkap} ${el.tingkat_sekolah_id}-${kelasArray[el.kelas]}</option>`);
                            }
                        }
                    })

                    let array = [];
                    response.data.santri.forEach((el) => {
                        array.push(el.id)
                    })

                    $(".edit_select").val(array).trigger('change');
                    $('#before_edit').val(array);

                    $lastChar = response.data.asatidz.nama_lengkap.slice(-1);
                    if ($lastChar == ')') {
                        // disable edit kelas id & edit tingkat sekolah id
                        $('#edit_kelas_id').attr("disabled", true);
                        $('#edit_tingkat_sekolah_id').attr("disabled", true);
                    }
                    
                    //open modal
                    $('#modal-edit').modal('show');
                }
            });
        });

        //action update post
        $('#saveUpdate').click(function(e) {
                e.preventDefault();
                let listSantri = $('.edit_select').val();
                let santriLength = listSantri.length;

                // confirmasi update jika tingkat sekolah berubah
                if(awalTingkatSekolah != $('#edit_tingkat_sekolah_id').val() && santriLength != 0 || awalKelas != $('#edit_kelas_id').val() && santriLength != 0) {
                    if(confirm('Apakah anda yakin ingin mengubah tingkat sekolah atau kelas? Santri yang sudah di assign akan diset ulang')) {
                        Update();
                    } else {
                        return false;
                    }
                } else {
                    Update();
                }

            });

        function Update() {
            //define letiable
            let id = $('#id_muaddib').val();
            let kelas_id = $('#edit_kelas_id').val();
            let tingkat_sekolah_id = $('#edit_tingkat_sekolah_id').val();
            let santri_id = $('.edit_select').val();
            let before_edit = $('#before_edit').val();
            let edit_no_majmuah = $('#edit_no_majmuah').val();

            $.ajax({

                url: `/admin/muaddib-ubah/${id}`,
                type: "PUT",
                cache: false,
                data: {
                    "kelas_id": kelas_id,
                    "tingkat_sekolah_id": tingkat_sekolah_id,
                    "santri_id": santri_id,
                    "before_edit": before_edit,
                    "no_majmuah": edit_no_majmuah,
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {

                    //show success message
                    toastr.success(response.success, 'BERHASIL!');
                    setTimeout(() => {
                        window.location.href = response.Url
                    }, 1000);

                    //close modal
                    $('#modal-edit').modal('hide');

                }
            });
        }

        // onchange santri_id
        $('#santri_id').change(function() {
            let id = $(this).val();
            
            // assign $santris to let santri
            let santri = @json($santris);
            santri = santri.filter((el) => {
                return el.id == id;
            })
            console.log(santri);

            // select tingkat sekolah
            $('#tingkat_sekolah_ids').val(santri[0].tingkat_sekolah_id);

            // select kelas
            $('#kelas_ids').val(santri[0].kelas);

        })
    </script>
@endpush