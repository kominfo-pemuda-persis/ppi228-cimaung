@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Semester</li>
@endsection

@section('title', 'Data Semester')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_semester-create')
                <button data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-sm">Tambah
                    Semester</button>
                @endpermission
            </div>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Semester</th>
                        <th>Tahun Ajaran</th>
                        <th>Status</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!$datas->isEmpty())
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->start_year.'-'.$data->end_year}}</td>
                            <td>{{ $data->status }}</td>
                            <td>{{ $data->keterangan }}</td>
                            <td>
                                @if($data->status != 'aktif')
                                <button type="button" class="btn btn-primary button-active" data-idSemester="{{ $data->id }}">Aktifkan</button>
                                @endif
                                @permission('menu_semester-update')
                                <a href="javascript:void(0)" data-id="{{ $data->id }}" id="ubahSemester"
                                class="btn btn-warning">Edit</a>
                                @endpermission
                                @permission('menu_semester-delete')
                                @if($data->status == 'draft')
                                <a href="{{ route('semester.delete', $data->id) }}"
                                onclick="return confirm('apakah yakin ingin menghapus data ini?')"
                                class="btn btn-danger">Hapus</a>
                                @endpermission
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <div class="alert alert-danger">
                            Data Semester belum Tersedia.
                        </div>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Semester</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('semester.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="semester">Semester</label>
                            <select name="nama" id="" class="form-control" required>
                                <option value="Genap">Genap</option>
                                <option value="Ganjil">Ganjil</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tahun_ajaran">Tahun Ajaran</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="yearpicker form-control @error('tahunAwal') is-invalid @enderror"
                                        placeholder="tahun awal" id="startYear" name="tahunAwal" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="yearpicker form-control @error('tahunAhir') is-invalid @enderror"
                                        placeholder="tahun selesai" id="endYear" name="tahunAhir" required>
                                </div>
                            </div>
                            @error('tahunAwal')
                                <div class="alert alert-danger mt-2" role="alert" id="alert-tahun">{{ $message }}</div>
                            @enderror
                            @error('tahunAhir')
                                <div class="alert alert-danger mt-2" role="alert" id="alert-tahun">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" name="keterangan" placeholder="masukkan keterangan">
                            @error('keterangan')
                                <div class="alert alert-danger mt-2" role="alert" id="alert-keterangan">{{ $message }}</div>
                            @enderror
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- EDIT --}}
    <div class="modal fade" id="modal-edit" ria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Semester</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_semester">
                    <div class="form-group">
                        <label for="semester">Semester</label>
                        <select name="nama" id="edit_nama" class="form-control" required>
                            <option value="Genap">Genap</option>
                            <option value="Ganjil">Ganjil</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tahun_ajaran">Tahun Ajaran</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="yearpicker form-control @error('tahunAwal') is-invalid @enderror"
                                    placeholder="tahun awal" id="startYearEdit" name="tahunAwal" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="yearpicker form-control @error('tahunAhir') is-invalid @enderror"
                                    placeholder="tahun selesai" id="endYearEdit" name="tahunAhir" required>
                            </div>
                        </div>
                        <div class="alert alert-danger mt-2 d-none" role="alert" id="alert-tahun-awal"></div>
                        <div class="alert alert-danger mt-2 d-none" role="alert" id="alert-tahun-ahir"></div>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" class="form-control" id="edit_keterangan" placeholder="masukkan keterangan">
                        <div class="alert alert-danger mt-2 d-none" role="alert" id="alert-keterangan-edit"></div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="saveUpdate" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL AKTIF -->
    <div class="modal fade" id="modal-active" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Aktivasi Semester</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('semester.aktif') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <p>Mengaktifkan Semester akan membuat satu-satunya yang aktif.</p>
                        <input type="hidden" id="input-active" name="idSemester">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Aktifkan!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('datepicker/css/yearpicker.css') }}" />
    <style>
        .yearpicker-container {
            z-index: 9999 !important;
        }
    </style>
@endpush

@push('scripts')
<script src="{{ asset('datepicker/js/yearpicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            const thisYear = (new Date).getFullYear();
            const startYear = thisYear - 2;
            const endYear = thisYear + 4;
            $("#startYear").yearpicker({
                year: thisYear,
                startYear: startYear,
                endYear: endYear,
            });
            $('#endYear').yearpicker({
                year: thisYear+1,
                startYear: startYear,
                endYear: endYear,
            });
        });
        
        $('#edit_nama').val("");
        $('#startYearEdit').val("");
        $('#endYearEdit').val("");
        $('#edit_status').val("");
        $('#edit_keterangan').val("");

        @if(count($errors) > 0)
            $('#modal-tambah').modal('show')
        @endif

        $('body').on('click', '#ubahSemester', function() {
            let id = $(this).data('id');
            //fetch detail post with ajax
            $.ajax({
                url: `/admin/master/semester-ubah/${id}`,
                type: "GET",
                cache: false,
                success: function(response) {
                    //fill data to form
                    $('#id_semester').val(response.data.id);
                    $('#edit_nama').val(response.data.nama).prop('selected', true);
                    $('#startYearEdit').val(response.data.start_year);
                    $('#endYearEdit').val(response.data.end_year);
                    $('#edit_status').val(response.data.status).prop('selected', true);
                    $('#edit_keterangan').val(response.data.keterangan);
                    //open modal
                    $("#startYearEdit").yearpicker({
                        year: response.data.start_year,
                        startYear: startYear,
                        endYear: endYear,
                    });
                    $('#endYearEdit').yearpicker({
                        year: response.data.end_year,
                        startYear: startYear,
                        endYear: endYear,
                    });
                    $('#modal-edit').modal('show');
                }
            });
        });

        //action update post
        $('#saveUpdate').click(function(e) {
            e.preventDefault();
            let id = $('#id_semester').val();
            //define variable
            let edit_nama = $('#edit_nama').val();
            let tahunAwal = $('#startYearEdit').val();
            let tahunAhir = $('#endYearEdit').val();
            let edit_status = $('#edit_status').val();
            let edit_keterangan = $('#edit_keterangan').val();

            //ajax
            $.ajax({

                url: `/admin/master/semester-ubah/${id}`,
                type: "PUT",
                cache: false,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "nama": edit_nama,
                    "tahunAwal": tahunAwal,
                    "tahunAhir": tahunAhir,
                    "status": edit_status,
                    "keterangan": edit_keterangan,
                },
                success: function(response) {
                    console.log(response);
                    //show success message
                    toastr.success(response.success, 'BERHASIL!');
                    setTimeout(() => {
                        window.location.href = response.Url
                    }, 1000);

                    //close modal
                    $('#modal-edit').modal('hide');

                },
                error:function(error) {
                    if(error.responseJSON.tahunAhir[0]) {
                        //show alert
                        $('#alert-tahun-ahir').removeClass('d-none');
                        $('#alert-tahun-ahir').addClass('d-block');

                        //add message to alert
                        $('#alert-tahun-ahir').html(error.responseJSON.tahunAhir[0] ? error.responseJSON.tahunAhir[0] : error);
                    }

                    if(error.responseJSON.tahunAwal[0]) {
                        //show alert
                        $('#alert-tahun-awal').removeClass('d-none');
                        $('#alert-tahun-awal').addClass('d-block');

                        //add message to alert
                        $('#alert-tahun-awal').html(error.responseJSON.tahunAwal[0] ? error.responseJSON.tahunAwal[0] : error);
                    }

                    if(error.responseJSON.keterangan[0]) {
                        //show alert
                        $('#alert-keterangan-edit').removeClass('d-none');
                        $('#alert-keterangan-edit').addClass('d-block');

                        //add message to alert
                        $('#alert-keterangan-edit').html(error.responseJSON.keterangan[0]);
                    } 
                }
            });
        });

        $(".button-active").click(function (e) {
            $('#modal-active').modal('show');
            // get the value for the data attributes here and use in postdata
            const idSemester = $(this).attr('data-idSemester');
            $("#input-active").val(idSemester);
            // var postdata = { "analystID": analystID, "symbol": /* use value here */, "status": /* use value here */ };

        //etc
        });
    </script>
@endpush
