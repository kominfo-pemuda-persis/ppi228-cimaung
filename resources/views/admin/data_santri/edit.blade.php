@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Edit Santri</li>
@endsection

@section('title', 'Data Santri')

@section('content')
    <p style="margin-top:-20px; background-color:rgb(172, 191, 64); color:white;" class="text-center">{{ strtoupper($data->nama_lengkap) .' - '. ($data->tingkat_sekolah_id == 'MLN' ? 'Muallimien' : 'Tsanawiyyah') .' - '. $data->kelas }}</p>
    <div class="card p-2">
        <form action="{{ route('santri.update', $data->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nis">NIS</label>
                        {{-- add old value --}}
                        <input type="text" class="form-control @error('nis') is-invalid @enderror" name="nis" placeholder="Masukkan No.NIS" value="{{ old('nis', $data->nis) }}" readonly="readonly">
                        @error('nis')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nisn">NISN</label>
                        <input type="text" class="form-control @error('nisn') is-invalid @enderror" name="nisn" placeholder="Masukkan No.NISN" value="{{ old('nisn', $data->nisn) }}">
                        @error('nisn')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_lengkap">Nama Lengkap</label>
                        <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" placeholder="Masukkan Nama Lengkap" value="{{ old('nama_lengkap', $data->nama_lengkap) }}">
                        @error('nama_lengkap')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nisn">NISM/NSPP</label>
                        <input type="text" class="form-control @error('nism') is-invalid @enderror" name="nism" placeholder="Masukkan No.NISM" value="{{ old('nism', $data->nism) }}">
                        @error('nism')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" value="{{ old('tempat_lahir', $data->tempat_lahir) }}">
                        @error('tempat_lahir')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                        <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" id="tanggal_lahir" name="tanggal_lahir" value="{{ $data->tanggal_lahir, date('Y-m-d') }}">
                        @error('tanggal_lahir')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alamat_kota">Kota</label>
                        <input type="text" class="form-control @error('alamat_kota') is-invalid @enderror" name="alamat_kota" placeholder="Masukkan Nama Kota" value="{{ old('alamat_kota', $data->alamat_kota) }}">
                        @error('alamat_kota')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="jk">Jenis Kelamin</label>
                        <select name="jk"  class="form-control" id="jk" required>
                            <option value="">-- Pilih Jenis Kelamin --</option>
                            <option value="RG" {{ $data->jk == 'RG' ? 'selected' : ''  }}>RG</option>
                            <option value="UG" {{ $data->jk == 'UG' ? 'selected' : ''  }}>UG</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="no_hp">No Hp</label>
                        <input type="text" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" placeholder="Masukkan No Hp" value="{{ old('no_hp', $data->no_hp) }}">
                        @error('no_hp')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="asal_sekolah">Asal Sekolah</label>
                        <input type="text" class="form-control @error('asal_sekolah') is-invalid @enderror" name="asal_sekolah" placeholder="Masukkan Asal Sekolah" value="{{ old('asal_sekolah', $data->asal_sekolah) }}">
                        @error('asal_sekolah')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alamat_asal_sekolah">Alamat Asal Sekolah</label>
                        <input type="text" class="form-control @error('alamat_asal_sekolah') is-invalid @enderror" name="alamat_asal_sekolah" placeholder="Masukkan Alamat Asal Sekolah" value="{{ old('alamat_asal_sekolah', $data->alamat_asal_sekolah) }}">
                        @error('alamat_asal_sekolah')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        @php
                            $kelas_tingkat = $data->kelas."-".$data->tingkat_sekolah_id;
                        @endphp
                        <label for="tingkat_sekolah_id">Kelas</label>
                        <select name="kelas"  class="form-control" required>
                            @if ($data->tingkat_sekolah_id == 'TSN')
                                <option value="1-TSN" {{ '1-TSN' == $kelas_tingkat ? 'selected' : '' }}>TSN-1</option>
                                <option value="2-TSN" {{ '2-TSN' == $kelas_tingkat ? 'selected' : '' }}>TSN-2</option>
                                <option value="3-TSN" {{ '3-TSN' == $kelas_tingkat ? 'selected' : '' }}>TSN-3</option>
                            @else
                                <option value="1-MLN" {{ '1-MLN' == $kelas_tingkat ? 'selected' : '' }}>MLN-1</option>
                                <option value="2-MLN" {{ '2-MLN' == $kelas_tingkat ? 'selected' : '' }}>MLN-2</option>
                                <option value="3-MLN" {{ '3-MLN' == $kelas_tingkat ? 'selected' : '' }}>MLN-3</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_ortu_ayah">Nama Ayah</label>
                        <input type="text" class="form-control @error('nama_ortu_ayah') is-invalid @enderror" name="nama_ortu_ayah" placeholder="Masukkan Nama Ortu Ayah" value="{{ old('nama_ortu_ayah', $data->nama_lengkap_ayah) }}">
                        @error('nama_ortu_ayah')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pekerjaan_ayah">Pekerjaan Ayah</label>
                        <input type="text" class="form-control @error('pekerjaan_ayah') is-invalid @enderror" name="pekerjaan_ayah" placeholder="Masukkan Pekerjaan Ayah" value="{{ old('pekerjaan_ayah', $data->pekerjaan_ayah) }}">
                        @error('pekerjaan_ayah')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_ortu_ibu">Nama Ibu</label>
                        <input type="text" class="form-control @error('nama_ortu_ibu') is-invalid @enderror" name="nama_ortu_ibu" placeholder="Masukkan Nama Ortu Ibu" value="{{ old('nama_ortu_ibu', $data->nama_lengkap_ibu) }}">
                        @error('nama_ortu_ibu')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pekerjaan_ibu">Pekerjaan Ibu</label>
                        <input type="text" class="form-control @error('pekerjaan_ibu') is-invalid @enderror" name="pekerjaan_ibu" placeholder="Masukkan Pekerjaan Ibu" value="{{ old('pekerjaan_ibu', $data->pekerjaan_ibu) }}">
                        @error('pekerjaan_ibu')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_lengkap_wali">Nama Lengkap Wali</label>
                        <input type="text" class="form-control @error('nama_lengkap_wali') is-invalid @enderror" name="nama_lengkap_wali" placeholder="Masukkan Nama Lengkap Wali" value="{{ old('nama_lengkap_wali', $data->nama_lengkap_wali) }}">
                        @error('nama_lengkap_wali')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pekerjaan_wali">Pekerjaan Wali</label>
                        <input type="text" class="form-control @error('pekerjaan_wali') is-invalid @enderror" name="pekerjaan_wali" placeholder="Masukkan Pekerjaan Wali" value="{{ old('pekerjaan_wali', $data->pekerjaan_wali) }}">
                        @error('pekerjaan_wali')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" placeholder="Masukkan Alamat">{{ old('alamat', $data->alamat) }}</textarea>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alamat">Status Santri</label>
                        <select name="status_active"  class="form-control" id="status_active" required>
                            <option value="">-- Pilih Status Santri --</option>
                            <option value="active" {{ $data->status_active == 'active' ? 'selected' : ''  }}>Active</option>
                            <option value="dikeluarkan" {{ $data->status_active == 'dikeluarkan' ? 'selected' : ''  }}>Dikeluarkan</option>
                            <option value="pindah" {{ $data->status_active == 'pindah' ? 'selected' : ''  }}>Pindah</option>
                            <option value="meninggal" {{ $data->status_active == 'meninggal' ? 'selected' : ''  }}>Meninggal</option>
                            <option value="alumni" {{ $data->status_active == 'alumni' ? 'selected' : ''  }}>Alumni</option>
                        </select>                    
                    </div>
                </div>
                {{-- @if($data->kelas != 1)
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alamat">Status Kenaikan</label>
                        <select name="kelas"  class="form-control" required>
                            <option value="">-- Pilih Status Santri --</option>
                            <option value="TIDAK_NAIK_KELAS_2" {{ $data->kelas == 'TIDAK_NAIK_KELAS_2' ? 'selected' : ''  }}>Tidak Naik Kelas 2</option>
                            <option value="TIDAK_NAIK_KELAS_3" {{ $data->kelas == 'TIDAK_NAIK_KELAS_3' ? 'selected' : ''  }}>Tidak Naik Kelas 3</option>
                            <option value="{{ $data->kelas }}" selected>Naik Kelas {{ $data->kelas != 1 ? $data->kelas : ''  }}</option>
                            @if($data->status_active == 'alumni')
                                <option value="TIDAK_LULUS" {{ $data->kelas == 'TIDAK_LULUS' ? 'selected' : ''  }}>Tidak Lulus</option>
                                <option value="LULUS" {{ $data->kelas == 'LULUS' ? 'selected' : ''  }}>Lulus</option>
                            @endif
                        </select>                    
                    </div>
                </div>
                @else
                <div class="col-sm-6">
                    <div class="form-group" >
                        <label for="kelas">Status Kenaikan</label>
                        <select name="kelas"  class="form-control">
                            <option value="{{ $data->kelas }}" selected>{{ $data->kelas }}</option>
                        </select>                    
                    </div>
                </div> --}}
                {{-- @endif --}}
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ $data->user->email }}" name="email" placeholder="Email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" value="{{ $data->user->username }}" required name="username" placeholder="Username">
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="gantiPassword">
                        <label class="form-check-label" for="gantiPassword">Ganti Password</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input disabled type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password-confirm">Konfirmasi Password</label>
                        <input disabled type="password" id="password-confirm" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password">
                    </div>
                </div>
            </div>
            <a href="{{ route('santri.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
@endsection

@push('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })

  $('#gantiPassword').click(function (event) {
        // $('#password').removeAttr('disabled', false)
        if (this.checked) {
            $('#password').removeAttr('disabled')
            $('#password-confirm').removeAttr('disabled')
        } else {
            $('#password').attr('disabled', true)
            $('#password-confirm').attr('disabled', true)
        }
    })
</script>
@endpush