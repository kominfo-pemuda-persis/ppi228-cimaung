@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Detail Santri</li>
@endsection

@section('title', 'Detail Santri')

@section('content')
<style>
table th, table td{
    padding:0.75rem;
}
</style>

    <a href="{{ route('santri.index') }}" class="btn btn-secondary my-2">Kembali</a>
<div class="row">
<div class="col-12 col-md-6 col-lg-6">
    <div class="card">
        <div class="card-body">
            <b>BIODATA SANTRI</b>
            <table>
                    <tr>
                        <td>NISM/NSPP</td>
                        <td>:</td>
                        <td>{{ $santri->nism ?? 'belum di isi' }}</td>
                    </tr>
                    <tr>
                        <td>NISN</td>
                        <td>:</td>
                        <td>{{ $santri->nisn ?? 'belum di isi' }}</td>
                    </tr>
                    <tr>
                        <td>NIS</td>
                        <td>:</td>
                        <td>{{ $santri->nis ?? 'belum di isi' }}</td>
                    </tr>
                    <tr>
                        <td>NIK</td>
                        <td>:</td>
                        <td>{{ $santri->nik ?? 'belum di isi' }}</td>
                    </tr>
                    <tr>
                        <td>Nama Lengkap</td>
                        <td>:</td>
                        <td>{{ $santri->nama_lengkap ?? 'belum di isi' }}</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ $santri->jk == 'RG' || $santri->jk == 'L' ? 'laki-laki' : 'perempuan' }}</td>
                    </tr>
                    <tr>
                        <td>Tempat Tanggal Lahir</td>
                        <td>:</td>
                        <td>{{ $santri->tempat_lahir .", ". $santri->tanggal_lahir ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>Kota</td>
                        <td>:</td>
                        <td>{{ $santri->alamat_kota ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{ $santri->alamat ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>No HP</td>
                        <td>:</td>
                        <td>{{ $santri->no_hp ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ $santri->email ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>Asal Sekolah</td>
                        <td>:</td>
                        <td>{{ $santri->asal_sekolah ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>Alamat Asal Sekolah</td>
                        <td>:</td>
                        <td>{{ $santri->alamat_asal_sekolah ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>Akte</td> 
                        <td>:</td> 
                        <td>@if(!empty($santri->path_akte))
                                    <a href="/admin/santri/file/download?url=data/santri/akte/{{ $santri->path_akte }}&filename={{ $santri->path_akte }}" download="{{ $santri->path_akte }}">Download</a>
                                    @else
                                    <span class="text-danger">belum upload akte</span>
                                @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Photo Formal Santri</td> 
                        <td>:</td> 
                        <td>@if(!empty($santri->path_foto_santri))
                                    <a href="/admin/santri/file/download?url=data/santri/photo_formal/{{ $santri->path_foto_santri }}&filename={{ $santri->path_foto_santri }}" download="{{ $santri->path_foto_santri }}">Download</a>
                                    @else
                                    <span class="text-danger">belum upload foto formal</span>
                                @endif
                        </td>
                    </tr>
                    @if($santri->tingkat_sekolah_id != 'TSN')
                    <tr>
                        <td>Ijazah SMP/MTS</td> 
                        <td>:</td> 
                        <td>@if(!empty($santri->path_ijazah_mts))
                                    <a href="/admin/santri/file/download?url=data/santri/ijazah-mts/{{ $santri->path_ijazah_mts }}&filename={{ $santri->path_ijazah_mts }}" download="{{ $santri->path_ijazah_mts }}">Download</a>
                                    @else
                                    <span class="text-danger">belum upload ijazah SMP/MTS</span>
                                @endif
                        </td>
                    </tr>
                    @else
                    <tr>
                        <td>Ijazah SD/MI</td> 
                        <td>:</td> 
                        <td>@if(!empty($santri->path_ijazah_sd))
                                    <a href="/admin/santri/file/download?url=data/santri/ijazah-sd/{{ $santri->path_ijazah_sd }}&filename={{ $santri->path_ijazah_sd }}" download="{{ $santri->path_ijazah_sd }}">Download</a>
                                    @else
                                    <span class="text-danger">belum upload ijazah SD/MI</span>
                                @endif
                        </td>
                    </tr>
                    @endif
            </table>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <b>PRESTASI</b>
            @forelse ($santri->prestasi as $prestasiData)
                <table>
                    <tr>
                        <td>Jenis Prestasi</td> 
                        <td>:</td> 
                        <td>{{ $prestasiData->jenis_prestasi ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>Tingkat</td> 
                        <td>:</td> 
                        <td>{{ $prestasiData->tingkat ?? 'belum di isi'}}</td>
                    </tr>
                    <tr>
                        <td>Tahun</td> 
                        <td>:</td> 
                        <td>{{ $prestasiData->tahun ?? 'belum di isi'}}</td>
                    </tr>
                </table>
                <hr>
            @empty
                <br>
                <span class="text-danger">Data Prestasi Belum Diisi!</span>
            @endforelse 
        </div>
    </div>
<div class="card">
    <div class="card-body">
        <table>
                <tr>
                    <td>Hafalan</td> 
                    <td>:</td> 
                    <td>{{ $santri->hafalan ?? 'belum di isi'}}</td>
                </tr>
        </table>
        </div>
    </div>
</div>
<div class="col-12 col-md-6 col-lg-6">
    <div class="card">
        <div class="card-body">
            <b>BIODATA ORANG TUA</b>
            <table>
                <tr>
                    <td>Nama Lengkap Ayah</td>
                    <td>:</td>
                    <td>{{ $santri->nama_lengkap_ayah ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>No KTP Ayah</td>
                    <td>:</td>
                    <td>{{ $santri->no_ktp_ayah ?? 'belum di isi'}}</td>
                </tr>
                    <td>No HP Ayah</td>
                    <td>:</td>
                    <td>{{ $santri->no_hp_ayah ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Pendidikan Ayah</td>
                    <td>:</td>
                    <td>{{ $santri->pendidikan_ayah ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Pekerjaan Ayah</td>
                    <td>:</td>
                    <td>{{ $santri->pekerjaan_ayah ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Penghasilan Ayah</td>
                    <td>:</td>
                    <td>{{ $santri->penghasilan_ayah ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Nomor Kartu Keluarga</td>
                    <td>:</td>
                    <td>{{ $santri->nomor_kartu_keluarga ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Nama Lengkap Ibu</td> 
                    <td>:</td> 
                    <td>{{ $santri->nama_lengkap_ibu ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>No KTP Ibu</td> 
                    <td>:</td> 
                    <td>{{ $santri->no_ktp_ibu ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>No HP Ibu</td> 
                    <td>:</td> 
                    <td>{{ $santri->no_hp_ibu ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Pendidikan Ibu</td> 
                    <td>:</td> 
                    <td>{{ $santri->pendidikan_ibu ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Pekerjaan Ibu</td> 
                    <td>:</td> 
                    <td>{{ $santri->pekerjaan_ibu ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Penghasilan Ibu</td> 
                    <td>:</td> 
                    <td>{{ $santri->penghasilan_ibu ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Nama Lengkap Wali</td> 
                    <td>:</td> 
                    <td>{{ $santri->nama_lengkap_wali ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>No KTP Wali</td> 
                    <td>:</td> 
                    <td>{{ $santri->no_ktp_wali ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>No HP Wali</td> 
                    <td>:</td> 
                    <td>{{ $santri->no_hp_wali ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Pendidikan Wali</td> 
                    <td>:</td> 
                    <td>{{ $santri->pendidikan_wali ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Pekerjaan Wali</td> 
                    <td>:</td> 
                    <td>{{ $santri->pekerjaan_wali ?? 'belum di isi'}}</td>
                </tr>
                <tr>
                    <td>Penghasilan Wali</td> 
                    <td>:</td> 
                    <td>{{ $santri->penghasilan_wali ?? 'belum di isi'}}</td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
