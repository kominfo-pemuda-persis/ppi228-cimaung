@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Tambah Santri</li>
@endsection

@section('title', 'Data Santri')

@section('content')
    <div class="card p-2">
        <form action="{{ route('santri.store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nis">NIS</label>
                        <input type="text" class="form-control" name="nis" value="{{ old('nis') }}" placeholder="Masukkan No.NIS">
                        @error('nis')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nisn">NISN</label>
                        <input type="number" class="form-control" name="nisn" value="{{ old('nisn') }}" placeholder="Masukkan No.NISN">
                        @error('nisn')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tahun_masuk">Tahun Masuk</label>
                        <input type="text" class="form-control" name="tahun_masuk" value="{{ old('tahun_masuk') }}" placeholder="Tahun Masuk">
                        @error('tahun_masuk')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tahun_lulus">Angkatan</label>
                        <input type="text" class="form-control" name="tahun_lulus" value="{{ old('tahun_lulus') }}" placeholder="Angkatan">
                        @error('tahun_lulus')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_lengkap">Nama Lengkap</label>
                        <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" value="{{ old('nama_lengkap') }}" required name="nama_lengkap" placeholder="Nama Lengkap">
                        @error('nama_lengkap')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" value="{{ old('tempat_lahir') }}" required name="tempat_lahir" placeholder="Tempat Lahir">
                        @error('tempat_lahir')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                        <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" value="{{ old('tanggal_lahir') }}" required name="tanggal_lahir" placeholder="Tanggal Lahir">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                    <label for="jk">Jenis Kelamin</label>
                        <select name="jk"  class="form-control" id="jk" required>
                            <option value="">-- Pilih Jenis Kelamin --</option>
                            <option value="RG">RG</option>
                            <option value="UG">UG</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="no_hp">No Hp</label>
                        <input type="number" class="form-control @error('no_hp') is-invalid @enderror" value="{{ old('no_hp') }}" required name="no_hp" placeholder="No Hp">
                        @error('no_hp')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="asal_sekolah">Asal Sekolah</label>
                        <input type="text" class="form-control @error('asal_sekolah') is-invalid @enderror" value="{{ old('asal_sekolah') }}" required name="asal_sekolah" placeholder="Asal Sekolah">
                        @error('asal_sekolah')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select name="kelas"  class="form-control" required>
                            <option value="1-TSN" {{ old('kelas') == '1-TSN' ? 'selected' : '' }}>TSN-1</option>
                            <option value="2-TSN" {{ old('kelas') == '2-TSN' ? 'selected' : '' }}>TSN-2</option>
                            <option value="3-TSN" {{ old('kelas') == '3-TSN' ? 'selected' : '' }}>TSN-3</option>
                            <option value="1-MLN" {{ old('kelas') == '1-MLN' ? 'selected' : '' }}>MLN-1</option>
                            <option value="2-MLN" {{ old('kelas') == '2-MLN' ? 'selected' : '' }}>MLN-2</option>
                            <option value="3-MLN" {{ old('kelas') == '3-MLN' ? 'selected' : '' }}>MLN-3</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_ortu_ayah">Nama Ayah</label>
                        <input type="text" class="form-control @error('nama_ortu_ayah') is-invalid @enderror" value="{{ old('nama_ortu_ayah') }}" required name="nama_ortu_ayah" placeholder="Nama Ayah">
                        @error('nama_ortu_ayah')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pekerjaan_ayah">Pekerjaan Ayah</label>
                        <input type="text" class="form-control @error('pekerjaan_ayah') is-invalid @enderror" value="{{ old('pekerjaan_ayah') }}" required name="pekerjaan_ayah" placeholder="Pekerjaan Ayah">
                        @error('pekerjaan_ayah')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_ortu_ibu">Nama Ibu</label>
                        <input type="text" class="form-control @error('nama_ortu_ibu') is-invalid @enderror" value="{{ old('nama_ortu_ibu') }}" required name="nama_ortu_ibu" placeholder="Nama Ibu">
                        @error('nama_ortu_ibu')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pekerjaan_ibu">Pekerjaan Ibu</label>
                        <input type="text" class="form-control @error('pekerjaan_ibu') is-invalid @enderror" value="{{ old('pekerjaan_ibu') }}" required name="pekerjaan_ibu" placeholder="Pekerjaan Ibu">
                        @error('pekerjaan_ibu')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nama_lengkap_wali">Nama Lengkap Wali</label>
                        <input type="text" class="form-control @error('nama_lengkap_wali') is-invalid @enderror" value="{{ old('nama_lengkap_wali') }}" required name="nama_lengkap_wali" placeholder="Nama Lengkap Wali">
                        @error('nama_lengkap_wali')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="pekerjaan_wali">Pekerjaan Wali</label>
                        <input type="text" class="form-control @error('pekerjaan_wali') is-invalid @enderror" value="{{ old('pekerjaan_wali') }}" required name="pekerjaan_wali" placeholder="Pekerjaan Wali">
                        @error('pekerjaan_wali')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" required placeholder="Alamat">{{ old('alamat') }}</textarea>
                    </div>
                </div>
                {{-- <div class="col-sm-6">
                    <div class="form-group">
                        <label for="muaddib_id">Muaddib</label>
                        <select name="muaddib_id"  class="form-control select2bs4">
                            @foreach ($muaddib as $data)
                                <option value="{{ $data->id }}" {{ old('muaddib_id') == $data->id ? 'selected' : '' }}>{{ $data->asatidz->nama_lengkap ?? '' }} - {{ $data->kelas_id }} - {{ $data->tingkat_sekolah_id }}</option>
                            @endforeach
                        </select>
                        @error('muaddib_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div> --}}
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required name="email" placeholder="Email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" required name="username" placeholder="Username">
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" required name="password" placeholder="Password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password-confirm">Konfirmasi Password</label>
                        <input type="password" id="password-confirm" class="form-control" required name="password_confirmation" placeholder="Konfirmasi Password">
                    </div>
                </div>
            </div>
            <a href="{{ route('santri.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
@endsection

@push('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })
</script>
@endpush
