<style>
  .table-row {
    margin-bottom: 10px; /* Sesuaikan nilai spasi sesuai kebutuhan Anda */
    padding-left: 50px;
    padding-right: 50px;
  }
</style>

<table>
  <thead>
    <tr class="table-row">
      <th>NO</th>
      <th>STATUS SANTRI</th>
      <th>NISN</th>
      <th>NIS</th>
      <th>NIK</th>
      <th>NAMA</th>
      <th>JENIS KELAMIN</th>
      <th>TEMPAT LAHIR</th>
      <th>TANGGAL LAHIR</th>
      <th>ALAMAT KOTA</th>
      <th>ALAMAT</th>
      <th>NO HP</th>
      <th>EMAIL</th>
      <th>ASAL SEKOLAH</th>
      <th>ALAMAT ASAL SEKOLAH</th>
      <th>NAMA AYAH</th>
      <th>KTP AYAH</th>
      <th>HP AYAH</th>
      <th>PENDIDIKAN AYAH</th>
      <th>PEKERJAAN AYAH</th>
      <th>PENGHASILAN AYAH</th>
      <th>NOMOR KARTU KELUARGA</th>
      <th>NAMA IBU</th>
      <th>KTP IBU</th>
      <th>HP IBU</th>
      <th>PENDIDIKAN IBU</th>
      <th>PEKERJAAN IBU</th>
      <th>PENGHASILAN IBU</th>
      <th>NAMA WALI</th>
      <th>KTP WALI</th>
      <th>HP WALI</th>
      <th>PENDIDIKAN WALI</th>
      <th>PEKERJAAN WALI</th>
      <th>PENGHASILAN WALI</th>
    </tr>
  </thead>
  <tbody>
    @foreach($santri as $data)
    <tr style="text-align: left;">
      <td>{{ $loop->iteration }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->status_active }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->nisn }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->nis }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->nik }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->nama_lengkap }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->jk }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->tempat_lahir }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->tanggal_lahir }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->alamat_kota }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->alamat }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->no_hp }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->email }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->asal_sekolah }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->alamat_asal_sekolah }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->nama_lengkap_ayah }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ "'".$data->no_ktp_ayah  }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->no_hp_ayah }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->pendidikan_ayah }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->pekerjaan_ayah }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->penghasilan_ayah }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ "'".$data->nomor_kartu_keluarga }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->nama_lengkap_ibu }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ "'".$data->no_ktp_ibu }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->no_hp_ibu }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->pendidikan_ibu }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->pekerjaan_ibu }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->penghasilan_ibu }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->nama_lengkap_wali }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ "'".$data->no_ktp_wali }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->no_hp_wali }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->pendidikan_wali }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->pekerjaan_wali }}</td>
      <td style="background-color: {{ $data->status_active === 'alumni' ? '#C8E6C9' : ($data->status_active === 'dikeluarkan' || $data->status_active === 'meninggal' ? '#FFCDD2' : '#FFFFFF') }}">{{ $data->penghasilan_wali }}</td>
    </tr>
    @endforeach
  </tbody>
</table>