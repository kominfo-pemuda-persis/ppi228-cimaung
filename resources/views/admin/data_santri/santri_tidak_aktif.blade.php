@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Santri Tidak Aktif</li>
@endsection

@section('title', 'Data Santri Tidak Aktif')

@section('content')
    <div class="card">
        
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Kelas</label>
                    <select id="filter-kelas" class="form-control filter">
                        <option value="">Filter Kelas</option>
                        <option value="1">Kelas 1</option>
                        <option value="2">Kelas 2</option>
                        <option value="3">Kelas 3</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Jenjang</label>
                    <select id="filter-jenjang" class="form-control filter">
                        <option value="">Filter Jenjang</option>
                        <option value="TSN">Tsanawiyyah</option>
                        <option value="MLN">Mu'allimin</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Sakan</label>
                    <select id="filter-sakan" class="form-control filter">
                        <option value="">Filter Sakan</option>
                        <option value="ya">Santri Sakan</option>
                        <option value="tidak">Santri Tidak Sakan</option>
                    </select>
                </div>
                <div class="col-md-4 mt-2">
                    <label>Status Santri</label>
                    <select id="filter-santri" class="form-control filter">
                        <option value="">Filter Status Santri</option>
                        <option value="dikeluarkan">Santri Dikeluarkan</option>
                        <option value="pindah">Santri Pindah</option>
                        <option value="meninggal">Santri Meninggal</option>
                    </select>
                </div>
                <div class="col-md-4 mt-2">
                    <label>Angkatan</label>
                    <select id="filter-angkatan" class="form-control filter">
                        <option value="">Angkatan</option>
                        @for ($tahun = 2019; $tahun<= 2030; $tahun++)
                            <option value="{{ $tahun }}">{{ $tahun }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <hr class="divider">
            <div class="row" id="row-tampilan_1">
                <div class="col-sm-3">
                    <label>
                        <input type="checkbox" class="tampilan" data-kolom="5" checked> Show Username
                    </label>
                </div>
                {{-- show export --}}
                <div class="col-sm-3">
                    <label>
                        <input type="checkbox" class="tampilan" data-kolom="0" data-type="export" checked> Show Export
                    </label>
                    <div class="mb-12 col-md-12 d-none" id="btn-export-santri">
                        <button class="btn btn-sm btn-success btn-block">Export</button>
                    </div>
                    <form action="{{ route('santri-tidak-aktif.export') }}" method="post" id="form-export-santri">
                        @csrf
                        <input type="hidden" name="ids" id="ids">
                    </form>
                </div>
            </div>
            <hr class="divider">
            <div class="table-responsive">
                <table id="table-santri" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="cb-parent">
                            </th>
                            <th>NIS</th>
                            <th>NISN</th>
                            <th>Nama Lengkap</th>
                            <th>Kelas</th>
                            <th>Username</th>
                            <th>Angkatan</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $('.tampilan').prop('checked', false);

        let kelas = $("#filter-kelas").val()
        let jenjang = $("#filter-jenjang").val();
        let angkatan = $("#filter-angkatan").val();
        let sakan = $("#filter-sakan").val();
        let santri = $("#filter-santri").val();

        const table = $('#table-santri').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "order": [
                [1, "desc"]
            ],
            "autoWidth": false,
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/santri-tidak-aktif-data/{jenis}",
                type: "POST",
                data:function(d){
                    d.kelas = kelas;
                    d.angkatan = angkatan;
                    d.jenjang = jenjang;
                    d.sakan = sakan;
                    d.santri = santri;
                    return d
                }
            },
            "initComplete": function(settings, json) {
                const all_checkbox_view = $("#row-tampilan_1 div input[type='checkbox']")
                console.log(all_checkbox_view)
                $.each(all_checkbox_view, function(key, checkbox) {
                    let kolom = $(checkbox).data('kolom')
                    let is_checked = checkbox.checked
                    table.column(kolom).visible(is_checked)
                })
                setTimeout(function() {
                    table.columns.adjust().draw();
                }, 3000)
            },
            columnDefs: [
                {
                    targets: '_all',
                    visible: true
                },
                {
                    "targets": 0,
                    "class": "text-nowrap",
                    "sortable": false,
                    "render": function(data, type, row, meta) {
                        return `<input type="checkbox" class="cb-child" value="${row.id}">`;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        // list_karyawan[row.id] = row;
                        return row.nis;
                    }
                },
                {
                    "targets": 2,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nisn;
                    }
                },
                {
                    "targets": 3,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nama_lengkap;
                    }
                },
                {
                    "targets": 5,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.user.username;
                    }
                },
                {
                    "targets": 4,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        // return row.tingkat_sekolah_id + " - " + (row.kelas == 'XII' ? '3' : '' || row
                        //     .kelas == 'XI' ? '2' : '' || row.kelas == 'X' ? '1' : '');
                        let kelas = row.tingkat_sekolah_id == 'LULUS' ? '' : " - "+row.kelas 
                        return row.tingkat_sekolah_id + kelas;
                    }
                },
                {
                    "targets": 6,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.tahun_lulus;
                    }
                },
                {
                    "targets": 7,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        let badgeLulus = row.status_active == "alumni" ? 'badge-success' : 'badge-secondary';
                        return row.status_active == "alumni" ? 
                        `<span class="badge ${badgeLulus}">Lulus</span>` : 
                        `<span class="badge ${badgeLulus}">${row.status_active}</span>`;
                    }
                },
                {
                    "targets": 8,
                    "render": function(data, type, row, meta) {
                        let tampilan = `
                        <div class="d-flex">
                            <a href="/admin/santri-detail/${row.id}" class="btn btn-sm btn-primary mr-2">Detail</a>
                            </div>
                            `
                        return tampilan;
                    }
                },

            ]
        });

        $(".filter").on('change',function(){
            kelas = $("#filter-kelas").val()
            jenjang = $("#filter-jenjang").val()
            angkatan = $("#filter-angkatan").val()
            sakan = $("#filter-sakan").val()
            santri = $("#filter-santri").val()
            table.ajax.reload(null,false)
        })

        $("#row-tampilan_1 input[type='checkbox']").on('change',function(){
            let checkbox = $(this)
            let type = $(this).data('type')
            let kolom = $(this).data('kolom')
            let is_checked = checkbox[0].checked
            table.column(kolom).visible(is_checked)
            if (kolom == 0) {
                if (type == 'export') {
                    if (is_checked) {
                        $("#btn-export-santri").removeClass('d-none')
                    }else{
                        $("#btn-export-santri").addClass('d-none')
                    }
                } else {
                    if (is_checked) {
                        $("#btn-setlulus").removeClass('d-none')
                    }else{
                        $("#btn-setlulus").addClass('d-none')
                    }
                }
            }
        })

         // .cb-parent on change
        $(".cb-parent").on('change', function() {
            let is_checked = this.checked
            $(".cb-child").prop('checked', is_checked)
        })

        // onclick btn-export-santri
        $("#btn-export-santri button").on('click', function() {
            let ids = []
            $.each($(".cb-child"), function(key, checkbox) {
                if (checkbox.checked) {
                    ids.push(checkbox.value)
                }
            });
            if (ids.length == 0) {
                alert('Pilih santri terlebih dahulu')
                return false
            }
            if (ids.length > 0) {
                $("#ids").val(ids)
                $("#form-export-santri").submit()
            }
        })
            
    </script>
@endpush
