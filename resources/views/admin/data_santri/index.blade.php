@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Santri Aktif</li>
@endsection

@section('title', 'Data Santri Aktif')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_data_santri-create')
                <a href="{{ route('santri.create') }}" class="btn btn-primary btn-sm">Tambah Santri</a>
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-default">Import</button>
                <a href="{{ route('santri.export') }}" class="btn btn-primary btn-sm">Export</a>
                @endpermission
            </div>
        </div>
        
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Kelas</label>
                    <select id="filter-kelas" class="form-control filter">
                        <option value="">Filter Kelas</option>
                        <option value="1">Kelas 1</option>
                        <option value="2">Kelas 2</option>
                        <option value="3">Kelas 3</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Jenjang</label>
                    <select id="filter-jenjang" class="form-control filter">
                        <option value="">Filter Jenjang</option>
                        <option value="TSN">Tsanawiyyah</option>
                        <option value="MLN">Mu'allimin</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Sakan</label>
                    <select id="filter-sakan" class="form-control filter">
                        <option value="">Filter Sakan</option>
                        <option value="ya">Santri Sakan</option>
                        <option value="tidak">Santri Tidak Sakan</option>
                    </select>
                </div>
            </div>
            <div class="row" id="row-tampilan" style="display: none;">
                <div class="col-sm-3">
                    <label>
                        <input type="checkbox" class="tampilan" data-kolom="4"> Show Username
                    </label>
                </div>
            </div>
            @permission('menu_data_santri-create')
            <hr class="divider">
            <div class="row" id="row-tampilan">
                <div class="col-sm-3">
                    <label>
                        <input type="checkbox" class="tampilan" data-kolom="4"> Show Username
                    </label>
                </div>
                <div class="col-sm-3">
                    <label>
                        <input type="checkbox" data-type="kenaikan"> Show Naik Kelas
                    </label>
                    <div class="mb-12 col-md-12 d-none" id="btn-kenaikan">
                        <button class="btn btn-sm btn-success btn-block" id="btn-set-kenaikan">Submit Naik Kelas</button>
                    </div>
                </div>
                <div class="col-sm-3">
                    <label>
                        <input type="checkbox" class="tampilan" data-kolom="0" checked> Show Kelulusan
                    </label>
                    <div class="mb-12 col-md-12 d-none" id="btn-setlulus">
                        <button class="btn btn-sm btn-success btn-block" id="btn-set-lulus">Submit Kelulusan</button>
                    </div>
                </div>
                {{-- show export --}}
                <div class="col-sm-3">
                    <label>
                        <input type="checkbox" class="tampilan" data-kolom="0" data-type="export" checked> Show Export
                    </label>
                    <div class="mb-12 col-md-12 d-none" id="btn-export-santri">
                        <button class="btn btn-sm btn-success btn-block">Export</button>
                    </div>
                    <form action="{{ route('santri-aktif.export') }}" method="post" id="form-export-santri">
                        @csrf
                        <input type="hidden" name="ids" id="ids">
                    </form>
                </div>
            </div>
            @endpermission
            <hr class="divider">
            <div class="table-responsive">
                <table id="table-santri" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="cb-parent">
                            </th>
                            <th>NIS</th>
                            <th>NISN</th>
                            <th>Nama Lengkap</th>
                            <th>Username</th>
                            <th>Kelas</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                <h6 class="modal-title">Import File Santri</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form action="{{ route('santri.import') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-2">
                            <a href="{{ Storage::disk('s3')->url('static/template-santri.xlsx') }}" download="template-santri" class="btn btn-success btn-sm">Download Template</a>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="file" required class="custom-file-input @error('file') is-invalid @enderror" id="exampleInputFile" onchange="showFileName()">
                                        <label class="custom-file-label" for="exampleInputFile" id="fileRemark">Pilih file</label>
                                    </div>                
                                </div>
                                @error('file')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                    </div>     
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success btn-sm">Import</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@push('scripts')
    <script>
        $('.tampilan').prop('checked', false);

        let kelas = $("#filter-kelas").val()
        let jenjang = $("#filter-jenjang").val();
        let sakan = $("#filter-sakan").val();

        const table = $('#table-santri').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "order": [
                [1, "desc"]
            ],
            "autoWidth": false,
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/santri-data/{jenis}",
                type: "POST",
                data:function(d){
                    d.kelas = kelas;
                    d.jenjang = jenjang;
                    d.sakan = sakan;
                    return d
                }
            },
            "initComplete": function(settings, json) {
                const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
                $.each(all_checkbox_view, function(key, checkbox) {
                    let kolom = $(checkbox).data('kolom')
                    let is_checked = checkbox.checked
                    table.column(kolom).visible(is_checked)
                })
                setTimeout(function() {
                    table.columns.adjust().draw();
                }, 3000)
            },
            columnDefs: [
                {
                    targets: '_all',
                    visible: true
                },
                {
                    "targets": 0,
                    "class": "text-nowrap",
                    "sortable": false,
                    "render": function(data, type, row, meta) {
                        return `<input type="checkbox" class="cb-child" value="${row.id}">`;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        // list_karyawan[row.id] = row;
                        return row.nis;
                    }
                },
                {
                    "targets": 2,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nisn;
                    }
                },
                {
                    "targets": 3,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nama_lengkap;
                    }
                },
                {
                    "targets": 4,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.user.username;
                    }
                },
                {
                    "targets": 5,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        // return row.tingkat_sekolah_id + " - " + (row.kelas == 'XII' ? '3' : '' || row
                        //     .kelas == 'XI' ? '2' : '' || row.kelas == 'X' ? '1' : '');
                        let kelas = row.tingkat_sekolah_id == 'LULUS' ? '' : " - "+row.kelas 
                        return row.tingkat_sekolah_id + kelas;
                    }
                },
                {
                    "targets": 6,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        let badgeLulus = row.status_active == "alumni" ? 'badge-success' : 'badge-secondary';
                        return row.status_active == "alumni" ? 
                        `<span class="badge ${badgeLulus}">Lulus</span>` : 
                        `<span class="badge ${badgeLulus}">${row.status_active}</span>`;
                    }
                },
                {
                    "targets": 7,
                    "render": function(data, type, row, meta) {
                        let tampilan = `
                            <div class="d-flex">
                                @permission('menu_data_santri-update')
                                <a href="/admin/santri-detail/${row.id}" class="btn btn-sm btn-primary mr-2">Detail</a>
                                <a href="/admin/santri-edit/${row.id}" class="btn btn-sm btn-warning mr-2">Edit</a>
                                @endpermission
                            </div>
                            `
                        /*
                        @permission('menu_data_santri-delete')
                        <a href="/admin/santri-delete/${row.id}" onclick="return confirm('Apakah ingin menghapus santri ini')"  class="btn btn-sm btn-danger mr-2">Delete</a>
                        @endpermission*/
                        return tampilan;
                    }
                },

            ]
        });

        $(".filter").on('change',function(){
            kelas = $("#filter-kelas").val()
            jenjang = $("#filter-jenjang").val()
            sakan = $("#filter-sakan").val()
            table.ajax.reload(null,false)
        })

        $("#row-tampilan input[type='checkbox']").on('change',function(){
            let checkbox = $(this)
            let kolom = $(this).data('kolom')
            let type = $(this).data('type')
            let is_checked = checkbox[0].checked
            table.column(kolom).visible(is_checked)
            if (kolom == 0) {
                if (type == 'export') {
                    table.column(kolom).visible(is_checked)
                    if (is_checked) {
                        $("#btn-export-santri").removeClass('d-none')
                    }else{
                        $("#btn-export-santri").addClass('d-none')
                    }
                } else {
                    table.column(kolom).visible(is_checked)
                    if (is_checked) {
                        $("#btn-setlulus").removeClass('d-none')
                    }else{
                        $("#btn-setlulus").addClass('d-none')
                    }
                }
            }

            if(kolom == 4) {
                table.column(kolom).visible(is_checked)
            }

            if (checkbox[0].dataset.type == 'kenaikan') {
                table.column(kolom).visible(is_checked)
                if (is_checked) {
                    $("#btn-kenaikan").removeClass('d-none')
                }else{
                    $("#btn-kenaikan").addClass('d-none')
                }
            }
            
        })
        // .cb-parent on change
        $(".cb-parent").on('change', function() {
            let is_checked = this.checked
            $(".cb-child").prop('checked', is_checked)
        })

        // onclick btn-set-lulus
        $("#btn-set-lulus").on('click', function() {
            let ids = []
            $.each($(".cb-child"), function(key, checkbox) {
                if (checkbox.checked) {
                    ids.push(checkbox.value)
                }
            });
            if (ids.length == 0) {
                alert('Pilih santri terlebih dahulu')
                return false
            }
            if (ids.length > 0) {
                $.ajax({
                    url: "{{ env('APP_URL') }}/admin/santri/set-lulus",
                    type: "PUT",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: ids
                    },
                    success: function(res) {
                        $(".cb-parent").prop('checked', false)
                        $("#btn-setlulus").addClass('d-none')
                        // tampilan data kolom 0 di hide
                        $("#row-tampilan div input[type='checkbox'][data-kolom='0']").prop('checked', false)
                        table.column(0).visible(false)
                        if (res.status == 'success') {
                            toastr.success('{{ session('success') }}', 'BERHASIL! '+ res.message); 
                            table.ajax.reload(null, false)
                        }else{
                            toastr.error('{{ session('error') }}', 'GAGAL! '+ res.message); 
                        }
                    }
                })
            }
        })

        // onclick btn-set-kenaikan
        $("#btn-set-kenaikan").on('click', function() {
            if(confirm('Data Santri akan naik semuanya')) {
                $.ajax({
                    url: "{{ env('APP_URL') }}/admin/santri/set-kenaikan",
                    type: "PUT",
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(res) {
                        $(".cb-parent").prop('checked', false)
                        $("#btn-setlulus").addClass('d-none')
                        // tampilan data kolom 0 di hide
                        $("#row-tampilan div input[type='checkbox'][data-kolom='0']").prop('checked', false)
                        table.column(0).visible(false)
                        if (res.status == 'success') {
                            toastr.success('{{ session('success') }}', 'BERHASIL! '+ res.message); 
                            table.ajax.reload(null, false)
                        }else{
                            toastr.error('{{ session('error') }}', 'GAGAL! '+ res.message); 
                        }
                    }
                })
            }
        })

        // onclick btn-export-santri
        $("#btn-export-santri button").on('click', function() {
            let ids = []
            $.each($(".cb-child"), function(key, checkbox) {
                if (checkbox.checked) {
                    ids.push(checkbox.value)
                }
            });
            if (ids.length == 0) {
                alert('Pilih santri terlebih dahulu')
                return false
            }
            if (ids.length > 0) {
                $("#ids").val(ids)
                $("#form-export-santri").submit()
            }
        })

        function showFileName(){
            let input = document.getElementById('exampleInputFile');
            let fileName = input.files[0].name;
            document.getElementById('fileRemark').innerText = 'File : ' + fileName;
        }
    </script>
@endpush
