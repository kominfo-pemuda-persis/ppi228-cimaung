@extends('layouts.dashboard')

@section('title', 'Penilaian')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Penilaian</li>
@endsection

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card card-outline card-success">
        <div class="card-body">
          <form action="{{  route('penilaian.createRaporNilai') }}" method="post">
            @csrf
            <div class="row">
              
              <div class="col-md-6">
                <div class="form-group">
                    <label for="pelajaran">PIlih Pelajaran</label>
                    <select name="pelajaran" id="pelajaran" required class="form-control select2 select2bs4" style="width: 100%;">
                      @foreach($dataPelajaran as $pelajaran)
                        <option value="{{ $pelajaran->id }}" {{ request('pelajaran') == $pelajaran->id ? 'selected':'' }}>{{ $pelajaran->nama_pelajaran }}</option>
                      @endforeach
                    </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label for="kelas">PIlih Kelas</label>
                    <select name="kelas" id="kelas" required class="form-control">
                        <option value="1" {{ request('kelas') == '1' ? 'selected':'' }}>1</option>
                        <option value="2" {{ request('kelas') == '2' ? 'selected':'' }}>2</option>
                        <option value="2" {{ request('kelas') == '3' ? 'selected':'' }}>3</option>
                    </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label for="tingkat">PIlih Tingkat Sekolah</label>
                    <select name="tingkat" id="tingkat" required class="form-control">
                        <option value="TSN" {{ request('tingkat') == 'TSN' ? 'selected':'' }}>Tsanawiyyah</option>
                        <option value="MLN" {{ request('tingkat') == 'MLN' ? 'selected':'' }}>Muallimien</option>
                    </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label for="semester">Pilih Semester</label>
                    <select name="semester" id="semester" required class="form-control">
                      @foreach($dataSemester as $semester)
                      <option value="{{ $semester->id }}" {{ request('semester') == $semester->id ? 'selected':'' }}>{{ ucfirst($semester->nama) }} | {{ $semester->start_year }} - {{ $semester->end_year }}</option>
                      @endforeach
                    </select>
                </div>
              </div>

            </div>

            <div class="row">
              <div class="col-md">
                <button type="submit" class="btn btn-primary">Beri Nilai</button>
              </div>
            </div>
          </form>
        </div>
    </div>

    @if($rapor)
    <div class="card">
      <div class="card-header">
        <h5>Pengisian Penilaian</h5>
      </div>
      <form action="{{ route('penilaian.updatePenilaian') }}" method="post">
        @csrf
        <div class="card-body">
          <div class="form-group row">
            <label for="kb" class="col-3 col-form-label">Ketuntasan Belajar (KB)</label>
            <div class="col-2">
              <input type="number" name="kb" required class="form-control" id="kb" value="{{ $kb }}">
            </div>
          </div>
  
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr class="text-center">
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Nilai</th>
                  <th scope="col">Deskripsi (Maksimal 2000 karakter)</th>
                </tr>
              </thead>
              <tbody>
                
                @foreach($nilaiRaporPendidikan as $nilai)
                  <tr class="text-center">
                    <td scope="row">{{ $loop->iteration }}</td>
                    <td>{{ $nilai->raporPendidikan->santri->nama_lengkap }}</td>
                    <td>
                      <input type="hidden" name="id[]" required class="form-control" value="{{ $nilai->id }}">
                      <input type="number" name="nilai[]" required class="form-control" value="{{ $nilai->nilai }}" @if($status == 'BAGI RAPOR') readonly @endif min="0" max="100">
                    </td>
                    <td>
                      <textarea name="deskripsi[]"  class="form-control" placeholder="Masukkan Deskripsi" @if($status == 'BAGI RAPOR') readonly @endif maxlength="2000">{{ $nilai->deskripsi }}</textarea>
                    </td>
                  </tr>
                @endforeach
                
              </tbody>
            </table>
          </div>
  
        </div>
        @if($status == 'PENILAIAN')
        <div class="card-footer text-right">
          <button type="submit" class="btn btn-primary">Simpan Nilai</button>
        </div>
        @endif
      </form>
    </div>
    @endif

@endsection

@push('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endpush