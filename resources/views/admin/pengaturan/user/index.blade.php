@extends('layouts.dashboard')

@section('menu-pengaturan', 'menu-open')
@section('pengaturan', 'active')
@section('user', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> User</li>
@endsection

@section('title', 'Data User')

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8">
                    @permission('menu_user-create')
                    <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm">Tambah User</a>
                    @endpermission
                    <a href="{{ route('refresh.data.duplicate') }}" class="btn btn-secondary btn-sm"><i class="fas fa-sync-alt"></i> Refresh Data Duplikat</a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Roles</label>
                    <select id="filter-role" class="form-control filter">
                        <option value="">Filter Role</option>
                        <option value="admin">Admin</option>
                        <option value="mudir_am">Mudir'am</option>
                        <option value="tasykil">Tasykil</option>
                        <option value="mudir_mln">Mudir MLN</option>
                        <option value="mudir_tsn">Mudir TSN</option>
                        <option value="asatidz_mln">Asatidz MLN</option>
                        <option value="asatidz_tsn">Asatidz TSN</option>
                        <option value="orangtua_mln">Orangtua MLN</option>
                        <option value="orangtua_tsn">Orangtua TSN</option>
                        <option value="santri_mln">Santri MLN</option>
                        <option value="santri_tsn">Santri TSN</option>
                    </select>
                </div>
            </div>
            <hr class="divider">
            <table class="table table-bordered" id="table-users">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    {{-- DELETE --}}
    <div class="modal fade" id="modal-delete" ria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title">Konfirmasi Hapus</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method="post" id="deleteConfirm">
                    @method('delete')
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="hapus_user_id">
                        <div id="konfirmasi-hapus"></div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">Hapus</button>
                        <!-- <button type="button" id="destroy" class="btn btn-danger">Hapus</button> -->
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    @if ($errors->any())
        <script>
            $('#modal-tambah').modal('show');
        </script>
    @endif
    <script>
        let roles = $("#filter-role").val()

        $(".filter").on('change',function(){
            roles = $("#filter-role").val()
            table.ajax.reload(null,false)
        })

        const table = $('#table-users').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "order": [
                [1, "desc"]
            ],
            "autoWidth": false,
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/pengaturan/users-data/{jenis}",
                type: "POST",
                data:function(d){
                    d.roles = roles;
                    return d
                }
            },
            "initComplete": function(settings, json) {
                const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
                $.each(all_checkbox_view, function(key, checkbox) {
                    let kolom = $(checkbox).data('kolom')
                    let is_checked = checkbox.checked
                    table.column(kolom).visible(is_checked)
                })
                setTimeout(function() {
                    table.columns.adjust().draw();
                }, 3000)
            },
            columnDefs: [
                {
                    targets: '_all',
                    visible: true
                },
                {
                    "targets": 0,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.name;
                    }
                },
                {
                    "targets": 2,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.email;
                    }
                },
                {
                    "targets": 3,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        var labels = [];
                        var rolesString = row.display_names; // Ambil string peran dari objek row

                        // Pisahkan string peran menjadi array menggunakan koma sebagai delimiter, kemudian hapus spasi di awal dan akhir setiap elemen
                        var rolesArray = rolesString.split(',').map(function(role) {
                            return role.trim(); // Output: ["Admin", "Tasykil", "Asatidz"]
                        });

                        // Loop melalui setiap peran dan tambahkan label dengan warna primary yang sesuai
                        rolesArray.forEach(function(role) {
                            var labelClass = '';
                            var labelText = role.trim();
                            if (labelText === 'asatidz') {
                                labelClass = 'badge badge-primary';
                            } else if (labelText === 'tasykil') {
                                labelClass = 'badge badge-success';
                            } else if (labelText === 'admin') {
                                labelClass = 'badge badge-danger';
                            } else {
                                labelClass = 'badge badge-secondary'; // Warna default jika peran tidak cocok dengan yang disebutkan di atas
                            }

                            // Tambahkan label ke array labels
                            labels.push('<span class="' + labelClass + '">' + labelText + '</span>');
                        });

                        // Gabungkan label-label menjadi satu string
                        var labelsString = labels.join(' ');

                        return labelsString;
                    }
                },
                {
                    "targets": 4,
                    "render": function(data, type, row, meta) {
                        let tampilan = '<div class="d-flex">';
                        // Periksa izin dan tambahkan tombol Edit jika diperbolehkan
                        tampilan += `<a href="/admin/pengaturan/users/${row.id}/edit" class="btn btn-warning btn-sm mr-2">Edit</a>`;
                        
                        // Periksa izin dan tambahkan tombol Hapus jika diperbolehkan
                        tampilan += `<a href="javascript:void(0)" data-id="${row.id}" data-name="${row.name}" id="delete" class="btn btn-danger btn-sm">Hapus</a>`;
                        
                        tampilan += '</div>';
                        
                        return tampilan;
                    }
                },

            ]
        });

        $('body').on('click', '#delete', function() {
            let id = $(this).data('id');
            $('#hapus_user_id').val(id);

            let name = $(this).data('name');

            $("#deleteConfirm").attr("action", `/admin/pengaturan/users/${id}`);

            // Membersihkan konten modal sebelum menambahkan konten baru
            $('#konfirmasi-hapus').empty().append(`<span>Yakin ingin hapus data <strong>${name}</strong> ?</span>`);

            // Buka modal
            $('#modal-delete').modal('show');
        });

        $('#destroy').click(function(e) {
            // e.preventDefault();
            const id = $('#hapus_user_id').val();
            // console.log(id, 'idnya')

            // Ajax request
            $.ajax({
                url: `/admin/pengaturan/users/${id}`,
                type: "DELETE",
                cache: false,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(response) {
                    console.log(response);
                    // Menampilkan pesan sukses
                    toastr.success(response.success, 'BERHASIL!');
                    setTimeout(() => {
                        window.location.href = response.Url
                    }, 1000);

                    // Menutup modal
                    $('#modal-delete').modal('hide');
                },
                error: function(response) {
                    console.log(response);
                    const error = response.responseJSON.errors;
                    for (const message in error) {
                        toastr.error(error[message], 'GAGAL!');
                    }
                }
            });

            // e.preventDefault();
        });
    
    </script>
@endpush
