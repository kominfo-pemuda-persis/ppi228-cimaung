@extends('layouts.dashboard')

@section('title', 'Create User')

@section('menu-pengaturan', 'menu-open')
@section('pengaturan', 'active')
@section('user', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Create User</li>
@endsection

@section('content')
    
    <div class="card">
        <div class="card-header">
            <a href="{{ route('users.index') }}" class="btn btn-dark">Back</a>
        </div>
        <form action="{{ route('users.store') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input required type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" required placeholder="Nama" value="{{ old('name') }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input required type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" placeholder="Username" value="{{ old('username') }}">
                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input required type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="example@gmail.com" value="{{ old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input required type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>  
                        <div class="form-group">
                            <label for="password-confirm">Konfirmasi Password</label>
                            <input required type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" id="password-confirm" placeholder="Konfirmasi Password">
                        </div>

                        <div class="form-group">
                            <label>Role</label>
                            <select required class="select2bs4 @error('role') is-invalid @enderror" multiple="multiple" data-placeholder="Pilih Role"
                                    style="width: 100%;" name="role[]">
                                @foreach($roles as $role)
                                <option value="{{ $role->name }}">{{ $role->display_name }}</option>
                                @endforeach
                            </select>
                            @error('role')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div> 
                  
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary w-30"><i class="fas fa-paper-plane"></i> Save</button>
                </div>    
            </div>
        </form>
    </div>

@endsection

@push('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('admin_lte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })
  
</script>
@endpush