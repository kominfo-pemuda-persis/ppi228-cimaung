@extends('layouts.dashboard')

@section('title', 'Create Permission')

@section('menu-pengaturan', 'menu-open')
@section('pengaturan', 'active')
@section('permission', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Create Permission</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <a href="{{ route('permission.index') }}" class="btn btn-dark">Back</a>
        </div>
        <form action="{{ route('permission.store') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="menu">Menu</label>
                            <select name="menu" id="menu" class="form-control @error('menu') is-invalid @enderror" value="{{ old('menu') }}" required>
                                <option value="">Choose Menu</option>
                                @foreach($menus as $menu)
                                    <option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                @endforeach
                            </select>
                            @error('menu')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="display_name">Display Name</label>
                            <input type="text" class="form-control @error('display_name') is-invalid @enderror" name="display_name" id="display_name" required placeholder="Display Name" value="{{ old('display_name') }}">
                            @error('display_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="desrciption">Desrciption</label>
                            <input type="text" class="form-control @error('desrciption') is-invalid @enderror" name="desrciption" id="desrciption" placeholder="Desrciption" value="{{ old('description') }}">
                            @error('desrciption')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>    
                
                <div class="row">
                    <div class="col-md-6">
                        <hr>
                        <button type="submit" class="btn btn-primary w-100"><i class="fas fa-paper-plane"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection