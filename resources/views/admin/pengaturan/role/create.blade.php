@extends('layouts.dashboard')

@section('title', 'Create Role')

@section('menu-pengaturan', 'menu-open')
@section('pengaturan', 'active')
@section('role', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Create Role</li>
@endsection

@section('content')

<form action="{{ route('role.store') }}" method="post">
    @csrf
    <div class="card">
        <div class="card-header">
            <a href="{{ route('role.index') }}" class="btn btn-dark"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="display_name">Display Name</label>
                        <input type="text" class="form-control @error('display_name') is-invalid @enderror" name="display_name" id="display_name" required placeholder="Display Name" value="{{ old('display_name') }}">
                        @error('display_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="description">description</label>
                        <input type="text" class="form-control @error('description') is-invalid @enderror" name="description" id="description" placeholder="description" value="{{ old('description') }}">
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>  
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Form Menu - Permission
        </div>
        <div class="card-body">
            <div class="row">
                @foreach($menus as $menu)
                <div class="col-md-4">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header bg-info" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left btn-sm text-light" type="button" data-toggle="collapse" data-target="#collapse{{ $menu->id }}" aria-expanded="true" aria-controls="collapseOne">
                                    <h5>{{ $menu->name }}</h5>
                                    @error('permissions_id')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    </button>
                                </h2>
                            </div>
        
                            <div id="collapse{{ $menu->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                @foreach($menu->permissions as $permission)
                                    <div class="form-check d-inline">
                                        &nbsp;<input
                                            class="form-check-input @error('permissions_id') is-invalid @enderror"
                                            type="checkbox" id=""
                                            name="permissions_id[]"
                                            value="{{ $permission->id }}"
                                        >
                                        <label class="form-check-label" for="">{{ $permission->display_name }}</label>
                                        <br>
                                    </div>
                                @endforeach

                                @error('permissions_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit" class="btn btn-primary btn-lg"><i class="fas fa-paper-plane"></i> Save</button>
        </div>
    </div>
</form>

@endsection