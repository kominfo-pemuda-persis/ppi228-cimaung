@extends('layouts.dashboard')

@section('title', 'Role')

@section('menu-pengaturan', 'menu-open')
@section('pengaturan', 'active')
@section('role', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Role</li>
@endsection

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8">
                    @permission('menu_role-create')
                    <a href="{{ route('role.create') }}" class="btn btn-primary">Create role</a>
                    @endpermission
                </div>

                <div class="col-md-4">
                    <form method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search by name..." name="search" value="{{ request('search') }}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-primary" type="submit" id="button-addon2">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="card-body">
            <table class="table table-striped table-responsive-sm table-sm" width="100%">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Name</th>
                        {{-- <th>Display Name</th> --}}
                        {{-- <th>Permission</th> --}}
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($datas as $data)
                        <tr class="text-center">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->name }}</td>
                            {{-- <td>{{ $data->display_name }}</td> --}}
                            {{-- <td>
                                @foreach($data->permissions as $permission)
                                    <span class="badge badge-success">
                                        {{ $permission->display_name }}
                                    </span>
                                @endforeach
                            </td> --}}
                            <td>{{ $data->description }}</td>
                            <td>
                                <form action="{{ route('role.destroy', $data->id) }}" method="post">
                                  @method('delete')
                                  @csrf
                                  <input type="hidden" value="{{ $data->id }}">
                                  <div class="btn-group" role="group" aria-label="Basic example">
                                    @permission('menu_role-update')
                                    <a href="{{ route('role.edit', $data->id) }}" class="btn btn-warning">Edit</a>
                                    @endpermission
                                    @permission('menu_role-delete')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('are you sure for delete this data?')">Delete</button>
                                    @endpermission
                                  </div>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <div class="alert alert-danger">
                            Data Menu not available.
                        </div>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="card-footer">
            <div class="d-flex justify-content-center">
            {{ $datas->links() }}
            </div>
        </div>
    </div>

@endsection