@extends('layouts.dashboard')

@section('title', 'Edit Menu')

@section('menu-pengaturan', 'menu-open')
@section('pengaturan', 'active')
@section('menu', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Edit Menu</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <a href="{{ route('menu.index') }}" class="btn btn-dark">Back</a>
        </div>
        <form action="{{ route('menu.update', $menu->id) }}" method="post">
            @method('put')
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" required placeholder="Name" value="{{ $menu->name }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="desrciption">Desrciption</label>
                            <input type="text" class="form-control @error('desrciption') is-invalid @enderror" name="desrciption" id="desrciption" placeholder="Desrciption" value="{{ $menu->description }}">
                            @error('desrciption')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>    
                
                <div class="row">
                    <div class="col-md-6">
                        <hr>
                        <button type="submit" class="btn btn-primary w-100"><i class="fas fa-paper-plane"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection