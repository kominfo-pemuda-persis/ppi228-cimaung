@extends('layouts.dashboard')

@section('title', 'Edit Kokurikuler')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Edit Kokurikuler</li>
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('kokurikuler.index') }}" class="btn btn-sm btn-dark">Kembali</a>
            </div>
            <form action="{{ route('kokurikuler.update', $data->id) }}" method="post">
                @method('put')
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" required placeholder="Masukkan Nama" value="{{ $data->nama }}">
                        @error('nama')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>  

                    <button type="submit" class="btn btn-primary w-100"><i class="fas fa-paper-plane"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection