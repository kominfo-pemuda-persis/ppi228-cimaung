@extends('layouts.dashboard')

@section('title', 'Kokurikuler')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Kokurikuler</li>
@endsection

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8">
                  @permission('menu_kokurikuler-read')
                    <a href="{{ route('kokurikuler.create') }}" class="btn btn-primary">Tambah Kokurikuler</a>
                  @endpermission
                </div>

                <div class="col-md-4">
                    <form method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search by name..." name="search" value="{{ request('search') }}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-primary" type="submit" id="button-addon2">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="card-body">
            <table class="table table-striped table-responsive-sm table-sm" width="100%">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($datas as $data)
                        <tr class="text-center">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->nama }}</td>
                            <td>
                                <form action="{{ route('kokurikuler.destroy', $data->id) }}" method="post">
                                  @method('delete')
                                  @csrf
                                  <input type="hidden" value="{{ $data->id }}">
                                  <div class="btn-group" role="group" aria-label="Basic example">
                                    @permission('menu_kokurikuler-update')
                                    <a href="{{ route('kokurikuler.edit', $data->id) }}" class="btn btn-warning">Edit</a>
                                    @endpermission
                                    @permission('menu_kokurikuler-delete')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('are you sure for delete this data?')">Hapus</button>
                                    @endpermission
                                  </div>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <div class="alert alert-danger">
                            Data Menu not available.
                        </div>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="card-footer">
            <div class="d-flex justify-content-center">
            {{ $datas->links() }}
            </div>
        </div>
    </div>

@endsection