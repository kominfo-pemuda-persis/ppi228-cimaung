@extends('layouts.dashboard')

@push('css')
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.css')}}">
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Asatidz & Pelajaran</li>
@endsection

@section('title', 'Data Asatidz & Pelajaran')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                @permission('menu_astidz_pelajaran-create')
                <button type="button" data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-sm">Tambah
                    Asatidz & Pelajaran</button>
                @endpermission
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Kelas</label>
                    <select id="filter-kelas" class="form-control filter">
                        <option value="">Pilih Kelas</option>
                        <option value="1">Kelas 1</option>
                        <option value="2">Kelas 2</option>
                        <option value="3">Kelas 3</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Jenjang</label>
                    <select id="filter-jenjang" class="form-control filter">
                        <option value="">Pilih Jenjang</option>
                        <option value="TSN">Tsanawiyyah</option>
                        <option value="MLN">Mu'allimin</option>
                    </select>
                </div>
            </div>
            <hr class="divider">
            <div class="table-responsive">
                <table class="table table-bordered" id="table-asatidz">
                    <thead>
                        <tr>
                            <th width="30%">Nama Asatidz</th>
                            <th width="40%">Pelajaran</th>
                            <th>Kelas</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Asatidz & Pelajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('asatidz-pelajaran.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="asatidz_id">Nama Asatidz</label>
                            <select name="asatidz_id" id="asatidz_id" class="form-control select2bs4" required>
                                <option value="" selected disabled>Pilih Nama Asatidz</option>
                                @foreach ($asatidz as $key => $data)
                                    <option value="{{ $key }}">{{ $data }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pelajaran_id">Pelajaran</label>
                            <select name="pelajaran_id" id="pelajaran_id" class="form-control select2bs4" required>
                                <option value="" selected disabled>Pilih Pelajaran</option>
                                @foreach ($pelajaran as $data)
                                    <option value="{{ $data->id }}">{{ $data->nama_pelajaran ." - ". $data->tingkat_sekolah_id }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="kelas_id">Kelas</label>
                            <select name="kelas_id" id="kelas_id" class="form-control" required>
                                <option value="" selected disabled>Pilih Kelas</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                            <select name="tingkat_sekolah_id" id="tingkat_sekolah_id" class="form-control" required>
                                <option value="" selected disabled>Pilih Tingkat Sekolah</option>
                                <option value="TSN">TSN</option>
                                <option value="MLN">MLN</option>
                            </select>
                        </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script src="{{ asset('admin_lte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        let kelas = $("#filter-kelas").val()
        let jenjang = $("#filter-jenjang").val();

        const table = $('#table-asatidz').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "order": [
                [1, "desc"]
            ],
            "autoWidth": false,
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/asatidz-pelajaran-data/{jenis}",
                type: "POST",
                data:function(d){
                    d.kelas = kelas;
                    d.jenjang = jenjang;
                    return d
                }
            },
            "initComplete": function(settings, json) {
                const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
                $.each(all_checkbox_view, function(key, checkbox) {
                    let kolom = $(checkbox).data('kolom')
                    let is_checked = checkbox.checked
                    table.column(kolom).visible(is_checked)
                })
                setTimeout(function() {
                    table.columns.adjust().draw();
                }, 3000)
            },
            columnDefs: [
                {
                    targets: '_all',
                    visible: true
                },
                {
                    "targets": 0,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nama_lengkap;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nama_pelajaran
                    }
                },
                {
                    "targets": 2,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.tingkat_sekolah_id + " - " + row.kelas_id;
                    }
                },
                {
                    "targets": 3,
                    "render": function(data, type, row, meta) {
                        let tampilan = `
                        <div class="d-flex">
                            @permission('menu_astidz_pelajaran-update')
                            <a href="/admin/asatidz-pelajaran-ubah/${row.id_ap}" class="btn btn-sm btn-warning mr-2">Edit</a>
                            @endpermission
                            @permission('menu_astidz_pelajaran-delete')
                            <a href="/admin/asatidz-pelajaran-delete/${row.id_ap}" onclick="return confirm('Apakah ingin menghapus asatidz dan pelajaran ini')"  class="btn btn-sm btn-danger mr-2">Delete</a>
                            @endpermission
                            </div>
                            `
                        return tampilan;
                    }
                },

            ]
        });

        $('body').on('click', '#ubah', function() {
            let id = $(this).data('id');
            //fetch detail post with ajax
            $.ajax({
                url: `/admin/asatidz-pelajaran-ubah/${id}`,
                type: "GET",
                cache: false,
                success: function(response) {
                    console.log(response.data[0]);
                    //fill data to form
                    $('#id_asatidz').val(response.data[0].id);
                    $('#edit_asatidz_id').val(response.data[0].asatidz.nama_lengkap);
                    $('#edit_pelajaran_id').val(response.data[0].pelajaran.id);
                    $('#edit_kelas_id').val(response.data[0].kelas_id);
                    $('#edit_tingkat_sekolah_id').val(response.data[0].tingkat_sekolah_id);
                    //open modal
                    $('#modal-edit').modal('show');
                }
            });
        });

        $(".filter").on('change',function(){
            kelas = $("#filter-kelas").val()
            jenjang = $("#filter-jenjang").val()
            table.ajax.reload(null,false)
        })

        $("#row-tampilan input[type='checkbox']").on('change',function(){
            let checkbox = $(this)
            let kolom = $(this).data('kolom')
            let is_checked = checkbox[0].checked
            table.column(kolom).visible(is_checked)
        })
    </script>
@endpush
