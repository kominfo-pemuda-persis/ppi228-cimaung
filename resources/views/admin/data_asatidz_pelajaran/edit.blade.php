@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Edit Asatidz Pelajaran</li>
@endsection

@section('title', 'Data Asatidz dan Pelajaran')

@section('content')
    <div class="card p-2">
        <form action="{{ route('asatidz-pelajaran.update', $data->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="asatidz_id">Nama Asatidz</label>
                        <input type="text" value="{{ $data->asatidz->nama_lengkap }}" class="form-control" disabled>
                        <input type="hidden" value="{{ $data->asatidz->id }}" name="old_asatidz" class="form-control">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="kelas_id">Pelajaran</label>
                        <select name="pelajaran_id"  class="form-control select2bs4">
                            @foreach ($pelajaran as $data_pel)
                                <option value="{{ $data_pel->id }}" {{ $data->pelajaran->id == $data_pel->id ? 'selected' : '' }}>{{ $data_pel->nama_pelajaran }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="kelas_id">Kelas</label>
                        <select name="kelas_id" class="form-control">
                            <option value="1" {{ $data->kelas_id == "1" ? 'selected' : '' }}>1</option>
                            <option value="2" {{ $data->kelas_id == "2" ? 'selected' : '' }}>2</option>
                            <option value="3" {{ $data->kelas_id == "3" ? 'selected' : '' }}>3</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                        <select name="tingkat_sekolah_id" class="form-control">
                            <option value="TSN" {{ $data->tingkat_sekolah_id == "TSN" ? 'selected' : '' }}>TSN</option>
                            <option value="MLN" {{ $data->tingkat_sekolah_id == "MLN" ? 'selected' : '' }}>MLN</option>
                        </select>
                    </div>
                </div>
            </div>
            <a href="{{ route('asatidz-pelajaran.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
@endsection

@push('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })
</script>
@endpush