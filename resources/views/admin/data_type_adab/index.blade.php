@extends('layouts.dashboard')

@section('title', 'Type Adab')

<!-- @section('typeadab', 'active') -->

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Type Adab</li>
@endsection

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8">
                    @permission('menu_type_adab-create')
                    <a href="{{ route('type-adab.create') }}" class="btn btn-primary">Create Type Adab</a>
                    @endpermission
                </div>

                <div class="col-md-4">
                    <form method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search by name..." name="search" value="{{ request('search') }}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-primary" type="submit" id="button-addon2">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="card-body">
            <table class="table table-striped table-responsive-sm table-sm" width="100%">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($datas as $data)
                        <tr class="text-center">
                            <td>{{ $datas->firstItem() + $loop->index }}</td>
                            <td>{{ $data->nama }}</td>
                            <td>{{ $data->keterangan }}</td>
                            <td>
                                <form action="{{ route('type-adab.delete', $data->id) }}" method="post">
                                  @method('delete')
                                  @csrf
                                  <input type="hidden" value="{{ $data->id }}">
                                  <div class="btn-group" role="group" aria-label="Basic example">
                                    @permission('menu_type_adab-update')
                                    <a href="{{ route('type-adab.edit', $data->id) }}" class="btn btn-warning">Edit</a>
                                    @endpermission
                                    @permission('menu_type_adab-delete')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('are you sure for delete this type adab?')">Delete</button>
                                    @endpermission
                                  </div>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <div class="alert alert-danger">
                            Data Type Adab not available.
                        </div>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="card-footer">
            <div class="d-flex justify-content-center">
            {{ $datas->links() }}
            </div>
        </div>
    </div>

@endsection