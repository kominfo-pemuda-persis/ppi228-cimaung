@extends('layouts.dashboard')

@section('title', 'Type Adab')

@section('typeadab', 'active')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Create Menu</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <a href="{{ route('type-adab.index') }}" class="btn btn-dark">Back</a>
        </div>
        <form action="{{ route('type-adab.store') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="nama" required placeholder="Nama" value="{{ old('nama') }}">
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="desrciption">Keterangan</label>
                            <input type="text" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" id="keterangan" placeholder="Keterangan" value="{{ old('keterangan') }}">
                            @error('keterangan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>    
                
                <div class="row">
                    <div class="col-md-6">
                        <hr>
                        <button type="submit" class="btn btn-primary w-100"><i class="fas fa-paper-plane"></i> Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection