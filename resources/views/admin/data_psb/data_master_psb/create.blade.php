@extends('layouts.dashboard')

@section('title', 'Create Tahun Ajaran')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Tambah Tahun Ajaran</li>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('psb.index') }}" class="btn btn-primary btn-sm">Kembali</a>
                </div>
                <form action="{{ route('psb.store') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="start_year">Tahun Ajaran Mulai</label>
                                    <input type="text" class="startSet form-control @error('start_year') is-invalid @enderror" name="start_year" id="start_year" required value="{{ old('start_year') }}" />
                                    @error('start_year')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="end_year">Tahun Ajaran Selesai</label>
                                    <input type="text" class="endSet form-control @error('end_year') is-invalid @enderror" name="end_year" id="end_year" required value="{{ old('end_year') }}" />
                                    @error('end_year')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="opened_at">Pembukaan Pendaftaran TSN/MLN</label>
                                    <input type="date" class="form-control @error('opened_at') is-invalid @enderror" name="opened_at" id="opened_at" required value="{{ old('opened_at') }}">
                                    @error('opened_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="closed_at">Penutupan Pendaftaran TSN/MLN</label>
                                    <input type="date" class="form-control @error('closed_at') is-invalid @enderror" name="closed_at" id="closed_at" required value="{{ old('closed_at') }}">
                                    @error('closed_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="opened_mi_at">Pembukaan Pendaftaran MI</label>
                                    <input type="date" class="form-control @error('opened_mi_at') is-invalid @enderror" name="opened_mi_at" id="opened_mi_at" required value="{{ old('opened_mi_at') }}">
                                    @error('opened_mi_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="closed_mi_at">Penutupan Pendaftaran MI</label>
                                    <input type="date" class="form-control @error('closed_mi_at') is-invalid @enderror" name="closed_mi_at" id="closed_mi_at" required value="{{ old('closed_mi_at') }}">
                                    @error('closed_mi_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        </div>   
                        
                        <div class="row">
                            <div class="col-md-12">
                              <button type="submit" class="btn btn-sm btn-primary w-100"><i class="fas fa-paper-plane"></i> Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script src="{{ asset('datepicker/js/yearpicker.js') }}"></script>
<Script>
    $(document).ready(function() {
        $(".startSet").yearpicker({
            year: 2023,
            startYear: 2010,
            endYear: 2050,
        });

        $(".endSet").yearpicker({
            year: 2024,
            startYear: 2010,
            endYear: 2050,
        });
    });
</Script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('datepicker/css/yearpicker.css') }}" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
@endpush