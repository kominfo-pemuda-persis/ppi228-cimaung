@extends('layouts.dashboard')

@section('title', 'Set Tahun Ajaran')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Set Tahun Ajaran</li>
@endsection

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8">
                    @permission('menu_set_tahun_ajaran-create')
                    <a href="{{ route('psb.create') }}" class="btn btn-primary btn-sm">Create Tahun Ajaran</a>
                    @endpermission
                </div>
            </div>

        </div>

        <div class="card-body">
            <table class="table table-striped table-responsive-sm table-sm" width="100%">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Tahun Ajaran</th>
                        <th>Pembukaan TSN/MLN</th>
                        <th>Penutupan TSN/MLN</th>
                        <th>Pembukaan MI</th>
                        <th>Penutupan MI</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($datas as $data)
                        <tr class="text-center">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->start_year }} - {{ $data->end_year }}</td>
                            <td>
                                {{ $data->opened_at ? Carbon\Carbon::parse($data->opened_at)->isoFormat('D MMMM Y') : null }}
                            </td>
                            <td>
                                {{ Carbon\Carbon::parse($data->closed_at)->isoFormat('D MMMM Y') }}
                            </td>
                            <td>
                                {{ Carbon\Carbon::parse($data->opened_mi_at)->isoFormat('D MMMM Y') }}
                            </td>
                            <td>
                                {{ Carbon\Carbon::parse($data->closed_mi_at)->isoFormat('D MMMM Y') }}
                            </td>
                            <td>{{ $data->status }}</td>
                            <td>
                                <form action="{{ route('psb.destroy', $data->id) }}" method="post">
                                  @method('delete')
                                  @csrf
                                  <input type="hidden" value="{{ $data->id }}">
                                  <div class="btn-group" role="group" aria-label="Basic example">
                                    @permission('menu_set_tahun_ajaran-update')
                                    @if($data->status == 'DRAFT' || $data->status == 'ACTIVE') 
                                    <a href="{{ route('psb.edit', $data->id) }}" class="btn btn-warning btn-sm mr-2">Edit</a>                                 
                                    @endif 
                                    @if($data->status == 'DRAFT' || $data->status == 'NON ACTIVE')   
                                    <button type="button" class="btn btn-primary btn-sm mr-2 button-active" data-idPSB="{{ $data->id }}" id="buttonActive">Active</button>                                 
                                    @endif
                                    @endpermission
                                    @permission('menu_set_tahun_ajaran-delete') 
                                    @if($data->status == 'DRAFT')   
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure for delete this data?')">Delete</button>
                                    @endif
                                    @endpermission
                                  </div>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <div class="alert alert-danger">
                            Data PSB not available.
                        </div>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="card-footer">
            <div class="d-flex justify-content-center">
            {{ $datas->links() }}
            </div>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-active" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Aktivasi Tahun Ajaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('psb.active') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <p>Mengaktifkan Tahun Ajaran akan membuat satu-satunya yang aktif.</p>
                        <input type="hidden" id="input-active" name="idPSB">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script>
    $(".button-active").click(function (e) {
        $('#modal-active').modal('show');
        // get the value for the data attributes here and use in postdata
        const idPSB = $(this).attr('data-idPSB');
        $("#input-active").val(idPSB);
        // var postdata = { "analystID": analystID, "symbol": /* use value here */, "status": /* use value here */ };

    //etc
    });
</script>
@endpush