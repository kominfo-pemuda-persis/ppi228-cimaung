@extends('layouts.dashboard')

@section('title', 'Edit Tahun Ajaran')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Edit Tahun Ajaran</li>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('psb.index') }}" class="btn btn-dark">Back</a>
                </div>
                <form action="{{ route('psb.update', $psb->id) }}" method="post">
                    @method('put')
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="start_year">Tahun Ajaran Mulai</label>
                                    <input {{ $psb->status == 'DRAFT' ? '':'readonly' }} type="text" class="yearpicker form-control @error('start_year') is-invalid @enderror" name="start_year" id="start_year" required value="{{ $psb->start_year }}" />
                                    @error('start_year')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="end_year">Tahun Ajaran Selesai</label>
                                    <input {{ $psb->status == 'DRAFT' ? '':'readonly' }} type="text" class="yearpicker form-control @error('end_year') is-invalid @enderror" name="end_year" id="end_year" required value="{{ $psb->end_year }}" />
                                    @error('end_year')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="opened_at">Pembukaan Pendaftaran TSN/MLN</label>
                                    <input type="date" class="form-control @error('opened_at') is-invalid @enderror" name="opened_at" id="opened_at" required value="{{ $psb->opened_at }}">
                                    @error('opened_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="closed_at">Penutupan Pendaftaran TSN/MLN</label>
                                    <input type="date" class="form-control @error('closed_at') is-invalid @enderror" name="closed_at" id="closed_at" required value="{{ $psb->closed_at }}">
                                    @error('closed_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="opened_mi_at">Pembukaan Pendaftaran TSN/MLN</label>
                                    <input type="date" class="form-control @error('opened_mi_at') is-invalid @enderror" name="opened_mi_at" id="opened_mi_at" required value="{{ $psb->opened_mi_at }}">
                                    @error('opened_mi_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="closed_mi_at">Penutupan Pendaftaran TSN/MLN</label>
                                    <input type="date" class="form-control @error('closed_mi_at') is-invalid @enderror" name="closed_mi_at" id="closed_mi_at" required value="{{ $psb->closed_mi_at }}">
                                    @error('closed_mi_at')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <hr>
                            <button type="submit" class="btn btn-primary w-100"><i class="fas fa-paper-plane"></i> Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection