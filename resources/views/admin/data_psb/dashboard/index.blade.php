@extends('layouts.dashboard')

@section('breadcrumb')

@php
$title = "Dashboard Penerimaan Santri Baru ". $tahun_ajaran;
@endphp

@section('title', $title)

@section('content')
<!-- Main content -->
<section class="content">

    <div class="container-fluid">

        <div class="row">

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-secondary">
                    <div class="inner">
                        <h3>{{ $santri_tsn }}</h3>
                        <p>Calon Santri Tsanawiyyah</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-child"></i>
                    </div>
                    <a href="{{ route('rekap.calon.santri') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-danger">
                    <div class="inner">
                        <h3>{{ $santri_tsn_no_paid }}</h3>
                        <p>Calon Santri Tsanawiyyah Belum Bayar</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-wallet"></i>
                    </div>
                    <a href="{{ route('rekap.calon.santri') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h3>{{ $santri_tsn_paid }}</h3>
                        <p>Calon Santri Tsanawiyyah Sudah Bayar</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-wallet"></i>
                    </div>
                    <a href="{{ route('rekap.calon.santri') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-secondary">
                    <div class="inner">
                        <h3>{{ $santri_mln }}</h3>
                        <p>Calon Santri Mu`allimin</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-child"></i>
                    </div>
                    <a href="{{ route('rekap.calon.santri') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>


            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-danger">
                    <div class="inner">
                        <h3>{{ $santri_mln_no_paid }}</h3>
                        <p>Calon Santri Mu`allimin Belum Bayar</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-wallet"></i>
                    </div>
                    <a href="{{ route('rekap.calon.santri') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>

            

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-gradient-primary">
                    <div class="inner">
                        <h3>{{ $santri_mln_paid }}</h3>
                        <p>Calon Santri Mu`allimin Sudah Bayar</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-wallet"></i>
                    </div>
                    <a href="{{ route('rekap.calon.santri') }}" class="small-box-footer">Selengkapnya <i
                            class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-12 text-center">
                <!-- small box -->
                <div class="small-box bg-gradient-success">
                    <div class="inner">
                        <h3>{{ $total_keseluruhan_santri }}</h3>
                        <p>Jumlah Calon Santri Keseluruhan</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-child"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Calon Santri TSN</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Tabel Santri TSN</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>Laki-laki</td>
                                        <td class="text-center"> {{ $santri_tsn_lk }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Perempuan</td>
                                        <td class="text-center"> {{ $santri_tsn_pr }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Santri</td>
                                        <td class="text-center">{{ $santri_tsn_lk + $santri_tsn_pr }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Grafik Santri TSN</p>
                                <canvas id="donutChartTSN"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Calon Santri MLN</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Tabel Santri MLN</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>Laki-laki</td>
                                        <td class="text-center">{{ $santri_mln_lk }}</td>
                                    </tr>
                                    <tr>
                                        <td>Perempuan</td>
                                        <td class="text-center">{{ $santri_mln_pr }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Santri</td>
                                        <td class="text-center">{{ $santri_mln_lk + $santri_mln_pr }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Grafik Santri MLN</p>
                                <canvas id="pieChartTSN"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Calon Santri TSN - SAKAN</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Tabel Santri TSN - SAKAN</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>Laki-laki</td>
                                        <td class="text-center"> {{ $santri_tsn_sakan_rg }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Perempuan</td>
                                        <td class="text-center"> {{ $santri_tsn_sakan_ug }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Santri Sakan</td>
                                        <td class="text-center">{{ $santri_tsn_sakan_rg + $santri_tsn_sakan_ug }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Grafik Santri TSN Sakan</p>
                                <canvas id="donutChartTSNSAKAN"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-6">
                <div class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title text-success font-weight-bold">Calon Santri MLN - SAKAN</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Tabel Santri MLN - SAKAN</p>
                                <table class="table table-lg table-striped table-bordered">
                                    <tr>
                                        <td>Laki-laki</td>
                                        <td class="text-center">{{ $santri_mln_sakan_rg }}</td>
                                    </tr>
                                    <tr>
                                        <td>Perempuan</td>
                                        <td class="text-center">{{ $santri_mln_sakan_ug }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Santri</td>
                                        <td class="text-center">{{ $santri_mln_sakan_rg + $santri_mln_sakan_ug }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-6">
                                <p class="text-center font-weight-bold">Grafik Santri MLN - SAKAN</p>
                                <canvas id="donutChartMLNSAKAN"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection

@push('css')
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@push('scripts')
<!-- SweetAlert2 -->
<script src="{{ asset('admin_lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- DataTables  & admin_lte -->
<script src="{{ asset('admin_lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_lte/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('admin_lte/plugins/chart.js/Chart.min.js') }}"></script>
<!-- FLOT CHARTS -->
<script src="{{ asset('admin_lte/plugins/flot/jquery.flot.js') }}"></script>


<script>
    $(function () {
        //-------------
        //- PIE CHART SAKAN -
        //-------------

        var donutChartCanvas = $('#donutChartTSNSAKAN')
        var donutData        = {
            labels: [
                'Laki-laki',
                'Perempuan',
            ],
            datasets: [
                {
                data: [{{ $santri_tsn_sakan_rg }}, {{ $santri_tsn_sakan_ug }}],
                backgroundColor : ['#00a65a','#fdff80'],
                }
            ]
        }
        var donutOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })

        var donutChartCanvas = $('#donutChartMLNSAKAN')
        var donutData        = {
            labels: [
                'Laki-laki',
                'Perempuan',
            ],
            datasets: [
                {
                data: [{{ $santri_mln_sakan_rg }}, {{ $santri_mln_sakan_ug }}],
                backgroundColor : ['#00a65a','#fdff80'],
                }
            ]
        }
        var donutOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })

        //-------------
        //- PIE CHART JUMLAH CALON SANTRI -
        //-------------

        var donutChartCanvas = $('#donutChartTSN')
        var donutData        = {
            labels: [
                'Laki-laki',
                'Perempuan',
            ],
            datasets: [
                {
                data: [{{ $santri_tsn_lk }}, {{ $santri_tsn_pr }}],
                backgroundColor : ['#00a65a','#fdff80'],
                }
            ]
        }
        var donutOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })


        var DataTSN  = {
        labels: [
            'Laki-laki',
            'Perempuan',
        ],
        datasets: [
            {
            data: [{{ $santri_mln_lk }}, {{ $santri_mln_pr }}],
            backgroundColor : ['#00a65a','#fdff80'],
            }
        ]
        }
        var pieChartCanvas = $('#pieChartTSN')
        var pieDataTSN        = DataTSN;
        var pieOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        new Chart(pieChartCanvas, {
            type: 'pie',
            data: pieDataTSN,
            options: pieOptions
        })
    });
</script>
@endpush