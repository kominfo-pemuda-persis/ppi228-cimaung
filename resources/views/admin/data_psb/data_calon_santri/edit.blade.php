@extends('layouts.dashboard')

@section('title', 'Edit Calon Santri')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Edit Calon Santri</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <a href="{{ route('data.calon.santri') }}" class="btn btn-dark"><i class="fas fa-arrow-left"></i> Kembali</a>
        </div>
        <form action="{{ route('data.calon.santri.update', $santri->id) }}" method="post">
            @method('put')
            @csrf
            <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
              <div class="row">
                <div class="col-md 6">
                  <div class="form-group">
                      <label for="nama_lengkap">Nama</label>
                      <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" id="nama_lengkap" required placeholder="Nama..." value="{{ $santri->nama_lengkap }}">
                      @error('nama_lengkap')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="nik">NIK</label>
                      <input type="text" class="form-control @error('nik') is-invalid @enderror" name="nik" id="nik" required placeholder="Nama..." value="{{ $santri->nik }}">
                      @error('nik')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nomor_kartu_keluarga">No KK</label>
                        <input type="text" class="form-control @error('nomor_kartu_keluarga') is-invalid @enderror" name="nomor_kartu_keluarga" id="nomor_kartu_keluarga" required placeholder="No Kartu Keluarga..." value="{{ $santri->nomor_kartu_keluarga }}">
                        @error('nomor_kartu_keluarga')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                  </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="tempat_lahir">Tempat Lahir</label>
                      <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" id="tempat_lahir" required placeholder="Nama..." value="{{ $santri->tempat_lahir }}">
                      @error('tempat_lahir')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" id="tanggal_lahir" required placeholder="Nama..." value="{{ $santri->tanggal_lahir }}">
                      @error('tanggal_lahir')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="jk">Jenis Kelamin</label>
                      <select name="jk" id="jk" class="form-control @error('jk') is-invalid @enderror">
                        <option value="RG" {{ $santri->jk == 'RG' ? 'selected':'' }}>Laki-laki</option>
                        <option value="UG" {{ $santri->jk == 'UG' ? 'selected':'' }}>Perempuan</option>
                      </select>
                      @error('jk')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="alamat_kota">Kota</label>
                      <input type="text" class="form-control @error('alamat_kota') is-invalid @enderror" name="alamat_kota" id="alamat_kota" required placeholder="Nama..." value="{{ $santri->alamat_kota }}">
                      @error('alamat_kota')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <textarea name="alamat" id="alamat" class="form-control @error('alamat') is-invalid @enderror" required>{{ $santri->alamat }}</textarea>
                      @error('alamat')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="nama_lengkap_ayah">Ayah</label>
                      <input type="text" class="form-control @error('nama_lengkap_ayah') is-invalid @enderror" name="nama_lengkap_ayah" id="nama_lengkap_ayah" required placeholder="Nama..." value="{{ $santri->nama_lengkap_ayah }}">
                      @error('nama_lengkap_ayah')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="nama_lengkap_ibu">Ibu</label>
                      <input type="text" class="form-control @error('nama_lengkap_ibu') is-invalid @enderror" name="nama_lengkap_ibu" id="nama_lengkap_ibu" required placeholder="Nama..." value="{{ $santri->nama_lengkap_ibu }}">
                      @error('nama_lengkap_ibu')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="no_hp">HP</label>
                      <input type="text" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" id="no_hp" required placeholder="Nama..." value="{{ $santri->no_hp }}">
                      @error('no_hp')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" required placeholder="Nama..." value="{{ $santri->email }}">
                      @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="asal_sekolah">Asal Sekolah</label>
                      <input type="text" class="form-control @error('asal_sekolah') is-invalid @enderror" name="asal_sekolah" id="asal_sekolah" required placeholder="Nama..." value="{{ $santri->asal_sekolah }}">
                      @error('asal_sekolah')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="alamat_asal_sekolah">Alamat Sekolah</label>
                      <textarea name="alamat_asal_sekolah" id="alamat_asal_sekolah" class="form-control @error('alamat_asal_sekolah') is-invalid @enderror" required>{{ $santri->alamat_asal_sekolah }}</textarea>
                      @error('alamat_asal_sekolah')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="tingkat_sekolah_id">Tingkat</label>
                      <select name="tingkat_sekolah_id" id="tingkat_sekolah_id" class="form-control @error('tingkat_sekolah_id') is-invalid @enderror">
                        <option value="TSN" {{ $santri->tingkat_sekolah_id == 'TSN' ? 'selected':'' }}>Tsanawiyyah</option>
                        <option value="MLN" {{ $santri->tingkat_sekolah_id == 'MLN' ? 'selected':'' }}>Mu'allimien</option>
                      </select>
                      @error('tingkat_sekolah_id')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="status_keperluan_asrama">Keperluan Asrama</label>
                      <select name="status_keperluan_asrama" id="status_keperluan_asrama" class="form-control @error('status_keperluan_asrama') is-invalid @enderror">
                        <option value="ya" {{ $santri->status_keperluan_asrama == 'ya' ? 'selected':'' }}>Ya</option>
                        <option value="tidak" {{ $santri->status_keperluan_asrama == 'tidak' ? 'selected':'' }}>Tidak</option>
                      </select>
                      @error('status_keperluan_asrama')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                  </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Hafalan</label>
                        <select required class="select2bs4 @error('hafalan') is-invalid @enderror" multiple="multiple" data-placeholder="Pilih Juz" name="hafalan[]" style="width: 100%;">
                            @foreach($juzs as $juz)
                                <option value="{{ $juz->id }}"
                                @foreach($santri->juzs as $santriJuz) @if($santriJuz->id == $juz->id)selected="selected"@endif @endforeach>{{ $juz->nama }}</option>
                            @endforeach
                        </select>
                        @error('hafalan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

            </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-warning"><i class="fas fa-paper-plane"></i> Simpan</button>
            </div>
        </form>
    </div>

@endsection

@push('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="{{ asset('admin_lte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('admin_lte/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{ asset('admin_lte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })
</script>
@endpush