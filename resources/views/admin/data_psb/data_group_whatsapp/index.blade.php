@extends('layouts.dashboard')
<?php
use Carbon\Carbon;
?>

@section('title', 'Set Group Whatsapp Ortu')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Set Group Whatsapp Ortu</li>
@endsection

@section('content')
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                @if(count($datas) < 2)
                    <div class="col-md-8">
                        @permission('menu_set_group_whatsapp-create')
                        <button type="button" data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-sm mr-2">Tambah Group 
                            Whatsapp</button>                    
                        @endpermission
                    </div>
                @endif
            </div>

        </div>

        <div class="card-body">
            <table class="table table-striped table-responsive-sm table-sm" id="table-gwhatsapp">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Tingkat Sekolah</th>
                        <th>Whatsapp Url</th>
                        <th>Tanggal Diperbaharui</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($datas as $data)
                        <tr class="text-center">
                            @php
                                $date = Carbon::parse($data->updated_at)->locale('id');
                                $date_created = Carbon::parse($data->created_at)->locale('id');
                                $date->settings(['formatFunction' => 'translatedFormat']);
                                $date_created->settings(['formatFunction' => 'translatedFormat']);
                            @endphp 
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $data->tingkat_sekolah_id }}</td>
                            <td><a href="{{ $data->whatsappUrl }}" target="_blank">{{ $data->whatsappUrl }}</a></td>
                            <td>{{ 
                                   (!empty($data->updated_at) && $data->date_created) ? $date_created->format('l, j F Y') : $date->format('l, j F Y') }}
                            </td>
                            <td>
                                <form action="{{ route('groupwhatsapp.delete', $data->id) }}" method="post">
                                  @method('delete')
                                  @csrf
                                  <div class="btn-group" role="group" aria-label="Basic example">
                                    @permission('menu_set_group_whatsapp-update')
                                    <a href="{{ route('groupwhatsapp.detail', $data->id) }}" class="btn btn-sm btn-warning mr-2">Edit</a>
                                    @endpermission
                                    @permission('menu_set_group_whatsapp-delete') 
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('are you sure for delete this data?')">Delete</button>
                                    @endpermission
                                  </div>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <div class="alert alert-danger">
                            Data Set Group Whatsapp Ortu not available.
                        </div>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    {{-- ADD --}}
    <div class="modal fade" id="modal-tambah" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Link Whatsapp Group Ortu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('groupwhatsapp.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                            <select name="tingkat_sekolah_id" id="tingkat_sekolah_id" class="form-control" required>
                                <option value="" selected disabled>Pilih Tingkat Sekolah</option>
                                <option value="TSN">TSN</option>
                                <option value="MLN">MLN</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="whatsappUrl">Link Group Whatsapp</label>
                            <input type="text" class="form-control" name="whatsappUrl" id="whatsappUrl" required placeholder="Masukkan Link Group Whatsapp">
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        // Initialize the DataTable
        const table = $('#table-gwhatsapp').DataTable();
    </script>
@endpush