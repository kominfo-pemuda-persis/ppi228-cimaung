@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Edit Set Group Whatsapp Ortu/li>
@endsection

@section('title', 'Edit Set Group Whatsapp Ortu')

@section('content')
    <div class="card p-2">
        <form action="{{ route('groupwhatsapp.update', $data->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="tingkat_sekolah_id">Tingkat Sekolah</label>
                        <select name="tingkat_sekolah_id" class="form-control">
                            <option value="TSN" {{ $data->tingkat_sekolah_id == "TSN" ? 'selected' : '' }}>TSN</option>
                            <option value="MLN" {{ $data->tingkat_sekolah_id == "MLN" ? 'selected' : '' }}>MLN</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="whatsappUrl">Whatsapp Url</label>
                        <input type="text" value="{{ $data->whatsappUrl }}" class="form-control" name="whatsappUrl">
                    </div>
                </div>
            </div>
            <a href="{{ route('groupwhatsapp.index') }}" class="btn btn-secondary">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
@endsection