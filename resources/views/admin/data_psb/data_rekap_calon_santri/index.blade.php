@extends('layouts.dashboard')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active"> Rekap Calon Santri</li>
@endsection

@section('title', 'Data Rekap Calon Santri')

@section('content')
    @if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{$error}} <br>
        @endforeach
    </ul>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Jenjang</label>
                    <select id="filter-jenjang" class="form-control filter">
                        <option value="">Pilih Jenjang</option>
                        <option value="TSN">Tsanawiyyah</option>
                        <option value="MLN">Mu'allimin</option>
                    </select>
                </div>
            </div>
            <hr class="divider">
            <div class="table-responsive">
                <table id="table-rekap" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Tingkat Sekolah</th>
                            <th>Tempat/Tgl Lahir</th>
                            <th>Alamat</th>
                            <th>Jenis Kelamin</th>
                            <th>Status Pembayaran</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- DETAIL --}}
    <div class="modal fade" id="modal-detail" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h4 class="modal-title">Detail Calon Santri</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <strong>Nama</strong>
                            <p id="nama"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Tempat/Tgl Lahir</strong>
                            <p id="lahir"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>NO. Kartu Keluarga</strong>
                            <p id="kk"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>NIK</strong>
                            <p id="nik"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Jenis Kelamin</strong>
                            <p id="jk"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Nama Ayah</strong>
                            <p id="ayah"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Nama Ibu</strong>
                            <p id="ibu"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Nomor HP</strong>
                            <p id="hp"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Masuk Tingkat</strong>
                            <p id="tingkat"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Email</strong>
                            <p id="email"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Alamat</strong>
                            <p id="alamat"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Keperluan Asrama</strong>
                            <p id="asrama"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Sekolah Asal</strong>
                            <p id="sekolah"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Alamat Sekolah Asal</strong>
                            <p id="alamatSekolah"></p>
                        </div>
                        <div class="col-md-4">
                            <strong>Kota</strong>
                            <div id="kota"></div>
                        </div>
                        <div class="col-md-4">
                            <strong>Kode Daftar</strong>
                            <div id="kode_daftar"></div>
                        </div>
                        <div class="col-md-4">
                            <strong>Hafalan</strong>
                            <div id="hafalan" class="row">-</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    
@endsection
@push('scripts')
    <script>
        // Initialize the DataTable
        let kelas = $("#filter-kelas").val()
        let jenjang = $("#filter-jenjang").val();

        const table = $('#table-rekap').DataTable({
            "pageLength": 10,
            "lengthMenu": [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, 'semua']
            ],
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "processing": true,
            "bServerSide": true,
            "autoWidth": true,
            "order": [
                [1, "desc"]
            ],
            "ajax": {
                url: "{{ env('APP_URL') }}/admin/data-psb/rekap-calon-santri-data/{jenis}",
                type: "POST",
                data:function(d){
                    d.jenjang = jenjang;
                    return d
                }
            },
            "initComplete": function(settings, json) {
                const all_checkbox_view = $("#row-tampilan div input[type='checkbox']")
                $.each(all_checkbox_view, function(key, checkbox) {
                    let kolom = $(checkbox).data('kolom')
                    let is_checked = checkbox.checked
                    table.column(kolom).visible(is_checked)
                })
                setTimeout(function() {
                    table.columns.adjust().draw();
                }, 3000)
            },
            columnDefs: [
                {
                    targets: '_all',
                    visible: true,
                },
                {
                    "targets": 0,
                    "data": null,
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    "targets": 1,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.nama_lengkap;
                    }
                },
                {
                    "targets": 2,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.tingkat_sekolah_id;
                    }
                },
                {
                    "targets": 3,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.tempat_lahir + ", " + row.tanggal_lahir;
                    }
                },
                {
                    "targets": 4,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return row.alamat;
                    }
                },
                {
                    "targets": 5,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        console.log(row)
                        return (row.jk == 'RG' ? 'Laki-laki' : '' || row
                            .jk == 'UG' ? 'Perempuan' : '')
                    }
                },
                {
                    "targets": 6,
                    "class": "text-nowrap",
                    "render": function(data, type, row, meta) {
                        return `<span class="badge badge-danger">Belum Bayar</span>`;
                    }
                },
                {
                    "targets": 7,
                    "render": function(data, type, row, meta) {
                        let id = row.id;
                        let tampilan = `
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <div class="d-flex">
                                @permission('menu_rekap_calon_santri-update')
                                <a data-id="${row.id}" id="detail" class="btn btn-warning btn-sm mr-2 mb-1">Detail Santri</a>
                                @endpermission
                            </div>
                            <form action="/admin/data-psb/rekap-calon-santri/${id}" method="post">
                                @method('put')
                                @csrf
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    @permission('menu_rekap_calon_santri-update')
                                    <button onclick="return confirm('apakah yakin sudah ada nafaqah pendaftaran?')" class="btn btn-sm btn-success" type="submit">Sudah Bayar</button>
                                    @endpermission
                                </div>
                            </form>
                        </div>
                        <a href="{{ url('/admin/data-psb/rekap-calon-santri-edit') }}/${row.id}" class="btn btn-primary btn-sm">Edit Santri</a>
                        `
                        return tampilan;
                    }
                },

            ]
        });


        $(".filter").on('change',function(){
            kelas = $("#filter-kelas").val()
            jenjang = $("#filter-jenjang").val()
            table.ajax.reload(null,false)
        })

        $("#row-tampilan input[type='checkbox']").on('change',function(){
            let checkbox = $(this)
            let kolom = $(this).data('kolom')
            let is_checked = checkbox[0].checked
            table.column(kolom).visible(is_checked)
        })

        function getHtml(hafalan) {
            const html = '<div class="col-sm-4">' +
                    '<ul id="hafalan'+hafalan+'"></ul>' +
                    '</div>';
            return html;
        }

        $('body').on('click', '#detail', function() {
            let id = $(this).data('id');
            //fetch detail post with ajax
            $.ajax({
                url: `/admin/data-psb/rekap-calon-santri/${id}`,
                type: "GET",
                cache: false,
                success: function(response) {
                    //fill data to form
                    $('#nama').text(response.data.nama_lengkap);
                    $('#kk').text(response.data.nomor_kartu_keluarga);
                    $('#nik').text(response.data.nik);
                    $('#lahir').text(response.data.tempat_lahir + ', ' +response.data.tanggal_lahir);
                    const jk = response.data.jk == 'RG' ? 'Laki-laki':'Perempuan'
                    $('#jk').text(jk);
                    $('#ayah').text(response.data.nama_lengkap_ayah);
                    $('#ibu').text(response.data.nama_lengkap_ibu);
                    $('#hp').text(response.data.no_hp);
                    const tingkat = response.data.tingkat_sekolah_id == 'TSN' ? 'Tsanawiyyah':'Muallimien'
                    $('#tingkat').text(tingkat);
                    $('#email').text(response.data.email);
                    $('#alamat').text(response.data.alamat);
                    $('#asrama').text(response.data.status_keperluan_asrama);
                    $('#sekolah').text(response.data.asal_sekolah);
                    $('#alamatSekolah').text(response.data.alamat_asal_sekolah);
                    $('#kota').text(response.data.alamat_kota ?? '-');
                    $('#kode_daftar').text(response.data.kode_daftar ?? '-');
                    $('#hafalan').empty();

                    const part = [0, 5, 10, 15, 20, 25];
                    let hafalan = 0;
                    $.each(response.data.juzs, function( index, value ) {

                        if(part.includes(index)) {
                            hafalan = index;
                            const html = getHtml(hafalan);
                            $('#hafalan').append(html)    
                        }

                        $('#hafalan'+hafalan).append('<li>'+ this.nama +'</li>')
                    });

                    if (response.data.juzs.length == 0) {
                        $('#hafalan').append('<li>-</li>')
                    }
                    // //open modal
                    $('#modal-detail').modal('show');
                }
            });
        });

    </script>
@endpush