@extends('layouts.app')

@push('style_css')
<link href="{{ asset('assets/css/style-login.css') }}" rel="stylesheet">
@endpush

@section('content')
<section class="vh-100">
    <div class="container-fluid h-custom">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-md-9 col-lg-6 col-xl-5">
          <img src="https://pesantrenpersis27.com/wp-content/uploads/2022/01/PPI-27-PNG-1024x301.png"
            class="img-fluid" alt="Sample image">
        </div>
        <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
          <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="divider d-flex align-items-center my-1">
              <p class="text-center fw-bold mx-3 mb-0">Sign in</p>
            </div>
  
            <!-- Email input -->
            <div class="form-outline mb-4">
              <label class="form-label" for="username">Email or Username</label>
              <input type="text" id="username" name="username" class="form-control form-control-lg @error('username') is-invalid @enderror"
                placeholder="Enter a valid email or username"  value="{{ old('username') }}" required autocomplete="username" autofocus />
              @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
  
            <!-- Password input -->
            <div class="form-outline mb-3">
              <label class="form-label" for="password">Password</label>
              <input type="password" name="password" id="password" class="form-control form-control-lg"
                placeholder="Enter password" />
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
  
            <div class="d-flex justify-content-between align-items-center">
              <!-- Checkbox -->
              <div class="form-check mb-0">
                <input class="form-check-input me-2" type="checkbox" value="" id="form2Example3" />
                <label class="form-check-label" for="form2Example3">
                  Remember me
                </label>
              </div>
              <a href="#!" class="text-body">Forgot password?</a>
            </div>
  
            <div class="text-center text-lg-start mt-2 pt-2">
              <button type="submit" class="btn btn-set btn-lg"
                style="padding-left: 2.5rem; padding-right: 2.5rem; background-color: #006d47; color:#fff;">Login</button>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </section>
@endsection
