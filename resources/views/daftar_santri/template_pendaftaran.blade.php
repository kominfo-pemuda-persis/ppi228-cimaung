<!DOCTYPE html>
<html lang="en" style="width: 100%;height: 100%;">
<title>Formulir Pendaftaran Santri Baru</title>
<style>
  .truncate {
    /**Major Properties**/
    width:350px;
    white-space:nowrap;
    overflow:hidden;
    text-overflow:ellipsis;
  }
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<body style="position: relative;color: rgb(68, 67, 67); margin: 0; height: 100%">
    <div style="width:100%; height:100%; background-size: cover; background-repeat: no-repeat; background-image: url({{ Storage::disk('s3')->url('static/FORMULIR-PPDB.webp') }})"></div>
    <p 
      style="
        position: absolute;
        /* text-align: center; */
        /* width: 100%; */
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
        /* max-width: 1000px;  */
        top: 158px;
        left:185px;">
        <strong>
          {{ $santri->start_year }}/{{ $santri->start_year + 1 }}
        </strong>
    </p>

    <p 
      style="
        position: absolute;
        font-size: 40px;
        top: 95px;
        left:410px;">
      {{ $santri->kode_daftar }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 252px;
        left:80px;">
      {{ $santri->tingkat_sekolah_id == 'TSN' ? 'Tsanawiyyah':'Muallimien' }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 251px;
        left:535px;">
      {{ $santri->created_at }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 323px;
        left: 210px;">
      {{ ucwords(strtolower($santri->nama_lengkap)) }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 348px;
        left: 210px;">
      {{ $santri->nik }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 373px;
        left: 210px;">
      {{ $santri->jk == 'RG' ? 'Laki-laki':'Perempuan' }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 398px;
        left: 210px;">
      {{ ucwords(strtolower($santri->tempat_lahir)) }}, {{ $santri->tanggal_lahir }}
    </p>

    <p
      class="truncate"
      style="
        position: absolute;
        font-size: 14px;
        top: 423px;
        left: 210px;">
      {{ ucwords(strtolower($santri->alamat)) }}
    </p>
    
    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 451px;
        left: 210px;">
      {{ ucwords(strtolower($santri->alamat_kota)) }}
    </p>

    <p 
      class="truncate"
      style="
        position: absolute;
        font-size: 14px;
        top: 476px;
        left: 210px;">
      {{ ucwords(strtolower($santri->alamat_asal_sekolah)) }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 502px;
        left: 210px;">
      {{ ucwords(strtolower($santri->nama_lengkap_ibu)) }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 531px;
        left: 210px;">
      {{ ucwords(strtolower($santri->nama_lengkap_ayah)) }}
    </p>

    <p
      class="truncate"
      style="
        width:337px;
        position: absolute;
        font-size: 14px;
        top: 557px;
        left: 210px;">
        @foreach($santri->juzs as $key => $juz)
          {{ $juz->nama }}{{ ($key + 1) == count($santri->juzs) ? '':',' }}
        @endforeach
    </p>
    
    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 651px;
        left: 51px;">
      {{ $santri->no_hp }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 651px;
        left: 210px;">
      {{ $santri->no_hp }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 651px;
        left: 364px;">
      {{ $santri->email }}
    </p>

    <p 
      style="
        position: absolute;
        font-size: 14px;
        top: 785px;
        left: 460px;">
      {{ ucwords(strtolower($santri->nama_lengkap)) }}
    </p>

    @if($lolos == 'lolos')
    <h3 
      style="
        position: absolute;
        /* font-size: 14px; */
        top: 840px;
        left: 320px;">
      Selamat!
    </h3>

    <h5 
      style="
        position: absolute;
        /* font-size: 14px; */
        top: 870px;
        left: 130px;">
      Anda Lolos seleksi di Pesantren Persis 27. segera lengkapi administrasi santri.
    </h5>
    @endif

    @if($lolos == 'gagal')
    <h3 
      style="
        position: absolute;
        /* font-size: 14px; */
        top: 840px;
        left: 320px;">
      Maaf!
    </h3>

    <h5 
      style="
        position: absolute;
        /* font-size: 14px; */
        top: 870px;
        left: 280px;">
      Anda belum lolos seleksi.
    </h5>
    @endif

    <p 
      style="
        position: absolute;
        font-size: 11px;
        top: 973px;
        left: 545px;">
        {{ Carbon\Carbon::now()->translatedFormat('d F Y H:i:s') }}
    </p>
</body> 

</html>