<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="https://pesantrenpersis27.com/wp-content/uploads/2022/02/cropped-LOGO-PPI-27-192x192.png">
  <link rel="canonical" href="{{ env('APP_URL') }}">
  <meta name=author content="pesantrenpersis27.com">
  <meta name=language content="Indonesia">
  <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
  <meta name=description content="PSB - Pesantren Persis 27 {{ date('Y') + 1 }} - {{ date('Y') + 2 }}">
  <meta name=keywords content="pesantrenpersis27.com, ppi27, pesantrenpersis, persis, pemudapersis, pemudipersis, pendidikan">
  <meta name="google-site-verification" content="heKeYO4Auf8RRB3xaro35G5eBwA9OqIma9kw38lUe94" />
  <meta name=twitter:card content="summary">
  <meta name=title content="PSB - Pesantren Persis 27 {{ date('Y') + 1 }} - {{ date('Y') + 2 }}">
  <meta property="og:title" content="PSB - Pesantren Persis 27 {{ date('Y') + 1 }} - {{ date('Y') + 2 }}" />
  <meta property="og:url" content="{{ env('APP_URL') }}">
  <meta property="og:description" content="PSB - Pesantren Persis 27">
  <meta property="og:image" content="https://pesantrenpersis27.com/wp-content/uploads/2022/02/cropped-LOGO-PPI-27-192x192.png">
  <meta property="og:type" content="website" />
  <meta property="og:site_name" content="PSB - Pesantren Persis 27" />
  <meta property="og:image:type" content="image/jpg">
  <meta property='og:image:width' content='300' />
  <meta property='og:image:height' content='300' />
  <title>PSB - Pesantren Persis 27 {{ date('Y') + 1 }} - {{ date('Y') + 2 }}</title>
  
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" integrity="sha512-eyHL1atYNycXNXZMDndxrDhNAegH2BDWt1TmkXJPoGf1WLlNYt08CSjkqF5lnCRmdm3IrkHid8s2jOUY4NIZVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">


  <link rel="stylesheet" href="{{ asset('assets/css/style-daftar.css') }}">
  <style>
    .text-center {
      width: 100%;
      position: relative;
      height: 100%;
    }
    
    .spinner-border {
      display: block;
      position: fixed;
      top: calc(50% - (58px / 2));  
      right: calc(50% - (58px / 2));
      z-index: 10;
    }
    
    #backdrop {
      position:fixed;
      top:0;
      width: 100vw;
      height: 100vh;
      z-index: 9;
      background-color: rgb(0,0,0,0.5);
    }
  </style>
</head>
<body class="bg-secondary">
  <div id="backdrop" style="display: none">
    <div class="text-center">
        <div class="spinner-border text-success" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
  </div>
  <div class="container-fluid">
      @if($psb && $psb->closed_at >= date_format(now(), 'Y-m-d'))
      <h4 class="text-center mt-5 mb-3">Formulir Pendaftaran Santri Baru {{ $psb->start_year }} - {{ $psb->end_year }}</h4>
      <h1 class="text-center mt-3">Penutupan Pendaftaran:<br>
        <span style="font-size: 20px;">{{ Carbon\Carbon::parse($psb->closed_at)->isoFormat('D MMMM Y') }}</span>
      </h1>
      <section style="padding-top: 10px;">
        <div class="col-md-8 offset-md-2">
          @if($errors->any())
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul style="list-style-type: none; ">
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
          @endif
        </div>
        

        <div class="row">
          <div class="col-md-8 offset-md-2">
            <form method="get">
              <div class="input-group mb-3">
                <input name="search" type="number" max="9999999999999999" class="form-control" placeholder="Search berdasarkan NIK atau Kode Daftar" aria-label="Cari NIK atau Kode Daftar" aria-describedby="button-addon2" value="{{ request('search') }}">
                <button type="submit" class="btn btn-info text-light" type="button" id="button-addon2">Search</button>
              </div>
              <p style="font-size:10px; color:rgb(104, 115, 19)!important; font-style:italic; margin-left:5px;">Silahkan lakukan search berdasarkan NIK atau Kode Daftar setelah pendaftaran. guna mengetahui tahap prosesi Penerimaan Santri Baru. (Sudah Daftar, Belum Bayar, Tahap Seleksi, Tidak Diterima maupun Diterima), info didapat pada Cetak Formulir setelah search.</p>
            </form>

            @if($status)
            <div class="row">
              <div class="col-lg-12">
                <a href="{{ route('preview', request('search')) }}" class="btn btn-success mb-2" target="_blank">Cetak Formulir</a>
              </div>
              <div class="col-lg-12">
                  <div class="card">
                      <div class="card-body">
    
                          <div class="hori-timeline" dir="ltr">
                              <ul class="list-inline events">
                                  <li class="list-inline-item event-list">
                                      <div class="px-4">
                                          <div class="event-date 
                                          @if($status == 'pendaftaran' || $status == 'infaq' || $status == 'gagal' || $status == 'lolos')
                                          bg-success
                                          @else
                                          bg-secondary
                                          @endif
                                          text-light">1</div>
                                          <h6 class="font-size-8">Pendaftaran</h6>
                                          @if($status == 'pendaftaran' || $status == 'infaq' || $status == 'gagal' || $status == 'lolos')
                                          <div class="badge badge-success text-light">Selesai</div>
                                          @endif
                                      </div>
                                  </li>
                                  <li class="list-inline-item event-list">
                                      <div class="px-4">
                                          <div class="event-date
                                          @if($status == 'infaq' || $status == 'gagal' || $status == 'lolos')
                                          bg-success
                                          @else
                                          bg-secondary
                                          @endif
                                          text-light">2</div>
                                          <h6 class="font-size-8">Infaq Pendaftaran</h6>
                                          @if($status == 'pendaftaran')
                                          <div class="badge badge-secondary text-light">Dalam Proses</div>
                                          @elseif($status == 'infaq' || $status == 'gagal' || $status == 'lolos')
                                          <div class="badge badge-success text-light">Selesai</div>
                                          @else
                                          <!-- <div class="badge badge-success text-light">Selesai</div> -->
                                          @endif
                                      </div>
                                  </li>
                                  <li class="list-inline-item event-list">
                                      <div class="px-4">
                                          <div class="event-date
                                          @if($status == 'gagal' || $status == 'lolos')
                                          bg-success
                                          @else
                                          bg-secondary
                                          @endif
                                          text-light">3</div>
                                          <h6 class="font-size-8">Tahap Seleksi</h6>
                                          @if($status == 'infaq' || $status == 'pendaftaran')
                                          <div class="badge badge-secondary text-light">Dalam Proses</div>
                                          @elseif($status == 'gagal' || $status == 'lolos')
                                          <div class="badge badge-success text-light">Selesai</div>
                                          @else
                                          <!-- <div class="badge badge-success text-light">Selesai</div> -->
                                          @endif
                                      </div>
                                  </li>
                                  <li class="list-inline-item event-list">
                                      <div class="px-4">
                                          <div class="event-date
                                          @if($status == 'gagal')
                                          bg-danger
                                          @elseif($status == 'lolos')
                                          bg-success
                                          @else
                                          bg-secondary
                                          @endif
                                          text-light">4</div>
                                          <h6 class="font-size-8">Hasil Seleksi</h6>
                                          @if($status == 'gagal')
                                          <div class="badge badge-danger text-light">'Afwan, Anda belum diterima</div>
                                          @elseif($status == 'lolos')
                                          <div class="badge badge-success text-light">Selamat, Anda diterima</div>
                                          @else
                                          <div class="badge badge-secondary text-light">Dalam Proses</div>
                                          @endif
                                      </div>
                                  </li>
                              </ul>
                          </div>

                          @if($status == 'infaq' || $status == 'gagal' || $status == 'lolos')
                              @if($santri->tingkat_sekolah_id == "TSN")
                                <a href="{{ $dataWaTSN }}" target="_blank" style="font-size: 12px; color:white;" class="btn btn-success btn-sm text-center"><i class="fab fa-whatsapp"></i> Group Ortu Santri Baru (TSN)</a>
                              @else
                                <a href="{{ $dataWaMLN }}" target="_blank" style="font-size: 12px; color:white;"  class="btn btn-success btn-sm text-center"><i class="fab fa-whatsapp"></i> Group Ortu Santri Baru (MLN)</a>
                              @endif
                          @endif
                      </div>
                  </div>
                  <!-- end card -->
              </div>
            </div>
            @endif

            @if(!$status)
              <div class="card card-xl bg-card">
                <div class="card-body">

                  <swiper-container class="mySwiper mb-4" space-between="30"
                  slides-per-view="3">
                    <swiper-slide class="nav-link text-small shadow-sm step0 border ml-1">Pilih Pendaftaran</swiper-slide>
                    <swiper-slide class="nav-link text-small shadow-sm step1 border ml-1 border-secondary text-secondary">Data Diri</swiper-slide>
                    <swiper-slide class="nav-link text-small shadow-sm step2 border ml-1 border-secondary text-secondary">Data Keluarga</swiper-slide>
                    <swiper-slide class="nav-link text-small shadow-sm step3 border ml-1 border-secondary text-secondary" id="pendidikan">Pendidikan</swiper-slide>
                    <swiper-slide class="nav-link text-small shadow-sm step4 border ml-1 border-secondary text-secondary">Nafaqah Pendafatran</swiper-slide>
                  </swiper-container>

                  <form action="/daftarSantri" class="contact-form" method="post">
                  <!-- <form class="contact-form"> -->
                    @csrf
                    <input type="hidden" id="daftar" value="{{ $sudahDaftar }}">
                    <input type="hidden" name="start_year" value="{{ $psb->start_year }}">

                    <!-- Step 0 -->
                    <div class="form-section text-light">

                      <label class="mt-3">Masuk Tingkat:</label>
                      @if(!$sudahDaftar)
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="tingkat" value="MI" id="mi" @if($santri)
                          {{ $santri->tingkat_sekolah_id == 'MI' ? 'checked':'' }}
                        @else
                          checked
                        @endif>
                        <label class="form-check-label" for="mi">
                          Ibtida'iyyah
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="tingkat" value="TSN" id="tsn" @if($santri)
                          {{ $santri->tingkat_sekolah_id == 'TSN' ? 'checked':'' }}
                        @else
                          checked
                        @endif>
                        <label class="form-check-label" for="tsn">
                          Tsanawiyyah
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="tingkat" value="MLN" id="mln"
                        @if($santri)
                          {{ $santri->tingkat_sekolah_id == 'MLN' ? 'checked':'' }}
                        @endif>
                        <label class="form-check-label" for="mln">
                          Muallimien
                        </label>
                      </div>
                      @endif
                      <br>

                      <label for="siswa_lanjutan_baru">Pilih Pendaftaran</label>
                      <select name="siswa_lanjutan_baru" id="siswa_lanjutan_baru" class="form-control">
                        <option value="baru">Baru</option>
                        <option value="lanjutan">Lanjutan</option>
                      </select>

                      <br>

                      <div id="inputNIS">
                        <label for="nis">NIS</label>
                        <input type="text" name="nis" class="form-control" placeholder="Masukkan NIS (Santri PPI27)">
                      </div>

                    </div>

                    <!-- Step 1 -->
                    <div class="form-section text-light">

                      <label for="nama">Nama Calon Santri</label>
                      <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama Calon Santri" value="{{ $santri->nama_lengkap ?? '' }}">

                      <br>

                      <label for="nik">Nomor Induk Kependudukan (NIK) Santri</label>
                      <input type="number" name="nik" id="nik" class="form-control" placeholder="Masukkan NIK Calon Santri" min="1000000000000000" max="9999999999999999" value="{{ $santri->nik ?? '' }}">
                      
                      <br>

                      <label for="nomor_kartu_keluarga">Nomor Kartu Keluarga</label>
                      <input type="number" name="nomor_kartu_keluarga" id="nomor_kartu_keluarga" class="form-control" placeholder="Masukkan KK Calon Santri" min="1000000000000000" max="9999999999999999" value="{{ $santri->nomor_kartu_keluarga ?? '' }}">

                      <div class="row mt-3">
                        <div class="col-md-6">
                          <label for="tempatLahir">Tempat Lahir</label>
                          <input type="text" name="tempatLahir" class="form-control" placeholder="Masukkan Tempat Lahir" value="{{ $santri->tempat_lahir ?? '' }}">
                        </div>

                        <div class="col-md-6">
                          <label for="tanggalLahir">Tanggal Lahir</label>
                          <input type="date" name="tanggalLahir" class="form-control" min="2000-01-01" value="{{ $santri->tanggal_lahir ?? '' }}">
                        </div>
                      </div>
                      <div class="row mt-3">
                        <div class="col-md-6">
                          <label class="mt-3">Jenis Kelamin:</label>
                          @if(!$sudahDaftar)
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="jk" value="RG" id="jk1" 
                              @if($santri)
                              {{ $santri->jk == 'RG' ? 'checked':'' }}
                              @else
                              checked
                              @endif>
                              <label class="form-check-label" for="jk1">
                                Laki-laki
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="jk" value="UG" id="jk2"
                              @if($santri)
                              {{ $santri->jk == 'UG' ? 'checked':'' }}
                              @endif>
                              <label class="form-check-label" for="jk2">
                                Perempuan
                              </label>
                            </div>
                          @endif
                        </div>
                        <div class="col-md-6">
                          <label for="kota">Kota</label>
                          <input type="text" name="kota" placeholder="Masukkan Kota" class="form-control" value="{{ $santri->alamat_kota ?? '' }}">
                        </div>
                      </div>

                      <div class="mt-3">
                        <label for="alamat">Alamat</label>
                        <textarea name="alamat" id="alamat" class="form-control" placeholder="Masukkan Alamat">{{ $santri->alamat ?? '' }}</textarea>
                      </div>

                    </div>

                    <!-- Step 2 -->
                    <div class="form-section text-light">
                      <label for="ayah">Nama Ayah</label>
                      <input type="text" name="ayah" id="ayah" class="form-control" placeholder="Masukkan Nama Ayah" value="{{ $santri->nama_lengkap_ayah ?? '' }}">

                      <label class="mt-3" for="ibu">Nama Ibu</label>
                      <input type="text" name="ibu" class="form-control" placeholder="Masukkan Nama Ibu" value="{{ $santri->nama_lengkap_ayah ?? '' }}">

                      <label class="mt-3" for="hp">No Whatsapp</label>
                      <input type="tel" name="hp" class="form-control" placeholder="Masukkan Whatsapp (Orangtua aktif)" value="{{ $santri->no_hp ?? '' }}">

                      <label class="mt-3" for="email">Email</label>
                      <input type="email" name="email" class="form-control" placeholder="Masukkan Email (Santri/Orangtua aktif)" value="{{ $santri->email ?? '' }}">
                    </div>

                    <!-- Step 3 -->
                    <div class="form-section text-light">

                      <div id="asalSekolah">
                        <label for="asalSekolah">Asal Sekolah</label>
                        <input type="text" id="asalSekolah" name="asalSekolah" class="form-control" placeholder="Masukkan Nama Asal Sekolah" value="{{ $santri->asal_sekolah ?? '' }}">
  
                        <label for="alamatSekolah" class="mt-3">Alamat Sekolah</label>
                        <textarea name="alamatSekolah" id="alamatSekolah" class="form-control" placeholder="Masukkan Alamat Asal Sekolah">{{ $santri->alamat_asal_sekolah ?? '' }}</textarea>
                      </div>

                      <div id="asalTKAlamat" style="display: none;">
                        <label for="asalTK">Asal TK (optional)</label>
                        <input type="text" id="asalTK" name="asalTK" class="form-control" placeholder="Masukkan Nama TK" value="{{ $santri->asal_sekolah ?? '' }}">
  
                        <label for="alamatTK" class="mt-3">Alamat TK (optional)</label>
                        <textarea name="alamatTK" id="alamatTK" class="form-control" placeholder="Masukkan Alamat TK">{{ $santri->alamat_asal_sekolah ?? '' }}</textarea>
                      </div>
                  
                      <div id="hafalanAlquran">
                        <label class="mt-3">Hafalan Al-Qur'an:</label>
                        @if(!$sudahDaftar && !$santri)
                        <div class="row">
                          @foreach($juzs as $juz)
                          @if($juz->id == 1 || $juz->id == 11 || $juz->id == 21)
                          <div class="col-md-4">
                            @endif
                            <div class="form-check">
                              <input name="juz[]" class="form-check-input" type="checkbox" id="1" value="{{ $juz->id }}">
                              <label class="form-check-label" for="1">
                                {{ $juz->nama }}
                              </label>
                            </div>
                            @if($juz->id == 10 || $juz->id == 20 || $juz->id == 30)
                            </div>
                            @endif
                          @endforeach
                        </div>
                        @else
                        <ul>
                          @foreach($santri->juzs as $juz)
                          <li>{{ $juz->nama }}</li>
                          @endforeach
                        </ul>
                        @endif
                      </div>

                      <div id="keperluanAsrama">
                        <label class="mt-3">Keperluan Asrama:</label>
                        @if(!$sudahDaftar)
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="asrama" value="ya" id="ya" @if($santri)
                            {{ $santri->status_keperluan_asrama == 'ya' ? 'checked':'' }}
                          @else
                            checked
                          @endif>
                          <label class="form-check-label" for="ya">
                            Ya
                          </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="asrama" value="tidak" id="tidak"
                          @if($santri)
                            {{ $santri->status_keperluan_asrama == 'tidak' ? 'checked':'' }}
                          @endif>
                          <label class="form-check-label" for="tidak">
                            Tidak
                          </label>
                        </div>
                        @endif
                      </div>
                    </div>

                    <!-- Step 4 -->
                    <div class="form-section text-light">
                      <h3>Nafaqah Pendaftaran</h3>

                      <div class="d-flex ml-n3 mt-3">
                        <div class="col-md-6">
                          <label class="form-label">Nafaqah pendaftaran</label>
                        </div>
                        <div style="font-size:18px;"> : Rp. {{ number_format(200000) }}
                        </div>
                      </div>
                      <div class="d-flex ml-n3">
                        <div class="col-md-6">
                          <label for="" class="form-label" style="font-size:16px;">Rekening BSI (Bank Syariah Indonesia)</label>
                        </div>
                        <div style="font-size:16px;"> : a.n Pesantren Persatuan Islam 27 (7776660277) </div>
                      </div>
                      <div class="d-flex ml-n3">
                        <div class="col-md-6">
                          <label for="" class="form-label font-bold" style="font-size:16px;">No Whatsapp (Fikri Taufik Rahman)</label>
                        </div>
                        <div style="font-size:16px;"> : 0878-0545-1496 </div>
                      </div>

                      <hr>

                      <div class="form-heading">
                        <h4 style="color:#fff;">Atau Konfirmasi ke salah satu nomor whatsapp dibawah ini</h4>
                      </div>
                      <div class="d-flex ml-n3 mt-3">
                        <div class="col-md-6">
                          <label for="" class="form-label font-bold" style="font-size:16px;">No Whatsapp (Setiawan)</label>
                        </div>
                        <div style="font-size:16px;"> : 0812-2259-8403 </div>
                      </div>
                      <div class="d-flex ml-n3 mt-3">
                        <div class="col-md-6">
                          <label for="" class="form-label font-bold" style="font-size:16px;">No Whatsapp (Ghifar Al-Mughni)</label>
                        </div>
                        <div style="font-size:16px;"> : 0857-9534-0143 </div>
                      </div>
                      <div class="d-flex ml-n3 mt-3">
                        <div class="col-md-6">
                          <label for="" class="form-label font-bold" style="font-size:16px;">No Whatsapp (Pesantren)</label>
                        </div>
                        <div style="font-size:16px;"> : 0813-2302-8451 </div>
                      </div>

                    </div>

                    <div class="form-navigation">
                      <button type="button" class="previous btn mt-5 btn-info float-left ml-3" style="display:none">< Sebelumnya</button>
                      <button type="button" class="next btn mt-5 btn-info float-right">Selanjutnya ></button>
                      @if(!$sudahDaftar)
                        @if(!$santri)
                        <button type="submit" class="btn mt-5 btn-success float-right">Daftarkan!</button>
                        @endif
                      @endif
                    </div>
                  </form>
                </div>
              </div>
              <br>

              @if($santri)
              <a href="{{ route('psb') }}" class="btn btn-success">Daftarkan Calon Santri Lain</a>
              @endif
            @endif

            <br><br>
          </div>
        </div>
      </section>
      @elseif($psb && $psb->closed_at < now())
      <div class="container text-center">
        <h4 class="mt-5 mb-3">Maaf, Pendaftaran Telah Ditutup!</h4>
        <h1 class="text-center mt-2 mb-5">Penutupan Pendaftaran di tanggal, {{ Carbon\Carbon::parse($psb->closed_at)->isoFormat('D MMMM Y') }}</h1>
        <img src="https://persis92majalengka.com/wp-content/uploads/2020/05/Web-Pesantren-Persis-9232-1024x809.png" width="300" alt="">
      </div>
      @else
      <div class="container text-center">
        <h1 class="text-center mt-5 mb-5">Formulir Pendaftaran Santri Baru Belum Dibuka, Silakan hubungi pihak Pesantren</h1>
        <img src="https://persis92majalengka.com/wp-content/uploads/2020/05/Web-Pesantren-Persis-9232-1024x809.png" width="300" alt="">
      </div>
      @endif
  </div>

  <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-element-bundle.min.js"></script>
  <script>

    $(function(){
      const lanjutan = $("#siswa_lanjutan_baru").val()
      $('#inputNIS').hide();
      const daftar = $('#daftar').val();
      var $sections = $('.form-section');


      $('input[name="tingkat"]').change(function () {
        // Cek apakah radio "MI" dipilih
        if ($(this).val() === 'MI') {
          // Sembunyikan elemen jika "MI" dipilih
          $('#hafalanAlquran').hide();
          $('#asalSekolah').hide();
          $('#keperluanAsrama').hide();
          $('#asalTKAlamat').show();
        } else {
          // Tampilkan elemen jika pilihan lain dipilih
          $('#hafalanAlquran').show();
          $('#asalSekolah').show();
          $('#asalTKAlamat').hide();
          $('#keperluanAsrama').show();
        }
      });

      function navigateTo(index, type){
        $sections.removeClass('current').eq(index).addClass('current');
        // $('.previous').style('display', 'block')
        $('.form-navigation .previous').toggle(index>0);
        var atTheEnd = index >= $sections.length - 1;
        $('.form-navigation .next').toggle(!atTheEnd);
        $('.form-navigation [type=submit]').toggle(atTheEnd);

        if(type == 'lanjutan') {
          $('.form-navigation .previous').toggle(index<0);
        }

        for(let i = 0; i<= index; i++) {
          const step = $('.step'+i).addClass('bg-success border-success text-light');
        }
      }

      function curIndex() {
        return $sections.index($sections.filter('.current'));
      }

      $('.form-navigation .previous').click(function() {
        for(i=0; i<5; i++) {
          $('.step' + i).removeClass('bg-success border-success text-light');
        }
        navigateTo(curIndex()-1);
      });

      $('.form-navigation .next').click(function() {
        $('.contact-form').parsley().whenValidate({
          group: 'block-' + curIndex()
        }).done(function() {
          navigateTo(curIndex()+1);
        });
      });

      $sections.each(function(index, section) {
        $(section).find(':input').attr('data-parsley-group', 'block-'+index);
      })

      $('#siswa_lanjutan_baru').change(function () {
        var selectedValue = $(this).val();

        // Jika pilihan "lanjutan" dipilih, tampilkan input NIS
        if (selectedValue === 'lanjutan') {
          $('#inputNIS').show();
        } else {
          // Jika pilihan "baru" atau lainnya dipilih, sembunyikan input NIS
          $('#inputNIS').hide();
        }
      });

      $('.form-navigation .next').click(function () {
        // Ambil nilai lanjutan saat tombol next diklik
        var lanjutan = $('#siswa_lanjutan_baru').val();
        // Tentukan logika navigasi berdasarkan nilai lanjutan
        if (lanjutan === 'lanjutan') {
          navigateTo(4, 'lanjutan'); // Melompat ke langkah ke-4 jika siswa adalah lanjutan
        } else {
          navigateTo(curIndex());
        }
      });

      if(daftar) {
        navigateTo(3); // Jika siswa bukan lanjutan, tetapkan langkah ke-3
        $('.step0, .step1, .step2').addClass('bg-success border-success text-light');
        $('input, textarea').prop("readonly", true);
      } else {
        navigateTo(0);
      }
    });

    $('.contact-form').on('submit', function(e) {
      $('body').css('overflow', 'hidden');
      $('#backdrop').show();
    });
  </script>

</body>
</html>