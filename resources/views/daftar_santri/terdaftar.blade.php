<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="https://pesantrenpersis27.com/wp-content/uploads/2022/02/cropped-LOGO-PPI-27-192x192.png">
  <link rel="canonical" href="{{ env('APP_URL') }}">
  <meta name=author content="pesantrenpersis27.com">
  <meta name=language content="Indonesia">
  <meta name=googlebot-news content="noindex">
  <meta name=googlebot content="noindex">
  <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
  <meta name=description content="PSB - Pesantren Persis 27 {{ date('Y') + 1 }} - {{ date('Y') + 2 }}">
  <meta name=keywords content="pesantrenpersis27.com, ppi27, pesantrenpersis, persis, pemudapersis, pemudipersis, pendidikan">
  <meta name=twitter:card content="summary">
  <meta name=title content="PSB - Pesantren Persis 27 {{ date('Y') + 1 }} - {{ date('Y') + 2 }}">
  <meta property="og:title" content="PSB - Pesantren Persis 27 {{ date('Y') + 1 }} - {{ date('Y') + 2 }}" />
  <meta property="og:url" content="{{ env('APP_URL') }}">
  <meta property="og:description" content="PSB - Pesantren Persis 27">
  <meta property="og:image" content="https://pesantrenpersis27.com/wp-content/uploads/2022/02/cropped-LOGO-PPI-27-192x192.png">
  <meta property="og:type" content="website" />
  <meta property="og:site_name" content="PSB - Pesantren Persis 27" />
  <meta property="og:image:type" content="image/jpg">
  <meta property='og:image:width' content='300' />
  <meta property='og:image:height' content='300' />
  <title>PSB - Pesantren Persis 27 {{ date('Y') + 1 }} - {{ date('Y') + 2 }}</title>
  
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" integrity="sha512-eyHL1atYNycXNXZMDndxrDhNAegH2BDWt1TmkXJPoGf1WLlNYt08CSjkqF5lnCRmdm3IrkHid8s2jOUY4NIZVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <style>
    body {
      background: #fff!important;
    }
    section{
      padding-top: 100px;
    }
    .form-section{
      padding-left: 15px;
      display: none;
    }
    .form-section.current{
      display: inherit;
    }
    /* .btn-info, .btn-primary{
      margin-top: 10px;
    } */
    .parsley-errors-list{
      margin: 2px 0 3px;
      padding: 0;
      list-style-type: none;
      color: red;
    }
    .bg-card {
      background: #a6b56b;
    }
  </style>
</head>
<body class="bg-secondary">
  <div class="container-fluid">
    <h3 class="text-center text-light mt-3">Pendaftaran Santri Baru Berhasil</h3>
    <h1 class="text-center text-light mt-3 mb-n4">Silahkan Lakukan Nafaqah Pendafatran dan Konfrimasi</h1>
    <section>
      <div class="row mt-n5">
        <div class="col-md-10 offset-md-1">
          <div class="card card-xl bg-card">
            <div class="card-body">

              <h5 class="text-light">Data Diri</h5>
              <div class="row">
                <div class="col-md-2">
                  <strong>Nama Calon Santri</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->nama_lengkap }}
                </div>
                <div class="col-md-2">
                  <strong>Tempat/Tgl Lahir</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->tempat_lahir }}, {{ Carbon\Carbon::parse($santri->tanggal_lahir)->isoFormat('D MMMM Y') }}
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <strong>Jenis Kelamin</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->jk == 'RG' ? 'Laki-laki':'Perempuan' }}
                </div>
                <div class="col-md-2">
                  <strong>Alamat</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->alamat }}
                </div>
              </div>

              <h5 class="text-light mt-5">Data Keluarga</h5>
              <div class="row">
                <div class="col-md-2">
                  <strong>Nama Ayah</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->nama_lengkap_ayah }}
                </div>
                <div class="col-md-2">
                  <strong>Nama Ibu</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->nama_lengkap_ibu }}
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <strong>HP/Whatsapp</strong>
                </div>
                <div class="col-md-4">
                  08123456789
                </div>
                <div class="col-md-2">
                  <strong>Email</strong>
                </div>
                <div class="col-md-4">
                  ortu@gmail.com
                </div>
              </div>

              <h5 class="text-light mt-5">Pendidikan</h5>
              <div class="row">
                <div class="col-md-2">
                  <strong>Asal Sekolah</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->asal_sekolah }}
                </div>
                <div class="col-md-2">
                  <strong>Alamat Sekolah</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->alamat_asal_sekolah }}
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <strong>Masuk Tingkat</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->tingkat_sekolah_id == 'TSN' ? 'Tsanawiyyah':'Muallimien' }}
                </div>
                <div class="col-md-2">
                  <strong>Hafalan</strong>
                </div>
                <div class="col-md-4">
                  @foreach($santri->juzs as $index => $juz)
                    {{ $juz->nama }}@if(($index + 1) != $santri->juzs->count() && ($index + 1) != ($santri->juzs->count() - 1)),@endif
                    @if(($index + 1) == ($santri->juzs->count() - 1))
                      dan
                    @endif
                  @endforeach
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <strong>Keperluan Asrama</strong>
                </div>
                <div class="col-md-4">
                  {{ $santri->status_keperluan_asrama == 'ya' ? 'Ya':'Tidak' }}
                </div>
              </div>

              <hr>

              <h3 class="text-light">Nafaqah Pendaftaran</h3>

              <div class="d-flex ml-n3 mt-3">
                <div class="col-md-6">
                  <label class="form-label"><strong>Nafaqah pendaftaran</strong></label>
                </div>
                <div style="font-size:18px;"> : Rp. {{ number_format(200000) }}
                </div>
              </div>
              <div class="d-flex ml-n3">
                <div class="col-md-6">
                  <label for="" class="form-label" style="font-size:16px;"><strong>Rekening BSI (Bank Syariah Indonesia)</strong></label>
                </div>
                <div style="font-size:16px;"> : a.n Pesantren Persatuan Islam 27 (7776660277) </div>
              </div>
              <div class="d-flex ml-n3">
                <div class="col-md-6">
                  <label for="" class="form-label font-bold" style="font-size:16px;"><strong>No Whatsapp (Ghifar)</strong></label>
                </div>
                <div style="font-size:16px;"> : 0857-2379-5741 </div>
              </div>

              <hr>

              <div class="form-heading">
                <h4 style="color:#fff;">Konfirmasi ke salah satu nomor whatsapp dibawah ini</h4>
              </div>
              <div class="d-flex ml-n3 mt-3">
                <div class="col-md-6">
                  <label for="" class="form-label font-bold" style="font-size:16px;"><strong>No Whatsapp (Setiawan)</strong></label>
                </div>
                <div style="font-size:16px;"> : 0812-2259-8403 </div>
              </div>
              <div class="d-flex ml-n3">
                <div class="col-md-6">
                  <label for="" class="form-label font-bold" style="font-size:16px;"><strong>No Whatsapp (Fikri Taufik Rahman)</strong></label>
                </div>
                <div style="font-size:16px;"> : 0878-0545-1496 </div>
              </div>
              
            </div>
          </div>

          <br>
          <br>
          <br>

        </div>
      </div>
    </section>
  </div>

  <script>
    $(function(){
      var $sections = $('.form-section');

      function navigateTo(index){
        $sections.removeClass('current').eq(index).addClass('current');
        $('.form-navigation .previous').toggle(index>0);
        var atTheEnd = index >= $sections.length - 1;
        $('.form-navigation .next').toggle(!atTheEnd);
        $('.form-navigation [type=submit]').toggle(atTheEnd);

        // const step = document.querySelector('.step'+index);
        const step = $('.step'+index).addClass('bg-success border-success text-light');
      }

      function curIndex() {
        return $sections.index($sections.filter('.current'));
      }

      $('.form-navigation .previous').click(function() {
        navigateTo(curIndex()-1);
      });

      $('.form-navigation .next').click(function() {
        $('.contact-form').parsley().whenValidate({
          group: 'block-' + curIndex()
        }).done(function() {
          navigateTo(curIndex()+1);
        });
      });

      $sections.each(function(index, section) {
        $(section).find(':input').attr('data-parsley-group', 'block-'+index);
      })

      navigateTo(0);
    });
  </script>
</body>
</html>