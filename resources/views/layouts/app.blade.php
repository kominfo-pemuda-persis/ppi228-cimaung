<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="https://pesantrenpersis27.com/wp-content/uploads/2022/02/cropped-LOGO-PPI-27-192x192.png">
    <link rel="canonical" href="{{ env('APP_URL') }}">
    <meta name=author content="pesantrenpersis27.com">
    <meta name=language content="Indonesia">
	<meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
    <meta name=description content="SIMASIS - Sistem Manajemen Pesantren Persis 27">
    <meta name=keywords content="pesantrenpersis27.com, ppi27, pesantrenpersis, persis, pemudapersis, pemudipersis, pendidikan">
    <meta name="google-site-verification" content="heKeYO4Auf8RRB3xaro35G5eBwA9OqIma9kw38lUe94" />
    <meta name=twitter:card content="summary">
    <meta name=title content="SIMASIS - Sistem Manajemen Pesantren Persis 27">
    <meta property="og:title" content="SIMASIS - Sistem Manajemen Pesantren Persis 27" />
    <meta property="og:url" content="{{ env('APP_URL') }}">
    <meta property="og:description" content="SIMASIS - Sistem Manajemen Pesantren Persis 27">
    <meta property="og:image" content="https://pesantrenpersis27.com/wp-content/uploads/2022/02/cropped-LOGO-PPI-27-192x192.png">
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="SIMASIS - Sistem Manajemen Pesantren Persis 27" />
    <meta property="og:image:type" content="image/jpg">
    <meta property='og:image:width' content='300' />
    <meta property='og:image:height' content='300' />
    <title>SIMASIS - Sistem Manajemen Pesantren Persis 27</title>

    @stack('style_css')
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <div id="app">
        <main>
            @yield('content')
        </main>
    </div>
</body>
</html>
