<style>
    .profile-username {
        font-size: 12px!important;
    }
</style>
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar linkss -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle border-0" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-expanded="false">  {{ Auth::user()->name }}
            </button>
            <div class="dropdown-menu ml-4 m-0" style="margin-left:47px!important;"
                aria-labelledby="dropdownMenuButton">
                <div class="img-bar pt-3 pb-1 mb-2">
                    @if(empty($photoprof->photo_path))
                        <div class="rounded-circle m-auto" style="overflow:hidden; width: 3em !important; height: 3em !important">
                        <img class="profile-rounded-img2 border"
                                src="{{ asset('admin_lte/dist/img/avatar.png') }}"
                                alt="User profile picture">
                        </div>
                    @else
                        @role('admin|asatidz|asatidz_tsn|asatidz_mln|muaddib|mudir_tsn|mudir_mln|mudir_am')
                        <div class="rounded-circle m-auto" style="overflow:hidden; width: 3em !important; height: 3em !important">
                            <img class="profile-rounded-img2 border"
                                    src="{{ Storage::disk('s3')->url('data/photo/asatidz/'.$photoprof->photo_path) }}"
                                    alt="User profile picture">
                        </div>
                        @endrole
                        @role('santri_tsn|santri_mln')
                        <div class="rounded-circle m-auto" style="overflow:hidden; width: 3em !important; height: 3em !important">
                            <img class="profile-rounded-img2 border"
                                src="{{ Storage::disk('s3')->url('data/photo/santri/'.$photoprof->photo_path) }}"
                                alt="User profile picture">
                        </div>
                        @endrole
                        @role('orangtua_tsn|orangtua_mln')
                        <div class="rounded-circle m-auto" style="overflow:hidden; width: 3em !important; height: 3em !important">
                            <img class="profile-rounded-img2 border"
                                src="{{ Storage::disk('s3')->url('data/photo/orangtua/'.$photoprof->photo_path) }}"
                                    alt="User profile picture">
                        </div>
                        @endrole
                    @endif
                </div>
                @role('admin|asatidz|asatidz_tsn|asatidz_mln|muaddib|mudir_tsn|mudir_mln|mudir_am')
                <div class="mx-auto text-center" style="position: relative">
                @foreach(Auth::user()->roles as $key => $role)
                    @if($key == 0) 
                        <label style="font-size:8px!important;" class="badge-success px-1 text-center rounded py-1">{{ $role->display_name }}</label>
                    @endif
                @endforeach
                <h5 class="profile-username text-center px-2 d-block mt-1">{{ Auth()->user()->name }}</h5>
                </div>
                <hr>
                <a class="dropdown-item" href="{{ route('profile.index') }}?pills=general">Data Asatidz</a>
                @endrole
                @role('santri_tsn|santri_mln')
                <div class="mx-auto text-center" style="position: relative">
                @foreach(Auth::user()->roles as $key => $role)
                    @if($key == 0) 
                        <label style="font-size:8px!important;" class="badge-success px-1 text-center rounded py-1">{{ $role->display_name }}</label>
                    @endif
                @endforeach
                <h5 class="profile-username text-center px-2 d-block mt-1">{{ Auth()->user()->name }}</h5>
                </div>
                <hr>
                <a class="dropdown-item" href="{{ route('profile.index') }}?pills=general">Data Santri</a>
                @endrole
                @role('orangtua_tsn|orangtua_mln')
                <div class="mx-auto text-center" style="position: relative">
                @foreach(Auth::user()->roles as $key => $role)
                    @if($key == 0) 
                        <label style="font-size:8px!important;" class="badge-success px-1 text-center rounded py-1">{{ $role->display_name }}</label>
                    @endif
                @endforeach
                <h5 class="profile-username text-center px-2 d-block mt-1">{{ Auth()->user()->name }}</h5>
                </div>
                <hr>
                <a class="dropdown-item" href="{{ route('profile.index') }}?pills=general">Data Orangtua</a>
                @endrole
                <a class="dropdown-item" href="{{ route('profile.index') }}?pills=account">Account</a>
                <a class="dropdown-item" href="{{ route('profile.index') }}?pills=password">Change Password</a>
                <hr>
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault(); document.getElementById('form-logout').submit();">Logout</a>
                <form id="form-logout" action="{{ route('logout') }}" method="POST" class="d-none">@csrf
                </form>
            </div>
        </div>
    </ul>
</nav>
@push('scripts')
<script>
    let img2 = document.querySelector('.profile-rounded-img2');
    if (img2.naturalWidth > img2.naturalHeight) {
        img2.style.height = '100%';
        // center the image
        img2.style.left = '25%';
        img2.style.transform = 'translateX(-21%)';
    } else {
        img2.style.width = '100%';
    }
</script>
@endpush