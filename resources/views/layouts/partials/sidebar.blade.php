<aside class="main-sidebar sidebar-light-primary elevation-3">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard') }}" class="brand-link">
        <img src="{{ asset('img/ppi27-logo.png') }}" alt="AdminLTE Logo" style="width: 240px; margin-left:-10px;">
        <span class="brand-text font-weight-light"><br></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}"
                        class="nav-link {{ request()->is('admin/dashboard') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                @permission('menu_utama-read')
                <li class="nav-header">Menu Utama</li>
                @endpermission
                @permission('menu_psb-read')
                    <li class="nav-item {{ request()->is('admin/data-psb/*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('admin/data-psb/*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Data PSB
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="margin-left:15px;">
                            @permission('menu_set_tahun_ajaran-read')
                            <li class="nav-item">
                                <a href="{{ route('psb.index') }}" class="nav-link {{ request()->is('admin/data-psb/psb*') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Set Tahun Ajaran</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_set_group_whatsapp-read')
                            <li class="nav-item">
                                <a href="{{ route('groupwhatsapp.index') }}" class="nav-link {{ request()->is('admin/data-psb/adding-group-whatsapp*') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Set Group Wa Ortu</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_dashboard_calon_santri-read')
                            <li class="nav-item">
                                <a href="{{ route('data-psb.dashboard.index') }}" class="nav-link {{ request()->is('admin/data-psb/dashboard') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Dashboard Calon Santri</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_rekap_calon_santri-read')
                            <li class="nav-item">
                                <a href="{{ route('rekap.calon.santri') }}" class="nav-link {{ request()->is('admin/data-psb/rekap-calon-santri') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Rekap Calon Santri</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_data_calon_santri-read')
                            <li class="nav-item">
                                <a href="{{ route('data.calon.santri') }}" class="nav-link {{ request()->is('admin/data-psb/data-calon-santri') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Data Calon Santri</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_data_gagal_test-read')
                            <li class="nav-item">
                                <a href="{{ route('data.gagal.test') }}" class="nav-link {{ request()->is('admin/data-psb/data-gagal-test') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Data Gagal Seleksi</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_data_lolos_test-read')
                            <li class="nav-item">
                                <a href="{{ route('data.lolos.test') }}" class="nav-link {{ request()->is('admin/data-psb/data-lolos-test') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Data Lolos Seleksi</p>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                @endpermission
                @permission('menu_tasykil-read')
                    <li class="nav-item">
                        <a href="{{ route('tasykil.index') }}" class="nav-link {{ request()->is('admin/tasykil') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-chalkboard-teacher"></i>
                            <p>
                                Data Tasykil
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_asatidz-read')
                    <li class="nav-item">
                        <a href="{{ route('asatidz.index') }}" class="nav-link {{ request()->is('admin/asatidz') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-chalkboard-teacher"></i>
                            <p>
                                Data Asatidz/ah
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_wali_kelas-read')
                    <li class="nav-item">
                        <a href="{{ route('walikelas.index') }}" class="nav-link {{ request()->is('admin/walikelas') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-chalkboard-teacher"></i>
                            <p>
                                Data Wali Kelas
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_muaddib-read')
                    <li class="nav-item">
                        <a href="{{ route('muaddib.index') }}" class="nav-link {{ request()->is('admin/muaddib') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-chalkboard-teacher"></i>
                            <p>
                                Data Muaddib
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_astidz_pelajaran-read')
                    <li class="nav-item">
                        <a href="{{ route('asatidz-pelajaran.index') }}" class="nav-link {{ request()->is('admin/asatidz-pelajaran') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-calendar-alt"></i>
                            <p>
                                Asatidzh & Pelajaran
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_jadwal_pelajaran-read')
                    <li class="nav-item {{ request()->is('admin/jadwal-pelajaran/*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('admin/jadwal-pelajaran/*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-briefcase"></i>
                            <p>
                                Jadwal Pelajaran
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="margin-left:15px;">
                            @permission('menu_nomor_urut_asatidz-read')
                            <li class="nav-item">
                                <a href="{{ route('nomor.urut') }}" class="nav-link {{ request()->is('admin/jadwal-pelajaran/nomor-urut') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Nomor Urut Asatidz</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_jadwal_mengajar_intra-read')
                            <li class="nav-item">
                                <a href="{{ route('jadwal.intra') }}" class="nav-link {{ request()->is('admin/jadwal-pelajaran/intra') || request()->is('admin/jadwal-pelajaran/jam-mengajar') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Jadwal Mengajar Intra</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_jadwal_mengajar_ekstra-read')
                            <li class="nav-item">
                                <a href="{{ route('jadwal.ekstra') }}" class="nav-link {{ request()->is('admin/jadwal-pelajaran/ekstra') || request()->is('admin/jadwal-pelajaran/jam-extra') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Jadwal Mengajar Extra</p>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                @endpermission
                @permission('menu_karya_tulis_ilmiah-read')
                    <li class="nav-item">
                        <a href="{{ route('karya-tulis-ilmiah.index') }}" class="nav-link {{ request()->is('admin/karya-tulis-ilmiah') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-book-open"></i>
                            <p>
                                Karya Tulis Ilmiah
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_ujian-read')
                <li class="nav-item">
                    @permission('menu_ujian-tanggapan')
                    <a href="https://ujian.pesantrenpersis27.com/login" class="nav-link">
                        <i class="nav-icon fas fa-laptop"></i>
                        <p>
                            Ujian Santri
                        </p>
                    </a>
                    @endpermission
                    @permission('menu_ujian-santri')
                    <a href="https://ujian.pesantrenpersis27.com" class="nav-link">
                        <i class="nav-icon fas fa-laptop"></i>
                        <p>
                            Ujian Santri
                        </p>
                    </a>
                    @endpermission
                </li>
                @endpermission
                @permission('menu_data_santri-read')
                    <li class="nav-item {{ request()->is('admin/santri*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('admin/santri*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-briefcase"></i>
                            <p>
                                Data Santri MLN-TSN
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="margin-left:15px;">
                            <li class="nav-item">
                                <a href="{{ route('santri.index') }}" class="nav-link {{ request()->is('admin/santri') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Data Santri Aktif
                                    </p>
                                </a>
                            </li>
                            @permission('menu_data_santri-tanggapan')
                            <li class="nav-item">
                                <a href="{{ route('santri-tidak-aktif.index') }}" class="nav-link {{ request()->is('admin/santri-tidak-aktif') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Data Santri Tidak Aktif
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('santri-alumni.index') }}" class="nav-link {{ request()->is('admin/santri-alumni') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Data Santri Alumni
                                    </p>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                @endpermission
                @permission('menu_data_orangtua-read')
                    <li class="nav-item">
                        <a href="{{ route('orangtua.index') }}" class="nav-link {{ request()->is('admin/orangtua') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Data Orangtua
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_data_master-read')
                    <li class="nav-item {{ request()->is('admin/master/*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('admin/master/*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-briefcase"></i>
                            <p>
                                Data Master
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="margin-left:15px;">
                            @permission('menu_kelompok_pelajaran-read')
                            <li class="nav-item">
                                <a href="{{ route('kelompok_pelajaran.index') }}" class="nav-link {{ request()->is('admin/master/kelompok-pelajaran') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Kelompok Pelajaran</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_semester-read')
                            <li class="nav-item">
                                <a href="{{ route('semester.index') }}" class="nav-link {{ request()->is('admin/master/semester') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Semester</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_ekstrakurikuler-read')
                            <li class="nav-item">
                                <a href="{{ route('ekstrakurikuler.index') }}" class="nav-link {{ request()->is('admin/master/ekstrakurikuler') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Ekstrakulikuler</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_pelajaran-read')
                            <li class="nav-item">
                                <a href="{{ route('pelajaran.index') }}" class="nav-link {{ request()->is('admin/master/pelajaran') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Pelajaran</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_type_adab-read')
                            <li class="nav-item">
                                <a href="{{ route('type-adab.index') }}" class="nav-link {{ request()->is('admin/master/type-adab') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Type Adab</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_program_unggulan-read')
                            <li class="nav-item">
                                <a href="{{ route('program-unggulan.index') }}" class="nav-link {{ request()->is('admin/master/program-unggulan') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Program Unggulan</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_kokurikuler-read')
                            <li class="nav-item">
                                <a href="{{ route('kokurikuler.index') }}" class="nav-link {{ request()->is('admin/master/kokurikuler') ? 'active' : '' }}">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Kokurikuler</p>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                @endpermission
                @permission('menu_informasi-read')
                <li class="nav-header">Menu Informasi</li>
                @endpermission
                @permission('menu_informasi-read')
                    <li class="nav-item">
                        <a href="{{ route('informasi.index') }}" class="nav-link {{ request()->is('admin/menu-informasi') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-envelope"></i>
                            <p>
                                Data Informasi
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_pengolahan-read')
                    <li class="nav-header">Menu Pengolahan</li>
                @endpermission
                @permission('menu_penilaian_pendidikan-read')
                    <li class="nav-item">
                        <a href="{{ route('penilaian.index') }}" class="nav-link {{ request()->is('admin/penilaian') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-chart-bar"></i>
                            <p>
                                Penilaian TSN & MLN
                            </p>
                        </a>
                    </li>
                @endpermission
                @permission('menu_data_rapor-read')
                    <li class="nav-item {{ request()->is('admin/data-rapor/*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('admin/data-rapor/*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-briefcase"></i>
                            <p>
                                Data Rapor 
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="margin-left:15px;">
                            @permission('menu_rapor_muaddib-read')
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-chart-bar"></i>
                                    <p>
                                        Rapor Muaddib
                                    </p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_rapor_tsn-read')
                            <li class="nav-item">
                                <a href="{{ route('raportsn.index') }}" class="nav-link {{ request()->is('admin/data-rapor/raportsn') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-chart-bar"></i>
                                    <p>
                                        Rapor TSN
                                    </p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_rapor_mln-read')
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-chart-bar"></i>
                                    <p>
                                        Rapor MLN
                                    </p>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                @endpermission
                @permission('menu_setting_system-read')
                    <li class="nav-header">Setting System</li>
                @endpermission
                @permission('menu_pengaturan-read')
                    <li class="nav-item @yield('menu-pengaturan')">
                        <a href="#" class="nav-link @yield('pengaturan')">
                            <i class="nav-icon fas fa-cog"></i>
                            <p>
                                Pengaturan
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="margin-left:15px;">
                            @permission('menu_user-read')
                            <li class="nav-item">
                                <a href="{{ route('users.index') }}" class="nav-link @yield('user')">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>User</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_menu-read')
                            <li class="nav-item">
                                <a href="{{ route('menu.index') }}" class="nav-link @yield('menu')">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Menu</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_role-read')
                            <li class="nav-item">
                                <a href="{{ route('role.index') }}" class="nav-link @yield('role')">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Role</p>
                                </a>
                            </li>
                            @endpermission
                            @permission('menu_permission-read')
                            <li class="nav-item">
                                <a href="{{ route('permission.index') }}" class="nav-link @yield('permission')">
                                    <i class="fas fa-hockey-puck nav-icon"></i>
                                    <p>Permission</p>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                @endpermission
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>