# SIMASIS-PPI27

Sistem aplikasi manajemen data Pesantren Persatuan Islam 27 Situaksan Bandung (SIMASIS).
Aplikasi ini dikembangkan menggunakan teknologi Laravel.

## Panduan Kontribusi

### Instalasi

- Clone repository 

```
git clone git@gitlab.com:hilfi/erapor-ppi27.git erapor-ppi27.local
```

- Copy .env.example

Pastikan .env.example ada dalam folder aplikasi. Kemudian rename file tersebut menjadi .env. 
Untuk pengguna macOS ataupun GNU/Linux, jalankan perintah dibawah pada console terminal 

```
cp .env.example .env
```

Untuk Windows OS jalankan perintah pada cmd

```
copy .env.example .env
```

- Composer

Pastikan composer telah terpasang pada lokal development Anda. 
Laravel membutuhkan composer untuk instalasi dependensi atau library yang dibutuhkan. 
Pastikan jalankan perintah di bawah sebelum memulai development.

```
composer install
```

- Generate Key

Jalankan perintah dibawah untuk menggenerate key pada file .env.
Key dibutuhkan untuk proses enkripsi data sensitif pada laravel seperti Cookies atau Session. 

```
php artisan key:generate
```

- Konfigurasi Database

Buka .env lalu arahkan database ke lokal masing-masing.

```
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=<NAMA DATABASE>
DB_USERNAME=<USERNAME>
DB_PASSWORD=<PASSWORD>
```

- Migrasi DB

```
php artisan migrate
```

- Seeding Data.

Pastikan jalankan composer dump autoload untuk mengupdate class cache.

```
composer dump-autoload
```

Lalu jalankan seed

```
php artisan db:seed
```

- Jalankan mesin MySQL dan start server.

```
sudo service mysql start
php artisan serve
```

- Email Testing

Layanan email yang digunakan untuk keperluan testing ialah [mailtrap](https://mailtrap.io). 
Pastikan konfigurasi email pada file .env seperti di bawah ini.

```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=<USERNAME>
MAIL_PASSWORD=<PASSWORD>
MAIL_ENCRYPTION=tls
```
Anda dapat mendaftar akun mailtrap untuk mendapatkan username dan password.

### Instalasi Menggunakan Docker

pastikan docker dan docker-compose sudah terinstal pada local komputer anda.

untuk menjalankan simasis pada mode docker silahkan jalankan perintah di bawah pada root folder erapor-ppi27

```
docker-compose -f docker-compose-local.yml build
```

Jika docker image sudah selesai dibuat silahkan jalankan perintah

```
docker-compose -f docker-compose-local.yml up -d
```

Untuk menjalankan migrate db pada mode docker silahkan jalankan perintah

```
docker-compose -f docker-compose-local.yml exec web php artisan migrate
```

Untuk menjalankan seed db pada mode docker silahkan jalankan perintah

```
docker-compose -f docker-compose-local.yml exec web php artisan db:seed
```

CATATAN: pastikan port 80, 3306 dan 8000 tidak digunakan karena port tersebut akan digunakan selama docker raporppi berjalan

Port 80 akan menjadi default mapping port aplikasi raporppi

Port 3306 akan menjadi default mapping port docker mysql

Port 8000 akan menjadi default mapping port phpmyadmin

Anda bisa melakukan penyesuain port ini dengan mengubah konfigurasi pada file `docker-compose-local.yml` pada root folder aplikasi

### Masuk ke Aplikasi

Buka `http://localhost:8000`

Masuk dengan akun admin:
Username: admin
password: 12345
