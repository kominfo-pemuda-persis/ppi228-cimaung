<?php

namespace App\Imports;

use App\Models\Asatidz;
use App\Models\User;
use Exception;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;

class AsatidzImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            
            try {
                DB::beginTransaction();
                
                $niat = rand(10000000, 99999999);
                if($row['niat']) {
                    $niat = $row['niat'];
                }

                $user = User::create([
                    'name' => ucwords(strtolower($row['nama'])),
                    'username' => $niat,
                    'password' => bcrypt('12345')
                ]);
    
                Asatidz::create([
                    'niat'          => $niat,
                    'nama_lengkap'  => ucwords(strtolower(($row['nama']))),
                    'jenis_kelamin' => $row['jenis_kelamin'],
                    'pendidikan_terakhir' => $row['pendidikan_terakhir'],
                    'no_telp'       => $row['no_hp'],
                    'user_id'       => $user->id
                ]);

                $user->attachRole('asatidz');

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
            }
        }
    }

    public function headingRow(): int
    {
        return 3;
    }
}