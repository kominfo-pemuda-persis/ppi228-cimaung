<?php

namespace App\Imports;

use App\Models\Pelajaran;
use Exception;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PelajaranImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            try {
                DB::beginTransaction();

                Pelajaran::create([
                    'kode'           => $row['kode'],
                    'nama_pelajaran' => $row['nama_pelajaran'],
                    'kelompok_id'    => $row['kelompok_id'],
                    'urutan'         => $row['urutan'],
                    'tingkat_sekolah_id' => $row['tingkat']
                ]);

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
            }
        }
    }

    public function headingRow(): int
    {
        return 3;
    }
}
