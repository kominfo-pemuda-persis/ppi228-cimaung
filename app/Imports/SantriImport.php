<?php

namespace App\Imports;

use App\Models\Orangtua;
use App\Models\Santri;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SantriImport implements ToCollection, WithHeadingRow
{

    public function collection(Collection $rows)
    {
        
        function konversiBulan($bulanIndonesia)
        {
            $bulanInggris = [
                'Januari' => 'January',
                'Februari' => 'February',
                'Maret' => 'March',
                'April' => 'April',
                'Mei' => 'May',
                'Juni' => 'June',
                'Juli' => 'July',
                'Agustus' => 'August',
                'September' => 'September',
                'Oktober' => 'October',
                'November' => 'November',
                'Desember' => 'December'
            ];

            return $bulanInggris[$bulanIndonesia];
        }

        foreach ($rows as $row) 
        {
            $existingSantri = Santri::where('nis', $row['nis'])->first();
            if ($existingSantri) {
                // Data sudah ada, tampilkan pesan toastr dan lewati impor data ini
                \Session::flash('error', 'Data dengan nomor induk ' . $row['nis'] . ' sudah ada dalam sistem.');
                return redirect()->route('santri.index');
            }

            // Validasi status_active, tahun_masuk, dan tahun_lulus
            if (empty($row['status']) || empty($row['tahun_masuk']) || empty($row['tahun_lulus'])) {
                \Session::flash('error', 'Status, Tahun Masuk, dan Tahun Lulus harus diisi.');
                return redirect()->route('santri.index');
            }

            try {
                
                DB::beginTransaction();

                $userOrangTua = User::create([
                    'name' => ucwords(strtolower($row['nama_ayah'])),
                    'username' => $row['nis'] . '.ortu',
                    'password' => bcrypt('12345')
                ]);

                $userSantri = User::create([
                    'name' => ucwords(strtolower($row['nama_lengkap'])),
                    'username' => $row['nis'],
                    'password' => bcrypt('12345')
                ]);

                $orangtua = Orangtua::create([
                    'nama' => ucwords(strtolower($row['nama_ayah'])),
                    'user_id' => $userOrangTua->id,
                ]);

                $ttl = $row['ttl'];

                // Memisahkan nama kota dan tanggal menggunakan regex
                preg_match('/([A-Za-z]+), (\d+ [\w]+ \d{4})/', $ttl, $matches);

                $namaKota = $matches[1];
                $tanggalArray = explode(" ", $matches[2]);
                $bulanInggris = konversiBulan($tanggalArray[1]);

                $tanggal_lahir = Carbon::createFromFormat('j F Y', $tanggalArray[0] . ' ' . $bulanInggris . ' ' . $tanggalArray[2])->format('Y-m-d');

                Santri::create([
                    'nism'                      => $row['nism'],
                    'nis'                       => $row['nis'],
                    'nama_lengkap'              => ucwords(strtolower(($row['nama_lengkap']))),
                    'jk'                        => $row['jenis_kelamin'],
                    'nik'                       => $row['nik'],
                    'nisn'                      => $row['nisn'],
                    'tempat_lahir'              => ucwords(strtolower($namaKota)),
                    'tanggal_lahir'             => $tanggal_lahir,
                    'alamat'                    => ucwords(strtolower($row['alamat'])),
                    'no_hp'                     => $row['no_hp'],
                    'email'                     => $row['email'],
                    'asal_sekolah'              => ucwords(strtolower($row['asal_sekolah'])),
                    'alamat_asal_sekolah'       => ucwords(strtolower($row['alamat_sekolah'])),
                    'kelas'                     => $row['kelas'], // 1,2,3,LULUS
                    'tingkat_sekolah_id'        => $row['tingkat'], // TSN, MLN
                    'nomor_kartu_keluarga'  => $row['no_kk'],
                    'nama_lengkap_ayah'     => ucwords(strtolower($row['nama_ayah'])),
                    'no_ktp_ayah'           => $row['no_ktp_ayah'],
                    'no_hp_ayah'            => $row['no_hp_ayah'],
                    'pendidikan_ayah'       => $row['pendidikan_ayah'], 
                    'pekerjaan_ayah'        => $row['pekerjaan_ayah'],
                    'nama_lengkap_ibu'              => ucwords(strtolower($row['nama_ibu'])),
                    'no_ktp_ibu'                    => $row['no_ktp_ibu'],
                    'no_hp_ibu'                     => $row['no_hp_ibu'],
                    'pendidikan_ibu'                => $row['pendidikan_ibu'],
                    'pekerjaan_ibu'                 => ucwords(strtolower($row['pekerjaan_ibu'])),
                    'nama_lengkap_wali'                     => ucwords(strtolower($row['nama_wali'])),
                    'no_ktp_wali'                           => $row['no_ktp_wali'],
                    'no_hp_wali'                            => $row['no_hp_wali'],
                    'pendidikan_wali'                       => $row['pendidikan_wali'],
                    'pekerjaan_wali'                        => $row['pekerjaan_wali'],
                    'status_active'             => $row['status'], //active, non active, pindah, meninggal, dikeluarkan, rejected, alumni
                    'status_pembayaran'         => 'approved',
                    'tahun_masuk'               => $row['tahun_masuk'],
                    'tahun_lulus'               => $row['tahun_lulus'],
                    'user_id'           => $userSantri->id,
                    'orangtua_id'       => $orangtua->id
                ]);

                if(Str::upper($row['tingkat']) == 'TSN') {
                    $userSantri->attachRole('santri_tsn');
                    $userOrangTua->attachRole('orangtua_tsn');
                }

                if(Str::upper($row['tingkat']) == 'MLN') {
                    $userSantri->attachRole('santri_mln');
                    $userOrangTua->attachRole('orangtua_mln');
                }                      
                
                DB::commit();
                \Session::flash('success', 'Data Santri Berhasil di Import');
            } catch (\Illuminate\Database\QueryException $e) {
                DB::rollBack();
                return redirect()->route('santri.index')->with('error', 'Gagal mengimpor data: ' . $e->getMessage());
            } catch (\Exception $e) {
                // Tangkap kesalahan umum
                DB::rollBack();
                return redirect()->route('santri.index')->with('error', 'Gagal mengimpor data: ' . $e->getMessage());
            }
        }
        return redirect()->route('santri.index');
    }
    
    public function headingRow(): int
    {
        return 4;
    }
}
