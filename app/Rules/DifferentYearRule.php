<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DifferentYearRule implements Rule
{

    public $startYear;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($startYear)
    {
        $this->startYear = $startYear;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(($value - $this->startYear) != 1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Perbedaan tahun mulai dan selesai harus 1 tahun.';
    }
}
