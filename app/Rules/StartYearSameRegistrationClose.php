<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StartYearSameRegistrationClose implements Rule
{
    public $startYear;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($startYear)
    {
        $this->startYear = $startYear;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $startOnlyYear = date_create($this->startYear);
        $closedAt = date_create($value);

        if(date_format($startOnlyYear,"Y") != date_format($closedAt,"Y")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tahun mulai dan tahun penutupan harus sama.';
    }
}
