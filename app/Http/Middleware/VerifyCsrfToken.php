<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        '/admin/santri-data*',
        '/admin/santri-tidak-aktif-data*',
        '/admin/santri-alumni*',
        '/admin/asatidz-pelajaran-data*',
        '/admin/walikelas-data*',
        '/admin/orangtua-data*',
        '/admin/master/pelajaran-data*',
        '/admin/muaddib-data*',
        '/admin/data-psb/*',
        '/admin/karya-tulis-ilmiah-data/*',
        '/admin/pengaturan/users-data*',
    ];
}
