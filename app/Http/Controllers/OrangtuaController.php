<?php

namespace App\Http\Controllers;

use App\Models\Orangtua;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class OrangtuaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->hasPermission('menu_data_orangtua-read')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data orangtua');
        } else {
            return view('admin.data_orangtua.index', [
                'orangtua' => Orangtua::all()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasPermission('menu_data_orangtua-create')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data orangtua');
        } else {
            return view('admin.data_orangtua.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required', 'string', 'max:255'],
        ]);

        try {
            DB::beginTransaction();

            Orangtua::create([
                'nama' => $request->nama,
                'hp' => $request->hp,
                'kerja' => $request->kerja,
                'alamat' => $request->alamat,
            ]);

            DB::commit();

            return redirect()->route('orangtua.index')->with('success', 'Tambah data orangtua berhasil');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->hasPermission('menu_data_orangtua-update')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data orangtua');
        } else {
            try {
                $ortu = Orangtua::findOrFail($id);
                return view('admin.data_orangtua.edit', [
                    'ortu' => $ortu
                ]);
            } catch (Exception $e) {
                return redirect()->back()->with('error', 'Error ' . $e);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required', 'string', 'max:255'],
        ]);

        try {
            $ortu = Orangtua::findOrFail($id);

            DB::beginTransaction();

            $ortu->update([
                'nama' => $request->nama,
                'hp' => $request->hp,
                'kerja' => $request->kerja,
                'alamat' => $request->alamat,
            ]);

            User::where('id', $ortu->user_id)->update([
                'name' => $request->nama
            ]);

            DB::commit();

            return redirect()->route('orangtua.index')->with('success', 'Tambah data orangtua berhasil');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();

            $ortu = Orangtua::findOrFail($id);
            if ($ortu->user_id) {
                $user = User::findOrFail($ortu->user_id);

                $user->detachRoles(['orangtua_mln', 'orangtua_tsn']);

                $user->delete();
            }
            $ortu->delete();

            DB::commit();

            return redirect()->route('orangtua.index')->with('success', 'Hapus data orangtua berhasil');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_orangtua.nama';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_orangtua.nama';
                break;
            case "1":
                $orderBy = 'tb_orangtua.hp';
                break;
            case "2":
                $orderBy = 'tb_orangtua.alamat';
                break;
        }

        $data = Orangtua::select([
            'tb_orangtua.*'
        ]);

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(tb_orangtua.nama) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_orangtua.hp) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_orangtua.alamat) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        // if ($request->input('kelas') != null) {
        //     $data = $data->where('kelas_id', $request->kelas);
        // }

        // if ($request->input('jenjang') != null) {
        //     $data = $data->where('tb_asatidz_pelajaran.tingkat_sekolah_id', $request->jenjang);
        // }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('user')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }
}