<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\Orangtua;
use App\Models\Prestasi;
use App\Models\Santri;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index()
    {
        $data = null;
        $photo = null;
        if(Auth::user()->hasRole(['admin']))
            $data = User::where('id', Auth::user()->id)->first();
        if(Auth::user()->hasRole(['admin','asatidz','asatidz_tsn','asatidz_mln','muaddib','mudir_tsn','mudir_mln','mudir_am'])) {
            $data = Asatidz::where('user_id', Auth::user()->id)->first();
        } elseif (Auth::user()->hasRole(['santri_tsn', 'santri_mln'])) {
            $idSantri = Santri::where('user_id', Auth::user()->id)->first();
            $array1['prestasi'] = DB::table('tb_prestasi')
                ->select('tb_prestasi.*')
                ->join('tb_santri', 'tb_santri.id', 'tb_prestasi.santri_id')
                ->where('tb_prestasi.santri_id', $idSantri->id)
                ->get()->map(function ($item) {
                    return (array) $item;
                })->toArray();

            $array2 = (array) Santri::where('user_id', Auth::user()->id)->first()->toArray();;
            $data = (object) array_merge($array1, $array2);
            // dd($data->prestasi);

        } elseif (Auth::user()->hasRole(['orangtua_tsn', 'orangtua_mln'])) {
            $idOrtu = Orangtua::where('user_id', Auth::user()->id)->first()->user_id;
            $data = DB::table('tb_orangtua')
                    ->select('tb_santri.*','tb_orangtua.photo_path as photo_path')
                    ->join('tb_santri', 'tb_santri.orangtua_id', 'tb_orangtua.id')
                    ->join('users', 'users.id', 'tb_orangtua.user_id')
                    ->where('users.id', $idOrtu)
                    ->first();
        }
        
        return view('admin.data_profile.index', [
            'data' => $data
        ]);
    }

    public function account()
    {
        $data = null;
        if(Auth::user()->hasRole(['asatidz|asatidz_tsn','asatidz_mln','muaddib','mudir_tsn','mudir_mln','mudir_am']))
            $data = Asatidz::where('user_id', Auth::user()->id)->first();

        if(Auth::user()->hasRole(['santri_tsn', 'santri_mln']))
            $data = Santri::where('user_id', Auth::user()->id)->first();

        if(Auth::user()->hasRole(['orangtua_tsn', 'orangtua_mln']))
            $data = Orangtua::where('user_id', Auth::user()->id)->first();

        return view('admin.data_profile.index', [
            'data' => $data
        ]);
    }

    public function uploadfile(Request $request, $id)
    {
        request()->validate([
            'image'  => 'mimes:jpeg,png,jpg|max:1048',
        ]);

        if(Auth::user()->hasRole(['admin','asatidz','asatidz_tsn','asatidz_mln','muaddib','mudir_tsn','mudir_mln','mudir_am'])) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');

                $filenameAsal = Asatidz::where('user_id', $id)->first();
                $filenameAsal = $filenameAsal->photo_path;
        
                $fileName = 'photo_'. date('YmdHis') . '.'.$file->getClientOriginalExtension();
                Storage::disk('s3')->put('data/photo/asatidz/' .$fileName, file_get_contents($file), 'public');

                //store your file into database
                $asatidz = Asatidz::where('user_id', $id)->update([
                    "photo_path" => $fileName
                ]);

                if ($asatidz) {
                    // delete file lama
                    Storage::disk('s3')->delete('data/photo/asatidz/' .$filenameAsal);
                }

                return Response()->json([
                    "success" => true,
                    "message" => "Berhasil, Anda telah menambahkan photo profile",
                    "url" => "/admin/profile?pills=general"
                ]);
            }else{
                return Response()->json([
                    "success" => false,
                    "message" => "Gagal, menambahkan photo profile",
                    "url" => "/admin/profile?pills=general"
                ]);
            }
        }

        if(Auth::user()->hasRole(['santri_tsn', 'santri_mln'])) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');

                $filenameAsal = Santri::where('user_id', $id)->first();
                $filenameAsal = $filenameAsal->photo_path;
        
                $fileName = 'photo_'. date('YmdHis') . '.'.$file->getClientOriginalExtension();
                Storage::disk('s3')->put('data/photo/santri/' .$fileName, file_get_contents($file), 'public');

                //store your file into database
                $asatidz = Santri::where('user_id', $id)->update([
                    "photo_path" => $fileName
                ]);

                if ($asatidz) {
                    // delete file lama
                    Storage::disk('s3')->delete('data/photo/santri/' .$filenameAsal);
                }
                
                return Response()->json([
                    "success" => true,
                    "message" => "Berhasil, Anda telah menambahkan photo profile",
                    "url" => "/admin/profile?pills=general"
                ]);
            }else{
                return Response()->json([
                    "success" => false,
                    "message" => "Gagal, menambahkan photo profile",
                    "url" => "/admin/profile?pills=general"
                ]);
            }
        }

        if(Auth::user()->hasRole(['orangtua_tsn', 'orangtua_mln'])) {
            // dd($id);
            if ($request->hasFile('image')) {
                $file = $request->file('image');

                $filenameAsal = DB::table('tb_orangtua')
                    ->select('tb_orangtua.*')
                    ->where('user_id', $id)
                    ->first();
                if ($filenameAsal) {
                    $filenameAsal = $filenameAsal->photo_path;
                }
        
                $fileName = 'photo_'. date('YmdHis') . '.'.$file->getClientOriginalExtension();  
                Storage::disk('s3')->put('data/photo/orangtua/' .$fileName, file_get_contents($file), 'public');

                //store your file into database
                $ortu =  DB::table('tb_orangtua')
                    ->where('user_id', $id)
                    ->update([
                        "photo_path" => $fileName
                    ]);

                if ($filenameAsal) {
                    Storage::disk('s3')->delete('data/photo/orangtua/' .$filenameAsal);
                }
                
                return Response()->json([
                    "success" => true,
                    "message" => "Berhasil, Anda telah menambahkan photo profile",
                    "url" => "/admin/profile?pills=general"
                ]);
            }else{
                return Response()->json([
                    "success" => false,
                    "message" => "Gagal, menambahkan photo profile",
                    "url" => "/admin/profile?pills=general"
                ]);
            }
        }

        return Response()->json([
            "success" => false,
            "message" => "Gagal, menambahkan photo profile",
            "url" => "/admin/profile?pills=general"
        ]);
    }


    public function update(Request $request, User $user)
    {
        try {

            $username = strtolower(str_replace(' ', '', $request->username));

            DB::beginTransaction();

            if($request->has('pills') && $request->pills == 'account') {
                $user->update([
                    'email' => $request->email,
                    'username' => $username
                ]);
                if(Auth::user()->hasRole(['admin'])) {
                    $user->update([
                        'name' => Auth()->user()->name,
                        'email' => $request->email,
                        'username' => $username
                    ]);
                }
            }

            if($request->has('pills') && $request->pills == 'general') {
                if(Auth::user()->hasRole(['admin','asatidz','asatidz_tsn','asatidz_mln','muaddib','mudir_tsn','mudir_mln','mudir_am'])) {
                    $user->update([
                        'name' => $request->nama,
                    ]);
                    $asatidz = Asatidz::where('user_id', $user->id)->first();
    
                    $filenameAsal = Asatidz::where('user_id', $user->id)->first();

                    $file = $request->file('uploadIjazah');
                    $file1 = $request->file('upload_kk');
                    $file2 = $request->file('upload_akte');
                    $file3 = $request->file('upload_ktp');
                    $file4 = $request->file('upload_buku_rekening');

                    if($request->hasFile('uploadIjazah')) {
                        $extension = $file->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File Ijazah harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_ijazah = $filenameAsal->upload_ijazah;
                        Storage::disk('s3')->delete('data/asatidz/ijazah/' .$upload_ijazah);

                        $fileijazah = 'ijazah_'. date('YmdHis') . '.'.$file->getClientOriginalExtension();
                        Storage::disk('s3')->put('data/asatidz/ijazah/' .$fileijazah, file_get_contents($file), 'public');
                    }

                    if($request->hasFile('upload_kk')) {
                        $extension = $file1->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File Kartu Keluarga harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_kk = $filenameAsal->upload_kk;
                        Storage::disk('s3')->delete('data/asatidz/kartu_keluarga/' .$upload_kk);


                        $filekk = 'kk_'. date('YmdHis') . '.'.$file1->getClientOriginalExtension();  
                        Storage::disk('s3')->put('data/asatidz/kartu_keluarga/' .$filekk, file_get_contents($file1), 'public');
                    }

                    if($request->hasFile('upload_akte')) {
                        $extension = $file2->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File Akte Kelahiran harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_akte = $filenameAsal->upload_akte;
                        Storage::disk('s3')->delete('data/asatidz/akte/' .$upload_akte);


                        $fileakte = 'akte_'. date('YmdHis') . '.'.$file2->getClientOriginalExtension();
                        Storage::disk('s3')->put('data/asatidz/akte/' .$fileakte, file_get_contents($file2), 'public');
                    }

                    if($request->hasFile('upload_ktp')) {
                        $extension = $file3->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File KTP harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_ktp = $filenameAsal->upload_ktp;
                        Storage::disk('s3')->delete('data/asatidz/ktp/' .$upload_ktp);

                        $filektp = 'ktp_'. date('YmdHis') . '.'.$file3->getClientOriginalExtension();  
                        Storage::disk('s3')->put('data/asatidz/ktp/' .$filektp, file_get_contents($file3), 'public');

                    }

                    if($request->hasFile('upload_buku_rekening')) {
                        $extension = $file4->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File Buku Rekening harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_buku_rekening = $filenameAsal->upload_buku_rekening;
                        Storage::disk('s3')->delete('data/asatidz/buku_rekening/' .$upload_buku_rekening);

                        $filerekening = 'rekening_'. date('YmdHis') . '.'.$file4->getClientOriginalExtension();  
                        Storage::disk('s3')->put('data/asatidz/buku_rekening/' .$filerekening, file_get_contents($file4), 'public');
                    }
    
                    //store your file into database
                    $asatidz = Asatidz::where('user_id', $user->id)->update([
                        'niat' => $request->niat,
                        'nama_lengkap' => $request->nama,
                        'jenis_kelamin' => $request->jenis_kelamin,
                        'status_menikah' => $request->status_menikah,
                        'tsn' => $request->tsn,
                        'mln' => $request->mln,
                        's1' => $request->s1,
                        's2' => $request->s2,
                        's3' => $request->s3,
                        'pendidikan_terakhir' => $request->pendidikan_terakhir,
                        'no_telp' => $request->no_telp, 
                        'alamat' => $request->alamat,
                        'tempat_lahir' => $request->tempat_lahir,
                        'tgl_lahir' => $request->tgl_lahir,
                        'hafalan' => $request->hafalan,
                        "upload_ijazah" => isset($fileijazah) ? $fileijazah : ($filenameAsal->upload_ijazah ? $filenameAsal->upload_ijazah : null),
                        "upload_kk" => isset($filekk)  ? $filekk : ($filenameAsal->upload_kk ? $filenameAsal->upload_kk : null),
                        "upload_akte" => isset($fileakte) ? $fileakte : ($filenameAsal->upload_akte ? $filenameAsal->upload_akte : null),
                        "upload_ktp" => isset($filektp) ? $filektp : ($filenameAsal->upload_ktp ? $filenameAsal->upload_ktp : null),
                        "upload_buku_rekening" => isset($filerekening) ? $filerekening : ($filenameAsal->upload_buku_rekening ? $filenameAsal->upload_buku_rekening : null),
                    ]);
                }

                if(Auth::user()->hasRole(['orangtua_tsn', 'orangtua_mln'])) {
                    $user->update([
                        'name' => $request->nama_lengkap_ayah,
                    ]);

                    $orangtua = Orangtua::where('user_id', $user->id)->first();
                    $orangtua->update([
                        'nama'      => $request->nama_lengkap_ayah,
                        'hp'        => $request->no_ktp_ayah,
                        'alamat'    => $request->alamat,
                        'kerja'     => $request->pekerjaan_ayah,
                    ]);

                    $santri = Santri::where('orangtua_id', $orangtua->id)->first();
                    $santri->update([
                        'nik'                   => $request->nik,
                        'nomor_kartu_keluarga'  => $request->no_kartu_keluarga,
                        'nama_lengkap_ayah'     => $request->nama_lengkap_ayah,
                        'pekerjaan_ayah'        => $request->pekerjaan_ayah,
                        'no_ktp_ayah'           => $request->no_ktp_ayah,
                        'no_hp_ayah'            => $request->no_hp_ayah,
                        'pendidikan_ayah'       => $request->pendidikan_ayah,
                        'pekerjaan_ayah'        => $request->pekerjaan_ayah,
                        'penghasilan_ayah'      => $request->penghasilan_ayah,
                        'nama_lengkap_ibu'      => $request->nama_lengkap_ibu,
                        'no_ktp_ibu'            => $request->no_ktp_ibu,
                        'no_hp_ibu'             => $request->no_hp_ibu,
                        'pekerjaan_ibu'         => $request->pekerjaan_ibu,
                        'penghasilan_ibu'       => $request->penghasilan_ibu,
                        'pendidikan_ibu'        => $request->pendidikan_ibu,
                        'nama_lengkap_wali'     => $request->nama_lengkap_wali,
                        'no_ktp_wali'           => $request->no_ktp_wali,
                        'no_hp_wali'            => $request->no_hp_wali,
                        'pekerjaan_wali'        => $request->pekerjaan_wali,
                        'pendidikan_wali'       => $request->pendidikan_wali,
                    ]);
                }

                if(Auth::user()->hasRole(['santri_tsn', 'santri_mln'])) {
                    $santri = Santri::where('user_id', $user->id)->first();
                    $file1 = $request->file('uploadIjazahSD');
                    $file2 = $request->file('uploadIjazahMTS');
                    $file3 = $request->file('upload_photo_santri');
                    $file4 = $request->file('upload_akte');

                    // Mengambil data dari request
                    $jenisPrestasi = $request->input('jenis_prestasi');
                    $tingkatPrestasi = $request->input('tingkat_prestasi');
                    $tahunPrestasi = $request->input('tahun_prestasi');

                    // Loop untuk menyimpan setiap data ke dalam tabel
                    for ($i = 0; $i < count($jenisPrestasi); $i++) {

                        $validator = Validator::make(['jenis_prestasi' => $jenisPrestasi[$i]], Prestasi::$rules);

                        if ($validator->fails()) {
                            return redirect()->back()->with('error', 'Jenis Prestasi Sama')->withInput();
                        }
                    
                        // Mengambil data berdasarkan santri_id dan jenis_prestasi
                        $dataAda = Prestasi::where('santri_id', $santri->id)
                        ->where('jenis_prestasi', $jenisPrestasi[$i])
                        ->first();

                        if(!$dataAda) {
                            // Menghitung jumlah data yang sudah ada untuk jenis prestasi ini
                            $countData = Prestasi::where('santri_id', $santri->id)
                                ->count();
                            if ($countData < 3) {
                                Prestasi::create(
                                    [
                                        'santri_id' => $santri->id,
                                        'jenis_prestasi' => $jenisPrestasi[$i],
                                        'tingkat' => $tingkatPrestasi[$i],
                                        'tahun' => $tahunPrestasi[$i],
                                    ]
                                );
                            } else {
                                // Jika jumlah data sudah 3, berikan pesan error
                                return redirect()->back()->with('error', 'Anda sudah memiliki 3 data. Tidak dapat menambahkan lebih banyak.')->withInput();
                            }  
                        }else{
                            $dataAda->update(
                                [
                                    'jenis_prestasi' => $jenisPrestasi[$i],
                                    'tingkat' => $tingkatPrestasi[$i],
                                    'tahun' => $tahunPrestasi[$i],
                                ]
                            );
                        }
                        
                    }

                    $filenameAsal = Santri::where('user_id', $user->id)->first();

                    if($request->hasFile('uploadIjazahSD')) {
                        $extension = $file1->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File Ijazah harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_ijazah_sd = $filenameAsal->path_ijazah_sd;
                        Storage::disk('s3')->delete('data/santri/ijazah-sd/' .$upload_ijazah_sd);

                        $fileijazahsd = 'ijazah_sd_'. date('YmdHis') . '.'.$file1->getClientOriginalExtension();
                        Storage::disk('s3')->put('data/santri/ijazah-sd/' .$fileijazahsd, file_get_contents($file1), 'public');
                    }

                    if($request->hasFile('uploadIjazahMTS')) {
                        $extension = $file2->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File Ijazah harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_ijazah_mts = $filenameAsal->path_ijazah_mts;
                        Storage::disk('s3')->delete('data/santri/ijazah-mts/' .$upload_ijazah_mts);

                        $fileijazahmts = 'ijazah_mts_'. date('YmdHis') . '.'.$file2->getClientOriginalExtension();
                        Storage::disk('s3')->put('data/santri/ijazah-mts/' .$fileijazahmts, file_get_contents($file2), 'public');
                    }

                    if($request->hasFile('upload_photo_santri')) {
                        $extension = $file3->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File Ijazah harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_foto_santri = $filenameAsal->path_foto_santri;
                        Storage::disk('s3')->delete('data/santri/photo_formal/' .$upload_foto_santri);

                        $filefotosantri = 'photo_formal_santri_'. date('YmdHis') . '.'.$file3->getClientOriginalExtension();
                        Storage::disk('s3')->put('data/santri/photo_formal/' .$filefotosantri, file_get_contents($file3), 'public');
                    }

                    if($request->hasFile('upload_akte')) {
                        $extension = $file4->getClientOriginalExtension();
                        if(!in_array($extension, ['png','jpg','pdf','jpeg'])) {
                            return redirect()->back()->with('error', 'File Ijazah harus Gambar (jpg, png) atau pdf')->withInput();
                        }
                        $upload_akte = $filenameAsal->path_akte;
                        Storage::disk('s3')->delete('data/santri/akte/' .$upload_akte);

                        $fileakte = 'akte_santri_'. date('YmdHis') . '.'.$file4->getClientOriginalExtension();
                        Storage::disk('s3')->put('data/santri/akte/' .$fileakte, file_get_contents($file4), 'public');
                    }

                    $santri->update([
                        'nama_lengkap'          => $request->nama_lengkap,
                        'nis'                   => $request->nis,
                        'nisn'                  => $request->nisn,
                        'jk'                    => $request->jk,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'alamat_kota'           => $request->kota,
                        'alamat'                => $request->alamat,
                        'no_hp'                 => $request->no_hp,
                        'asal_sekolah'          => $request->asal_sekolah,
                        'alamat_asal_sekolah'   => $request->alamat_asal_sekolah,
                        'hafalan'               => $request->hafalan,
                        'path_akte'             => isset($fileakte) ? $fileakte : ($filenameAsal->path_akte ? $filenameAsal->path_akte : null),
                        'path_foto_santri'      => isset($filefotosantri) ? $filefotosantri : ($filenameAsal->path_foto_santri ? $filenameAsal->path_foto_santri : null),
                        'path_ijazah_sd'        => isset($fileijazahsd) ? $fileijazahsd : ($filenameAsal->path_ijazah_sd ? $filenameAsal->path_ijazah_sd : null),
                        'path_ijazah_mts'       => isset($fileijazahmts) ? $fileijazahmts : ($filenameAsal->path_ijazah_mts ? $filenameAsal->path_ijazah_mts : null),
                    ]);

                    $user->update([
                        'name' => $request->nama_lengkap,
                    ]);
                }
            }

            if(Auth::user()->hasRole(['santri_tsn', 'santri_mln'])) {
                if ($request->has('pills') && $request->pills == 'dataortu') {
                    $santri = Santri::where('user_id', $user->id)->first();
                    $orangtua = Orangtua::where('id', $santri->orangtua_id)->first();
                    $userOrangtua = User::where('id', $orangtua->user_id)->first();
                    $userOrangtua->update([
                        'name' => $request->nama_lengkap_ayah,
                    ]);

                    $orangtua->update([
                        'nama'      => $request->nama_lengkap_ayah,
                        'hp'        => $request->no_ktp_ayah,
                        'alamat'    => $request->alamat,
                        'kerja'     => $request->pekerjaan_ayah,
                    ]);

                    $santri->update([
                        'nik'                   => $request->nik,
                        'nomor_kartu_keluarga'  => $request->no_kartu_keluarga,
                        'nama_lengkap_ayah'     => $request->nama_lengkap_ayah,
                        'pekerjaan_ayah'        => $request->pekerjaan_ayah,
                        'no_ktp_ayah'           => $request->no_ktp_ayah,
                        'no_hp_ayah'            => $request->no_hp_ayah,
                        'pendidikan_ayah'       => $request->pendidikan_ayah,
                        'pekerjaan_ayah'        => $request->pekerjaan_ayah,
                        'penghasilan_ayah'      => $request->penghasilan_ayah,
                        'nama_lengkap_ibu'      => $request->nama_lengkap_ibu,
                        'no_ktp_ibu'            => $request->no_ktp_ibu,
                        'no_hp_ibu'             => $request->no_hp_ibu,
                        'pekerjaan_ibu'         => $request->pekerjaan_ibu,
                        'penghasilan_ibu'       => $request->penghasilan_ibu,
                        'pendidikan_ibu'        => $request->pendidikan_ibu,
                        'nama_lengkap_wali'     => $request->nama_lengkap_wali,
                        'no_ktp_wali'           => $request->no_ktp_wali,
                        'no_hp_wali'            => $request->no_hp_wali,
                        'pekerjaan_wali'        => $request->pekerjaan_wali,
                        'pendidikan_wali'       => $request->pendidikan_wali,
                        'alamat'                => $request->alamat
                    ]);
                }
            }            

            DB::commit();

            return redirect()->back()->with('success', 'Sukses Update Data');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Ada Error: ' . $e->getMessage())->withInput();
        }
    }

    public function ChangePassword(Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);

        try {
            DB::beginTransaction();

            $user = User::find(Auth::user()->id);
            $user->update([
                'password' => bcrypt($request->password)
            ]);  
            DB::commit();
            return redirect()->back()->with('success', 'Sukses Ganti Password');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Ada Error: ' . $e->getMessage());
        }
    }
}
