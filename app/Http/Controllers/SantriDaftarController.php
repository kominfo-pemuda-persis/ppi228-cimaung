<?php

namespace App\Http\Controllers;

use App\Models\GroupWhatsapp;
use App\Models\Juz;
use App\Models\Orangtua;
use App\Models\PSB;
use App\Models\Santri;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Barryvdh\DomPDF\Facade\Pdf;


class SantriDaftarController extends Controller
{
    public function index()
    {
        $santri = null;
        $status = null;
        $groupWa = GroupWhatsapp::get();
        if(request('search')) {
            $santri = Santri::where('kode_daftar', request('search'))->orWhere('nik', request('search'))->first();
            if(!$santri)
                return redirect()->route('psb')->with('error', 'NIK / Kode Daftar ' . request('search') . ' Tidak ditemukan');
            
            if($santri->status_pembayaran == 'pending' && $santri->status_active == 'non active') {
                $status = 'pendaftaran';
            } 
            
            if($santri->status_pembayaran == 'approved' && $santri->status_active == 'non active') {
                $status = 'infaq';
            } 
            
            if($santri->status_pembayaran == 'approved' && $santri->status_active == 'rejected') {
                $status = 'gagal';
            } 
            
            if($santri->status_pembayaran == 'approved' && $santri->status_active == 'active') {
                $status = 'lolos';
            }
        }

        $dataWaTSN = null;
        $dataWaMLN = null;
        foreach($groupWa as $wa) {
            if($wa->tingkat_sekolah_id == "TSN") {
                $dataWaTSN = $wa->whatsappUrl;
            }else{
                $dataWaMLN = $wa->whatsappUrl;
            }
        }

        return view('daftar_santri.index', [
            'juzs' => Juz::all(),
            'dataWaTSN' => $dataWaTSN,
            'dataWaMLN' => $dataWaMLN,
            'sudahDaftar' => false,
            'psb' => PSB::where('status', 'ACTIVE')->first(),
            'santri' => $santri,
            'status' => $status
        ]);
    }

    public function daftarSantri(Request $request)
    {
        if($request->siswa_lanjutan_baru == 'lanjutan') {
            $cekNIKsantri = Santri::select('nis')->where('nis', $request->nis)->first();
            if(!$cekNIKsantri)
                return redirect()->route('psb')->withErrors(['msg' => 'Maaf, NIS: ' . $request->nis . ' tidak terdaftar']);

            dd($request->all());
        }else{
            $this->validate($request, [
                'nama' => 'required',
                'tempatLahir' => 'required',
                'tanggalLahir' => 'required',
                'kota' => 'required',
                'jk' => 'required',
                'alamat' => 'required',
                'asalSekolah' => 'required',
                'alamatSekolah' => 'required',
                'tingkat' => 'required',
                'hp' => 'required',
                'ayah' => 'required',
                'ibu' => 'required',
                'juz' => 'required',
                'asrama' => 'required',
                'nik' => 'required',
                'nomor_kartu_keluarga' => 'required'
            ]);

            $cekNIKsantri = Santri::select('nik')->where('nik', $request->nik)->first();
            if($cekNIKsantri)
                return redirect()->route('psb')->withErrors(['msg' => 'Maaf, NIK: ' . $request->nik . ' sudah terdaftar']);

            dd($request->all());

        }

        try {
            
            DB::beginTransaction();

            $kodeDaftar = time();

            $santri = Santri::create([
                'nama_lengkap'  => ucwords(strtolower($request->nama)),
                'tempat_lahir'  => ucwords(strtolower($request->tempatLahir)),
                'tanggal_lahir' => $request->tanggalLahir,
                'alamat_kota'   => ucwords(strtolower($request->kota)),
                'jk'            => $request->jk,
                'alamat'        => ucwords(strtolower($request->alamat)),
                'asal_sekolah'  => ucwords(strtolower($request->asalSekolah)),
                'alamat_asal_sekolah'   => ucwords(strtolower($request->alamatSekolah)),
                'tingkat_sekolah_id'    => $request->tingkat,
                'kelas'                 => '1',
                'no_hp'                 => $request->hp,
                'email'                 => $request->email,
                'nama_lengkap_ayah'     => ucwords(strtolower($request->ayah)),
                'nama_lengkap_ibu'      => ucwords(strtolower($request->ibu)),
                'hafalan'               => count($request->juz) . ' Juz',
                'status_keperluan_asrama' => $request->asrama,
                'status_active'         => 'non active',
                'start_year'            => $request->start_year,
                'nik'                   => $request->nik,
                'nomor_kartu_keluarga'  => $request->nomor_kartu_keluarga,
                'kode_daftar'           => $kodeDaftar,
                'kelas_masuk'           => '1',
                'tingkat_masuk'         => $request->tingkat,
                'tahun_masuk'           => $request->start_year
            ]);

            $santri->juzs()->attach($request->juz);
            
            $curl = curl_init(); 
            curl_setopt_array($curl, array( 
                CURLOPT_URL => 'https://wagateway.pesantrenpersis27.com/send-message', 
                CURLOPT_RETURNTRANSFER => true, 
                CURLOPT_ENCODING => '', 
                CURLOPT_MAXREDIRS => 10, 
                CURLOPT_TIMEOUT => 0, 
                CURLOPT_FOLLOWLOCATION => true, 
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, 
                CURLOPT_CUSTOMREQUEST => 'POST', 
                CURLOPT_POSTFIELDS => 
                    array( 
                        'message' => 'TERIMAKASIH, Anda sudah terdaftar sebagai calon santri PPI27', 
                        'number' => '0859106987014', 
                        'file_dikirim'=> '' ), )
                    ); 
            $response = curl_exec($curl); curl_close($curl);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('psb')->with(['error', 'Maaf telah terjadi error, dikarnakan '.$e->getMessage()]);
        }

        return redirect()->route('psb')->with(['error', 'Maaf telah terjadi error, dikarnakan '.$e->getMessage()]);


        $lolos = false;
        if($santri->status_pembayaran == 'approved' && $santri->status_active == 'active') 
            $lolos = 'lolos';
        
        if($santri->status_pembayaran == 'approved' && $santri->status_active == 'rejected') 
            $lolos = 'gagal';

        return PDF::loadView('daftar_santri.template_pendaftaran', [
            'santri' => $santri,
            'lolos' => $lolos
        ])->stream();
    }

    public function preview($kode)
    {
        $lolos = false;
        $santri = Santri::where('kode_daftar', $kode)->orWhere('nik', $kode)->first();
        if(!$santri)
            return redirect()->route('psb')->with('error', 'Data tidak ditemukan');
        
        if($santri->status_pembayaran == 'approved' && $santri->status_active == 'active') 
            $lolos = 'lolos';
        
        if($santri->status_pembayaran == 'approved' && $santri->status_active == 'rejected') 
            $lolos = 'gagal';
            
        return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('daftar_santri.template_pendaftaran', [
            'santri' => $santri,
            'lolos' => $lolos
        ])->stream();
    }
}