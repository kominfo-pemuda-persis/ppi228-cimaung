<?php

namespace App\Http\Controllers;

use App\Models\KelompokPelajaran;
use Illuminate\Http\Request;

class KelompokPelajaranController extends Controller
{
    public function index()
    {
        $datas = KelompokPelajaran::all();
        return view('admin.data_kelompok_pelajaran.index', compact('datas'));
    }

    public function edit($id)
    {
        $data = KelompokPelajaran::findOrFail($id);

        return response()->json([
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = KelompokPelajaran::find($id);

        $data->update([
            'nama_kelompok_pelajaran' => $request->kelompok_pelajaran,
            'keterangan' => $request->kelompok_keterangan
        ]);

        if($data) {
            return response()->json([
                'data' => $data,
                'success' => 'data kelompok pelajaran berhasil ke update',
                'Url' => '/admin/kelompok-pelajaran'
            ]);
        }
    }

    public function delete($id)
    {
        $data = KelompokPelajaran::find($id);
        $data->delete();

        if($data) {
            return redirect()->route('kelompok_pelajaran.index')->with('success', 'Data kelompok pelajaran Berhasil Dihapus');
        }else{
            return redirect()->route('kelompok_pelajaran.index')->with('error', 'Data kelompok pelajaran Gagal Dihapus');
        }
    }

    public function store(Request $request)
    {
        $data = new KelompokPelajaran;
        $data->nama_kelompok_pelajaran = $request->nama_kelompok_pelajaran;
        $data->keterangan = $request->keterangan;
        $data->save();

        if($data) {
            return redirect()->route('kelompok_pelajaran.index')->with('success', 'Data kelompok pelajaran Berhasil Disimpan');
        }else{
            return redirect()->route('kelompok_pelajaran.index')->with('error', 'Data kelompok pelajaran Gagal Disimpan');
        }
    }
}
