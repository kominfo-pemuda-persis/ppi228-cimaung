<?php

namespace App\Http\Controllers;

use App\Models\KaryaTulisIlmiah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class FrontKtiController extends Controller
{
    public function index()
    {
        $data = KaryaTulisIlmiah::join('tb_santri', 'tb_santri.id', '=', 'tb_karya_tulis_ilmiah.santri_id')
                ->select('tb_karya_tulis_ilmiah.id', 'tb_karya_tulis_ilmiah.angkatan', 'tb_karya_tulis_ilmiah.judul_kti', 'tb_karya_tulis_ilmiah.thumbnail_kti_path', 'tb_karya_tulis_ilmiah.tanggal_diujikan', 'tb_santri.nama_lengkap', 'tb_santri.kelas', 'tb_santri.tingkat_sekolah_id')
                ->where('tb_karya_tulis_ilmiah.status', 'publish')
                ->where('tb_karya_tulis_ilmiah.kti_path', '!=', "unset")
                ->orderBy('tb_karya_tulis_ilmiah.id', 'desc')
                ->limit(6)
                ->get();
        // format tanggal ke hari bulan tahun contoh: 12 Jan 2021 gunakan map
        $data->map(function ($item) {
            $item->tanggal_diujikan = date('d M Y', strtotime($item->tanggal_diujikan));
            $item->thumbnail_kti_path = Storage::disk('s3')->url('data/santri/thumbnail_kti/'.$item->thumbnail_kti_path);
            return $item;
        });
        return view('user.kti.index', compact('data'));
    }

    public function dataTableIndex()
    {
        $data = KaryaTulisIlmiah::leftJoin('tb_santri', 'tb_santri.id', '=', 'tb_karya_tulis_ilmiah.santri_id')
                ->leftJoin('tb_asatidz as pembimbing', 'pembimbing.id', '=', 'tb_karya_tulis_ilmiah.pembimbing')
                ->leftJoin('tb_asatidz as penguji', 'penguji.id', '=', 'tb_karya_tulis_ilmiah.penguji')
                ->select('tb_karya_tulis_ilmiah.angkatan', 'tb_karya_tulis_ilmiah.judul_kti', 'tb_karya_tulis_ilmiah.tanggal_diujikan', 'tb_karya_tulis_ilmiah.status', 'tb_santri.nama_lengkap', 'pembimbing.nama_lengkap as nama_pembimbing', 'penguji.nama_lengkap as nama_penguji')
                ->orderBy('tb_karya_tulis_ilmiah.id', 'desc')
                ->get();
        // datatable yajra
        $dataTable = DataTables::of($data)
            ->addIndexColumn()
            ->make(true);

        return $dataTable;
    }

    public function listKti()
    {
        $search = request()->search;
        $angkatan = request()->angkatan;

        $data = KaryaTulisIlmiah::join('tb_santri', 'tb_santri.id', '=', 'tb_karya_tulis_ilmiah.santri_id')
                ->select('tb_karya_tulis_ilmiah.id','tb_karya_tulis_ilmiah.angkatan', 'tb_karya_tulis_ilmiah.judul_kti', 'tb_karya_tulis_ilmiah.thumbnail_kti_path', 'tb_karya_tulis_ilmiah.tanggal_diujikan', 'tb_santri.nama_lengkap', 'tb_santri.kelas', 'tb_santri.tingkat_sekolah_id')
                ->where('tb_karya_tulis_ilmiah.status', 'publish')
                ->where('tb_karya_tulis_ilmiah.kti_path', '!=', "unset")
                ->where(function ($query) use ($search) {
                    $query->where('tb_karya_tulis_ilmiah.judul_kti', 'like', "%$search%")
                        ->orWhere('tb_santri.nama_lengkap', 'like', "%$search%");
                })
                ->orderBy('tb_karya_tulis_ilmiah.id', 'desc');
        if ($angkatan != null) {
            $data->where('tb_karya_tulis_ilmiah.angkatan', $angkatan);
        }
        $data = $data->paginate(18);
        // format tanggal ke hari bulan tahun contoh: 12 Jan 2021 gunakan map
        $data->getCollection()->map(function ($item) {
            $item->tanggal_diujikan = date('d M Y', strtotime($item->tanggal_diujikan));
            $item->thumbnail_kti_path = Storage::disk('s3')->url('data/santri/thumbnail_kti/'.$item->thumbnail_kti_path);
            return $item;
        });
        return view('user.kti.list_karya', compact('data'));
    }

    public function detailKti($id)
    {
        $data = KaryaTulisIlmiah::join('tb_santri', 'tb_santri.id', '=', 'tb_karya_tulis_ilmiah.santri_id')
        ->join('tb_asatidz as pembimbing', 'pembimbing.id', '=', 'tb_karya_tulis_ilmiah.pembimbing')
        ->join('tb_asatidz as penguji', 'penguji.id', '=', 'tb_karya_tulis_ilmiah.penguji')
        ->select('tb_karya_tulis_ilmiah.angkatan', 'tb_karya_tulis_ilmiah.judul_kti', 'tb_karya_tulis_ilmiah.tanggal_diujikan', 'tb_karya_tulis_ilmiah.status', 'tb_santri.nama_lengkap', 'pembimbing.nama_lengkap as nama_pembimbing', 'penguji.nama_lengkap as nama_penguji', 'tb_karya_tulis_ilmiah.thumbnail_kti_path', 'tb_karya_tulis_ilmiah.kti_path', 'tb_karya_tulis_ilmiah.lihat')
        ->where('tb_karya_tulis_ilmiah.id', $id)
        ->first();
        // format tanggal ke hari bulan tahun contoh: 12 Jan 2021 gunakan map
        $data->tanggal_diujikan = date('d M Y', strtotime($data->tanggal_diujikan));
        $data->thumbnail_kti_path = Storage::disk('s3')->url('data/santri/thumbnail_kti/'.$data->thumbnail_kti_path);
        $data->kti_path = Storage::disk('s3')->url('data/santri/kti/'.$data->kti_path);
        return view('user.kti.detail', compact('data'));
    }

    public function increaseView($id)
    {
        $data = KaryaTulisIlmiah::find($id);
        if ($data->lihat == null) {
            $data->lihat = 1;
            $data->save();
        } else {
            $data->increment('lihat');
        }
        return response()->json(['success' => 'success']);
    }
}
