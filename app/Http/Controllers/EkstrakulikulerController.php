<?php

namespace App\Http\Controllers;

use App\Models\Ekstrakulikuler;
use Illuminate\Http\Request;

class EkstrakulikulerController extends Controller
{
    public function index()
    {
        $datas = Ekstrakulikuler::all();
        return view('admin.data_ekstrakulikuler.index', compact('datas'));
    }

    public function edit($id)
    {
        $data = Ekstrakulikuler::find($id);

        return response()->json([
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = Ekstrakulikuler::find($id);

        $data->update([
            'nama' => $request->nama,
            'keterangan' => $request->keterangan
        ]);

        if($data) {
            return response()->json([
                'data' => $data,
                'success' => 'data ekstrakurikuler berhasil ke update',
                'Url' => '/admin/ekstrakurikuler'
            ]);
        }
    }

    public function delete($id)
    {
        $data = Ekstrakulikuler::findOrFail($id);
        $data->delete();

        if($data) {
            return redirect()->route('ekstrakurikuler.index')->with('success', 'Data ekstrakurikuler Berhasil Dihapus');
        }else{
            return redirect()->route('ekstrakurikuler.index')->with('error', 'Data ekstrakurikuler Gagal Dihapus');
        }
    }

    public function store(Request $request)
    {
        $data = new Ekstrakulikuler;
        $data->nama = $request->nama;
        $data->keterangan = $request->keterangan;
        $data->save();

        if($data) {
            return redirect()->route('ekstrakurikuler.index')->with('success', 'Data ekstrakurikuler Berhasil Disimpan');
        }else{
            return redirect()->route('ekstrakurikuler.index')->with('error', 'Data ekstrakurikuler Gagal Disimpan');
        }
    }
}
