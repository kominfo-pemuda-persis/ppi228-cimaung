<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\JadwalKegiatanExtra;
use App\Models\JadwalKegiatanIntra;
use App\Models\JamMengajar;
use App\Models\JamMengajarExtra;
use App\Models\NomorUrutAsatidz;
use App\Models\Pelajaran;
// use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class JadwalPelajaranController extends Controller
{
    public function intra()
    {
        $jadwal = JadwalKegiatanIntra::with('asatidz', 'pelajaran', 'jam')
            ->join('tb_jam_mengajar_intra as jam', 'jam.id', '=', 'tb_jadwal_kegiatan_intra.jam_id')
            ->select('tb_jadwal_kegiatan_intra.*')
            ->orderby("tingkat", 'desc')
            ->orderby("kelas")
            ->orderby("hari")
            ->orderby("jam.jam_ke");
            
        if (request('hari')) {
            $jadwal = $jadwal->where('hari', request('hari'));
        }

        if (request('kelas')) {
            $jadwal = $jadwal->where('kelas', request('kelas'));
        }

        if (request('tingkat')) {
            $jadwal = $jadwal->where('tingkat', request('tingkat'));
        }

        $jadwal = $jadwal->paginate(24)->withQueryString();
        return view('admin.data_jadwal_pelajaran.jadwal_pelajaran_intra.index', compact('jadwal'));
    }

    public function showDataAdd()
    {
        $pelajaran = Pelajaran::orderBy('tingkat_sekolah_id', 'asc')->get();
        $jam = JamMengajar::orderBy('jam_ke', 'asc')->get();
        $asatidz = Asatidz::orderBy('id', 'asc')->get();
        return response()->json([
            "data" => [
                'pelajaran' => $pelajaran,
                'jam' => $jam,
                'asatidz' => $asatidz
            ]
        ]);
    }

    public function intraStore(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'hari' => 'required',
            'jam_id' => 'required',
            'kelas_id' => 'required',
            'tingkat_sekolah_id' => 'required'
        ]);
        $jam = JamMengajar::where('id', $req->jam_id)->first();
        if ($jam->kegiatan == 'Istirahat' || $jam->kegiatan == 'Hafalan') {
            $validator->addRules([
                'asatidz_id' => 'nullable',
                'pelajaran_id' => 'nullable'
            ]);
        } else {
            $validator->addRules([
                'asatidz_id' => 'required',
                'pelajaran_id' => 'required'
            ]);
        }

        if ($validator->fails()) {
            dd($validator->errors());
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $pelajaranId = $req->pelajaran_id;

        if ($jam->kegiatan == 'Istirahat') {
            $pelajaranId = null;
            $asatidzId = null;
            $jenis = "A";
        } else if ($jam->kegiatan == 'Hafalan') {
            $jenis = "M";
            $pelajaranId = null;
            $asatidzId = null;
        } else {
            $jenis = "A";
            $asatidzId = $req->asatidz_id;
        }

        $data = [
            'hari' => $req->hari,
            'jam_id' => $req->jam_id,
            'asatidz_id' => $asatidzId,
            'pelajaran_id' => $pelajaranId,
            'tingkat' => $req->tingkat_sekolah_id,
            'kelas' => $req->kelas_id,
            'jenis' => $jenis,
        ];
        JadwalKegiatanIntra::create($data);

        return redirect()->back()->with('success', 'Data berhasil ditambahkan');
    }

    public function showDataEdit($id)
    {
        $jadwal = JadwalKegiatanIntra::with('asatidz', 'pelajaran', 'jam')
            ->join('tb_jam_mengajar_intra as jam', 'jam.id', '=', 'tb_jadwal_kegiatan_intra.jam_id')
            ->where('tb_jadwal_kegiatan_intra.id', $id)
            ->first();
        $pelajaran = Pelajaran::orderBy('tingkat_sekolah_id', 'asc')->get();
        $jam = JamMengajar::orderBy('jam_ke', 'asc')->get();
        $asatidz = Asatidz::orderBy('id', 'asc')->get();
        return response()->json([
            "data" => [
                'jadwal' => $jadwal,
                'pelajaran' => $pelajaran,
                'jam' => $jam,
                'asatidz' => $asatidz
            ]
        ]);
    }

    public function intraUpdate($id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'hari' => 'required',
            'jam_id' => 'required',
            'kelas_id' => 'required',
            'tingkat_sekolah_id' => 'required'
        ]);
        $jam = JamMengajar::where('id', $req->jam_id)->first();

        if ($jam->kegiatan == 'Istirahat' || $jam->kegiatan == 'Hafalan') {
            $validator->addRules([
                'asatidz_id' => 'nullable',
                'pelajaran_id' => 'nullable'
            ]);
        } else {
            $validator->addRules([
                'asatidz_id' => 'required',
                'pelajaran_id' => 'required'
            ]);
        }
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $jenis = "A";
        $pelajaranId = $req->pelajaran_id;
        if ($jam->kegiatan == 'Istirahat') {
            $pelajaranId = null;
            $asatidzId = null;
            $jenis = "A";
        } else if ($jam->kegiatan == 'Hafalan') {
            $jenis = "M";
            $asatidzId = null;
            $pelajaranId = null;
        } else {
            $asatidzId = $req->asatidz_id;
        }
        $data = [
            'hari' => $req->hari,
            'jam_id' => $req->jam_id,
            'asatidz_id' => $asatidzId,
            'pelajaran_id' => $pelajaranId,
            'tingkat' => $req->tingkat_sekolah_id,
            'kelas' => $req->kelas_id,
            'jenis' => $jenis,
        ];
        try {
            $update = JadwalKegiatanIntra::where('id', $id)->update($data);
        } catch (\Throwable $th) {
        }

        return redirect()->back()->with('success', 'Data berhasil diubah');
    }

    public function intraDelete($id)
    {
        JadwalKegiatanIntra::where('id', $id)->delete();
        return redirect()->back()->with('success', 'Data berhasil dihapus');
    }
    public function extra()
    {
        $jadwal = JadwalKegiatanExtra::with('asatidz', 'pelajaran', 'jam')
            ->join('tb_jam_mengajar_extra as jam', 'jam.id', '=', 'tb_jadwal_kegiatan_extra.jam_id')
            ->select('tb_jadwal_kegiatan_extra.*')
            ->orderby("tingkat", 'desc')
            ->orderby("kelas")
            ->orderby("hari")
            ->orderby("jam.jam_ke");
            
        if (request('hari')) {
            $jadwal = $jadwal->where('hari', request('hari'));
        }

        if (request('kelas')) {
            $jadwal = $jadwal->where('kelas', request('kelas'));
        }

        if (request('tingkat')) {
            $jadwal = $jadwal->where('tingkat', request('tingkat'));
        }

        $urutan = NomorUrutAsatidz::orderBy('nomor_urut')->get();

        $jadwal = $jadwal->paginate(12)->withQueryString();
        return view('admin.data_jadwal_pelajaran.jadwal_pelajaran_extra.index', compact('jadwal', 'urutan'));
    }

    public function showDataAddExtra()
    {
        $pelajaran = Pelajaran::orderBy('tingkat_sekolah_id', 'asc')->get();
        $jam = JamMengajarExtra::orderBy('jam_ke', 'asc')->get();
        $asatidz = Asatidz::orderBy('id', 'asc')->get();
        return response()->json([
            "data" => [
                'pelajaran' => $pelajaran,
                'jam' => $jam,
                'asatidz' => $asatidz
            ]
        ]);
    }

    public function extraStore(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'hari' => 'required',
            'jam_id' => 'required',
            'kelas_id' => 'required',
            'tingkat_sekolah_id' => 'required',
            'urutan_id' => 'required'
        ]);
        $jam = JamMengajarExtra::where('id', $req->jam_id)->first();
        if ($jam->kegiatan == 'KBM') {
            $validator->addRules([
                'asatidz_id' => 'required',
                'pelajaran_id' => 'required'
            ]);
        } else {
            $validator->addRules([
                'asatidz_id' => 'nullable',
                'pelajaran_id' => 'nullable'
            ]);
        }

        if ($validator->fails()) {
            dd($validator->errors());
            return redirect()->back()->withErrors($validator->errors());
        }
        
        $pelajaranId = $req->pelajaran_id;

        if ($jam->kegiatan != 'KBM') {
            $pelajaranId = null;
            $asatidzId = null;
        } else {
            $asatidzId = $req->asatidz_id;
        }

        $data = [
            'hari' => $req->hari,
            'jam_id' => $req->jam_id,
            'asatidz_id' => $asatidzId,
            'pelajaran_id' => $pelajaranId,
            'tingkat' => $req->tingkat_sekolah_id,
            'kelas' => $req->kelas_id,
            'urutan_id' => $req->urutan_id
        ];
        JadwalKegiatanExtra::create($data);

        return redirect()->back()->with('success', 'Data berhasil ditambahkan');
    }

    public function showDataEditExtra($id)
    {
        $jadwal = JadwalKegiatanExtra::with('asatidz', 'pelajaran', 'jam')
            ->join('tb_jam_mengajar_extra as jam', 'jam.id', '=', 'tb_jadwal_kegiatan_extra.jam_id')
            ->where('tb_jadwal_kegiatan_extra.id', $id)
            ->first();
        $pelajaran = Pelajaran::orderBy('tingkat_sekolah_id', 'asc')->get();
        $jam = JamMengajarExtra::orderBy('jam_ke', 'asc')->get();
        $asatidz = Asatidz::orderBy('id', 'asc')->get();
        return response()->json([
            "data" => [
                'jadwal' => $jadwal,
                'pelajaran' => $pelajaran,
                'jam' => $jam,
                'asatidz' => $asatidz
            ]
        ]);
    }

    public function ExtraUpdate($id, Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'hari' => 'required',
            'jam_id' => 'required',
            'kelas_id' => 'required',
            'tingkat_sekolah_id' => 'required',
            'urutan_id_edit' => 'required'
        ]);
        $jam = JamMengajarExtra::where('id', $req->jam_id)->first();
        if ($jam->kegiatan == 'KBM') {
            $validator->addRules([
                'asatidz_id' => 'required',
                'pelajaran_id' => 'required'
            ]);
        } else {
            $validator->addRules([
                'asatidz_id' => 'nullable',
                'pelajaran_id' => 'nullable'
            ]);
        }
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $pelajaranId = $req->pelajaran_id;
        if ($jam->kegiatan != 'KBM') {
            $pelajaranId = null;
            $asatidzId = null;
        } else {
            $asatidzId = $req->asatidz_id;
        }
        // if urutan_id_edit is numeric
        if (is_numeric($req->urutan_id_edit)) {
            $urutan = NomorUrutAsatidz::where('id', $req->urutan_id_edit)->first();
            if ($urutan) {
                $urutanId = $urutan->nomor_urut;
            } else {
                $urutanId = $req->urutan_id_edit;
            }
        } else {
            $urutanId = $req->urutan_id_edit;
        }

        $data = [
            'hari' => $req->hari,
            'jam_id' => $req->jam_id,
            'asatidz_id' => $asatidzId,
            'pelajaran_id' => $pelajaranId,
            'tingkat' => $req->tingkat_sekolah_id,
            'kelas' => $req->kelas_id,
            'urutan_id' => $urutanId
        ];

        JadwalKegiatanExtra::where('id', $id)->update($data);

        return redirect()->back()->with('success', 'Data berhasil diubah');
    }

    public function ExtraDelete($id)
    {
        JadwalKegiatanExtra::where('id', $id)->delete();
        return redirect()->back()->with('success', 'Data berhasil dihapus');
    }


    //  export page
    public function exportPage()
    {
        $intra = JadwalKegiatanIntra::join('tb_jam_mengajar_intra as jam', 'jam.id', '=', 'tb_jadwal_kegiatan_intra.jam_id')
            ->join('tb_nomor_urut_asatidz as urutan', 'urutan.asatidz_id', '=', 'tb_jadwal_kegiatan_intra.asatidz_id')
            ->join('tb_pelajaran', 'tb_pelajaran.id', '=', 'tb_jadwal_kegiatan_intra.pelajaran_id')
            ->select('tb_jadwal_kegiatan_intra.*', 'urutan.nomor_urut', 'tb_pelajaran.nama_pelajaran as pelajaran_nama', 'jam.jam_ke', 'jam.kegiatan as jam_kegiatan', 'jam.awal', 'jam.akhir')
            ->orderby("tingkat", 'desc')
            ->orderby("kelas")
            ->orderby("hari")
            ->orderby("jam.jam_ke")->get();
        $extra = JadwalKegiatanExtra::join('tb_jam_mengajar_extra as jam', 'jam.id', '=', 'tb_jadwal_kegiatan_extra.jam_id')
            ->join('tb_pelajaran', 'tb_pelajaran.id', '=', 'tb_jadwal_kegiatan_extra.pelajaran_id')
            ->select('tb_jadwal_kegiatan_extra.*', 'tb_pelajaran.nama_pelajaran as pelajaran_nama', 'jam.jam_ke', 'jam.kegiatan as jam_kegiatan', 'jam.awal', 'jam.akhir')
            ->orderby("tingkat", 'desc')
            ->orderby("kelas")
            ->orderby("hari")
            ->orderby("jam.jam_ke")->get();
        $jamDB = [
            JamMengajar::orderBy('jam_ke', 'asc')->get(),
            JamMengajarExtra::orderBy('jam_ke', 'asc')->get()
        ];
        $urutan = NomorUrutAsatidz::join('tb_asatidz', 'tb_asatidz.id', '=', 'tb_nomor_urut_asatidz.asatidz_id')
            ->select('tb_nomor_urut_asatidz.*', 'tb_asatidz.nama_lengkap')
            ->orderBy('nomor_urut')
            ->get();
        return view('admin.data_jadwal_pelajaran.page_export_jadwal.index', compact('intra', 'extra', 'jamDB', 'urutan'));
    }
}
