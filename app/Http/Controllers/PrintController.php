<?php

namespace App\Http\Controllers;

use App\Models\KelompokPelajaran;
use App\Models\RaporPendidikan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrintController extends Controller
{
    public function printRaporPendidikanTSN($id)
    {
        $rapor = RaporPendidikan::find($id);
        if($rapor){
            $dataRapor = array();

            $kelompok = KelompokPelajaran::all();
            
            foreach($kelompok as $key => $k) {
                $dataRapor[$key]['kelompok'] = $k->nama_kelompok_pelajaran;
    
                $nilai = array();
    
                $pelajaran = DB::table('tb_kelompok_pelajaran as tkp')
                            ->join('tb_pelajaran as tp', 'tkp.id', '=', 'tp.kelompok_id')
                            ->join('tb_asatidz_pelajaran as tap', 'tp.id', '=', 'tap.pelajaran_id')
                            ->join('tb_asatidz as ta', 'ta.id', '=', 'tap.asatidz_id')
                            ->where('tkp.id', $k->id)
                            ->where('tap.kelas_id', $rapor->kelas)
                            ->where('tap.tingkat_sekolah_id', 'TSN')
                            ->orderBy('tp.urutan', 'asc')
                            ->get(['tp.id', 'tp.nama_pelajaran', 'ta.nama_lengkap']);
    
                // dd($pelajaran);
                foreach($pelajaran as $kunci => $plj) {
    
                    $nilaiRaporPendidikan = DB::table('tb_rapor_pendidikan as trp')
                                            ->join('tb_nilai_rapor_pendidikan as tnrp', 'trp.id', '=', 'tnrp.rapor_pendidikan_id')
                                            ->join('tb_santri as ts', 'ts.id', '=', 'trp.santri_id')
                                            ->where('tnrp.pelajaran_id', $plj->id)
                                            ->where('ts.id', $rapor->santri_id)
                                            ->where('trp.tingkat_sekolah_id', 'TSN')
                                            ->first(['tnrp.nilai', 'tnrp.predikat', 'tnrp.deskripsi']);
    
                    $nilai[$kunci]['pelajaran'] = $plj->nama_pelajaran;
                    $nilai[$kunci]['asatidz'] = $plj->nama_lengkap;
                    $nilai[$kunci]['nilai'] = $nilaiRaporPendidikan->nilai ?? 0;
                    $nilai[$kunci]['predikat'] = $nilaiRaporPendidikan->predikat ?? null;
                    $nilai[$kunci]['deskripsi'] = $nilaiRaporPendidikan->deskripsi ?? null;
                }
    
                $dataRapor[$key]['nilai'] = $nilai;
            }
            
            return view('admin.data_rapor.template.printRaporPendidikanTSN', [
                'rapor' => $rapor,
                'mudirTSN' => User::whereRoleIs('mudir_tsn')->first(),
                'dataRapor' => $dataRapor
            ]);
        } else {
            return redirect()->route('dashboard')->with('error', 'Data Rapor Tidak Ditemukan');
        }
    }
}
