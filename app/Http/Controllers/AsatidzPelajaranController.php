<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\AsatidzPelajaran;
use App\Models\Pelajaran;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AsatidzPelajaranController extends Controller
{
    public function index()
    {
        $asatidz = Asatidz::all()->pluck('nama_lengkap', 'id');
        $pelajaran = Pelajaran::all();

        $datas = AsatidzPelajaran::with(['asatidz', 'pelajaran'])->get();
        return view('admin.data_asatidz_pelajaran.index', compact('asatidz', 'pelajaran', 'datas'));
    }

    public function edit($id)
    {
        $data = AsatidzPelajaran::with(['asatidz', 'pelajaran'])->where('id', $id)->first();
        $pelajaran = Pelajaran::all();
        return view('admin.data_asatidz_pelajaran.edit', compact('data', 'pelajaran'));
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $asatidPelajaran = AsatidzPelajaran::where('asatidz_id', $request->old_asatidz)->where('pelajaran_id', $request->pelajaran_id)->where('kelas_id', $request->kelas_id)->where('tingkat_sekolah_id', $request->tingkat_sekolah_id)->first();
            if ($asatidPelajaran) {
                return redirect()->back()->with('error', 'Data Asatidz dan Pelajaran sudah tersedia.');
            }

            $asatidz = AsatidzPelajaran::where('pelajaran_id', $request->pelajaran_id)->where('kelas_id', $request->kelas_id)->where('tingkat_sekolah_id', $request->tingkat_sekolah_id)->first();
            if ($asatidz) {
                return redirect()->back()->with('error', 'Pelajaran ini sudah diampu oleh asatidz lain.');
            }

            $data = AsatidzPelajaran::where('id', $id);
            $data->update([
                'pelajaran_id' => $request->pelajaran_id,
                'kelas_id' => $request->kelas_id,
                'tingkat_sekolah_id' => $request->tingkat_sekolah_id
            ]);
            
            DB::commit();

            return redirect()->route('asatidz-pelajaran.index')->with('success', 'Data Asatidz dan Pelajaran Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('asatidz-pelajaran.index')->with('error', 'Data Asatidz dan Pelajaran Gagal Disimpan. Error ' . $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $asatidPelajaran = AsatidzPelajaran::where('asatidz_id', $request->asatidz_id)->where('pelajaran_id', $request->pelajaran_id)->where('kelas_id', $request->kelas_id)->where('tingkat_sekolah_id', $request->tingkat_sekolah_id)->first();
        if ($asatidPelajaran) {
            return redirect()->back()->with('error', 'Data Asatidz dan Pelajaran sudah tersedia.');
        }
        try {
            DB::beginTransaction();

            $data = new AsatidzPelajaran;
            $data->asatidz_id = $request->asatidz_id;
            $data->pelajaran_id = $request->pelajaran_id;
            $data->kelas_id = $request->kelas_id;
            $data->tingkat_sekolah_id = $request->tingkat_sekolah_id;
            $data->save();

            $asatidz = Asatidz::find($request->asatidz_id);

            $user = User::find($asatidz->user_id);

            if($request-> tingkat_sekolah_id == 'TSN')
                if(!$user->hasRole('asatidz_tsn'))
                    $user->attachRole('asatidz_' . Str::lower($request->tingkat_sekolah_id));

            if($request-> tingkat_sekolah_id == 'MLN')
                if(!$user->hasRole('asatidz_mln'))
                    $user->attachRole('asatidz_' . Str::lower($request->tingkat_sekolah_id));

            DB::commit();

            return redirect()->route('asatidz-pelajaran.index')->with('success', 'Data asatidz & pelajaran Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('asatidz-pelajaran.index')->with('error', 'Data asatidz & pelajaran Gagal Disimpan. Error: ' . $e->getMessage());
        }
    }

    public function delete($id)
    {
        $data = AsatidzPelajaran::findOrFail($id);
        $data->delete();

        if ($data) {
            return redirect()->route('asatidz-pelajaran.index')->with('success', 'Data asatidz & pelajaran Berhasil Dihapus');
        } else {
            return redirect()->route('asatidz-pelajaran.index')->with('error', 'Data asatidz & pelajaran Gagal Dihapus');
        }
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_asatidz_pelajaran.tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_asatidz_pelajaran.asatidz_id';
                break;
            case "1":
                $orderBy = 'tb_asatidz_pelajaran.pelajaran_id';
                break;
            case "2":
                $orderBy = 'tb_asatidz_pelajaran.tingkat_sekolah_id';
                break;
        }

        $data = AsatidzPelajaran::select([
            'tb_asatidz_pelajaran.id as id_ap','tb_asatidz_pelajaran.kelas_id', 'tb_asatidz_pelajaran.tingkat_sekolah_id', 'tb_pelajaran.nama_pelajaran as nama_pelajaran','tb_asatidz.*'
        ])
        ->join('tb_pelajaran', 'tb_asatidz_pelajaran.pelajaran_id', 'tb_pelajaran.id')
        ->join('tb_asatidz', 'tb_asatidz_pelajaran.asatidz_id', 'tb_asatidz.id');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(nama_pelajaran) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_asatidz_pelajaran.tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('kelas') != null) {
            $data = $data->where('kelas_id', $request->kelas);
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tb_asatidz_pelajaran.tingkat_sekolah_id', $request->jenjang);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('asatidz')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }
}