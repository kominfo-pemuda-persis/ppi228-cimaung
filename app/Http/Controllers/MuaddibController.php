<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\Muaddib;
use App\Models\Santri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class MuaddibController extends Controller
{
    public function index()
    {
        $asatidz = Asatidz::all();
        $datas = Muaddib::with('santri', 'asatidz')->orderBy('id', 'desc')->get();
        $santris = Santri::orderBy('tingkat_sekolah_id')->orderBy('kelas')->get();

        $listMuadibSantri = Muaddib::where('asatidz_id', 'LIKE', '%-s%')->select('asatidz_id')->get()->toArray();
        $idMuaddibSantri = [];
        $i = 0;
        foreach ($listMuadibSantri as $l) {
            $idMuaddibSantri[$i] = explode('-', $l['asatidz_id']);
            $idMuaddibSantri[$i] = $idMuaddibSantri[$i][0];
            $i++;
        }
        return view('admin.data_muaddib.index', compact('asatidz', 'datas', 'santris', 'idMuaddibSantri'));
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_muaddib.tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_muaddib.asatidz_id';
                break;
            case "1":
                $orderBy = 'tb_muaddib.asatidz_id';
                break;
            case "2":
                $orderBy = 'tb_muaddib.tingkat_sekolah_id';
                break;
            case "3":
                $orderBy = 'tb_muaddib.no_majmuah';
                break;
        }

        $data = Muaddib::with('asatidz', 'santri')->select([
            'tb_muaddib.*'
        ])
        ->leftjoin('tb_asatidz', 'tb_asatidz.id', 'tb_muaddib.asatidz_id');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(nama_pelajaran) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_asatidz_pelajaran.tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('kelas') != null) {
            $data = $data->where('kelas_id', $request->kelas);
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        $count = Muaddib::where('asatidz_id', $request->asatidz_id)->count();
        if ($count > 0) {
            return redirect()->route('muaddib.index')->with('error', 'Data Muaddib Gagal Disimpan, Muaddib sudah terdaftar');
        }
        $muaddib = $request->asatidz_id;

        $data = [
            'asatidz_id' => $muaddib,
            'kelas_id' => $request->kelas_id,
            'no_majmuah' => $request->no_majmuah,
            'tingkat_sekolah_id' => $request->tingkat_sekolah_id
        ];
        $muaddib = Muaddib::create($data);

        if ($muaddib) {
            return redirect()->route('muaddib.index')->with('success', 'Data Muaddib Berhasil Disimpan');
        } else {
            return redirect()->route('muaddib.index')->with('error', 'Data Muaddib Gagal Disimpan');
        }
    }

    public function edit(Request $request, $id)
    {
        $data = Muaddib::with('asatidz', 'santri')->find($id);
        $santris = Santri::where('tingkat_sekolah_id', $data->tingkat_sekolah_id)->where('muaddib_id', null)->where('kelas', $data->kelas_id)->orWhere('muaddib_id', $id)->get();

        return response()->json([
            'data' => $data,
            'santris' => $santris,
            'success' => 200
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = Muaddib::find($id);

        // jika ada perubahan tingkat sekolah santri akan set null tanpa muaddib
        if ($request->tingkat_sekolah_id != $data->tingkat_sekolah_id || $request->kelas_id != $data->kelas_id) {
            $data_santri = Santri::where('muaddib_id', $id);

            if ($data_santri != null) {
                $data_santri->update([
                    'muaddib_id' => null
                ]);

            }
            $data->update([
                'kelas_id' => $request->kelas_id,
                'no_majmuah' => $request->no_majmuah,
                'tingkat_sekolah_id' => $request->tingkat_sekolah_id,
            ]);
            return response()->json([
                'data' => $data,
                'success' => 'Data Muaddib berhasil ke update',
                'Url' => '/admin/muaddib'
            ]);
        }

        $data->update([
            'kelas_id' => $request->kelas_id,
            'no_majmuah' => $request->no_majmuah,
            'tingkat_sekolah_id' => $request->tingkat_sekolah_id,
        ]);

        if ($request->santri_id == null) {
            $data_santri = Santri::where('muaddib_id', $id);

            if ($data_santri != null) {
                $data_santri->update([
                    'muaddib_id' => null
                ]);
            }
        } elseif ($request->santri_id != null) {
            $before_edit = $request->before_edit;
            $before_edit = explode(',', $before_edit);
            $deleted = array_diff($before_edit, $request->santri_id);

            foreach ($deleted as $delete) {
                $data_santri = Santri::find($delete);
                if ($data_santri != null) {
                    $data_santri->update([
                        'muaddib_id' => null
                    ]);
                }
            }

            foreach ($request->santri_id as $santri) {
                $data_santri = Santri::whereIn('id', explode(',', $santri));

                if ($data_santri != null) {
                    $data_santri->update([
                        'muaddib_id' => $id
                    ]);
                }
            }
        }

        $data->update([
            'kelas_id' => $request->kelas_id,
            'no_majmuah' => $request->no_majmuah,
            'tingkat_sekolah_id' => $request->tingkat_sekolah_id,
        ]);


        return response()->json([
            'data' => $data,
            'success' => 'Data Muaddib berhasil ke update',
            'Url' => '/admin/muaddib'
        ]);
    }

    public function delete($id)
    {
        $data = Muaddib::find($id);$data_santri = Santri::where('muaddib_id', $id);

        if ($data_santri != null) {
            $data_santri->update([
                'muaddib_id' => null
            ]);
        }

        $data->delete();
        if ($data) {
            return redirect()->route('muaddib.index')->with('success', 'Data Muaddib Kelas Berhasil Ke hapus');
        } else {
            return redirect()->route('muaddib.index')->with('error', 'Data Muaddib Kelas Gagal Ke hapus');
        }
    }
    public function getMuaddib()
    {
        $muaddib = Muaddib::with('santri')->orderBy('id', 'desc')->get();
        return DataTables::of($muaddib)
            ->addColumnIndex()
            ->make(true);
    }
}