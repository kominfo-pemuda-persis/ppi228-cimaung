<?php

namespace App\Http\Controllers;

use App\Exports\AsatidzExport;
use App\Exports\TemplateAsatidzExport;
use App\Imports\AsatidzImport;
use App\Models\Asatidz;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Stmt\TryCatch;
use Maatwebsite\Excel\Facades\Excel;

class AsatidzController extends Controller
{
    public function index()
    {
        return view('admin.data_asatidz.index');
    }

    public function add()
    {
        return view('admin.data_asatidz.add');
    }

    public function store(Request $request)
    {
        $request->validate([
            'niat' => 'required|unique:tb_asatidz',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
            'tgl_lahir' => 'required|date',
        ]);

        try {
            DB::beginTransaction();
            $user = User::create([
                'name' => ucwords(strtolower($request->nama_lengkap)),
                'email' => $request->email,
                'username' => $request->username,
                'password' => bcrypt($request->password),
            ]);

            $asatidz = Asatidz::create($request->all());
            
            $user->attachRole('asatidz');

            $asatidz->user_id = $user->id;
            $asatidz->save();

            DB::commit();

            return redirect()->route('asatidz.index')->with('success', 'Data Asatidz Berhasil Tersimpan');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', 'Data Asatidz Gagal Tersimpan, karena ' . $th->getMessage());
        }
    }

    public function detail($id)
    {
        $asatidz = Asatidz::findOrFail($id);
        return view('admin.data_asatidz.detail', compact('asatidz'));
    }

    public function edit($id)
    {
        $asatidz = Asatidz::findOrFail($id);
        return view('admin.data_asatidz.edit', compact('asatidz'));
    }

    public function update(Request $request, $id)
    {
        $data = Asatidz::find($id);
        $request->validate([
            'niat' => 'required|unique:tb_asatidz,niat,'.$data->id,
            'email' => 'nullable|email|unique:users,email,'.$data->user_id.',id',
            'username' => ['required', 'string', 'max:255', 'unique:users,username,'.$data->user_id],
            'password' => ['nullable', 'string', 'min:5', 'confirmed'],
        ]);

        try {
            DB::beginTransaction();            
            $user = User::find($data->user_id);

            if(!$user) {
                User::create([
                    'name' => ucwords(strtolower($request->nama_lengkap)),
                    'email' => $request->email,
                    'username' => $request->username,
                    'password' => $request->password ? bcrypt($request->password) : $user->password,
                ]);
            }
            $user->update([
                'email' => $request->email,
                'username' => $request->username,
                'password' => $request->password ? bcrypt($request->password) : $user->password,
            ]);

            $data->update([
                'nama_lengkap' => ucwords(strtolower(($request->nama_lengkap))),
                'niat' => $request->niat,
                'jenis_kelamin' => $request->jenis_kelamin,
                'status_menikah' => $request->status_menikah,
                'tsn' => $request->tsn,
                'mln' => $request->mln,
                's1' => $request->s1,
                's2' => $request->s2,
                's3' => $request->s3,
                'pendidikan_terakhir' => $request->pendidikan_terakhir,
                'no_telp' => $request->no_telp,
                'alamat' => ucwords(strtolower($request->alamat)),
                'tempat_lahir' => ucwords(strtolower($request->tempat_lahir)),
                'tgl_lahir' => $request->tgl_lahir,
                'hafalan' => $request->hafalan,
                'user_id'       => $user->id
            ]);

            DB::commit();

            return redirect()->route('asatidz.index')->with('success', 'Data Asatidz Berhasil Ter update');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('asatidz.index')->with('error', 'Data Asatidz Gagal ter update, karena ' . $e->getMessage());
        }
    }

    public function delete($id)
    {
        $data = Asatidz::where('id', $id);

        $data->delete();

        if ($data) {
            return redirect()->route('asatidz.index')->with('success', 'Data Asatidz Berhasil Dihapus');
        } else {
            return redirect()->route('asatidz.index')->with('error', 'Data Asatidz Gagal Dihapus');
        }
    }

    public function formAssignUser($id)
    {
        try {
            $asatidz = Asatidz::findOrFail($id);
            return view('admin.data_asatidz.formAssignUser', [
                'asatidz' => $asatidz,
                'users' => User::all()
            ]);
        } catch (Exception $e) {
            return redirect()->route('asatidz.index')->with('error', 'Error' . $e);
        }
    }

    public function assignUser(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $asatidz = Asatidz::findOrFail($id);
            $asatidz->user_id = $request->user;
            $asatidz->save();

            DB::commit();

            return redirect()->route('asatidz.index')->with('success', 'Data User Asatidz Berhasil Ditambahkan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error' . $e);
        }
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_asatidz.nama_lengkap';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_asatidz.niat';
                break;
            case "1":
                $orderBy = 'tb_asatidz.nama_lengkap';
                break;
            case "2":
                $orderBy = 'tb_asatidz.no_telp';
                break;
            case "3":
                $orderBy = 'tb_asatidz.pendidikan_terakhir';
                break;
        }

        $data = Asatidz::select([
            'tb_asatidz.*'
        ]);

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(tb_asatidz.niat) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_asatidz.nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_asatidz.no_telp) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_asatidz.pendidikan_terakhir) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('user')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function export()
    {
        return Excel::download(new AsatidzExport, 'Asatidz.xlsx');
    }

    public function export_aktif(Request $req)
    {
        $ids = $req->ids;
        $ids = explode(',', $ids);
        return Excel::download(new AsatidzExport($ids), 'asatidz-aktif.xlsx');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        Excel::import(new AsatidzImport, $request->file('file'));

        return redirect()->back()->with('success', 'Data Asatidz Berhasil di Import');

    }

    public function download(Request $request)
    {
        $url = $request->url;
        $filename = $request->filename;
        $file = Storage::disk('s3')->get($url);
        return response()->streamDownload(function () use ($file) {
            echo $file;
        }, $filename);
    }
}