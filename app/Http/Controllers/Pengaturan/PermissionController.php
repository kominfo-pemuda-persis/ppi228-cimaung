<?php

namespace App\Http\Controllers\Pengaturan;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Permission;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->hasPermission('menu_permission-read')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data permission');
        } else {
            return view('admin.pengaturan.permission.index', [
                'datas' => Permission::where('name', 'like', '%' . request()->search . '%')->paginate(10)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasPermission('menu_permission-create')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data permission');
        } else {
            return view('admin.pengaturan.permission.create', [
                'menus' => Menu::get()
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'display_name' => ['required', 'string', 'max:255'],
        ]);

        try {
            DB::beginTransaction();

            Permission::create([
                'name' => str_replace(" ", "-", strtolower($request->display_name)),
                'display_name' => $request->display_name,
                'description' => $request->desrciption,
                'menu_id' => $request->menu
            ]);

            DB::commit();

            return redirect()->route('permission.index')->with('success', 'Created Permission is Successfully');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->hasPermission('menu_permission-update')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data permission');
        } else {
            try {
                $permission = Permission::findOrFail($id);
                return view('admin.pengaturan.permission.edit', [
                    'permission' => $permission,
                    'menus' => Menu::all()
                ]);
            } catch (Exception $e) {
                return redirect()->route('permission.index')->with('error', 'Error ' . $e);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'display_name' => ['required', 'string', 'max:255'],
        ]);

        try {
            DB::beginTransaction();

            $permission = Permission::findOrFail($id);
            $permission->name = str_replace(" ", "-", strtolower($request->display_name));
            $permission->display_name = $request->display_name;
            $permission->description = $request->desrciption;
            $permission->menu_id = $request->menu;
            $permission->save();

            DB::commit();

            return redirect()->route('permission.index')->with('success', 'Updated Permission is Successfully');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $permission = Permission::findOrFail($id);
            $permission->delete();
            DB::commit();
            return redirect()->route('permission.index')->with('success', 'Deleted Permission is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }
}
