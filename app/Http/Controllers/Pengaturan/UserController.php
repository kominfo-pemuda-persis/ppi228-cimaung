<?php

namespace App\Http\Controllers\Pengaturan;

use App\Http\Controllers\Controller;
use App\Models\Asatidz;
use App\Models\Orangtua;
use App\Models\Role;
use App\Models\Santri;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->hasPermission('menu_user-read')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data user');
        } 

        return view('admin.pengaturan.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        if(!Auth::user()->hasPermission('menu_user-create')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data user');
        } else {
            $datas = User::all();
            return view('admin.pengaturan.user.create', [
                'roles' => Role::all()
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);
        
        $validate['password'] = bcrypt($validate['password']);

        try {
            DB::beginTransaction();

            $user = User::create($validate);

            $user->attachRoles($request->role);

            DB::commit();

            return redirect()->route('users.index')->with('success', 'Data User Berhasil Tersimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('users.index')->with('error', 'Data User Gagal Tersimpan. error: ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        $data = array();
        foreach($user->roles as $role) {
            $data = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'role' => $role->name
            ];
        }

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $user = User::findOrFail($id);
            if(!Auth::user()->hasPermission('menu_user-update')) {
                return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data user');
            } else {
                $datas = User::all();
                return view('admin.pengaturan.user.edit', [
                    'roles' => Role::all(),
                    'user' => $user
                ]);
            }
        } catch (Exception $e) {
            return redirect()->route('users.index')->with('error', 'Maaf, Data Tidak Ditemukan');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->checkUser($id);

        $validate = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:users,username,' . $user->id],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $user->id],
            'password' => ['nullable', 'string', 'min:5', 'confirmed'],
        ]);
        
        if($request->password) {
            $validate['password'] = bcrypt($validate['password']);
        } else {
            $validate['password'] = $user->password;
        }

        try {
            DB::beginTransaction();

            $user->update($validate);

            $user->syncRoles($request->role);

            DB::commit();

            return redirect()->route('users.index')->with('success', 'Data User Berhasil Tersimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('users.index')->with('error', 'Data User Gagal Tersimpan. error: ' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            
            $user = User::findOrFail($id);

            if($user->hasRole('asatidz')) 
                Asatidz::where('user_id', $user->id)->delete();

            if($user->hasRole(['santri_tsn', 'santri_mln'])) {
                $santri = Santri::where('user_id', $user->id)->first();

                if($santri->orangtua_id != null) {

                    $jumlahSantriOrangtua = Santri::where('orangtua_id', $santri->orangtua_id)->get();
    
                    if($jumlahSantriOrangtua->count() == 1) {
                        $orangtua = Orangtua::where('id', $santri->orangtua_id)->first();
                        $userOrangtua = User::where('id', $orangtua->user_id)->first();

                        foreach($userOrangtua->roles as $roleOrangtua) {
                            $userOrangtua->detachRole($roleOrangtua->name);
                        }

                        $orangtua->delete();
                        $userOrangtua->delete();
                    }

                }

                $santri->delete();
            }

            if($user->hasRole(['orangtua_tsn', 'orangtua_mln'])) {
                $orangtua = Orangtua::where('user_id', $user->id)->first();
                $santri = Santri::where('orangtua_id', $orangtua->id)->get();
                foreach($santri as $s) {
                    Santri::where('id', $s->id)->delete();
                }
                $orangtua->delete();
            }

            foreach($user->roles as $role) {
                $user->detachRole($role->name);
            }

            $user->delete();

            DB::commit();

            // return response()->json([
            //     'success' => 'data user berhasil terhapus',
            //     'Url' => '/admin/pengaturan/users'
            // ]);
            return redirect()->route('users.index')->with('success', 'Deleted User is Successfully');
        } catch (Exception $e) {
            DB::rollBack();

            // return response()->json([
            //     'error' => $e->getMessage()
            // ]);
            return redirect()->route('users.index')->with('error', $e->getMessage());
        }
    }

    private function checkUser($id)
    {
        try {
            $user = User::findOrFail($id);
            return $user;
        } catch (\Throwable $th) {
            return redirect()->route('users.index')->with('error', 'Data User Tidak Ditemukan');
        }
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'users.name';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'users.name';
                break;
            case "1":
                $orderBy = 'users.email';
                break;
            case "2":
                $orderBy = 'roles.display_name';
                break;
        }

        $data = User::select([
            'users.id', 
            'users.name', 
            'users.email', 
            DB::raw('GROUP_CONCAT(roles.display_name) as display_names')    
        ])
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->groupBy('users.id', 'users.name', 'users.email');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(users.name) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(users.email) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(roles.display_name) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('roles') != null) {
            $data = $data->where('roles.name', $request->roles);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function refreshDataDuplicate()
    {
        $orangtuaIdSantriPertama = null;
        //ambil data duplikat
        $duplicateSantri = Santri::
            select('nik', DB::raw('count(nik) as jumlah'))
            ->whereNotNull('orangtua_id')
            ->groupBy('nik')
            ->having('jumlah', '>', 1)
            ->get();
        dd($duplicateSantri);

        //cek apa ada data duplikat
        if($duplicateSantri) {
            $array = [];
            foreach($duplicateSantri as $santri) {
                //ambil data id orangtua
                // $idOrangtuaSantri = Santri::where('nik', $santri->nik)->orderBy('created_at', 'asc')->offset(1)->limit(10)->get('orangtua_id');
                $idUserDanIdOrangtua = DB::table('tb_santri as ts')
                    ->select(['to.id as orangtua_id', 'u.id as user_id', 'ts.id as santri_id'])
                    ->join('tb_orangtua as to', 'to.id', '=', 'ts.orangtua_id')
                    ->join('users as u', 'u.id', '=', 'to.user_id')
                    ->where('ts.nik', $santri->nik)
                    ->orderBy('ts.created_at', 'asc')
                    ->get();
                $array[] = $idUserDanIdOrangtua;
                
                // foreach($idUserDanIdOrangtua as $key => $data) {
                //     if($key == 0) {
                //         $orangtuaIdSantriPertama = $data->orangtua_id;
                //     } else {
                //         $this->deleteUserOrangtua($data->orangtua_id, $data->user_id, $data->santri_id, $orangtuaIdSantriPertama);
                //     }
                // }
            }
            dd($array);
        }

        return redirect()->route('users.index')->with('success', 'Refresh Data Duplikat Berhasil');
    }

    private function deleteUserOrangtua($orangtua_id, $user_id, $santri_id, $orangtuaIdSantriPertama)
    {
        try {
            DB::beginTransaction();
            //ambil data user
            // $user = User::find($user_id);
    
            //hapus role
            // foreach($user->roles as $role) {
            //     $user->detachRole($role->name);
            // }
    
            // $user->delete();
    
            // Orangtua::where('id', $orangtua_id)->delete();

            // Santri::where('id', $santri_id)->update(['orangtua_id' => $orangtuaIdSantriPertama]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('users.index')->with('error', $e->getMessage());
        }
    }
}
