<?php

namespace App\Http\Controllers\Pengaturan;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Permission;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Stringable;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->hasPermission('menu_menu-read')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data menu');
        } else {
            return view('admin.pengaturan.menu.index', [
                'datas' => Menu::where('name', 'like', '%' . request()->search . '%')->paginate(10)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasPermission('menu_menu-create')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data menu');
        } else {
            return view('admin.pengaturan.menu.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);

        try {
            DB::beginTransaction();

            Menu::create([
                'name' => $request->name,
                'description' => $request->desrciption
            ]);

            DB::commit();

            return redirect()->route('menu.index')->with('success', 'Created Menu is Successfully');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->hasPermission('menu_menu-update')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data menu');
        } else {
            try {
                $menu = Menu::findOrFail($id);
                return view('admin.pengaturan.menu.edit', ['menu' => $menu]);
            } catch (Exception $e) {
                return redirect()->route('menu.index')->with('error', 'Error ' . $e);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);

        try {
            DB::beginTransaction();

            $menu = Menu::findOrFail($id);
            $menu->name = $request->name;
            $menu->description = $request->desrciption;
            $menu->save();

            DB::commit();

            return redirect()->route('menu.index')->with('success', 'Updated Menu is Successfully');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            
            $menu = Menu::findOrFail($id);
            $menu->delete();

            DB::commit();

            return redirect()->route('menu.index')->with('success', 'Deleted Menu is Successfully');
            
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }
}
