<?php

namespace App\Http\Controllers\Pengaturan;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Role;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->hasPermission('menu_role-read')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data role');
        } else {
            return view('admin.pengaturan.role.index', [
                'datas' => Role::where('name', 'like', '%' . request()->search . '%')->paginate(10)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasPermission('menu_role-create')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data role');
        } else {
            return view('admin.pengaturan.role.create', [
                'menus' => Menu::get()
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'display_name' => ['required', 'string', 'max:255'],
            'permissions_id' => ['required'],
        ]);

        try {
            DB::beginTransaction();

            $role = Role::create([
                'name' => str_replace(" ", "_", strtolower($request->display_name)),
                'display_name' => $request->display_name,
                'description' => $request->description,
            ]);

            $role->attachPermissions($request->permissions_id);

            
            DB::commit();

            return redirect()->route('role.index')->with('success', 'Created Role is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->hasPermission('menu_role-update')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data role');
        } else {
            try {
                $role = Role::findOrFail($id);
                $rolePermissions = $role->permissions()->get()->pluck('id')->toArray();
                return view('admin.pengaturan.role.edit', [
                    'menus' => Menu::get(),
                    'role' => $role,
                    'rolePermissions' => $rolePermissions
                ]);
            } catch (Exception $e) {
                return redirect()->back()->with('error', 'Error ' . $e);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'display_name' => ['required', 'string', 'max:255'],
            'permissions_id' => ['required'],
        ]);

        try {
            DB::beginTransaction();

            $role = Role::findOrFail($id);
            $role->name = str_replace(" ", "_", strtolower($request->display_name));
            $role->display_name = $request->display_name;
            $role->description = $request->description;
            $role->save();

            $role->syncPermissions($request->permissions_id);

            
            DB::commit();

            return redirect()->route('role.index')->with('success', 'Updated Role is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

                $role = Role::findOrFail($id);
                $role->detachPermissions($role->permissions()->get()->pluck('id')->toArray());
                $role->delete();

            DB::commit();

            return redirect()->route('role.index')->with('success', 'Deleted Role is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }
}
