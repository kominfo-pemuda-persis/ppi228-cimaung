<?php

namespace App\Http\Controllers;

use App\Models\JamMengajar;
use App\Models\JamMengajarExtra;
use Illuminate\Http\Request;

class JamMengajarController extends Controller
{
    public function intra()
    {
        $jam = JamMengajar::orderby('jam_ke')->get();
        return view('admin.data_jadwal_pelajaran.jadwal_pelajaran_intra.jam_mengajar_intra', compact('jam'));
    }

    public function extra()
    {
        $jam = JamMengajarExtra::orderby('jam_ke')->get();
        return view('admin.data_jadwal_pelajaran.jadwal_pelajaran_extra.jam_mengajar_extra', compact('jam'));
    }

    public function store(Request $req)
    {
        $validator = \Validator::make($req->all(), [
            'jam_ke' => 'required',
            'awal' => 'required',
            'akhir' => 'required',
            'kegiatan' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($req->all);
        }
        if ($req->extra) {
            JamMengajarExtra::create([
                'jam_ke' => $req->jam_ke,
                'awal' => $req->awal,
                'akhir' => $req->akhir,
                'kegiatan' => $req->kegiatan
            ]);
        } else {
            JamMengajar::create([
                'jam_ke' => $req->jam_ke,
                'awal' => $req->awal,
                'akhir' => $req->akhir,
                'kegiatan' => $req->kegiatan
            ]);
        }

        return redirect()->back()->with('success', 'Data berhasil ditambahkan');
    }

    public function update($id)
    {
        $validator = \Validator::make(request()->all(), [
            'jam_ke' => 'required',
            'awal' => 'required',
            'akhir' => 'required',
            'kegiatan' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($req->all);
        }
        if (request('extra')) {
            $jam = JamMengajarExtra::find($id);
        } else {
            $jam = JamMengajar::find($id);
        }
        $jam->jam_ke = request('jam_ke');
        $jam->awal = request('awal');
        $jam->akhir = request('akhir');
        $jam->kegiatan = request('kegiatan');
        $jam->save();

        return redirect()->back()->with('success', 'Data berhasil diubah');
    }

    public function destroy($id)
    {
        $jam = JamMengajar::find($id);
        $jam->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data berhasil dihapus'
        ]);
    }

    public function destroyExtra($id)
    {
        $jam = JamMengajarExtra::find($id);
        $jam->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data berhasil dihapus'
        ]);
    }
}
