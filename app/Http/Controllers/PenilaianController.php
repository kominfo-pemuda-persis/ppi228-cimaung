<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\AsatidzPelajaran;
use App\Models\NilaiRaporPendidikan;
use App\Models\Pelajaran;
use App\Models\RaporPendidikan;
use App\Models\Santri;
use App\Models\Semester;
use App\Models\WaliKelas;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PenilaianController extends Controller
{
    public function index()
    {
        $rapor = false;
        $nilaiRaporPendidikan = null; 
        $kb = null;
        $status = null;

        //jika ada semua request param
        if(request('pelajaran') && request('kelas') && request('tingkat') && request('semester')) {

            //ambil data rapor pendidikan
            $raporPendidikan = RaporPendidikan::where('semester_id', request('semester'))
                                                ->where('kelas', request('kelas'))
                                                ->where('tingkat_sekolah_id', request('tingkat'))
                                                ->get(['id', 'status']);
            $raporPendidikanId = [];                            
            foreach($raporPendidikan as $rp) {
                $raporPendidikanId[] = $rp->id;
                $status = $rp->status;
            }

            //ambil data asatidz pelajaran
            $asatidzPelajaran = AsatidzPelajaran::where('pelajaran_id', request('pelajaran'))
                                                ->where('kelas_id', request('kelas'))
                                                ->where('tingkat_sekolah_id', request('tingkat'))
                                                ->first();
            //jika tidak ada asatidz pelajaran
            if(!$asatidzPelajaran)
                return redirect()->back()->with('error', 'Belum ada yang mengajar atau belum ada pelajaran di kelas yang antum pilih');

            //jika rapor pendidikan dan pelajaran ada yang menagajar
            if(count($raporPendidikan) > 0 && $asatidzPelajaran) {
                $rapor = true;
                //ambil data nilai rapor pendidikan
                $nilaiRaporPendidikan = NilaiRaporPendidikan::where('pelajaran_id', request('pelajaran'))
                                                    ->whereIn('rapor_pendidikan_id', $raporPendidikanId)
                                                    ->get();
                $kb = $nilaiRaporPendidikan[0]['kb'];
            }

            if(Auth::user()->hasRole(['asatidz_tsn', 'asatidz_mln'])) {
                $asatidz = Asatidz::where('user_id', Auth::user()->id)->first();
                //jika asatidz bukan pengajar pelajaran ini
                if($asatidz->id != $asatidzPelajaran->asatidz_id)
                    return redirect()->route('penilaian.index')->with('error', 'Maaf antum bukan pengajar pelajaran ini');
            }
        }

        if(Auth::user()->hasRole(['asatidz_tsn', 'asatidz_mln'])) {
            
            $pelajaran = Asatidz::join('users', 'users.id', '=', 'tb_asatidz.user_id')
                            ->join('tb_asatidz_pelajaran', 'tb_asatidz_pelajaran.asatidz_id', '=', 'tb_asatidz.id')
                            ->join('tb_pelajaran', 'tb_pelajaran.id', '=', 'tb_asatidz_pelajaran.pelajaran_id')
                            ->where('users.id', Auth::user()->id)
                            ->select('tb_pelajaran.id', 'tb_pelajaran.nama_pelajaran')
                            ->get();
        } else {
            $pelajaran = Pelajaran::all();
        }

        return view('admin.data_penilaian.index', [
            'dataPelajaran' => $pelajaran,
            'dataSemester' => Semester::where('status', 'aktif')->get(),
            'rapor' => $rapor,
            'nilaiRaporPendidikan' => $nilaiRaporPendidikan,
            'kb' => $kb,
            'status' => $status
        ]);
    }

    public function createRaporNilai(Request $request)
    {
        //Ambil data asatidz pelajaran
        $asatidzPelajaran = AsatidzPelajaran::where('pelajaran_id', $request->pelajaran)
                                            ->where('kelas_id', $request->kelas)
                                            ->where('tingkat_sekolah_id', $request->tingkat)
                                            ->first();
        
        if(!$asatidzPelajaran)
            return redirect()->back()->with('error', 'Belum ada yang mengajar atau belum ada pelajaran di kelas yang antum pilih');

        //ambil data santri
        $dataSantri = Santri::where('kelas', $request->kelas)
                            ->where('tingkat_sekolah_id', $request->tingkat)
                            ->where('status_active', 'active')
                            ->get();

        //looping data santri
        foreach($dataSantri as $santri) {
            //ambil data rapor pendidikan
            $raporSantri = RaporPendidikan::where('santri_id', $santri->id)
                                        ->where('semester_id', $request->semester)
                                        ->where('kelas', $request->kelas)
                                        ->where('tingkat_sekolah_id', $request->tingkat)->first();

            //ambil data walikelas
            $walikelas = WaliKelas::where('kelas_id', $request->kelas)->where('tingkat_sekolah_id', $request->tingkat)->first();

            //jika rapor pendidikan santri tidak ada
            if(!$raporSantri) {
                try {
                    DB::beginTransaction();

                    //tambah data rapor pendidikan
                    $rapor = RaporPendidikan::create([
                        'santri_id' => $santri->id,
                        'semester_id' => $request->semester,
                        'kelas' => $request->kelas,
                        'tingkat_sekolah_id' => $request->tingkat,
                        'orangtua_id' => $santri->orangtua_id,
                        'walikelas_id' => $walikelas->asatidz_id,
                    ]);

                    //tambah data nilai rapor pendidikan
                    NilaiRaporPendidikan::create([
                        'rapor_pendidikan_id' => $rapor->id,
                        'pelajaran_id' => $request->pelajaran,
                        'asatidz_id' => $asatidzPelajaran->asatidz_id,
                    ]);

                    DB::commit();
                } catch (Exception $e) {
                    // dd($e->getMessage());
                    DB::rollBack();
                }
            //jika ada data rapor pendidikan santri
            } else {

                //ambil nilai rapor pendidikan santri
                $nilaiRapor = NilaiRaporPendidikan::where('rapor_pendidikan_id', $raporSantri->id)
                                                ->where('pelajaran_id', $request->pelajaran)
                                                ->where('asatidz_id', $asatidzPelajaran->asatidz_id)
                                                ->first();

                //jika tidak ada nilai rapor pendidikan santri
                if(!$nilaiRapor) {

                    //tambah data nilai rapor pendidikan santri
                    NilaiRaporPendidikan::create([
                        'rapor_pendidikan_id' => $raporSantri->id,
                        'pelajaran_id' => $request->pelajaran,
                        'asatidz_id' => $asatidzPelajaran->asatidz_id,
                    ]);
                }

            }
        }

        return redirect('admin/penilaian?pelajaran=' . $request->pelajaran . 
                        '&kelas=' . $request->kelas . 
                        '&tingkat=' . $request->tingkat . 
                        '&semester=' . $request->semester);
    }

    public function updatePenilaian(Request $request)
    {
        for($i = 0; $i < count($request->id); $i++) {
            $nilai = NilaiRaporPendidikan::find($request->id[$i]);
            $nilai->kb = $request->kb;
            $nilai->nilai = $request->nilai[$i];

            $predikat = 'D';
            if($request->nilai[$i] >= 60 && $request->nilai[$i] <= 69)
                $predikat = 'C';
            
            if($request->nilai[$i] >= 70 && $request->nilai[$i] <= 79)
                $predikat = 'B';
            
            if($request->nilai[$i] >= 80 && $request->nilai[$i] <= 89)
                $predikat = 'A';

            if($request->nilai[$i] >= 90 && $request->nilai[$i] <= 100)
                $predikat = 'A+';

            $nilai->predikat = $predikat;
            $nilai->deskripsi = $request->deskripsi[$i];
            $nilai->save();
        }

        return redirect()->back()->with('success','Nilai Berhasil Disimpan');
    }
}
