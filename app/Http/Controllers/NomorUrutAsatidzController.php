<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\NomorUrutAsatidz;
use Illuminate\Http\Request;

class NomorUrutAsatidzController extends Controller
{
    public function index()
    {
        $asatidz = Asatidz::get();
        $nomor_urut = NomorUrutAsatidz::with('asatidz')->orderby('nomor_urut')->get();
        return view('admin.data_jadwal_pelajaran.nomor_urut_asatidz.index', compact('nomor_urut', 'asatidz'));
    }

    public function store() 
    {
        $validator = \Validator::make(request()->all(), [
            'asatidz_id' => 'required',
            'nomor_urut' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $data['asatidz_id'] = request('asatidz_id');
        $data['nomor_urut'] = request('nomor_urut');

        NomorUrutAsatidz::create($data);
        return redirect()->back()->with('success', 'Data berhasil ditambahkan');
    }

    public function update($id)
    {
        $validator = \Validator::make(request()->all(), [
            'asatidz_id_edit' => 'required',
            'nomor_urut_edit' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $data['asatidz_id'] = request('asatidz_id_edit');
        $data['nomor_urut'] = request('nomor_urut_edit');

        NomorUrutAsatidz::where('id', $id)->update($data);
        return redirect()->back()->with('success', 'Data berhasil diubah');
    }

    public function destroy($id)
    {
        NomorUrutAsatidz::where('id', $id)->delete();
        return redirect()->back()->with('success', 'Data berhasil dihapus');
    }
}
