<?php

namespace App\Http\Controllers\DataPSB;

use App\Http\Controllers\Controller;
use App\Models\Santri;
use Illuminate\Http\Request;

class DataGagalTestController extends Controller
{
    public function index()
    {
        return view('admin.data_psb.data_gagal_test.index', [
            'santri' => Santri::where('status_active', 'rejected')->get()
        ]);
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_santri.tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_santri.id';
                break;
            case "1":
                $orderBy = 'tb_santri.tingkat_sekolah_id';
                break;
            case "2":
                $orderBy = 'tb_santri.tanggal_lahir';
                break;
            case "3":
                $orderBy = 'tb_santri.jk';
                break;
            case "4":
                $orderBy = 'tb_santri.id';
                break;
            case "5":
                $orderBy = 'tb_santri.id';
                break;
        }

        $data = Santri::select('nama_lengkap', 'tingkat_sekolah_id', 'tanggal_lahir','tempat_lahir', 'alamat', 'jk', 'start_year', 'id')
                ->where('status_active', 'rejected');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
        $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }
}
