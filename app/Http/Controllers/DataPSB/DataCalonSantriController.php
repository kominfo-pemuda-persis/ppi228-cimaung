<?php

namespace App\Http\Controllers\DataPSB;

use App\Http\Controllers\Controller;
use App\Models\Counter;
use App\Models\Juz;
use App\Models\Orangtua;
use App\Models\Santri;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DataCalonSantriController extends Controller
{
    public function index()
    {
        return view('admin.data_psb.data_calon_santri.index', [
            'santri' => Santri::where('status_pembayaran', 'approved')
                            ->where('status_active', 'non active')->get()
        ]);
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_santri.tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_santri.id';
                break;
            case "1":
                $orderBy = 'tb_santri.nama_lengkap';
                break;
            case "2":
                $orderBy = 'tb_santri.tingkat_sekolah_id';
                break;
            case "3":
                $orderBy = 'tb_santri.tanggal_lahir';
                break;
            case "4":
                $orderBy = 'tb_santri.jk';
                break;
            case "5":
                $orderBy = 'tb_santri.status_pembayaran';
                break;
                
        }

        $data = Santri::select('nama_lengkap', 'tingkat_sekolah_id', 'tanggal_lahir','tempat_lahir', 'alamat', 'jk','id')
            ->where('status_pembayaran', 'approved')->where('status_active', 'non active');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                ->orWhereRaw('LOWER(kode_daftar) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                ->orWhereRaw('LOWER(tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
        $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function lolos(Request $request, $id)
    {
        try {
            DB::beginTransaction();  

            $santri = Santri::findOrFail($id);
            // $faker = Faker::create('id_ID');

            // $nis = [
            //     'nis' => $faker->randomNumber(3, true).'.'.$faker->randomNumber(3, true).'.'.$faker->randomNumber(3, true).'.'.$faker->randomNumber(3, true).'.'.$faker->randomNumber(3, true)
            // ];

            // $userOrangTua = User::create([
            //     'name' => $santri->nama_lengkap_ayah,
            //     'username' => $nis['nis'].'.ortu',
            //     'password' => bcrypt('12345')
            // ]);
            
            // Orangtua::create([
            //     'nama' =>  $santri->nama_lengkap_ayah,
            //     'alamat' => $santri->alamat,
            //     'hp' => $santri->no_hp,
            //     'user_id' => $userOrangTua->id
            // ]);

            // if($santri->tingkat_sekolah_id == 'TSN') {
            //     $userOrangTua->attachRole('orangtua_tsn');
            // } else {
            //     $userOrangTua->attachRole('orangtua_mln');
            // }
            
            //GENERATE NISM
            $counterNISM = Counter::where('name', 'NISM')->where('level', $santri->tingkat_sekolah_id)->where('start_year', $santri->start_year)->first();
            if(!$counterNISM) {
                $counterNISM = Counter::create([
                    'name' => 'NISM',
                    'level' => $santri->tingkat_sekolah_id,
                    'office_code' => '500032730094',
                    'start_year' => $santri->start_year,
                    'code' => $santri->tingkat_sekolah_id == 'TSN' ? '01':'02',
                    'counter' => 1,
                    'digit_total' => 3
                ]);
            }
            $startYear = substr($counterNISM->start_year, -3);
            $digitNISM = null;
            for ($i=0; $i < $counterNISM->digit_total - strlen($counterNISM->counter); $i++) { 
                $digitNISM = $digitNISM . '0';
            }
            $nism = $counterNISM->office_code . '-' . $startYear . '-' . $counterNISM->code . '-' . $digitNISM . $counterNISM->counter;
            
            //GENERATE NIS
            $counterNIS = Counter::where('name', 'NIS')->where('level', $santri->tingkat_sekolah_id)->where('start_year', $santri->start_year)->first();
            if(!$counterNIS) {
                $counterNIS = Counter::create([
                    'name' => 'NIS',
                    'level' => $santri->tingkat_sekolah_id,
                    'office_code' => '27',
                    'start_year' => $santri->start_year,
                    'code' => $santri->tingkat_sekolah_id == 'TSN' ? '01':'02',
                    'counter' => 1,
                    'digit_total' => 3
                ]);
            }
            $startYearNIS = substr($counterNISM->start_year, -2);
            $digitNIS = null;
            for ($i=0; $i < $counterNIS->digit_total - strlen($counterNIS->counter); $i++) { 
                $digitNIS = $digitNIS . '0';
            }
            $nis = $counterNIS->office_code . '-' . $startYearNIS . $startYearNIS + 1 . '-' . $counterNIS->code . '-' . $digitNIS . $counterNIS->counter;

            //cek kesamaan orangtua
            $santriOrangtua = Santri::where('nomor_kartu_keluarga', $santri->nomor_kartu_keluarga)->where('id', '!=', $santri->id)->first();
            if($santriOrangtua) {
                $santri->orangtua_id = $santriOrangtua->orangtua_id;
            } else {
                //create user orangtua
                $userOrangTua = User::create([
                    'name' => $santri->nama_lengkap_ayah,
                    'username' => $nis .'.ortu',
                    'password' => bcrypt('12345')
                ]);
    
                //create role orangtua
                if($santri->tingkat_sekolah_id == 'TSN') {
                    $userOrangTua->attachRole('orangtua_tsn');
                } else {
                    $userOrangTua->attachRole('orangtua_mln');
                }
                
                //create orangtua
                $orangTua = Orangtua::create([
                    'nama' =>  $santri->nama_lengkap_ayah,
                    'alamat' => $santri->alamat,
                    'hp' => $santri->no_hp,
                    'user_id' => $userOrangTua->id
                ]);

                $santri->orangtua_id = $orangTua->id;

            }


            $userSantri = User::create([
                'name' => $santri->nama_lengkap,
                'username' => $nis,
                'password' => bcrypt('12345')
            ]);

            if($santri->tingkat_sekolah_id == 'TSN') {
                $userSantri->attachRole('santri_tsn');
            } else {
                $userSantri->attachRole('santri_mln');
            }

            $santri->user_id = $userSantri->id;
            $santri->nism = $nism;
            $santri->nis = $nis;
            $santri->status_active = 'active';
            $santri->updated_by = auth()->user()->id;
            $santri->save();

            $counterNISM->counter++;
            $counterNIS->counter++;

            $counterNISM->save();
            $counterNIS->save();

            DB::commit();

            return redirect()->route('data.lolos.test')->with('success', 'Santri ' . $santri->nama_lengkap . ' Lolos Test');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function gagal(Request $request, $id)
    {
        try {
            
            DB::beginTransaction();
            
            $santri = Santri::findOrFail($id);
            $santri->status_active = 'rejected';
            $santri->created_at = now();
            $santri->save();

            DB::commit();

            return redirect()->route('data.gagal.test')->with('success', 'Santri ' . $santri->nama_lengkap . ' Gagal Test');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function edit($id)
    {
        try {
            $santri = Santri::findOrFail($id);
            return view('admin.data_psb.data_calon_santri.edit', [
                'santri' => $santri,
                'juzs' => Juz::all()
            ]);
        } catch (Exception $e) {
            return redirect()->route('')->with('error', 'Data tidak ditemukan');
        }
    }

    public function update(Request $request, $id)
    {
        $santri = Santri::find($id);
        if(!$santri) return redirect()->route('rekap.calon.santri')->with('error', 'Data tidak ditemukan');

        $request->validate([
            'nama_lengkap' => 'required|max:255',
            'nik' => 'required|max:16',
            'nomor_kartu_keluarga' => 'required|max:16',
            'tempat_lahir' => 'required|max:255',
            'tanggal_lahir' => 'required',
            'jk' => 'required',
            'alamat_kota' => 'required|max:255',
            'alamat' => 'required|max:500',
            'nama_lengkap_ayah' => 'required|max:255',
            'nama_lengkap_ibu' => 'required|max:255',
            'no_hp' => 'required|max:20',
            'email' => 'required|max:255',
            'asal_sekolah' => 'required|max:255',
            'alamat_asal_sekolah' => 'required|max:500',
            'tingkat_sekolah_id' => 'required',
            'status_keperluan_asrama' => 'required',
            'hafalan' => 'required'
        ]);

        $datUpdate = [
            'nama_lengkap' => $request->nama_lengkap,
            'nik' => $request->nik,
            'nomor_kartu_keluarga' => $request->nomor_kartu_keluarga,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jk' => $request->jk,
            'alamat_kota' => $request->alamat_kota,
            'alamat' => $request->alamat,
            'nama_lengkap_ayah' => $request->nama_lengkap_ayah,
            'nama_lengkap_ibu' => $request->nama_lengkap_ibu,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'asal_sekolah' => $request->asal_sekolah,
            'alamat_asal_sekolah' => $request->alamat_asal_sekolah,
            'tingkat_sekolah_id' => $request->tingkat_sekolah_id,
            'status_keperluan_asrama' => $request->status_keperluan_asrama,
            'hafalan' => count($request->hafalan)
        ];

        try {
            DB::beginTransaction();
            $santri->update($datUpdate);
            $santri->juzs()->sync($request->hafalan);
            DB::commit();
            return redirect()->route('data.calon.santri')->with('success', 'Data Calon Santri Berhasil diedit');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'gagal Update: ' . $e->getMessage());
        }
    }
}
