<?php

namespace App\Http\Controllers\DataPSB;

use App\Http\Controllers\Controller;
use App\Models\PSB;
use App\Models\Santri;
use Illuminate\Http\Request;

class DashboardPSBController extends Controller
{
    public function index()
    {
        $psb = PSB::where('status', 'ACTIVE')->first();

        if(empty($psb->start_year)) {
            return redirect()->route('psb.index');
        }
        //total santri
        $santri_tsn = Santri::where('tingkat_sekolah_id', 'TSN')
                    ->where('tahun_masuk', $psb->start_year)
                    ->whereIn('status_active', ['non active', 'active', 'rejected'])->count();
        $santri_mln = Santri::where('tingkat_sekolah_id', 'MLN')
                    ->where('tahun_masuk', $psb->start_year)
                    ->whereIn('status_active', ['non active', 'active', 'rejected'])->count();

        $total_keseluruhan_santri = $santri_tsn + $santri_mln;

        //total santri JK TSN
        $santri_lk_tsn = Santri::where('tingkat_sekolah_id', 'TSN')
                    ->whereIn('status_active', ['non active', 'active', 'rejected'])
                    ->where('tahun_masuk', $psb->start_year)
                    ->where('jk', 'RG')->count();
        $santri_pr_tsn = Santri::where('tingkat_sekolah_id', 'TSN')
                    ->whereIn('status_active', ['non active', 'active', 'rejected'])
                    ->where('tahun_masuk', $psb->start_year)
                    ->where('jk', 'UG')->count();

        //total santri JK MLN
        $santri_lk_mln = Santri::where('tingkat_sekolah_id', 'MLN')
            ->whereIn('status_active', ['non active', 'active', 'rejected'])
            ->where('tahun_masuk', $psb->start_year)
            ->where('jk', 'RG')->count();
        $santri_pr_mln = Santri::where('tingkat_sekolah_id', 'MLN')
            ->whereIn('status_active', ['non active', 'active', 'rejected'])
            ->where('tahun_masuk', $psb->start_year)
            ->where('jk', 'UG')->count();

        //total santri belum bayar
        $santri_tsn_no_paid= Santri::where('tingkat_sekolah_id', 'TSN')
                    ->whereIn('status_active', ['non active', 'active', 'rejected'])
                    ->where('tahun_masuk', $psb->start_year)
                    ->where('status_pembayaran', 'pending')->count();
        $santri_mln_no_paid = Santri::where('tingkat_sekolah_id', 'MLN')
                    ->whereIn('status_active', ['non active', 'active', 'rejected'])
                    ->where('tahun_masuk', $psb->start_year)
                    ->where('status_pembayaran', 'pending')->count();

        //total santri sudah bayar
        $santri_tsn_paid = Santri::where('tingkat_sekolah_id', 'TSN')
                    ->whereIn('status_active', ['non active', 'active', 'rejected'])
                    ->where('tahun_masuk', $psb->start_year)
                    ->where('status_pembayaran', 'approved')->count();
        $santri_mln_paid = Santri::where('tingkat_sekolah_id', 'MLN')
                    ->whereIn('status_active', ['non active', 'active', 'rejected'])
                    ->where('tahun_masuk', $psb->start_year)
                    ->where('status_pembayaran', 'approved')->count();

        //total santri sakan
        $santri_tsn_sakan_rg = Santri::where('tingkat_sekolah_id', 'TSN')
        ->where('tahun_masuk', $psb->start_year)
        ->whereIn('status_active', ['non active', 'active', 'rejected'])
        ->where('status_keperluan_asrama', 'ya')
        ->where('jk', 'RG')->count();

        $santri_tsn_sakan_ug = Santri::where('tingkat_sekolah_id', 'TSN')
        ->where('tahun_masuk', $psb->start_year)
        ->whereIn('status_active', ['non active', 'active', 'rejected'])
        ->where('status_keperluan_asrama', 'ya')
        ->where('jk', 'UG')->count();

        $santri_mln_sakan_rg = Santri::where('tingkat_sekolah_id', 'MLN')
        ->where('tahun_masuk', $psb->start_year)
        ->whereIn('status_active', ['non active', 'active', 'rejected'])
        ->where('status_keperluan_asrama', 'ya')
        ->where('jk', 'RG')->count();

        $santri_mln_sakan_ug = Santri::where('tingkat_sekolah_id', 'MLN')
        ->where('tahun_masuk', $psb->start_year)
        ->whereIn('status_active', ['non active', 'active', 'rejected'])
        ->where('status_keperluan_asrama', 'ya')
        ->where('jk', 'UG')->count();
        
        $tahunAjaran = $psb ? $psb->start_year.'-'.$psb->end_year : '-';
        return view('admin.data_psb.dashboard.index', [
            'santri_tsn' => $santri_tsn,
            'santri_mln' => $santri_mln,
            'santri_tsn_no_paid' => $santri_tsn_no_paid,
            'santri_mln_no_paid' => $santri_mln_no_paid,
            'santri_tsn_paid' => $santri_tsn_paid,
            'santri_mln_paid' => $santri_mln_paid,
            'santri_tsn_lk' => $santri_lk_tsn,
            'santri_tsn_pr' => $santri_pr_tsn,
            'santri_mln_lk' => $santri_lk_mln,
            'santri_mln_pr' => $santri_pr_mln,
            'santri_tsn_sakan_rg' => $santri_tsn_sakan_rg,
            'santri_tsn_sakan_ug' => $santri_tsn_sakan_ug,
            'santri_mln_sakan_rg' => $santri_mln_sakan_rg,
            'santri_mln_sakan_ug' => $santri_mln_sakan_ug,
            'tahun_ajaran' => $tahunAjaran,
            'total_keseluruhan_santri' => $total_keseluruhan_santri
        ]);
    }
}
