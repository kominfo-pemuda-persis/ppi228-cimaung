<?php

namespace App\Http\Controllers\DataPSB;

use App\Http\Controllers\Controller;
use App\Models\GroupWhatsapp;
use App\Models\PSB;
use Illuminate\Http\Request;

class GroupWhatsappController extends Controller
{
    public function index()
    {
        $psb = PSB::where('status', 'ACTIVE')->first();

        if(empty($psb->start_year)) {
            return redirect()->route('psb.index');
        }
        
        $datas = GroupWhatsapp::all();
        return view('admin.data_psb.data_group_whatsapp.index', compact('datas'));
    }

    public function detail($id)
    {
        $data = GroupWhatsapp::findOrFail($id);
        return view('admin.data_psb.data_group_whatsapp.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $data = [
            'tingkat_sekolah_id' => $request->tingkat_sekolah_id,
            'whatsappUrl' => $request->whatsappUrl
        ];

        $GroupWa = GroupWhatsapp::create($data);

        if ($GroupWa) {
            return redirect()->route('groupwhatsapp.index')->with('success', 'Set Group Whatsapp Ortu Berhasil Tersimpan');
        } else {
            return redirect()->route('groupwhatsapp.index')->with('error', 'Set Group Whatsapp Ortu Gagal Tersimpan');
        }
    }

    public function update(Request $request, $id)
    {
        $GroupWa = GroupWhatsapp::find($id);

        $GroupWa->update([
            'tingkat_sekolah_id' => $request->tingkat_sekolah_id,
            'whatsappUrl' => $request->whatsappUrl
        ]);

        if ($GroupWa) {
            return redirect()->route('groupwhatsapp.index')->with('success', 'Set Group Whatsapp Ortu Berhasil DiUpdate');
        } else {
            return redirect()->route('groupwhatsapp.index')->with('error', 'Set Group Whatsapp Ortu Gagal DiUpdate');
        }
    }

    public function delete($id)
    {
        $GroupWa = GroupWhatsapp::find($id);

        $GroupWa->delete();

        if ($GroupWa) {
            return redirect()->route('groupwhatsapp.index')->with('success', 'Set Group Whatsapp Ortu Berhasil Dihapus');
        } else {
            return redirect()->route('groupwhatsapp.index')->with('error', 'Set Group Whatsapp Ortu Gagal Dihapus');
        }
    }
}
