<?php

namespace App\Http\Controllers\DataPSB;

use App\Http\Controllers\Controller;
use App\Models\Juz;
use App\Models\Santri;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekapCalonSantriController extends Controller
{
    public function index()
    {
        return view('admin.data_psb.data_rekap_calon_santri.index');
    }

    public function detail($id)
    {
        return response()->json([
            'data' => Santri::with([
                'juzs' => function ($q) {
                    $q->orderBy('id');
                }])->where('id', $id)->first()
        ]);
    }

    public function bayar(Request $request, $id)
    {
        try {

            DB::beginTransaction();

            $santri = Santri::findOrFail($id);
            $santri->status_pembayaran = 'approved';
            $santri->approved_payment_at = now();
            $santri->approved_payment_by = auth()->user()->id;
            $santri->approved_payment_by_name = auth()->user()->name;
            $santri->updated_by = auth()->user()->id;
            $santri->save();

            DB::commit();

            return redirect()->route('data.calon.santri')->with('success', 'Update Pembayaran Santri Berhasil');
        } catch (Exception $e) {
            // DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_santri.kode_daftar';
                break;
            case "1":
                $orderBy = 'tb_santri.nama_lengkap';
                break;
            case "2":
                $orderBy = 'tb_santri.tingkat_sekolah_id';
                break;
            case "3":
                $orderBy = 'tb_santri.tanggal_lahir';
                break;
            case "4":
                $orderBy = 'tb_santri.jk';
                break;
            case "5":
                $orderBy = 'tb_santri.status_pembayaran';
                break;
        }

        $data = Santri::select([
            'kode_daftar','nama_lengkap', 'tingkat_sekolah_id', 'tanggal_lahir','tempat_lahir', 'alamat', 'jk', 'id'
        ])
        ->where('status_pembayaran', 'pending')
        ->where('status_active', 'non active');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(kode_daftar) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function edit($id)
    {
        try {
            $santri = Santri::findOrFail($id);
            return view('admin.data_psb.data_rekap_calon_santri.edit', [
                'santri' => $santri,
                'juzs' => Juz::all()
            ]);
        } catch (Exception $e) {
            return redirect()->route('')->with('error', 'Data tidak ditemukan');
        }
    }

    public function update(Request $request, $id)
    {
        $santri = Santri::find($id);
        if(!$santri) return redirect()->route('rekap.calon.santri')->with('error', 'Data tidak ditemukan');

        $request->validate([
            'nama_lengkap' => 'required|max:255',
            'nik' => 'required|max:16',
            'nomor_kartu_keluarga' => 'required|max:16',
            'tempat_lahir' => 'required|max:255',
            'tanggal_lahir' => 'required',
            'jk' => 'required',
            'alamat_kota' => 'required|max:255',
            'alamat' => 'required|max:500',
            'nama_lengkap_ayah' => 'required|max:255',
            'nama_lengkap_ibu' => 'required|max:255',
            'no_hp' => 'required|max:20',
            'email' => 'required|max:255',
            'asal_sekolah' => 'required|max:255',
            'alamat_asal_sekolah' => 'required|max:500',
            'tingkat_sekolah_id' => 'required',
            'status_keperluan_asrama' => 'required',
            'hafalan' => 'required'
        ]);

        $datUpdate = [
            'nama_lengkap' => $request->nama_lengkap,
            'nik' => $request->nik,
            'nomor_kartu_keluarga' => $request->nomor_kartu_keluarga,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jk' => $request->jk,
            'alamat_kota' => $request->alamat_kota,
            'alamat' => $request->alamat,
            'nama_lengkap_ayah' => $request->nama_lengkap_ayah,
            'nama_lengkap_ibu' => $request->nama_lengkap_ibu,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'asal_sekolah' => $request->asal_sekolah,
            'alamat_asal_sekolah' => $request->alamat_asal_sekolah,
            'tingkat_sekolah_id' => $request->tingkat_sekolah_id,
            'status_keperluan_asrama' => $request->status_keperluan_asrama,
            'hafalan' => count($request->hafalan)
        ];

        try {
            DB::beginTransaction();
            $santri->update($datUpdate);
            $santri->juzs()->sync($request->hafalan);
            DB::commit();
            return redirect()->route('rekap.calon.santri')->with('success', 'Data Calon Santri Berhasil diedit');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'gagal Update: ' . $e->getMessage());
        }
    }
}