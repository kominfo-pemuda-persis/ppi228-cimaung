<?php

namespace App\Http\Controllers;

use App\Exports\SantriExport;
use App\Exports\TemplateSantriExport;
use App\Imports\SantriImport;
use App\Models\Santri;
use App\Models\Muaddib;
use App\Models\Orangtua;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class SantriController extends Controller
{
    public function index()
    {
        return view('admin.data_santri.index');
    }

    public function santri_inactive()
    {
        return view('admin.data_santri.santri_tidak_aktif');
    }

    public function santri_alumni()
    {
        return view('admin.data_santri.santri_alumni');
    }

    public function detail($id)
    {
        $santri = Santri::with('prestasi')->findOrFail($id);
        return view('admin.data_santri.detail', compact('santri'));
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_santri.tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_santri.nis';
                break;
            case "1":
                $orderBy = 'tb_santri.nisn';
                break;
            case "2":
                $orderBy = 'tb_santri.nama_lengkap';
                break;
            case "3":
                $orderBy = 'tb_santri.email';
                break;
            case "4":
                $orderBy = 'tb_santri.tingkat_sekolah_id';
                break;
        }

        $data = Santri::select([
            'id', 'nis', 'nisn', 'nama_lengkap', 'tingkat_sekolah_id', 'kelas', 'start_year', 'user_id', 'tahun_lulus', 'status_active'
        ])->with('user')->whereNull('deleted_at')->whereIn('status_active', ['active']);

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(tb_santri.nis) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.nisn) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('kelas') != null) {
            $data = $data->where('kelas', $request->kelas);
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        if ($request->input('sakan') != null) {
            $data = $data->where('status_keperluan_asrama', $request->sakan);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('user')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function data_tidak_aktif(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_santri.tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_santri.nis';
                break;
            case "1":
                $orderBy = 'tb_santri.nisn';
                break;
            case "2":
                $orderBy = 'tb_santri.nama_lengkap';
                break;
            case "3":
                $orderBy = 'tb_santri.email';
                break;
            case "4":
                $orderBy = 'tb_santri.tingkat_sekolah_id';
                break;
        }

        $data = Santri::select([
            'id', 'nis', 'nisn', 'nama_lengkap', 'tingkat_sekolah_id', 'kelas', 'user_id', 'status_active', 'tahun_lulus',
        ])->with('user')->whereNull('deleted_at')->whereIn('status_active', ['pindah', 'meninggal', 'dikeluarkan']);

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(tb_santri.nis) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.nisn) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('angkatan') != null) {
            $data = $data->where('tahun_lulus', $request->angkatan);
        }

        if ($request->input('santri') != null) {
            $data = $data->where('status_active', $request->santri);
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        if ($request->input('sakan') != null) {
            $data = $data->where('status_keperluan_asrama', $request->sakan);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('user')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function data_alumni(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_santri.tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_santri.nis';
                break;
            case "1":
                $orderBy = 'tb_santri.nisn';
                break;
            case "2":
                $orderBy = 'tb_santri.nama_lengkap';
                break;
            case "3":
                $orderBy = 'tb_santri.email';
                break;
            case "4":
                $orderBy = 'tb_santri.tingkat_sekolah_id';
                break;
        }

        $data = Santri::select([
            'id', 'nis', 'nisn', 'nama_lengkap', 'tingkat_sekolah_id', 'kelas', 'user_id', 'status_active','tahun_masuk', 'tahun_lulus'
        ])->with('user')->whereNull('deleted_at')->where('status_active', 'alumni');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(tb_santri.nis) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.nisn) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_santri.tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('angkatan') != null) {
            $data = $data->where('tahun_lulus', $request->angkatan);
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        if ($request->input('sakan') != null) {
            $data = $data->where('status_keperluan_asrama', $request->sakan);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('user')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function create()
    {
        $muaddib = Muaddib::all();
        return view('admin.data_santri.create', compact('muaddib'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nis'                => 'required | unique:tb_santri,nis',
            'nisn'               => 'required',
            'nama_lengkap'       => 'required',
            'jk'                 => 'required',
            'tempat_lahir'       => 'required',
            'email'              => 'required',
            'no_hp'              => 'required',
            'asal_sekolah'       => 'required',
            'kelas' => 'required',
            'nama_ortu_ayah'     => 'required',
            'pekerjaan_ayah'     => 'required',
            'nama_ortu_ibu'      => 'required',
            'pekerjaan_ibu'      => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);

        $kelas = $request->kelas;
        // assign variable tingkat dengan mengambil elemen sesudah "-" pada variable kelas
        $tingkat = substr($kelas, strpos($kelas, "-") + 1);
        $numberKelas = substr($kelas, 0, strpos($kelas, "-"));

        try {
            DB::beginTransaction();

            $userOrangTua = User::create([
                'name' => ucwords(strtolower($request->nama_ortu_ayah)),
                'username' => $request->nis . '.ortu',
                'password' => bcrypt('12345'),
            ]);

            $userSantri = User::create([
                'name' => ucwords(strtolower($request->nama_lengkap)),
                'email' => $request->email,
                'username' => $request->username,
                'password' => bcrypt($request->password),
            ]);

            if($tingkat == 'TSN') {
                $userSantri->attachRole('santri_tsn');
                $userOrangTua->attachRole('orangtua_tsn');
            }

            if($tingkat == 'MLN') {
                $userSantri->attachRole('santri_mln');
                $userOrangTua->attachRole('orangtua_mln');
            }

            $orangtua = Orangtua::create([
                'user_id' => $userOrangTua->id,
                'nama' => ucwords(strtolower(($request->nama_ortu_ayah))),
                'hp' => $request->no_hp,
                'alamat' => $request->alamat,
                'kerja' => $request->pekerjaan_ayah,
            ]);

            $santri = Santri::create([
                'nama_lengkap'       => ucwords(strtolower(($request->nama_lengkap))),
                'jk'                 => $request->jk,
                'tempat_lahir'       => $request->tempat_lahir,
                'tanggal_lahir'      => $request->tanggal_lahir,
                'nis'                => $request->nis,
                'nisn'               => $request->nisn,
                'tahun_lulus'        => $request->tahun_lulus,
                'tahun_masuk'        => $request->tahun_masuk,
                'alamat'             => $request->alamat,
                'no_hp'              => $request->no_hp,
                'email'              => $request->email,
                'asal_sekolah'       => $request->asal_sekolah,
                'nama_lengkap_ayah'  => $request->nama_ortu_ayah,
                'pekerjaan_ayah'     => $request->pekerjaan_ayah,
                'nama_lengkap_ibu'   => $request->nama_ortu_ibu,
                'pekerjaan_ibu'      => $request->pekerjaan_ibu,
                'nama_lengkap_wali'  => $request->nama_lengkap_wali,
                'pekerjaan_wali'     => $request->pekerjaan_wali,
                'tingkat_sekolah_id' => $tingkat,
                'kelas'              => $numberKelas,
                'status_active'      => 'active',
                'status_pembayaran'  => 'approved',
                'muaddib_id'         => $request->muaddib_id,
                'user_id'            => $userSantri->id,
                'orangtua_id'        => $orangtua->id,
                'created_by'         => auth()->user()->id,
                'updated_by'         => auth()->user()->id,
            ]);

            DB::commit();

            return redirect()->route('santri.index')->with('success', 'Data Santri Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('santri.index')->with('error', 'Data Santri Gagal Disimpan. Error ' . $e->getMessage());
        }
    }

    public function edit($id)
    {
        $data = Santri::findOrFail($id);
        $muaddib = Muaddib::all();
        return view('admin.data_santri.edit', compact('data', 'muaddib'));
    }

    public function update(Request $request, $id)
    {
        $santri = Santri::findOrFail($id);
        $request->validate([
            'nis' => 'required|unique:tb_santri,nis,' . $santri->id,
            'jk' => 'required',
            'tanggal_lahir' => 'required',
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'asal_sekolah' => 'required',
            'kelas'         => 'required',
            'nama_ortu_ayah'     => 'required',
            'pekerjaan_ayah'     => 'required',
            'nama_ortu_ibu'      => 'required',
            'pekerjaan_ibu'      => 'required',
            'username' => ['required', 'string', 'max:255', 'unique:users,username,' . $santri->user_id],
            'password' => ['nullable', 'string', 'min:5', 'confirmed'],
        ]);

        $kelas = $request->kelas;
        // assign variable tingkat dengan mengambil elemen sesudah "-" pada variable kelas
        $numberKelas = substr($kelas, 0, strpos($kelas, "-"));

        try {
            DB::beginTransaction();

            $santri->update([
                'nama_lengkap'       => ucwords(strtolower($request->nama_lengkap)),
                'tempat_lahir'       => $request->tempat_lahir,
                'tanggal_lahir'      => $request->tanggal_lahir,
                'nis'                => $request->nis,
                'nisn'               => $request->nisn,
                'nism'               => $request->nism,
                'jk'                 => $request->jk,
                'alamat_kota'               => $request->alamat_kota,
                'alamat_asal_sekolah' => $request->alamat_asal_sekolah,
                'alamat'             => $request->alamat,
                'no_hp'              => $request->no_hp,
                'email'              => $request->email,
                'asal_sekolah'       => $request->asal_sekolah,
                'nama_lengkap_ayah'  => ucwords(strtolower($request->nama_ortu_ayah)),
                'pekerjaan_ayah'     => $request->pekerjaan_ayah,
                'nama_lengkap_ibu'   => ucwords(strtolower($request->nama_ortu_ibu)),
                'pekerjaan_ibu'      => $request->pekerjaan_ibu,
                'nama_lengkap_wali'  => $request->nama_lengkap_wali,
                'pekerjaan_wali'     => $request->pekerjaan_wali,
                'kelas'              => $numberKelas,
                'status_active'      => $request->status_active,
                'updated_by'         => auth()->user()->id,
            ]);

            $user = User::find($santri->user_id);

            if($request->password != '' || $request->password_confirmation != '' ){
                $user->update([
                    'name' => ucwords(strtolower($request->nama_lengkap)),
                    'password' => bcrypt($request->password),
                ]);
            }

            $user->update([
                'name' => ucwords(strtolower($request->nama_lengkap)),
                'email' => $request->email,
                'username' => $request->username,
            ]);

            DB::commit();

            return redirect()->route('santri.index')->with('success', 'Data Santri Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('santri.index')->with('error', 'Data Santri Gagal Disimpan. Error ' . $e->getMessage());
        }
    }

    public function delete($id)
    {
        $santri = Santri::find($id);
        if(!$santri)
            return redirect()->route('santri.index')->with('error', 'Santri tidak ditemukan');

        try {
            DB::beginTransaction();

            $santri->deleted_at = now();
            $santri->deleted_by = auth()->user()->id;
            $santri->save();

            DB::commit();

            return redirect()->route('santri.index')->with('success', 'Data Santri Berhasil Dihapus');
        } catch (Exception $e) {
            return redirect()->route('santri.index')->with('error', 'Data Santri Gagal Dihapus ' . $e->getMessage());
        }
    }

    public function export()
    {
        return Excel::download(new SantriExport, 'semua-santri.xlsx');
    }

    public function templateExport()
    {
        return Excel::download(new TemplateSantriExport, 'Template_Santri.xlsx');
    }

    public function import(Request $request)
    {

        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx',
        ]);

        try {
            Excel::import(new SantriImport, $request->file('file'));
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $errorMessages = [];
            foreach ($failures as $failure) {
                $errorMessages[] = "Baris ke-{$failure->row()}: " . implode(', ', $failure->errors());
            }
            $errorMessage = implode('<br>', $errorMessages);
            \Session::flash('error', 'Terjadi kesalahan validasi saat mengimpor file: ' . $errorMessage);
        } catch (\Exception $e) {
            \Session::flash('error', 'Terjadi kesalahan saat mengimpor file: ' . $e->getMessage());
        }

        return redirect()->route('santri.index');

    }

    public function setBatchLulus(Request $req)
    {
        $req->validate([
            'id' => 'required',
        ]);
        try {
            DB::beginTransaction();

            // Mengambil data santri berdasarkan ID yang diberikan
            $santri = Santri::whereIn('id', $req->id)->get();
            $santri_ortu = DB::table('tb_santri')
                            ->select('tb_santri.id as id_santri', 'tb_orangtua.user_id as user_orangtua_id', 'tb_santri.nis as nis_santri')
                            ->join('tb_orangtua', 'tb_orangtua.id', '=', 'tb_santri.orangtua_id')
                            ->whereIn('tb_santri.id', $req->id)
                            ->get();

            // Memeriksa apakah ada santri kelas 2 dalam hasil query
            $santriKelas2 = $santri->whereIn('kelas', ['1','2'])->count();

            if ($santriKelas2 > 0) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => 'Data santri valid harus santri kelas 3.'
                ]);
            }

            // Memeriksa apakah ada santri kelas 3 dalam hasil query
            $santriKelas3 = $santri->where('kelas', '3')->count();

            if ($santriKelas3 > 0) {
                // Memperbarui data santri kelas 3 menjadi alumni
                Santri::whereIn('id', $req->id)->update([
                    'tahun_lulus' => date('Y'),
                    'status_active' => 'alumni',
                    'kelas' => 'LULUS',
                    'updated_by' => Auth::user()->id
                ]);

                foreach ($santri_ortu as $santriortu) {
                    User::where('id', $santriortu->user_orangtua_id)->update([
                        'status' => 'inactive'
                    ]);
                }

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Selamat! Data santri aktif sudah menjadi alumni.'
                ]);
            } else {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => 'Data santri valid harus santri kelas 3.'
                ]);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => 'Data Santri Gagal di Update',
                'error' => $e->getMessage()
            ]);
        }
    }

    public function setBatchKenaikan()
    {
        try {
            DB::beginTransaction();

            // Mengambil data santri berdasarkan ID yang diberikan
            $santris = Santri::whereIn('kelas', ['1', '2'])->where('status_active', 'active')->get();

            // Memeriksa apakah sudah ada santri kelas 3
            $santriKelas3 = Santri::where('kelas', '3')->where('status_active', 'active')->count();

            if ($santriKelas3 > 0) {
                DB::rollBack();
                return response()->json([
                    'status' => 'error',
                    'message' => 'Santri sudah berada di kelas 3. Harus lulus terlebih dahulu.'
                ]);
            }

            foreach ($santris as $santri) {
                if ($santri->kelas === '1') {
                    // Memperbarui data santri kelas 1 menjadi kelas 2
                    $santri->update([
                        'kelas' => '2',
                        'updated_by' => Auth::user()->id
                    ]);
                } elseif ($santri->kelas === '2') {
                    // Memperbarui data santri kelas 2 menjadi kelas 3
                    $santri->update([
                        'kelas' => '3',
                        'updated_by' => Auth::user()->id
                    ]);
                }
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Selamat! Data santri sudah diperbarui.'
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => 'Data Santri Gagal di Update',
                'error' => $e->getMessage()
            ]);
        }
    }

    public function download(Request $request)
    {
        $url = $request->url;
        $filename = $request->filename;
        $file = Storage::disk('s3')->get($url);
        return response()->streamDownload(function () use ($file) {
            echo $file;
        }, $filename);
    }

    public function export_aktif(Request $req)
    {
        $ids = $req->ids;
        $ids = explode(',', $ids);
        return Excel::download(new SantriExport($ids), 'santri-aktif.xlsx');
    }

    public function export_tidak_aktif(Request $req)
    {
        $ids = $req->ids;
        $ids = explode(',', $ids);
        return Excel::download(new SantriExport($ids), 'santri-tidak-aktif.xlsx');
    }

    public function export_alumni(Request $req)
    {
        $ids = $req->ids;
        $ids = explode(',', $ids);
        return Excel::download(new SantriExport($ids), 'santri-alumni.xlsx');
    }

}
