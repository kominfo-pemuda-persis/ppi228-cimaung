<?php

namespace App\Http\Controllers;

use App\Models\Counter;
use App\Models\PSB;
use App\Rules\DifferentYearRule;
use App\Rules\StartYearSameRegistrationClose;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PSBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->hasPermission('menu_set_tahun_ajaran-read')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Tahun Ajaran');
        } else {
            return view('admin.data_psb.data_master_psb.index', [
                'datas' => PSB::orderBY('start_year', 'DESC')->paginate(10)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasPermission('menu_set_tahun_ajaran-create')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Tahun Ajaran');
        } else {
            return view('admin.data_psb.data_master_psb.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'start_year' => 'required|numeric|digits:4|different:end_year|unique:tb_psb',
            'end_year' => ['required', 'numeric', 'digits:4', 'different:start_year', 'gt:start_year', 'unique:tb_psb', new DifferentYearRule($request->start_year)],
            'opened_at' => 'required|different:closed_at',
            'closed_at' => ['required', 'different:opened_at', 'after:opened_at', new StartYearSameRegistrationClose($request->start_year)],
            'opened_mi_at' => 'required|different:closed_mi_at',
            'closed_mi_at' => ['required', 'different:opened_mi_at', 'after:opened_mi_at', new StartYearSameRegistrationClose($request->start_year)],
        ], [
            'closed_at.after' => 'Tanggal Penutupan Harus Lebih Besar dari Tanggal Pembukaan',
            'closed_mi_at.after' => 'Tanggal Penutupan Harus Lebih Besar dari Tanggal Pembukaan'
        ]);

        try {
            DB::beginTransaction();

            PSB::create([
                'start_year' => $request->start_year,
                'end_year' => $request->end_year,
                'opened_at' => $request->opened_at,
                'closed_at' => $request->closed_at,
                'opened_mi_at' => $request->opened_mi_at,
                'closed_mi_at' => $request->closed_mi_at
            ]);

            DB::commit();

            return redirect()->route('psb.index')->with('success', 'Created PSB is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('psb.index')->with('error', 'Error: ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->hasPermission('menu_set_tahun_ajaran-update')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Tahun Ajaran');
        } else {
            try {
                $psb = PSB::findOrFail($id);
    
                return view('admin.data_psb.data_master_psb.edit', [
                    'psb' => $psb
                ]);
            } catch (Exception $e) {
                return redirect()->route('psb.index')->with('error', 'Error: ' . $e->getMessage());
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $psb = PSB::find($id);

        $request->validate([
            'start_year' => 'required|numeric|digits:4|different:end_year|unique:tb_psb',
            'end_year' => ['required', 'numeric', 'digits:4', 'different:start_year', 'gt:start_year', 'unique:tb_psb', new DifferentYearRule($request->start_year)],
            'opened_at' => 'required|different:closed_at',
            'closed_at' => ['required', 'different:opened_at', 'after:opened_at', new StartYearSameRegistrationClose($request->start_year)],
            'opened_mi_at' => 'required|different:closed_mi_at',
            'closed_mi_at' => ['required', 'different:opened_mi_at', 'after:opened_mi_at', new StartYearSameRegistrationClose($request->start_year)],
        ], [
            'closed_at.after' => 'Tanggal Penutupan Harus Lebih Besar dari Tanggal Pembukaan',
            'closed_mi_at.after' => 'Tanggal Penutupan Harus Lebih Besar dari Tanggal Pembukaan'
        ]);
        
        try {
            DB::beginTransaction();

            $psb->update([
                'start_year' => $request->start_year,
                'end_year' => $request->end_year,
                'opened_at' => $request->opened_at,
                'closed_at' => $request->closed_at,
                'opened_mi_at' => $request->opened_mi_at,
                'closed_mi_at' => $request->closed_mi_at
            ]);

            DB::commit();
            return redirect()->route('psb.index')->with('success', 'Update PSB is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $psb = PSB::findOrFail($id);
            DB::beginTransaction();
            $psb->delete();
            DB::commit();
            return redirect()->route('psb.index')->with('success', 'Delete PSB is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('psb.index')->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function active(Request $request)
    {
        try {
            $psb = PSB::findOrFail($request->idPSB);

            $counter = Counter::where('start_year', $psb->start_year)->first();

            DB::beginTransaction();

            if(!$counter) {
                $data = [
                    [
                        'name' => 'NIS',
                        'level' => 'TSN',
                        'office_code' => '27',
                        'start_year' => $psb->start_year,
                        'code' => '01',
                        'counter' => 1,
                        'digit_total' => 3,
                    ],
                    [
                        'name' => 'NIS',
                        'level' => 'MLN',
                        'office_code' => '27',
                        'start_year' => $psb->start_year,
                        'code' => '02',
                        'counter' => 1,
                        'digit_total' => 3,
                    ],
                    [
                        'name' => 'NISM',
                        'start_year' => $psb->start_year,
                        'code' => '500032730094',
                        'counter' => 1,
                        'digit_total' => 3,
                    ]
                ];

                for($i=0; $i < count($data); $i++) {
                    Counter::create($data[$i]);
                }
            }

            PSB::where('status', 'ACTIVE')->update(['status' => 'NON ACTIVE']);

            $psb->status = 'ACTIVE';
            $psb->save();

            DB::commit();
            return redirect()->route('psb.index')->with('success', 'Active PSB is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('psb.index')->with('error', 'Error: ' . $e->getMessage());
        }
    }
}
