<?php

namespace App\Http\Controllers;

use App\Models\Ekstrakulikuler;
use App\Models\EkstrakurikulerRaporPendidikan;
use App\Models\KelompokPelajaran;
use App\Models\Kokurikuler;
use App\Models\KokurikulerRaporPendidikan;
use App\Models\ProgramUnggulan;
use App\Models\ProgramUnggulanRaporPendidikan;
use App\Models\RaporPendidikan;
use App\Models\Santri;
use App\Models\Semester;
use App\Models\WaliKelas;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RaporTSNController extends Controller
{
    public function index()
    {
        $rapor = false;
        $dataRapor = array();
        $raporPendidikan = null;

        if(request('santri') && request('kelas') && request('semester')) {
            $raporPendidikan = RaporPendidikan::where('santri_id', request('santri'))->where('kelas', request('kelas'))->where('semester_id', request('semester'))->where('tingkat_sekolah_id', 'TSN')->first();
            if(!$raporPendidikan) {
                $santri = Santri::find(request('santri'));
                $semester = Semester::find(request('semester'));
                if(!$santri || !$semester)
                    return redirect()->back()->with('error', 'Santri atau semester yang antum pilih tidak ada');

                return redirect()->route('raportsn.index')->with('error', 'Belum ada yang memberi nilai pada santri: ' . $santri->nama_lengkap 
                                                    . ', kelas: ' . request('kelas') 
                                                    . '-Tsanawiyyah, 
                                                    Semester: ' . $semester->nama . ' | '
                                                . $semester->start_year . '-' . $semester->end_year);
            }
            
            $rapor = true;

            $kelompok = KelompokPelajaran::all();
            
            foreach($kelompok as $key => $k) {
                $dataRapor[$key]['kelompok'] = $k->nama_kelompok_pelajaran;
    
                $nilai = array();
    
                $pelajaran = DB::table('tb_kelompok_pelajaran as tkp')
                            ->join('tb_pelajaran as tp', 'tkp.id', '=', 'tp.kelompok_id')
                            ->join('tb_asatidz_pelajaran as tap', 'tp.id', '=', 'tap.pelajaran_id')
                            ->join('tb_asatidz as ta', 'ta.id', '=', 'tap.asatidz_id')
                            ->where('tkp.id', $k->id)
                            ->where('tap.kelas_id', request('kelas'))
                            ->where('tap.tingkat_sekolah_id', 'TSN')
                            ->orderBy('tp.urutan', 'asc')
                            ->get(['tp.id', 'tp.nama_pelajaran', 'ta.nama_lengkap']);
    
                // dd($pelajaran);
                foreach($pelajaran as $kunci => $plj) {
    
                    $nilaiRaporPendidikan = DB::table('tb_rapor_pendidikan as trp')
                                            ->join('tb_nilai_rapor_pendidikan as tnrp', 'trp.id', '=', 'tnrp.rapor_pendidikan_id')
                                            ->join('tb_santri as ts', 'ts.id', '=', 'trp.santri_id')
                                            ->where('tnrp.pelajaran_id', $plj->id)
                                            ->where('ts.id', request('santri'))
                                            ->where('trp.tingkat_sekolah_id', 'TSN')
                                            ->first(['tnrp.nilai', 'tnrp.predikat', 'tnrp.deskripsi']);
    
                    $nilai[$kunci]['pelajaran'] = $plj->nama_pelajaran;
                    $nilai[$kunci]['asatidz'] = $plj->nama_lengkap;
                    $nilai[$kunci]['nilai'] = $nilaiRaporPendidikan->nilai ?? 0;
                    $nilai[$kunci]['predikat'] = $nilaiRaporPendidikan->predikat ?? null;
                    $nilai[$kunci]['deskripsi'] = $nilaiRaporPendidikan->deskripsi ?? null;
                }
    
                $dataRapor[$key]['nilai'] = $nilai;
            }

        }

        if(!Auth::user()->hasPermission('menu_rapor_tsn-read')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Rapor');
        } else {
            return view('admin.data_rapor.rapor_tsn.index', [
                'datas' => RaporPendidikan::paginate(10),
                'dataSemester' => Semester::where('status', 'aktif')->get(),
                'dataSantri' => Santri::whereIn('kelas', ['1', '2', '3'])
                                    ->where('tingkat_sekolah_id', 'TSN')
                                    ->where('status_active', 'active')
                                    ->get(),
                'dataRapor' => $dataRapor,
                'rapor' => $rapor,
                'raporPendidikan' => $raporPendidikan
            ]);
        }
    }

    public function create()
    {
        if(!Auth::user()->hasPermission('rapor_tsn-create')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Rapor');
        } else {
            return view('admin.data_rapor.rapor_tsn.create', [
                'dataSantri' => Santri::where('tingkat_sekolah_id', 'TSN')->get(),
                'dataSemester' => Semester::where('status', 'aktif')->get()
            ]);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'santri' => 'required',
            'semester' => 'required',
            'kelas' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $santri = Santri::find($request->santri);
            $walikelas = WaliKelas::
                            where('kelas_id', $request->kelas)
                            ->where('tingkat_sekolah_id', 'TSN')
                            ->first();

            RaporPendidikan::create([
                'santri_id' => $request->santri,
                'semester_id' => $request->semester,
                'kelas' => $request->kelas,
                'tingkat_sekolah_id' => 'TSN',
                'orangtua_id' => $santri->orangtua_id,
                'walikelas_id' => $walikelas->asatidz_id,
            ]);

            DB::commit();

            return redirect()->route('raportsn.index')->with('success', 'Rapor Tsanawiyyah Berhasil ditambahkan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Gagal: ' . $e->getMessage());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if(!Auth::user()->hasPermission('rapor_tsn-update')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Rapor');
        } else {

            $rapor = RaporPendidikan::find($id);
            if($rapor) {
                return view('admin.data_rapor.rapor_tsn.edit', [
                    'dataSantri' => Santri::where('tingkat_sekolah_id', 'TSN')->get(),
                    'dataSemester' => Semester::where('status', 'aktif')->get(),
                    'rapor' => $rapor
                ]);
            } else {
                return redirect()->route('raportsn.index')->with('error', 'Data tidak ditemukan');
            }
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'santri' => 'required',
            'semester' => 'required',
            'kelas' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $santri = Santri::find($request->santri);
            $walikelas = WaliKelas::
                            where('kelas_id', $request->kelas)
                            ->where('tingkat_sekolah_id', 'TSN')
                            ->first();

            $rapor = RaporPendidikan::find($id);
            $rapor->update([
                'santri_id' => $request->santri,
                'semester_id' => $request->semester,
                'kelas' => $request->kelas,
                'tingkat_sekolah_id' => 'TSN',
                'orangtua_id' => $santri->orangtua_id,
                'walikelas_id' => $walikelas->asatidz_id,
            ]);

            DB::commit();

            return redirect()->route('raportsn.index')->with('success', 'Rapor Tsanawiyyah Berhasil Diedit');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Gagal: ' . $e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $rapor = RaporPendidikan::findOrFail($id);
            $rapor->delete();
            DB::commit();
            return redirect()->route('raportsn.index')->with('success', 'Rapor Tsanawiyyah Berhasil Diedit');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Gagal: ' . $e->getMessage());
        }
    }

    public function lihatRaporTSN(Request $request)
    {
        //ambil rapor pendidikan
        $raporPendidikan = RaporPendidikan::where('santri_id', request('santri'))->where('kelas', request('kelas'))->where('semester_id', request('semester'))->where('tingkat_sekolah_id', 'TSN')->first();

        //jika rapor pendidikan tidak ada
        if(!$raporPendidikan) {
            $santri = Santri::find(request('santri'));
            $semester = Semester::find(request('semester'));

            //kembalikan ke awal
            return redirect()->route('raportsn.index')->with('error', 'Belum ada yang memberi nilai pada santri: ' . $santri->nama_lengkap 
                                                . ', kelas: ' . request('kelas') 
                                                . '-Tsanawiyyah, 
                                                Semester: ' . $semester->nama . '|'
                                            . $semester->start_year . '-' . $semester->end_year);
        }

        //ambil program unggulan
        $programUnggulanRapor =  ProgramUnggulanRaporPendidikan::where('rapor_pendidikan_id', $raporPendidikan->id)->first();

        //jika program unggulan pada rapor ini tidak ada
        if(!$programUnggulanRapor) {
            $programUnggulan = ProgramUnggulan::all();

            //looping untuk menambahkan program unggulan
            foreach($programUnggulan as $pu) {
                try {
                    DB::beginTransaction();
                    ProgramUnggulanRaporPendidikan::create([
                        'rapor_pendidikan_id' => $raporPendidikan->id,
                        'program_unggulan_id' => $pu->id
                    ]);
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                }
            }
        }

        //ambil kokurikuler
        $kokurikulerRapor = KokurikulerRaporPendidikan::where('rapor_pendidikan_id', $raporPendidikan->id)->first();

        //jika program unggulan pada rapor ini tidak ada
        if(!$kokurikulerRapor) {
            $kokurikuler = Kokurikuler::all();

            //looping untuk menambahkan program unggulan
            foreach($kokurikuler as $ko) {
                try {
                    DB::beginTransaction();
                    KokurikulerRaporPendidikan::create([
                        'rapor_pendidikan_id' => $raporPendidikan->id,
                        'kokurikuler_id' => $ko->id
                    ]);
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                }
            }
        }

        $ekstrakurikulerRaporPendidikan = EkstrakurikulerRaporPendidikan::where('rapor_pendidikan_id', $raporPendidikan->id)->first();
        if(!$ekstrakurikulerRaporPendidikan) {
            $ekstrakulikuler = Ekstrakulikuler::all();

            //looping untuk menambahkan program unggulan
            foreach($ekstrakulikuler as $eks) {
                try {
                    DB::beginTransaction();
                    EkstrakurikulerRaporPendidikan::create([
                        'rapor_pendidikan_id' => $raporPendidikan->id,
                        'ekstrakurikuler_id' => $eks->id
                    ]);
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                }
            }
        }

        return redirect('admin/data-rapor/raportsn?santri=' . $request->santri . 
                        '&kelas=' . $request->kelas . 
                        '&semester=' . $request->semester)->with('success', 'Data Rapor Ditemukan');
    }

    public function simpanNilaiProgramUnggulan(Request $request)
    {
        try {
            DB::beginTransaction();
            for($i = 0; $i < count($request->id); $i++) {
                $nilai = ProgramUnggulanRaporPendidikan::find($request->id[$i]);
                $nilai->nilai = $request->nilai[$i];
    
                $predikat = 'D';
                if($request->nilai[$i] >= 60 && $request->nilai[$i] <= 69)
                    $predikat = 'C';
                
                if($request->nilai[$i] >= 70 && $request->nilai[$i] <= 79)
                    $predikat = 'B';
                
                if($request->nilai[$i] >= 80 && $request->nilai[$i] <= 89)
                    $predikat = 'A';
    
                if($request->nilai[$i] >= 90 && $request->nilai[$i] <= 100)
                    $predikat = 'A+';
    
                $nilai->predikat = $predikat;
                $nilai->jumlah_hafalan = $request->jumlah_hafalan[$i];
                $nilai->deskripsi = $request->deskripsi[$i];
                $nilai->save();
            }
            DB::commit();

            return redirect()->back()->with('success', 'Nilai Program Unggulan Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function simpanNilaiKokurikuler(Request $request)
    {
        try {
            DB::beginTransaction();
            for($i = 0; $i < count($request->id); $i++) {
                $nilai = KokurikulerRaporPendidikan::find($request->id[$i]);
                $nilai->nilai = $request->nilai[$i];
    
                $predikat = 'D';
                if($request->nilai[$i] >= 60 && $request->nilai[$i] <= 69)
                    $predikat = 'C';
                
                if($request->nilai[$i] >= 70 && $request->nilai[$i] <= 79)
                    $predikat = 'B';
                
                if($request->nilai[$i] >= 80 && $request->nilai[$i] <= 89)
                    $predikat = 'A';
    
                if($request->nilai[$i] >= 90 && $request->nilai[$i] <= 100)
                    $predikat = 'A+';
    
                $nilai->predikat = $predikat;
                $nilai->deskripsi = $request->deskripsi[$i];
                $nilai->save();
            }
            DB::commit();

            return redirect()->back()->with('success', 'Nilai Kokurikuler Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function simpanDeskripsiEkstrakurukuler(Request $request)
    {
        try {
            DB::beginTransaction();
            for($i = 0; $i < count($request->id); $i++) {
                $nilai = EkstrakurikulerRaporPendidikan::find($request->id[$i]);
                $nilai->deskripsi = $request->deskripsi[$i];
                $nilai->save();
            }
            DB::commit();

            return redirect()->back()->with('success', 'Deskripsi Ekstrakurikuler Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function simpanKetidakhadiran(Request $request, $id)
    {
        $request->validate([
            'sakit' => 'required|numeric',
            'izin' => 'required|numeric',
            'alpa' => 'required|numeric',
        ]);
        try {
            DB::beginTransaction();
            $raporPendidikan = RaporPendidikan::find($id);
            $raporPendidikan->update([
                'sakit' => $request->sakit,
                'izin' => $request->izin,
                'alpa' => $request->alpa,
            ]);
            DB::commit();

            return redirect()->back()->with('success', 'Ketidakhadiran Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function simpanCatatanWalikelas(Request $request, $id)
    {
        $request->validate([
            'catatanWalikelas' => 'required|max:2000',
        ]);
        try {
            DB::beginTransaction();
            $raporPendidikan = RaporPendidikan::find($id);
            $raporPendidikan->update([
                'catatan_walikelas' => $request->catatanWalikelas,
            ]);
            DB::commit();

            return redirect()->back()->with('success', 'Catatan Walikelas Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    public function simpanTanggapanOrangtua(Request $request, $id)
    {
        $request->validate([
            'tanggapanOrangtua' => 'required|max:2000',
        ]);
        try {
            DB::beginTransaction();
            $raporPendidikan = RaporPendidikan::find($id);
            $raporPendidikan->update([
                'tanggapan_orangtua' => $request->tanggapanOrangtua,
                'tanggal_tanggapan_orangtua' => now()
            ]);
            DB::commit();

            return redirect()->back()->with('success', 'Tanggapan Orangtua Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }
}
