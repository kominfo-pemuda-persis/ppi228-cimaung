<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\WaliKelas;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class WaliKelasController extends Controller
{
    public function index()
    {
        // $asatidz = Asatidz::all();
        $asatidz = Asatidz::orderBy('nama_lengkap', 'ASC')->get();
        return view('admin.data_walikelas.index', compact('asatidz'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            "asatidz_id" => "required|unique:tb_wali_kelas,asatidz_id",
            "kelas_id" => "required",
            "tingkat_sekolah_id" => "required"
        ]);

        if (!$validated) {
            return redirect()->route('pelajaran.index')->withErrors($validated);
        }
        $asatidz = [
            'asatidz_id' => $request->asatidz_id,
            'kelas_id' => $request->kelas_id,
            'tingkat_sekolah_id' => $request->tingkat_sekolah_id
        ];

        $data = WaliKelas::create($asatidz);

        if ($data) {
            return redirect()->route('walikelas.index')->with('success', 'Data Wali Kelas Berhasil Tersimpan');
        } else {
            return redirect()->route('walikelas.index')->with('error', 'Data Wali Kelas Gagal Tersimpan');
        }
    }

    public function edit($id)
    {
        $data = WaliKelas::with('asatidz')->find($id);
        return view('admin.data_walikelas.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $validated = $request->validate([
                "kelas_id" => "required",
                "tingkat_sekolah_id" => "required"
            ]);
            if (!$validated) {
                return response()->json([
                    'error' => $validated
                ]);
            }

            $data = WaliKelas::with('asatidz')->find($id);
            $data->update([
                'kelas_id' => $request->kelas_id,
                'tingkat_sekolah_id' => $request->tingkat_sekolah_id
            ]);
            
            DB::commit();

            return redirect()->route('walikelas.index')->with('success', 'Data Wali Kelas Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('walikelas.index')->with('error', 'Data Wali Kelas Gagal Disimpan. Error ' . $e->getMessage());
        }
    }

    public function delete($id)
    {
        $data = WaliKelas::find($id);
        $data->delete();
        if ($data) {
            return redirect()->route('walikelas.index')->with('success', 'Da Wali Kelas Berhasil Ke hapus');
        } else {
            return redirect()->route('walikelas.index')->with('error', 'Data Wali Kelas Gagal Ke hapus');
        }
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_wali_kelas.tingkat_sekolah_id';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_wali_kelas.asatidz_id';
                break;
            case "1":
                $orderBy = 'tb_wali_kelas.tingkat_sekolah_id';
                break;
        }

        $data = WaliKelas::select([
            'tb_wali_kelas.*'
        ])
        ->join('tb_asatidz', 'tb_wali_kelas.asatidz_id', 'tb_asatidz.id');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('kelas') != null) {
            $data = $data->where('kelas_id', $request->kelas);
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('asatidz')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }
}