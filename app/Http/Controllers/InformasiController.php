<?php

namespace App\Http\Controllers;

use App\Models\Informasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class InformasiController extends Controller
{
    public function index()
    {
        $datas = Informasi::all();
        if(!empty($datas)) {
            $datas = Informasi::all();
        }else{
            $datas->file = NULL;
        }
        return view('admin.data_informasi.index', compact('datas'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:pdf'
        ]);

        if($request->hasFile('file')) {
            $file = $request->file('file');
            
            $fileName = 'informasi_'. date('YmdHis') . '.'. $request->file->extension();  
            Storage::disk('s3')->put('data/informasi/' .$fileName, file_get_contents($file), 'public');

            Informasi::create([
                'nama' => Auth::user()->name,
                'keterangan' => $request->keterangan,
                'tanggal_dibuat' => now(),
                'file' => $fileName
            ]);

            return redirect()->back()->with('success', 'Berhasil, informasi telah di upload');
        }
        return redirect()->back()->with('error', 'Error, informasi tidak dapat di upload');
    }

    public function show($id) {
        $data = Informasi::find($id);
        return response()->json($data);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $validator = \Validator::make($request->all(), [ 
            'id' => 'required',
            'keterangan' => 'required',
            'file' => 'mimes:pdf'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Error, Harap isi form dengan benar')->withInput();
        }

        if($request->hasFile('file')) {
            $file = $request->file('file');

            $filenameAsal = Informasi::where('id', $id)->first();
            $filenameAsal = $filenameAsal->file;

            $fileName = 'informasi_'. date('YmdHis') . '.'. $request->file->extension();  
            Storage::disk('s3')->put('data/informasi/' .$fileName, file_get_contents($file), 'public');

            $update = Informasi::where('id', $id)->update([
                'nama' => Auth::user()->name,
                'keterangan' => $request->keterangan,
                'tanggal_dibuat' => now(),
                'file' => $fileName
            ]);

            if ($update) {
                // delete file lama
                Storage::disk('s3')->delete('data/informasi/' .$filenameAsal);
                return redirect()->back()->with('success', 'Berhasil, informasi telah di update');
            }
        } else {
            Informasi::where('id', $id)->update([
                'nama' => Auth::user()->name,
                'keterangan' => $request->keterangan,
                'tanggal_dibuat' => now(),
            ]);

            return redirect()->back()->with('success', 'Berhasil, informasi telah di update');
        }
        return redirect()->back()->with('error', 'Error, informasi tidak dapat di update');
    }

    public function destroy($id)
    {
        $data = Informasi::find($id);
        $filenameAsal = $data->file;
        $delete = $data->delete();

        if ($delete) {
            // delete file lama
            File::delete(public_path('assets/data/PDF/'.$filenameAsal));
            return redirect()->back()->with('success', 'Berhasil, informasi telah di hapus');
        }
        return redirect()->back()->with('error', 'Error, informasi tidak dapat di hapus');
    }
}

