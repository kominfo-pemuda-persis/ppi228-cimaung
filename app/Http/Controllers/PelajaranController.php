<?php

namespace App\Http\Controllers;

use App\Exports\PelajaranExport;
use App\Exports\TemplatePelajaranExport;
use App\Imports\PelajaranImport;
use App\Models\KelompokPelajaran;
use App\Models\Pelajaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PelajaranController extends Controller
{
    public function index()
    {
        // $datas = Pelajaran::with('kelompok_pelajaran')->get();
        $kelompok_pelajaran = KelompokPelajaran::get()->pluck('nama_kelompok_pelajaran', 'id');
        return view('admin.data_pelajaran.index', compact('kelompok_pelajaran'));
    }

    public function edit($id)
    {
        $dataid = Pelajaran::find($id);
        $datakelompokpel = KelompokPelajaran::all();
        return view('admin.data_pelajaran.edit', compact('dataid', 'datakelompokpel'));
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $validated = $request->validate([
                "kode" => "required|unique:tb_pelajaran,kode,".$id,
                "nama_pelajaran" => "required",
                "kelompok_id" => "required",
                "tingkat_sekolah_id" => "required",
                "urutan" => "required"
            ]);
    
            $data = Pelajaran::find($id);
    
            $data->update([
                'kode' => $request->kode,
                'nama_pelajaran' => $request->nama_pelajaran,
                'kelompok_id' => $request->kelompok_id,
                'tingkat_sekolah_id' => $request->tingkat_sekolah_id,
                'urutan' => $request->urutan
            ]);
            
            DB::commit();

            return redirect()->route('pelajaran.index')->with('success', 'Data Pelajaran Berhasil Disimpan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('pelajaran.index')->with('error', 'Data Pelajaran Gagal Disimpan. Error ' . $e->getMessage());
        }
    }

    public function delete($id)
    {
        $data = Pelajaran::findOrFail($id);
        $data->delete();

        if($data) {
            return redirect()->route('pelajaran.index')->with('success', 'Data Pelajaran Berhasil Dihapus');
        }else{
            return redirect()->route('pelajaran.index')->with('error', 'Data Pelajaran Gagal Dihapus');
        }
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            "kode" => "required|unique:tb_pelajaran,kode",
            "pelajaran" => "required",
            "kelompok_id" => "required",
            "tingkat_sekolah_id" => "required"  
        ]);

        if (!$validated) {
            return redirect()->route('pelajaran.index')->withErrors($validated);
        }

        $random = preg_replace("/[^a-zA-Z0-9]/", "", strtoupper($request->kode));

        $data = new Pelajaran;
        $data->kode = $random;
        $data->nama_pelajaran = $request->pelajaran;
        $data->kelompok_id = $request->kelompok_id;
        $data->tingkat_sekolah_id = $request->tingkat_sekolah_id;
        $data->save();

        if($data) {
            return redirect()->route('pelajaran.index')->with('success', 'Data Pelajaran Berhasil Disimpan');
        }else{
            return redirect()->route('pelajaran.index')->with('error', 'Data Pelajaran Gagal Disimpan');
        }
    }

    public function export()
    {
        return Excel::download(new PelajaranExport, 'Pelajaran.xlsx');
    }

    public function import(Request $request) 
    {

        $this->validate($request,[
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        Excel::import(new PelajaranImport, $request->file('file'));

        return redirect()->back()->with('success', 'Data Pelajaran Berhasil di Import');

    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_pelajaran.urutan';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_pelajaran.kode';
                break;
            case "1":
                $orderBy = 'tb_pelajaran.nama_pelajaran';
                break;
            case "2":
                $orderBy = 'tb_pelajaran.kelompok_id';
                break;
            case "3":
                $orderBy = 'tb_pelajaran.urutan';
                break;
            case "4":
                $orderBy = 'tb_pelajaran.tingkat_sekolah_id';
                break;
        }

        $data = Pelajaran::select([
            'tb_pelajaran.*'
        ]);

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(tb_pelajaran.kode) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_pelajaran.nama_pelajaran) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_pelajaran.kelompok_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_pelajaran.urutan) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_pelajaran.tingkat_sekolah_id) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        // if ($request->input('kelas') != null) {
        //     $data = $data->where('kelas_id', $request->kelas);
        // }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tb_pelajaran.tingkat_sekolah_id', $request->jenjang);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('kelompok_pelajaran')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }
}
