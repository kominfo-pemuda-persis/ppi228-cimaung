<?php

namespace App\Http\Controllers;

use App\Models\ProgramUnggulan;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProgramUnggulanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->hasPermission('menu_program_unggulan-read')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Program Unggulan');
        } else {
            return view('admin.data_program_unggulan.index', [
                'datas' => ProgramUnggulan::where('nama', 'like', '%' . request()->search . '%')->where('status', 'aktif')->paginate(10)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasPermission('menu_program_unggulan-create')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Program Unggulan');
        } else {
            return view('admin.data_program_unggulan.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:200|unique:tb_program_unggulan,nama'
        ]);

        try {
            DB::beginTransaction();
            ProgramUnggulan::create(['nama' => $request->nama]);
            DB::commit();
            return redirect()->route('program-unggulan.index')->with('success', 'Program Unggulan Berhasil Ditambahkan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->hasPermission('menu_program_unggulan-update')) {
            return redirect()->route('dashboard')->with('error', 'Maaf, anda tidak memiliki akses data Program Unggulan');
        } else {
            try {
                $data = ProgramUnggulan::findOrFail($id);
                return view('admin.data_program_unggulan.edit', [
                    'data' => $data
                ]);
            } catch (Exception $e) {
                return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ProgramUnggulan::find($id);
        $request->validate([
            'nama' => 'required|max:200|unique:tb_program_unggulan,nama,' . $data->id
        ]);

        try {
            DB::beginTransaction();
            $data->update(['nama' => $request->nama]);
            DB::commit();
            return redirect()->route('program-unggulan.index')->with('success', 'Program Unggulan Berhasil Diedit');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $data = ProgramUnggulan::findOrFail($id);
            $data->update(['status' => 'tidak aktif']);
            DB::commit();
            return redirect()->route('program-unggulan.index')->with('success', 'Program Unggulan Berhasil Dihapus');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error: ' . $e->getMessage());
        }
    }
}
