<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\Pelajaran;
use App\Models\Santri;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        //santri TSN
        $total_santri_tsn = DB::table('tb_santri')
                        ->selectRaw("SUM(IF(jk='RG',1,0)) as rg")
                        ->selectRaw("SUM(IF(jk='UG',1,0)) as ug")
                        ->where('tingkat_sekolah_id', 'TSN')
                        ->first();
        $santri_tsn = $total_santri_tsn->ug + $total_santri_tsn->rg;

        $total_tsn_sakan = DB::table('tb_santri')
                        ->selectRaw("SUM(IF(jk='RG',1,0)) as rg")
                        ->selectRaw("SUM(IF(jk='UG',1,0)) as ug")
                        ->where('tingkat_sekolah_id', 'TSN')
                        ->where('status_keperluan_asrama', 'ya')
                        ->first();

        $total_santri_mln = DB::table('tb_santri')
                        ->selectRaw("SUM(IF(jk='RG',1,0)) as rg")
                        ->selectRaw("SUM(IF(jk='UG',1,0)) as ug")
                        ->where('tingkat_sekolah_id', 'MLN')
                        ->first();
        $santri_mln = $total_santri_mln->ug + $total_santri_mln->rg;

        $total_mln_sakan = DB::table('tb_santri')
                        ->selectRaw("SUM(IF(jk='RG',1,0)) as rg")
                        ->selectRaw("SUM(IF(jk='UG',1,0)) as ug")
                        ->where('tingkat_sekolah_id', 'MLN')
                        ->where('status_keperluan_asrama', 'ya')
                        ->first();
        $data = [
            'santri_tsn' => $santri_tsn,
            'santri_tsn_lk' => $total_santri_tsn->rg,
            'santri_tsn_pr' => $total_santri_tsn->ug,
            'total_tsn_sakan_rg' => $total_tsn_sakan->rg,
            'total_tsn_sakan_ug' => $total_tsn_sakan->ug,
            'santri_mln' => $santri_mln,
            'santri_mln_lk' => $total_santri_mln->rg,
            'santri_mln_pr' => $total_santri_mln->ug,
            'total_mln_sakan_rg' => $total_mln_sakan->rg,
            'total_mln_sakan_ug' => $total_mln_sakan->ug,
        ];
        
        //asatidz
        $asatidz_tsn = Asatidz::whereHas('asatidz_pelajaran', function($q) {
            $q->where('tingkat_sekolah_id', 'TSN');
        })->count();
        $asatidz_mln = Asatidz::whereHas('asatidz_pelajaran', function($q) {
            $q->where('tingkat_sekolah_id', 'MLN');
        })->count();

        //STATUS PENDIDIKAN ASATIDZAH
        $jumlahTidakDiketahui_PA = DB::table('tb_asatidz')
                            ->whereNotIn('pendidikan_terakhir', ['SMA_SMK_MLN','S1','S2','S3'])
                            ->orWhereNull('pendidikan_terakhir')->count();
        // Jumlah SMA/SMK
        $jumlahSMA_SMK = DB::table('tb_asatidz')->where('pendidikan_terakhir', 'SMA_SMK_MLN')->count();
        // Jumlah S1
        $jumlahS1 = DB::table('tb_asatidz')->where('pendidikan_terakhir', 'S1')->count();
        // Jumlah S2
        $jumlahS2 = DB::table('tb_asatidz')->where('pendidikan_terakhir', 'S2')->count();
        // Jumlah S3
        $jumlahS3 = DB::table('tb_asatidz')->where('pendidikan_terakhir', 'S3')->count();

        //STATUS MERITAL ASATIDZAH
        $jumlahTidakDiketahui_MA = DB::table('tb_asatidz')
                            ->whereNotIn('status_menikah', ['SINGLE','MENIKAH','DUDA','JANDA'])
                            ->orWhereNull('status_menikah')->count();
        // Jumlah SINGLE
        $jumlahSINGLE = DB::table('tb_asatidz')->where('status_menikah', 'SINGLE')->count();
        // Jumlah MENIKAH
        $jumlahMENIKAH = DB::table('tb_asatidz')->where('status_menikah', 'MENIKAH')->count();
        // Jumlah DUDA
        $jumlahDUDA = DB::table('tb_asatidz')->where('status_menikah', 'DUDA')->count();
        // Jumlah JANDA
        $jumlahJANDA = DB::table('tb_asatidz')->where('status_menikah', 'JANDA')->count();


        //asatidz total
        $asatidz_jml = Asatidz::count();
        //santri total
        $santri_tsn = Santri::where('tingkat_sekolah_id', 'TSN')->count();
        $santri_mln = Santri::where('tingkat_sekolah_id', 'MLN')->count();

        $asatidz_jk_tsn_L = DB::table('tb_asatidz')
            ->select('.*')
            ->join('tb_asatidz_pelajaran', 'tb_asatidz.id', 'tb_asatidz_pelajaran.asatidz_id')
            ->where('tb_asatidz.jenis_kelamin', 'L')
            ->where('tb_asatidz_pelajaran.tingkat_sekolah_id', 'TSN')
            ->count();

        $asatidz_jk_tsn_P = DB::table('tb_asatidz')
            ->select('.*')
            ->join('tb_asatidz_pelajaran', 'tb_asatidz.id', 'tb_asatidz_pelajaran.asatidz_id')
            ->where('tb_asatidz.jenis_kelamin', 'P')
            ->where('tb_asatidz_pelajaran.tingkat_sekolah_id', 'TSN')
            ->count();

        $asatidz_jk_mln_L = DB::table('tb_asatidz')
            ->select('.*')
            ->join('tb_asatidz_pelajaran', 'tb_asatidz.id', 'tb_asatidz_pelajaran.asatidz_id')
            ->where('tb_asatidz.jenis_kelamin', 'L')
            ->where('tb_asatidz_pelajaran.tingkat_sekolah_id', 'MLN')
            ->count();

        $asatidz_jk_mln_P = DB::table('tb_asatidz')
            ->select('.*')
            ->join('tb_asatidz_pelajaran', 'tb_asatidz.id', 'tb_asatidz_pelajaran.asatidz_id')
            ->where('tb_asatidz.jenis_kelamin', 'P')
            ->where('tb_asatidz_pelajaran.tingkat_sekolah_id', 'MLN')
            ->count();

        //pelajaran
        $pelajaran_tsn = Pelajaran::where('tingkat_sekolah_id', 'TSN')->count();
        $pelajaran_mln = Pelajaran::where('tingkat_sekolah_id', 'MLN')->count();

        //total asatidzh
        $asatidz = $asatidz_jml;
        //total santri
        $santri = $santri_tsn + $santri_mln;

        return view('admin.dashboard.index', [
            'data' => $data,
            'santri' => $santri,
            'asatidzh' => $asatidz,
            'asatidz_tsn' => $asatidz_tsn,
            'asatidz_mln' => $asatidz_mln,
            'pelajaran_tsn' => $pelajaran_tsn,
            'pelajaran_mln' => $pelajaran_mln,
            'asatidz_jk_tsn_L' => $asatidz_jk_tsn_L,
            'asatidz_jk_tsn_P' => $asatidz_jk_tsn_P,
            'asatidz_jk_mln_L' => $asatidz_jk_mln_L,
            'asatidz_jk_mln_P' => $asatidz_jk_mln_P,
            'jumlahTidakDiketahui' => $jumlahTidakDiketahui_PA,
            'jumlahSMA_SMK' => $jumlahSMA_SMK,
            'jumlahS1' => $jumlahS1,
            'jumlahS2' => $jumlahS2,
            'jumlahS3' => $jumlahS3,
            'jumlahTidakDiketahui_MA' => $jumlahTidakDiketahui_MA,
            'jumlahSINGLE' => $jumlahSINGLE,
            'jumlahMENIKAH' => $jumlahMENIKAH,
            'jumlahDUDA' => $jumlahDUDA,
            'jumlahJANDA' => $jumlahJANDA
        ]);
    }
}
