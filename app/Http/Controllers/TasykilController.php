<?php

namespace App\Http\Controllers;

use App\Exports\AsatidzExport;
use App\Models\Asatidz;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TasykilController extends Controller
{
    public function index()
    {
        return view('admin.data_tasykil.index');
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_asatidz.nama_lengkap';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_asatidz.niat';
                break;
            case "1":
                $orderBy = 'tb_asatidz.nama_lengkap';
                break;
            case "2":
                $orderBy = 'tb_asatidz.no_telp';
                break;
            case "3":
                $orderBy = 'tb_asatidz.pendidikan_terakhir';
                break;
        }

        $users = User::whereRoleIs(['tasykil'])->get('id');
        $data = Asatidz::select([
            'tb_asatidz.*'
        ])->whereIn('user_id', $users);

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(tb_asatidz.niat) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_asatidz.nama_lengkap) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_asatidz.no_telp) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_asatidz.pendidikan_terakhir) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('user')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function detail($id)
    {
        try {
            $asatidz = Asatidz::findOrFail($id);
            return view('admin.data_tasykil.detail', compact('asatidz'));
        } catch (Exception $e) {
            return redirect()->route('tasykil.index')->with('error', 'Data tidak ditemukan');
        }
    }

    public function export_aktif(Request $req)
    {
        $ids = $req->ids;
        $ids = explode(',', $ids);
        return Excel::download(new AsatidzExport($ids), 'tasykil-aktif.xlsx');
    }
}
