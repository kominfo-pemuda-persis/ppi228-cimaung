<?php

namespace App\Http\Controllers;

use App\Models\Semester;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SemesterController extends Controller
{
    public function index()
    {
        $datas = Semester::all();
        return view('admin.data_semester.index', compact('datas'));
    }

    public function edit($id)
    {
        $data = Semester::find($id);
        return response()->json([
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        $tahunAwal = $request->tahunAwal;
        $validator = Validator::make($request->all(), [
            'tahunAwal' => 'required',
            'tahunAhir' => 'required|gt:tahunAwal|lt:'.(int)$tahunAwal+2,
            'keterangan' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = Semester::find($id);
        $data->update([
            'nama' => $request->nama,
            'start_year' => $request->tahunAwal,
            'end_year' => $request->tahunAhir,
            'keterangan' => $request->keterangan
        ]);

        if($data) {
            return response()->json([
                'data' => $data,
                'success' => 'data semester berhasil ke update',
                'Url' => '/admin/master/semester'
            ]);
        }
    }

    public function store(Request $request)
    {
        $tahunAwal = $request->tahunAwal;
        $validator = $request->validate([
            'tahunAwal' => 'required',
            'tahunAhir' => 'required|gt:tahunAwal|lt:'.(int)$tahunAwal+2,
            'keterangan' => 'required'
        ]);

        $data = new Semester;
        $data->nama = $request->nama;
        $data->start_year = $request->tahunAwal;
        $data->end_year = $request->tahunAhir;
        $data->keterangan = $request->keterangan;
        $data->save();

        if($data) {
            return redirect()->route('semester.index')->with('success', 'Data Semester Berhasil Disimpan');
        }else{
            return redirect()->route('semester.index')
                ->with('error', 'Data Semester Gagal Disimpan')
                ->withErrors($validator)
                ->withInput();;
        }
    }

    public function delete($id)
    {
        $data = Semester::findOrFail($id);
        $data->delete();

        if($data) {
            return redirect()->route('semester.index')->with('success', 'Data semester Berhasil Dihapus');
        }else{
            return redirect()->route('semester.index')->with('error', 'Data semester Gagal Dihapus');
        }
    }

    public function aktif(Request $request)
    {
        $semester = Semester::find($request->idSemester);
        if(!$semester) {
            return redirect()->route('semester.index')->with('error', 'Data tidak ditemukan');
        }

        try {
            DB::beginTransaction();
            Semester::where('status', 'aktif')->update(['status' => 'tidak-aktif']);
            $semester->status = 'aktif';
            $semester->save();
            DB::commit();
            return redirect()->route('semester.index')->with('success', 'Data Semester Berhasil Diaktifkan');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('semester.index')->with('error', $e->getMessage());
        }
    }
}
