<?php

namespace App\Http\Controllers;

use App\Models\Asatidz;
use App\Models\KaryaTulisIlmiah;
use App\Models\Santri;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class KTIController extends Controller
{
    public function index()
    {
        $santri = Santri::where('kelas', 3)
                        ->OrWhere('kelas', 'LULUS')
                        ->select('id', 'nama_lengkap', 'tingkat_sekolah_id', 'kelas')
                        ->orderBy('tingkat_sekolah_id')
                        ->get();
        $asatidz = Asatidz::select('id', 'nama_lengkap')
            ->get();
        return view('admin.data_kti.index', [
            'santri' => $santri,
            'asatidz' => $asatidz
        ]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'judul_kti' => 'required',
            'kti_path' => 'mimes:pdf',
            'thumbnail_kti_path' => 'mimes:jpeg,jpg,png',
            'santri_id' => 'required|unique:tb_karya_tulis_ilmiah,santri_id',
            'angkatan' => 'required',
            'tanggal_diujikan' => 'required',
            'pembimbing' => 'required',
            'penguji' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->errors()->first());
        }

        if($request->judul_kti) {
            if ($request->hasFile('kti_path')) {
                $file = $request->file('kti_path');
                $fileName = 'kti_'. date('YmdHis') . '.'. $file->extension();  
                Storage::disk('s3')->put('data/santri/kti/' .$fileName, file_get_contents($file), 'public');
            } else {
                $fileName = "unset";
            }

            if ($request->hasFile('thumbnail_kti_path')) {
                $thumbnail_kti_path = $request->file('thumbnail_kti_path');

                $fileName1 = 'kti_thumb_'. date('YmdHis') . '.'. $thumbnail_kti_path->extension();  
                Storage::disk('s3')->put('data/santri/thumbnail_kti/' .$fileName1, file_get_contents($thumbnail_kti_path), 'public');
            } else {
                $fileName1 = "default.png";
            }


            KaryaTulisIlmiah::create([
                'judul_kti' => $request->judul_kti,
                'santri_id' => $request->santri_id,
                'angkatan' => $request->angkatan,
                'tanggal_dibuat' => now(),
                'tanggal_diujikan' => $request->tanggal_diujikan,
                'keterangan' => $request->keterangan ? $request->keterangan : null,
                'halaman' => $request->halaman ? $request->halaman : 0,
                'kti_path' => $fileName,
                'thumbnail_kti_path' => $fileName1,
                'pembimbing' => $request->pembimbing,
                'penguji' => $request->penguji,
            ]);

            return redirect()->back()->with('success', 'Berhasil, Karya Tulis Ilmiah telah di upload');
        }
        return redirect()->back()->with('error', 'Error, Karya Tulis Ilmiah tidak dapat di upload');
    }

    public function show($id) {
        $data = DB::table('tb_karya_tulis_ilmiah')
                ->select('tb_karya_tulis_ilmiah.*', 'tb_santri.id as id_santri')
                ->join('tb_santri', 'tb_karya_tulis_ilmiah.santri_id', 'tb_santri.id')
                ->where('tb_karya_tulis_ilmiah.id', $id)
                ->first();
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $id = $request->id;
        $validator = \Validator::make($request->all(), [
            'edit_kti_path' => 'mimes:pdf',
            'edit_thumbnail_kti_path' => 'mimes:jpeg,jpg,png',
            'santri_id' => 'unique:tb_karya_tulis_ilmiah,santri_id,'.$id,
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" => $validator->errors()->first(),
                'url' => '/admin/karya-tulis-ilmiah'
            ]);
        }

        try{
            $file = $request->file('edit_kti_path');
            $file_thumb = $request->file('edit_thumbnail_kti_path');
            $filenameAsal = KaryaTulisIlmiah::where('id', $id)->first();

            if($request->hasFile('edit_kti_path')) {

                $kti = $filenameAsal->kti_path;
                Storage::disk('s3')->delete('data/santri/kti/' .$kti);
                
                $fileName = 'kti_'. date('YmdHis') . '.'. $file->extension();  
                Storage::disk('s3')->put('data/santri/kti/' .$fileName, file_get_contents($file), 'public');
            }

            if($request->hasFile('edit_thumbnail_kti_path')) {

                $thumbnail_kti = $filenameAsal->thumbnail_kti_path;
                Storage::disk('s3')->delete('data/santri/thumbnail_kti/' .$thumbnail_kti);

                $fileName_thumb = 'kti_thumb_'. date('YmdHis') . '.'. $file_thumb->extension();  
                Storage::disk('s3')->put('data/santri/thumbnail_kti/' .$fileName_thumb, file_get_contents($file_thumb), 'public');
            }

            $update = KaryaTulisIlmiah::where('id', $id)->update([
                'judul_kti' => $request->edit_judul_kti,
                'santri_id' => $request->edit_santri_id,
                'angkatan' => $request->edit_angkatan,
                'tanggal_dibuat' => now(),
                'tanggal_diujikan' => $request->edit_tanggal_diujikan,
                'kti_path' => isset($fileName) ? $fileName : ($filenameAsal->kti_path ? $filenameAsal->kti_path : null),
                'thumbnail_kti_path' => isset($fileName_thumb) ? $fileName_thumb : ($filenameAsal->thumbnail_kti_path ? $filenameAsal->thumbnail_kti_path : null),
                'halaman' => $request->edit_halaman,
                'keterangan' => $request->edit_keterangan,
                'pembimbing' => $request->pembimbing,
                'penguji' => $request->penguji,
            ]);

            return response()->json([
                "success" => true,
                'message' => 'Data KTI Berhasil Diupdate',
                'url' => '/admin/karya-tulis-ilmiah'
            ]);
            DB::commit();
        }catch(Exception $e) {
            DB::rollBack();
            return response()->json([
               "success" => false,
               "message" => 'Data KTI gagal Diupdate',
               'url' => '/admin/karya-tulis-ilmiah'
            ]);
        }
        
    }

    public function delete($id)
    {
        $filenameAsal = KaryaTulisIlmiah::where('id', $id)->first();
        $filenameAsal = $filenameAsal->kti_path;
        $filenameAsal1 = KaryaTulisIlmiah::where('id', $id)->first();
        $filenameAsal1 = $filenameAsal1->thumbnail_kti_path;

        Storage::disk('s3')->delete('data/santri/thumbnail_kti/' .$filenameAsal1);
        Storage::disk('s3')->delete('data/santri/kti/' .$filenameAsal);
        
        $kti = KaryaTulisIlmiah::where('id', $id);
        $kti->delete();

        if ($kti) {
            return redirect()->route('karya-tulis-ilmiah.index')->with('success', 'Data KTI Berhasil Dihapus');
        } else {
            return redirect()->route('karya-tulis-ilmiah.index')->with('error', 'Data KTI Gagal Dihapus');
        }
    }

    public function data(Request $request, $jenis)
    {
        $jenis = str_replace('-', ' ', $jenis);
        $orderBy = 'tb_karya_tulis_ilmiah.angkatan';
        switch ($request->input('order.0.column')) {
            case "0":
                $orderBy = 'tb_karya_tulis_ilmiah.thumbnail_kti_path';
                break;
            case "1":
                $orderBy = 'tb_karya_tulis_ilmiah.judul_kti';
                break;
            case "2":
                $orderBy = 'tb_karya_tulis_ilmiah.santri_id';
                break;
            case "3":
                $orderBy = 'tb_karya_tulis_ilmiah.angkatan';
                break;
            case "4":
                $orderBy = 'tb_karya_tulis_ilmiah.tanggal_dibuat';
                break;
            case "5":
                $orderBy = 'tb_karya_tulis_ilmiah.tanggal_diujikan';
                break;
        }

        $data = KaryaTulisIlmiah::select([
            'tb_karya_tulis_ilmiah.*'
        ])
        ->join('tb_santri', 'tb_karya_tulis_ilmiah.santri_id', 'tb_santri.id');

        if ($request->input('search.value') != null) {
            $data = $data->where(function ($q) use ($request) {
                $q->whereRaw('LOWER(tb_karya_tulis_ilmiah.judul_kti) like ? ', ['%' . strtolower($request->input('search.value')) . '%'])
                    ->orWhereRaw('LOWER(tb_karya_tulis_ilmiah.angkatan) like ? ', ['%' . strtolower($request->input('search.value')) . '%']);
            });
        }

        if ($request->input('jenjang') != null) {
            $data = $data->where('tingkat_sekolah_id', $request->jenjang);
        }

        $recordsFiltered = $data->get()->count();
        if ($request->input('length') != -1)
            $data = $data->skip($request->input('start'))->take($request->input('length'));
        $data         = $data->with('santri')->orderBy($orderBy, $request->input('order.0.dir'))->get();
        $recordsTotal = $data->count();
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data
        ]);
    }

    public function toggleStatus($id,$status)
    {
        try {
            $kti = KaryaTulisIlmiah::find($id);
            if ($kti->kti_path == "unset") {
                return response()->json([
                    'success' => false,
                    'message' => 'KTI belum diupload',
                    'url' => '/admin/karya-tulis-ilmiah'
                ]);
            } else {
                $kti->status = $status;
                $kti->save();
                return response()->json([
                    'success' => true,
                    'message' => 'Berhasil mengubah status KTI',
                    'url' => '/admin/karya-tulis-ilmiah'
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Gagal mengubah status KTI',
                'url' => '/admin/karya-tulis-ilmiah'
            ]);
        }
    }
}
