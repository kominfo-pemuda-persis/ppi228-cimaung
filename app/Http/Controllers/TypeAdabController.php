<?php

namespace App\Http\Controllers;

use App\Models\TypeAdab;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use Stringable;

class TypeAdabController extends Controller
{
    public function index(){
        return view ('admin.data_type_adab.index',[
            'datas' => TypeAdab::where('nama', 'like', '%' . request()->search . '%')->paginate(10)
        ]);
    }

    public function create(){
        return view ('admin.data_type_adab.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required', 'string', 'max:255'],
            'keterangan' => ['required', 'string', 'max:255'],
        ]);

        try {
            DB::beginTransaction();

            TypeAdab::create([
                'nama' => $request->nama,
                'keterangan' => $request->keterangan
            ]);

            DB::commit();

            return redirect()->route('type-adab.index')->with('success', 'Created Type Adab is Successfully');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try {
            $typeadab = TypeAdab::findOrFail($id);
            return view('admin.data_type_adab.edit', ['typeadab' => $typeadab]);
        } catch (Exception $e) {
            return redirect()->route('type-adab.index')->with('error', 'Error ' . $e);
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required', 'string', 'max:255'],
            'keterangan' => ['required', 'string', 'max:255'],
        ]);

        try {
            DB::beginTransaction();

            $typeadab = TypeAdab::findOrFail($id);
            $typeadab->nama = $request->nama;
            $typeadab->keterangan = $request->keterangan;
            $typeadab->save();

            DB::commit();

            return redirect()->route('type-adab.index')->with('success', 'Updated Type Adab is Successfully');
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            
            $data = TypeAdab::findOrFail($id);
            $data->delete();

            DB::commit();

            return redirect()->route('type-adab.index')->with('success', 'Deleted Type Adab is Successfully');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error ' . $e);
        }
    }

}
