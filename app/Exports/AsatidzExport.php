<?php

namespace App\Exports;

use App\Models\Asatidz;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AsatidzExport implements FromView, ShouldAutoSize
{
    private $ids;
    public function __construct($ids = null)
    {
        $this->ids = $ids;
    }

    public function view(): View
    {
        if ($this->ids) {

            $dataAsatidz = Asatidz::select([
                'user_id'
            ])->whereIn('id', $this->ids)->get();

            $users = User::whereRoleIs(['tasykil', 'admin', 'mudir_am', 'mudir_mln', 'mudir_tsn', 'asatidz', 'asatidz_tsn', 'asatidz_mln'])->whereIn('id', $dataAsatidz)->get('id');
            
            $data = Asatidz::select([
                'tb_asatidz.*'
            ])->whereIn('user_id', $users)->get();
            
            return view('admin.data_asatidz.export', [
                'asatidz' => $data             
            ]);
        } else {
            return view('admin.data_asatidz.export', [
                'asatidz' => Asatidz::orderBy('niat')->get()            
            ]);
        }
    }
}
