<?php

namespace App\Exports;

use App\Models\Pelajaran;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class PelajaranExport implements FromView
{
    public function view(): View
    {
        return view('admin.data_pelajaran.export', [
            'pelajaran' => Pelajaran::orderBy('urutan')->get()
        ]);
    }
}
