<?php

namespace App\Exports;

use App\Models\Santri;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class SantriExport implements FromView, ShouldAutoSize
{
    private $ids;
    public function __construct($ids = null)
    {
        $this->ids = $ids;
    }

    // public function columnWidths(): array
    // {
    //     return [
    //         'A' => 3,           
    //         'B' => 15,           
    //         'C' => 19,           
    //         'D' => 19,           
    //         'E' => 19,           
    //         'F' => 19,           
    //         'G' => 19,           
    //         'H' => 19,           
    //         'I' => 19,           
    //         'J' => 19,           
    //         'K' => 19,           
    //         'L' => 19,           
    //         'M' => 19,           
    //         'N' => 19,           
    //         'O' => 19,           
    //         'P' => 19,           
    //         'Q' => 19,           
    //         'R' => 19,           
    //         'S' => 19,           
    //         'T' => 19,           
    //         'U' => 19,           
    //         'V' => 19,           
    //         'W' => 19,           
    //         'X' => 19,           
    //         'Y' => 19,           
    //         'Z' => 19,           
    //         'AA' => 19,           
    //         'AB' => 19,           
    //         'AC' => 19,           
    //         'AD' => 19,
    //         'AE' => 19,            
    //         'AF' => 19,      
    //         'AG' => 19,
    //         'AH' => 19,        
    //     ];
    // }
    public function view(): View
    {
        if ($this->ids) {
            return view('admin.data_santri.export', [
                'santri' => Santri::where('status_pembayaran', 'approved')->whereIn('status_active', ['active', 'meninggal', 'dikeluarkan', 'alumni'])->whereIn('id', $this->ids)->get()
            ]);
        } else {
            return view('admin.data_santri.export', [
                'santri' => Santri::where('status_pembayaran', 'approved')->whereIn('status_active', ['active', 'meninggal', 'dikeluarkan', 'alumni '])->get()
            ]);
        }
    }
}
