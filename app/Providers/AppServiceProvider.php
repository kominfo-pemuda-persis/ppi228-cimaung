<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Models\User;
use App\Models\Asatidz;
use App\Models\Orangtua;
use App\Models\Santri;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(env('APP_ENV') !== 'local') {
            URL::forceScheme('https');
        }
        
        view()->composer('*', function ($view) 
        {
                if(!is_null(Auth::user())) {
                    if(Auth::user()->hasRole(['admin']))
                        $data = User::where('id', Auth::user()->id)->first();
                    if(Auth::user()->hasRole(['admin','asatidz','asatidz_tsn','asatidz_mln','muaddib','mudir_tsn','mudir_mln','mudir_am'])) {
                        $data = Asatidz::where('user_id', Auth::user()->id)->first();
                    } elseif (Auth::user()->hasRole(['santri_tsn', 'santri_mln'])) {
                        $data = Santri::where('user_id', Auth::user()->id)->first();
                    } elseif (Auth::user()->hasRole(['orangtua_tsn', 'orangtua_mln'])) {
                        $data = Orangtua::where('user_id', Auth::user()->id)->first();
                    }
                    $view->with('photoprof', $data);    
                }
        });
        Paginator::useBootstrap();
    }
}
