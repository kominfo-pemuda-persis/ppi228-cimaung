<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WaliKelas extends Model
{
    use HasFactory;
    protected $table = 'tb_wali_kelas';
    protected $fillable = [
        'asatidz_id',
        'kelas_id',
        'tingkat_sekolah_id'
    ];

    public function asatidz()
    {
        return $this->belongsTo(Asatidz::class, 'asatidz_id', 'id');
    }
}
