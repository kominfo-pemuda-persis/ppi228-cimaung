<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prestasi extends Model
{
    use HasFactory;
    protected $table = 'tb_prestasi';
    public $timestamps = false;

    public static $rules = [
        'jenis_prestasi' => 'unique:tb_prestasi,jenis_prestasi,NULL,id,santri_id,:santri_id',
    ];

    protected $fillable = [
        'santri_id',
        'jenis_prestasi',
        'tingkat',
        'tahun',
    ];
}
