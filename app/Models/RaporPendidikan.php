<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RaporPendidikan extends Model
{
    use HasFactory, UUIDAsPrimaryKey;

    protected $table = 'tb_rapor_pendidikan';
    public $guarded = [];

    public function santri()
    {
        return $this->belongsTo(Santri::class);
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function programUnggulan(): HasMany
    {
        return $this->hasMany(ProgramUnggulanRaporPendidikan::class, 'rapor_pendidikan_id');
    }

    public function kokurikulers(): HasMany
    {
        return $this->hasMany(KokurikulerRaporPendidikan::class, 'rapor_pendidikan_id');
    }

    public function ekstrakurikulerRaporPendidikan(): HasMany
    {
        return $this->hasMany(EkstrakurikulerRaporPendidikan::class, 'rapor_pendidikan_id');
    }

    public function walikelas()
    {
        return $this->belongsTo(Asatidz::class, 'walikelas_id', 'id');
    }

    public function getTanggalTanggapanOrangtuaAttribute()
    {
        return Carbon::parse($this->attributes['tanggal_tanggapan_orangtua'])->translatedFormat('d F Y');
    }
}
