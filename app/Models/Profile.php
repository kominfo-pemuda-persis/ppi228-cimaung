<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $table = 'tb_profil';
    protected $fillable = [
        'nama_lembaga',
        'nspp',
        'npsn',
        'tgl_hijriah',
        'tgl_masehi',
        'alamat',
        'rt',
        'rw',
        'provinsi',
        'kota',
        'kecamatan',
        'kode_pos',
    ];
}
