<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KokurikulerRaporPendidikan extends Model
{
    use HasFactory, UUIDAsPrimaryKey;

    protected $table = 'tb_kokurikuler_rapor_pendidikan';

    public $guarded = [];

    public function kokurikuler()
    {
        return $this->belongsTo(Kokurikuler::class);
    }
}
