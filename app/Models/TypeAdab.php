<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeAdab extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama',
        'keterangan'
    ];
    protected $table = 'tb_type_adab';
}
