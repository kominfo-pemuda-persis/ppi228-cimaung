<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KaryaTulisIlmiah extends Model
{
    use HasFactory, UUIDAsPrimaryKey;
    protected $table = 'tb_karya_tulis_ilmiah';
    protected $guarded = [];

    public function santri()
    {
        return $this->hasOne(Santri::class, 'id', 'santri_id');
    }
}
