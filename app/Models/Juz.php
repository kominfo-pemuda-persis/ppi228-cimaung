<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Juz extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama'
    ];

    public function santris()
    {
        return $this->belongsToMany(Santri::class);
    }
}
