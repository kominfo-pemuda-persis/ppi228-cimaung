<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NomorUrutAsatidz extends Model
{
    use HasFactory;
    protected $table = 'tb_nomor_urut_asatidz';
    protected $fillable = ['asatidz_id', 'nomor_urut'];

    // relasi asatidz
    public function asatidz()
    {
        return $this->belongsTo(Asatidz::class, 'asatidz_id', 'id');
    }
}
