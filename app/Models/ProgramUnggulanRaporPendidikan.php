<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramUnggulanRaporPendidikan extends Model
{
    use HasFactory, UUIDAsPrimaryKey;

    protected $table = 'tb_program_unggulan_rapor_pendidikan';

    public $guarded = [];

    public function programUnggulan()
    {
        return $this->belongsTo(ProgramUnggulan::class);
    }
}
