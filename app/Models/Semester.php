<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    use HasFactory;
    protected $table = 'tb_semester';
    protected $fillable = [
        'nama',
        'start_year',
        'end_year',
        'status',
        'keterangan'
    ];
}
