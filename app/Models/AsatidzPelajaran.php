<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsatidzPelajaran extends Model
{
    use HasFactory;
    protected $table = 'tb_asatidz_pelajaran';
    protected $fillable = [
        'asatidz_id',
        'pelajaran_id',
        'kelas_id',
        'tingkat_sekolah_id',
    ];

    public function asatidz()
    {
        return $this->belongsTo(Asatidz::class, 'asatidz_id', 'id');
    }

    public function pelajaran()
    {
        return $this->belongsTo(Pelajaran::class, 'pelajaran_id', 'id');
    }
}
