<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JadwalKegiatanIntra extends Model
{
    use HasFactory;
    protected $table = 'tb_jadwal_kegiatan_intra';
    protected $fillable = [
        'id',
        'hari',
        'jam_id',
        'asatidz_id',
        'pelajaran_id',
        'tingkat',
        'kelas',
        'jenis',
        'urutan_id',
    ];
    public function jam()
    {
        return $this->belongsTo(JamMengajar::class, 'jam_id');
    }
    public function asatidz()
    {
        return $this->belongsTo(Asatidz::class, 'asatidz_id');
    }
    public function pelajaran()
    {
        return $this->belongsTo(Pelajaran::class, 'pelajaran_id');
    }
}
