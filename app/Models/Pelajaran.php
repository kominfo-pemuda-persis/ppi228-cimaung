<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelajaran extends Model
{
    use HasFactory;
    protected $table = 'tb_pelajaran';
    protected $fillable = [
        'kode',
        'nama_pelajaran',
        'kelompok_id',
        'urutan',
        'tingkat_sekolah_id'
    ];

    public function kelompok_pelajaran()
    {
        return $this->belongsTo(KelompokPelajaran::class, 'kelompok_id', 'id');
    }
}
