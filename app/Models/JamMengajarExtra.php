<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JamMengajarExtra extends Model
{
    use HasFactory;
    protected $table = 'tb_jam_mengajar_extra';
    protected $fillable = [
        'id',
        'jam_ke',
        'awal',
        'akhir',
        'kegiatan'
    ];
}
