<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class KelompokPelajaran extends Model
{
    use HasFactory;
    protected $table = 'tb_kelompok_pelajaran';
    protected $fillable = [
        'nama_kelompok_pelajaran',
        'keterangan'
    ];

    public function pelajarans(): HasMany
    {
        return $this->hasMany(Pelajaran::class, 'kelompok_id');
    }
}
