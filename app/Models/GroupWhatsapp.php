<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupWhatsapp extends Model
{
    use HasFactory;
    protected $table = 'tb_group_whatsapp';
    protected $guarded = [];
}
