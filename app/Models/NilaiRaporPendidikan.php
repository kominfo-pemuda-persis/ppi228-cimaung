<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NilaiRaporPendidikan extends Model
{
    use HasFactory, UUIDAsPrimaryKey;

    protected $table = 'tb_nilai_rapor_pendidikan';
    
    public $guarded = [];

    public function raporPendidikan()
    {
        return $this->belongsTo(RaporPendidikan::class, 'rapor_pendidikan_id', 'id');
    }

    public function pelajaran()
    {
        return $this->belongsTo(Pelajaran::class, 'pelajaran_id', 'id');
    }

    public function asatidz()
    {
        return $this->belongsTo(Asatidz::class, 'asatidz_id', 'id');
    }
}
