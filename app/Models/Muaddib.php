<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Muaddib extends Model
{
    use HasFactory;
    protected $table = 'tb_muaddib';
    protected $fillable = [
        'asatidz_id',
        'kelas_id',
        'tingkat_sekolah_id',
        'santri_id',
        'no_majmuah'
    ];

    public function asatidz()
    {
        return $this->belongsTo(Asatidz::class, 'asatidz_id', 'id');
    }

    public function santri()
    {
        return $this->hasMany(Santri::class, 'muaddib_id', 'id');
    }
}
