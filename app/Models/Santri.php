<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Santri extends Model
{
    use HasFactory, UUIDAsPrimaryKey;
    protected $table = 'tb_santri';
    protected $guarded = [];

    public function orangtua()
    {
        return $this->belongsTo(Orangtua::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function prestasi()
    {
        return $this->hasMany(Prestasi::class);
    }

    public function juzs()
    {
        return $this->belongsToMany(Juz::class);
    }

    public function getTanggalLahirAttribute()
    {
        return Carbon::parse($this->attributes['tanggal_lahir'])->translatedFormat('Y-m-d');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('d F Y');
    }
}
