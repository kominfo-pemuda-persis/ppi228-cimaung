<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EkstrakurikulerRaporPendidikan extends Model
{
    use HasFactory, UUIDAsPrimaryKey;

    protected $table = 'tb_ekstrakurikuler_rapor_pendidikan';

    public $guarded = [];

    public function ekstra()
    {
        return $this->belongsTo(Ekstrakulikuler::class, 'ekstrakurikuler_id', 'id');
    }
}
