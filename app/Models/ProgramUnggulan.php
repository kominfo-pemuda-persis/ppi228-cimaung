<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramUnggulan extends Model
{
    use HasFactory, UUIDAsPrimaryKey;

    protected $table = 'tb_program_unggulan';

    public $guarded = [];
}
