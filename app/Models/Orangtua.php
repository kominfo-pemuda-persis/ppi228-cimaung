<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orangtua extends Model
{
    use HasFactory, UUIDAsPrimaryKey;

    protected $table = 'tb_orangtua';
    
    public $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
