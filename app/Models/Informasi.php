<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Informasi extends Model
{
    use HasFactory, UUIDAsPrimaryKey;
    protected $table = 'tb_informasi';
    protected $fillable = [
        'nama',
        'keterangan',
        'tanggal_dibuat',
        'file'
    ];
}
