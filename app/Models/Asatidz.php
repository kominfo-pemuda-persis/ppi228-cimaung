<?php

namespace App\Models;

use App\Traits\UUIDAsPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asatidz extends Model
{
    use HasFactory, UUIDAsPrimaryKey;

    protected $table = 'tb_asatidz';
    protected $fillable = [
        'niat',
        'nama_lengkap',
        'jenis_kelamin',
        'status_menikah',
        'tsn',
        'mln',
        's1',
        's2',
        's3',
        'pendidikan_terakhir',
        'no_telp',
        'alamat',
        'tempat_lahir',
        'tgl_lahir',
        'hafalan',
        'photo_path',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function asatidz_pelajaran()
    {
        return $this->hasMany(AsatidzPelajaran::class);
    }
}
